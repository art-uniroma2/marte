/************************************************************************/
/*                                                                      */
/*   kernel.h                                                           */
/*                                                                      */
/*   User defined kernel function. Feel free to plug in your own.       */
/*                                                                      */
/*   Copyright: Fabio                                                   */
/*   Date: 20.11.06                                                     */
/*                                                                      */
/************************************************************************/

// THIS IS THE EXPERIMENTAL KERNEL FOR RTE

// This kernel expects instances with:

//  value |BT| T_s |BT| H_s |BT| T_s_p |BT| H_s_p |ET| 1:LD |EV| CDS_T_a |EV| CDS_H_a |EV| CDS_T_b |EV| CDS_H_b |EV|

// The kernel  (-t 4) expects two parameters:
//     -T alpha
//     -U b_1b_2b_3b_4b_5 where b_i = {0,1,2}
//        if b_1 = 1 : Distance feature space: K(LD_1,LD_2) - lexical similarity is used
//        if b_2 = 1 : Pair feature space:     K(T_s_1,T_s_2)+K(H_s_1,H_s_2) - the basic tree kernel is used
//        if b_3 = 1 : Pair feature space:     K(T_s_p_1,T_s_p_2)+K(H_s_p_1,H_s_p_2) - the tree kernel with placeholders is used (second pair of trees)
//        if b_4 = 1 : Distance feature space: K(T_s_1,H_s_1)*K(T_s_2,H_s_2) - syntax trees should contain placeholders
//                 2 :
//        if b_5 = 1 : Entailment-trigger-feature-space : EntailmentTriggerFeatures
//                 2 : Entailment-trigger-feature-space :


extern void determine_sub_lists(FOREST *a, FOREST *b, nodePair *intersect, int *n);
extern double evaluateParseTreeKernel(nodePair *pairs, int n);
extern double evaluateParseTreeKernel(nodePair *pairs, int n);
extern double Entailment_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
extern double Entailment_tree_kernel_approx(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
extern double Entailment_tree_kernel_approx_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b,int pair_i, int pair_j);
extern double Entailment_tree_kernel_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int text, int hypo);


// THIS KERNEL IMPLEMENTS THE ACTUAL VERSION OF THE RTE KERNEL FUNCTION

double custom_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b)
{

  int n_pairs=0;

double trees=0;

//  double
//         max = 0,
//         basic=0,
//         trees=0,
//         a_tree_sim = 0,
//         b_tree_sim = 0,
//         intra_pair_similarity = 0,
//         ent_vector=0,
//         trees_with_placeholders=0,
//         comment_space = 0;
//
 nodePair intersect[MAX_NUMBER_OF_PAIRS];
//   //printf("Buongiorno!\n");
//   //printf("KP: %s\n", kernel_parm->custom);
// if (strlen(kernel_parm->custom) >= 1 && kernel_parm->custom[0]=='1' && b->num_of_vectors > 0 && a->num_of_vectors>0){
//   //printf("a: %f b: %f bk: %f \n", a->vectors[0]->words[0].weight, b->vectors[0]->words[0].weight, basic_kernel(kernel_parm, a, b, 0, 0));
//   basic = basic_kernel(kernel_parm, a, b, 0, 0)/
//              sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD);
//   max++;
// }
//
// // External Entailment Features
// if (strlen(kernel_parm->custom) >= 5 && (kernel_parm->custom[4]=='1' || kernel_parm->custom[4]=='2') && b->num_of_vectors > 1 && a->num_of_vectors>1){
//   ent_vector = basic_kernel(kernel_parm, a, b, 1, 1)/sqrt(a->vectors[1]->twonorm_STD * b->vectors[1]->twonorm_STD) +  basic_kernel(kernel_parm, a, b, 2, 2)/sqrt(a->vectors[2]->twonorm_STD * b->vectors[2]->twonorm_STD);
//   max++;
// }
//
//
//
// if (strlen(kernel_parm->custom) >= 2 &&  kernel_parm->custom[1]=='1' && b->num_of_trees > 1 && a->num_of_trees>1){
//        trees=0;
//        determine_sub_lists(a->forest_vec[0],b->forest_vec[0],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT));
//        determine_sub_lists(a->forest_vec[1],b->forest_vec[1],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[1]->twonorm_PT));
//        max++;
// }
//
// // sim(T_1,H_1)*sim(T_2,H_2)
// if (strlen(kernel_parm->custom) >= 4 &&  (kernel_parm->custom[3]=='1' || kernel_parm->custom[3]=='2') && b->num_of_trees > 1 && a->num_of_trees>1){
//        intra_pair_similarity=0;
//
//        determine_sub_lists(a->forest_vec[0],a->forest_vec[1],intersect,&n_pairs);
//        a_tree_sim = evaluateParseTreeKernel(intersect,n_pairs)/a->forest_vec[1]->twonorm_PT;
//        determine_sub_lists(b->forest_vec[0],b->forest_vec[1],intersect,&n_pairs);
//        b_tree_sim = evaluateParseTreeKernel(intersect,n_pairs)/b->forest_vec[1]->twonorm_PT;
//
//        /*
//        if (a_tree_sim > 0 && b_tree_sim > 0 ) {
//
//        intra_pair_similarity =
//          (a->vectors[0]->words[0].weight*b->vectors[0]->words[0].weight+
//           a_tree_sim*b_tree_sim)
//          /
//          sqrt((a->vectors[0]->words[0].weight*a->vectors[0]->words[0].weight+a_tree_sim*a_tree_sim)*
//               (b->vectors[0]->words[0].weight*b->vectors[0]->words[0].weight+b_tree_sim*b_tree_sim));
//        } else intra_pair_similarity = 1;
//
//        */
//
//          //printf("dist = %f\n" ,intra_pair_similarity);
//
//
//         //printf("dist = %f\n" ,intra_pair_similarity);
//         if (kernel_parm->custom[3]=='1')
//          intra_pair_similarity =
//                   (double) pow(((double)kernel_parm->coef_lin)*(double)
//                            (a_tree_sim*b_tree_sim)
//                                 +(double)kernel_parm->coef_const,
//                           (double) kernel_parm->poly_degree)/
//                   sqrt((double) pow(((double)kernel_parm->coef_lin)*(double) (a_tree_sim*a_tree_sim)
//                   +(double)kernel_parm->coef_const,(double) kernel_parm->poly_degree)*
//                   (double) pow(((double)kernel_parm->coef_lin)*(double) (b_tree_sim*b_tree_sim)
//                   +(double)kernel_parm->coef_const,(double) kernel_parm->poly_degree));
//          else
//           intra_pair_similarity =
//                   (double) pow(((double)kernel_parm->coef_lin)*(double)
//                            (a->vectors[0]->words[0].weight*b->vectors[0]->words[0].weight+
//                            a_tree_sim*b_tree_sim)
//                                 +(double)kernel_parm->coef_const,
//                           (double) kernel_parm->poly_degree)/
//                   sqrt((double) pow(((double)kernel_parm->coef_lin)*(double) (a->vectors[0]->words[0].weight*a->vectors[0]->words[0].weight+a_tree_sim*a_tree_sim)
//                   +(double)kernel_parm->coef_const,(double) kernel_parm->poly_degree)*
//                   (double) pow(((double)kernel_parm->coef_lin)*(double) (b->vectors[0]->words[0].weight*b->vectors[0]->words[0].weight+b_tree_sim*b_tree_sim)
//                   +(double)kernel_parm->coef_const,(double) kernel_parm->poly_degree));
//          max++;
//   }
//
//
//   if (strlen(kernel_parm->custom) >= 3 && kernel_parm->custom[2]=='1' && b->num_of_trees > 2  && a->num_of_trees > 2){
//       trees_with_placeholders = Entailment_tree_kernel(kernel_parm,a,b);
//       max++;
//   }
//
//   if (strlen(kernel_parm->custom) >= 3 && kernel_parm->custom[2]=='2' && b->num_of_trees > 2  && a->num_of_trees > 2){
//       trees_with_placeholders = Entailment_tree_kernel_approx(kernel_parm,a,b);
//       max++;
//   }
//   if (strlen(kernel_parm->custom) >= 4 && kernel_parm->custom[3]=='F' && b->num_of_trees > 4  && a->num_of_trees > 4){
//       trees_with_placeholders +=  Entailment_tree_kernel_approx_w(kernel_parm,a,b,4,5);
//       max++;
//   }
//
//   if (strlen(kernel_parm->custom) >= 3 && kernel_parm->custom[2]=='M' && b->num_of_trees > 6  && a->num_of_trees > 6){
//        trees_with_placeholders = Entailment_tree_kernel_approx(kernel_parm,a,b);
//        //printf("F: %f\n",trees_with_placeholders);
//        trees=0;
//        determine_sub_lists(a->forest_vec[5],b->forest_vec[5],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[5]->twonorm_PT * b->forest_vec[5]->twonorm_PT));
//        determine_sub_lists(a->forest_vec[6],b->forest_vec[6],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[6]->twonorm_PT * b->forest_vec[6]->twonorm_PT));
//        trees = trees/2;
//
//   }
//
//   if (strlen(kernel_parm->custom) >= 3 && kernel_parm->custom[2]=='B' && b->num_of_trees > 6  && a->num_of_trees > 6){
//        trees=0;
//        determine_sub_lists(a->forest_vec[5],b->forest_vec[5],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[5]->twonorm_PT * b->forest_vec[5]->twonorm_PT));
//        determine_sub_lists(a->forest_vec[6],b->forest_vec[6],intersect,&n_pairs);
//        trees+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[6]->twonorm_PT * b->forest_vec[6]->twonorm_PT));
//        //trees = trees;
//        printf("QUESTO %f\n",trees);
//
//   }
//
//   if (strlen(kernel_parm->custom) >= 4 && kernel_parm->custom[3]=='C' && b->num_of_trees > 4  && a->num_of_trees > 4) {
//
//       //SPACE OF THE COMMENTS
//        //printf("COMMENT SPACE ON\n");
//        determine_sub_lists(a->forest_vec[4],b->forest_vec[4],intersect,&n_pairs);
//        comment_space = (evaluateParseTreeKernel(intersect,n_pairs)/
//        sqrt(a->forest_vec[4]->twonorm_PT * b->forest_vec[4]->twonorm_PT));
//        //if (comment_space>0 && comment_space!=1) {
//        //    writeTreeString(a->forest_vec[4]->root);
//        //    printf("\n");
//        //    writeTreeString(b->forest_vec[4]->root);
//        //    printf("\ndist = %f\n---\n",comment_space);
//        //}
//
//       max++;
//   }
//
//   // Printing the distance:
////      if (b->num_of_trees > 1 && a->num_of_trees>1){
////           printf("%s;%s;%f;%f\n",
////           a->forest_vec[0]->root->pChild[0]->sName,
////           b->forest_vec[0]->root->pChild[0]->sName,
////           basic,trees_with_placeholders);
////       }
//
//   if (ent_vector==1 && strlen(kernel_parm->custom) >= 5 && kernel_parm->custom[4]=='2')
//   return max;
//   else
//   return basic +
//          intra_pair_similarity +
//          ent_vector +
//          kernel_parm->tree_constant*trees +
//          kernel_parm->tree_constant*trees_with_placeholders+
//          comment_space;
     double cds = 0;
     if (kernel_parm->custom[0]=='a' && b->num_of_vectors > 0 && a->num_of_vectors>0) {
        cds = 0;
        if (kernel_parm->custom[1]=='1')
            cds += basic_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD) ;
        if (kernel_parm->custom[2]=='1')
           cds +=
            (
             sprod_i(a, b, 1, 1)/sqrt(a->vectors[1]->twonorm_sq * b->vectors[1]->twonorm_sq)
             + sprod_i(a, b, 2, 2)/sqrt(a->vectors[2]->twonorm_sq * b->vectors[2]->twonorm_sq)
             //sprod_i(a, b, 1, 1)/(a->vectors[1]->twonorm_sq * b->vectors[1]->twonorm_sq)
             //+ sprod_i(a, b, 2, 2)/(a->vectors[2]->twonorm_sq * b->vectors[2]->twonorm_sq)
             );
        if (kernel_parm->custom[2]=='2')
           cds +=
            (
             sprod_i(a, b, 1, 1)/sqrt(a->vectors[1]->twonorm_sq * b->vectors[1]->twonorm_sq)
             * sprod_i(a, b, 2, 2)/sqrt(a->vectors[2]->twonorm_sq * b->vectors[2]->twonorm_sq)
             );
       }

     if (kernel_parm->custom[0]=='b' && b->num_of_trees >3 && a->num_of_trees>3) {
        cds = 0;
        if (kernel_parm->custom[1]=='1')
            cds += basic_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD) ;
        if (kernel_parm->custom[2]=='1') {
            determine_sub_lists(a->forest_vec[2],b->forest_vec[2],intersect,&n_pairs);
            cds+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[2]->twonorm_PT * b->forest_vec[2]->twonorm_PT));
            determine_sub_lists(a->forest_vec[3],b->forest_vec[3],intersect,&n_pairs);
            cds+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[3]->twonorm_PT * b->forest_vec[3]->twonorm_PT));
		}
        if (kernel_parm->custom[2]=='2') {
            determine_sub_lists(a->forest_vec[2],b->forest_vec[2],intersect,&n_pairs);
            trees = (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[2]->twonorm_PT * b->forest_vec[2]->twonorm_PT));
            determine_sub_lists(a->forest_vec[3],b->forest_vec[3],intersect,&n_pairs);
            trees =trees * (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[3]->twonorm_PT * b->forest_vec[3]->twonorm_PT));
            cds += trees;
        }
    }

    if (kernel_parm->custom[0]=='c' && b->num_of_trees >0 && a->num_of_trees>0) {
        cds = 0;
        clock_t start = clock();
        //Tree kernel
        if (kernel_parm->custom[1]=='1')
            cds += tree_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT);
        //Linear kernel (dot product)
        if (kernel_parm->custom[1]=='2')
            cds += sprod_i(a, b, 0, 0)/sqrt(a->vectors[0]->twonorm_sq * b->vectors[0]->twonorm_sq) ;
            //cds += sprod_i(a, b, 0, 0)/(a->vectors[0]->twonorm_sq * b->vectors[0]->twonorm_sq);
        //Polynomial kernel (default one of svm-light)
        if (kernel_parm->custom[1]=='3')
            cds += basic_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD) ;
        int index = (a->forest_vec[0]->listSize + b->forest_vec[0]->listSize) - 1;
        tempi[index] += (clock()-start);
        volte[index]++;
    }

    if ((kernel_parm->custom[0]=='d' || kernel_parm->custom[0]=='e') && b->num_of_trees >0 && a->num_of_trees>0) {
        //Subpath (d) or Route (e) Tree kernel
        cds = 0;
        //Standard kernel on single trees
        if (kernel_parm->custom[1]=='0')
            cds = tree_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT);
        //Kernel on tree pairs
        if (kernel_parm->custom[1]=='1' && b->num_of_trees >3 && a->num_of_trees>3) {
            //With lexical information
            if (kernel_parm->custom[2]=='1')
                cds += basic_kernel(kernel_parm, a, b, 0, 0)/sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD) ;
            cds += tree_kernel(kernel_parm, a, b, 2, 2)/sqrt(a->forest_vec[2]->twonorm_PT * b->forest_vec[2]->twonorm_PT);
            cds += tree_kernel(kernel_parm, a, b, 3, 3)/sqrt(a->forest_vec[3]->twonorm_PT * b->forest_vec[3]->twonorm_PT);
        }
    }

    return cds;
}
