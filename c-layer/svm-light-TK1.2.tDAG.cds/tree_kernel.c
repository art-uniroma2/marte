/***********************************************************************/
/*   FAST TREE KERNEL                                                  */
/*                                                                     */
/*   tree_kernel.c                                                     */
/*                                                                     */
/*   Fast Tree kernels for Support Vector Machines		               */
/*                                                                     */
/*   Author: Alessandro Moschitti 				                       */
/*   moschitti@info.uniroma2.it					                       */
/*   Date: 10.11.06                                                    */
/*                                                                     */
/*   Copyright (c) 2004  Alessandro Moschitti - All rights reserved    */
/*                                                                     */
/*   This software is available for non-commercial use only. It must   */
/*   not be modified and distributed without prior permission of the   */
/*   author. The author is not responsible for implications from the   */
/*   use of this software.                                             */
/*                                                                     */
/***********************************************************************/

// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA
// CERCARE "ATTENZIONE"!!!! LA NORMA NON E' CORRETTA


# include "svm_common.h"
# include "hash.c"
# include<limits.h>

# define prj(placeholder,projector) (placeholder>0? projector[placeholder-1]:placeholder)
#define max(A,B) ( (A) > (B) ? (A):(B))
#define min(A,B) ( (A) < (B) ? (A):(B))


double LAMBDA;
double SIGMA;

//int vettore_temporaneo[]  = {1,8,64,512,4096,32768}; // AGOSTO

//double delta_matrix[MAX_NUMBER_OF_NODES][MAX_NUMBER_OF_NODES];


// local functions



double choose_second_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b) ;
double choose_tree_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b);
double sequence(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
double AVA(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
double AVA_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
double sequence_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
void evaluateNorma(KERNEL_PARM * kernel_parm, DOC * d);
double basic_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b, int i, int j);
double tree_kernel(KERNEL_PARM *kernel_parm, DOC * a, DOC * b, int i, int j);
double subpath_tree_kernel(KERNEL_PARM *kernel_parm, DOC * a, DOC * b, int i, int j);
double route_tree_kernel(KERNEL_PARM *kernel_parm, DOC * a, DOC * b, int i, int j);
void determine_sub_lists(FOREST *a, FOREST *b, nodePair *intersect, int *n);
double evaluateParseTreeKernel(nodePair *pairs, int n);
double Delta( TreeNode * Nx, TreeNode * Nz);
double Delta_Ent( TreeNode * Nx, TreeNode * Nz, hash_table table, unsigned long long c, int C[]);
double advanced_kernels(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
double sequence_ranking(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int memberA, int memberB);//all_vs_all vectorial kernel
double Eval_Ent_ParseTreeKernel(nodePair *pairs, int n, hash_table table, unsigned long long c, int C[]);

double Delta_aug( TreeNode * Nx, TreeNode * Nz, hash_table table_new); // New Delta with the hash table as parameter
double compute_approx_entailment_kernel(nodePair *pairs_t, int n_t, nodePair *pairs_h, int n_h );
double Entailment_tree_kernel_approx(KERNEL_PARM * kernel_parm, DOC * a, DOC * b);
double Entailment_tree_kernel_approx_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b,int pair_i, int pair_j);
double Entailment_tree_kernel_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int text, int hypo);
hash_table table_t;
hash_table table_h;
hash_table table;

// PLACEHOLDER MANAGEMENT VARIABLES
int base = 16;
int max_placeholders = 16;
int * permutation_matrix = NULL;
int * factorials = NULL;

char mem[MAX_PRODUCTION_LENGTH*2];

/*char *make_key(char *x, char *y, char *z){

        sprintf(mem,"%s %s %s",x,y,z);
	return mem;
}
*/

char *make_key_ent(int x, int y, unsigned long long c){

    int n = sprintf(mem,"%d %d ",x,y);
    //Printing long longs with printf has some bug
    do
    {
       n = n+sprintf(mem+n,"%X",(int)(c%base));
       c = c/base;
    }
    while (c>0);
	return (char*) mem;
}

char *make_key(int x, int y){

	sprintf(mem,"%d %d",x,y);
	return (char*) mem;
}

char *make_key_depth(int x, int y, int depth){

	sprintf(mem,"%d %d %d",x,y,depth);
	return (char*) mem;
}

unsigned long long c_projection(unsigned long long c, unsigned long long p_n_1, unsigned long long p_n_2) {
	//printf("c %llu pn1 %llu pn2 %llu ", c, p_n_1, p_n_2);
	unsigned long long out = 0;
    int rest = 0;
	int i = 0;
	c = c & p_n_1;
	for (i = 0; c >= base ; i++) {
		rest = c%base;
		c = c/base;
		if (rest!=0)
           out += ((unsigned long long)pow((float) base,(rest-1))) * (i+1);
	}
	rest = c;
	if (rest!=0)
		out += ((unsigned long long) pow((float)base,(rest-1))) * (i+1);
	out = out & p_n_2;
    //printf("out %llu\n", out);
	return out;
}

// Numero             : 2138
// p_n_1              : 455
// p_n_2              : 4088




// Generator of All Permutations

int factorial(int n) {
	int fact = 1;
    int i = 0;
	if ( n > 1 ) for (i=1;i <= n; i++) fact = fact*i;
	return fact;
}

void fill_factorial_matrix() {
     int i;
     factorials = (int *) malloc(sizeof(int)*max_placeholders);
     for (i=0; i < max_placeholders; i++) factorials[i]=factorial(i);
}

int * all_permutations(int * elements, int length) {
	int i = 0,j = 0 ,n = 0 , k = 0;
	int * permutations = NULL;
	if (length >0) {
       permutations = (int *) malloc(factorial(length)*length*sizeof(int));
	   if (length > 1) {
	      for (i = 0; i < length; i++) {
				int * remaining = (int *) malloc((length - 1)*sizeof(int));
				k = 0;
				for (j = 0; j < length - 1; j++ )
                    {if (j == (length - i - 1 )) k++; remaining[j]=elements[k];k++;}
				int * remaning_permutations = all_permutations(remaining, length - 1);
				free(remaining);
				int fact = factorial(length-1);
				for (j=0; j < fact; j++) {
					permutations[(i*fact+j)*length + length - 1]=elements[(length - i - 1)];
					for (n=0;n < length - 1;n++)
						permutations[(i*fact+j)*length + length - 1 - n - 1] =
                            remaning_permutations[j*(length-1) + length - 2 - n];
				}
				free(remaning_permutations);

			}
		} else permutations[0] = elements[0];
	}
	return permutations;
}

void fill_permutation_matrix() {
     int placeholders[max_placeholders];
     int i;
     //printf("FILLING PLACEHOLEDER LIST\n\n\n");
     for (i=0; i < max_placeholders; i++) placeholders[i]=i+1;
     permutation_matrix = all_permutations(placeholders,max_placeholders);
}

////

void all_permutations_reduced(int * permutations, int * elements, int length, int * combinations) {
	int i = 0,j = 0 ,n = 0 , k = 0;
	short zero_done = 0;
	//int * permutations = NULL;
    int remaining[max_placeholders];
   	int remaning_permutations[factorials[length]*(length-1)];

	if (length >0) {
       //permutations = (int *) malloc(factorials[length]*length*sizeof(int));
	   if (length > 1) {
          int filled_line = 0;
	      for (i = 0; i < length; i++) {
            if (!zero_done) {
				//int * remaining = (int *) malloc((length - 1)*sizeof(int));
				k = 0;
				for (j = 0; j < length - 1; j++ )
                    {if (j == i) k++; remaining[j]=elements[k];k++;}
                int remanining_combiations = 0;
				all_permutations_reduced(remaning_permutations,remaining, length - 1, &remanining_combiations);
				//free(remaining);
				for (j=0; j < remanining_combiations; j++) {
					(*combinations)++;
                    permutations[(filled_line+j)*length]=elements[i];
					for (n=0;n < length - 1;n++) {
						permutations[(filled_line+j)*length + n + 1] =
                            remaning_permutations[j*(length-1) + n];
                        }
				}
				//free(remaning_permutations);
                zero_done = (elements[i] == 0);
                filled_line += remanining_combiations;
                }
			  }
		} else {permutations[0] = elements[0];(*combinations) = 1;}
	}
	//return permutations;
}

//NEW!
//Returns 1 if the placeholders of the productions in n1 and n2 match according to some constraint (stored in c), 0 otherwise
//We assume n1 and n2 have the same production
short check_constraint(TreeNode *n1, TreeNode *n2, int* c, int maxPh)
{
     int i,j;
     int cp1, cp2;
     int p1 = n1->place_holder;
     int p2 = n2->place_holder;
     //Checking constraint on n1 and n2
     if (p1>maxPh)
       p1 = -1;
     if (p2>maxPh)
       p2 = -1;
     if (p1>0 && p2<=0)
        return 0;
     if (p2>0 && p1<=0)
        return 0;
     if (p1>0 && p2>0)
        c[p1-1] = p2;
     //If n1, n2 are preterminal, there is no need to check the children
     if (n1->pre_terminal || n2->pre_terminal)
        return 1;
     //Checking constraint on the children of n1, n2
     for(i=0; i<n1->iNoOfChildren; i++)
     {
        TreeNode *cn1 = (n1->pChild)[i];
        TreeNode *cn2 = (n2->pChild)[i];
        p1 = cn1->place_holder;
        p2 = cn2->place_holder;
        if (p1>maxPh)
           p1 = -1;
        if (p2>maxPh)
           p2 = -1;
        if (p1>0 && p2<=0)
           return 0;
        if (p1<=0 && p2>0)
           return 0;
        if (p1<=0 && p2<=0)
           continue;
        //Checking that the new correspondence does not violate previous correspondences
        cp1 = 0;
        if (p1>0)
           cp1 = c[p1-1];
        cp2 = 0;
        if (p2>0)
            for (j=0; j<maxPh; j++)
                if (c[j] == p2)
                    cp2 = j+1;
        if (cp1>0 && cp1!=p2)
           return 0;
        if (cp2>0 && cp2!=p1)
           return 0;
        c[p1-1] = p2;
     }
     return 1;
}

//NEW!
//Returns -1 if ununifiable, 1 if 1 subset of 2, 2 if 2 subset of 1, 3 if equivalent, 0 otherwise
//Stores the (partially) unified constraint in store
int unify_constraints(int *ac1, int *ac2, int *store, int len)
{
    int j,k;
    int res = 3;
    //Building the unification
    for (k=0; k<len; k++)
     {
         if (ac1[k] == ac2[k])
            store[k] = ac1[k];
         else if (ac1[k] == 0)
         {
            for (j=k; j<len; j++)
                if (ac2[k] == ac1[j])
                   break;  //The same placeholder has two different correspondences
            if (j==len)
            {
               store[k] = ac2[k];     //The placeholder has a correspondence only in constraint ac2
               if ((res == 1) || (res == 3))
                  res = 1;
               else
                   res = 0;
            }
            else
                break;
         }
         else if (ac2[k] == 0)
         {
            for (j=k; j<len; j++)
                if (ac1[k] == ac2[j])
                   break;  //The same placeholder has two different correspondances
            if (j==len)
            {
               store[k] = ac1[k];     //The placeholder has a correspondence only in constraint ac1
               if ((res == 2) || (res == 3))
                  res = 2;
               else
                   res = 0;
            }
            else
                break;
         }
         else
         {
             break;
         }
     }
     if (k<len)
        return -1;
     else
         return res;
}

//NEW!
//Returns -1 if intersection is null, 1 if c1 is subset of c2, 2 if c2 is subset of c1, 3 if equivalent, 0 otherwise
//Stores the intersection in store
int intersect_constraints(int* c1, int* c2, int* store, int len)
{
    int i;
    int eq = 1;
    //Building the intersection
    for (i=0; i<len; i++)
    {
        eq = eq && (c1[i]==c2[i]);
        if (c1[i] == c2[i])
           store[i] = c1[i];
        else
            store[i] = 0;
    }
    if (eq) //All constraints are equivalent
       return 3;
    for (i=0; i<len; i++)
        if (store[i] != 0)
           break;
    if (i==len)  //There is no matching constraint
       return -1;
    for (i=0; i<len; i++)
        if (store[i] != c1[i])
           break;
    if (i==len)  //The intersection is equivalent to c1
       return 1;
    for (i=0; i<len; i++)
        if (store[i] != c2[i])
           break;
    if (i==len)  //The intersection is equivalent to c2
       return 2;
    return 0;
}

//NEW!
//Determines and stores the set of constraints covering all possible common subtrees rooted in the nodes of n-th pair
void get_ac(int n, nodePair* pairs, int n_pairs, int** table, int* table_dim, int maxPh)
{
     int temp[maxPh], temp1[maxPh], acs[min(500,n_pairs*4)][maxPh], acs_n=0, i, j, k, l, m, comp;
     if (table_dim[n] >=0)
        return;
     for (l=0; l<maxPh; l++)
     {
         temp[l] = 0;
         temp1[l] = 0;
     }
     if (!check_constraint(pairs[n].Nx, pairs[n].Nz, temp, maxPh))
     {
        //no common subtree is rooted in the node pair
        table_dim[n] = 0;
        return;
     }
     //temp holds the constraint yielding a common subtree consisting in the single production rooted in the node pair
     //this constraint is unified with all the constraints covering the subtrees rooted in the children
     for (i=0; i<pairs[n].Nx->iNoOfChildren; i++)
         for (j=0; j<n_pairs; j++)
             if (pairs[n].Nx->pChild[i]->nodeID==pairs[j].Nx->nodeID && pairs[n].Nz->pChild[i]->nodeID==pairs[j].Nz->nodeID)
             {
                  //j-th pair contains the two i-th children of the node pair
                  //if the constraints for pair j have not been determined, they are now
                  if (table_dim[j] < 0)
                     get_ac(j, pairs, n_pairs, table, table_dim, maxPh);
                  //trying to unify temp with the constraints for i-th children pair
                  for (k=0; k<table_dim[j]; k++)
                  {
                      comp = unify_constraints(temp, &(table[j][k*maxPh]), temp1, maxPh);
                      //if the result is different from temp, it is added to the set
                      if (comp==0 || comp==1)
                      {
                         //checking for duplicated constraints
                         for (l=0; l<acs_n; l++)
                         {
                             for (m=0; m<maxPh; m++)
                                 if (acs[l][m] != temp1[m])
                                    break;
                             if (m==maxPh)
                                break;
                         }
                         if (l==acs_n)
                         {
                            for (m=0; m<maxPh; m++)
                                acs[acs_n][m] = temp1[m];
                            acs_n++;
                         }
                      }
                  }
                  break;
             }
     //temp is added to the set
     for (j=0; j<maxPh; j++)
         acs[acs_n][j] = temp[j];
     acs_n++;
     //all combination of constraints yielding a valid unification are considered
     for (i=0; i<acs_n; i++)
     {
         for (j=0; j<acs_n; j++)
         {
             if (i==j)
                continue;
             if (unify_constraints(acs[i], acs[j], temp, maxPh) == 0)
             {
                //checking for duplicated constraints
                for (k=0; k<acs_n; k++)
                {
                    for (l=0; l<maxPh; l++)
                        if (temp[l]!=acs[k][l])
                           break;
                    if (l==maxPh)
                       break;
                }
                if (k==acs_n)
                {
                   for (k=0; k<maxPh; k++)
                         acs[acs_n][k] = temp[k];
                   acs_n++;
                }
             }
         }
     }
     //the set obtained is copied into the dynamic table
     table[n] = (int*)calloc((acs_n)*maxPh, sizeof(int));
     for (i=0; i<acs_n; i++)
         for (j=0; j<maxPh; j++)
             table[n][i*maxPh+j] = acs[i][j];
     table_dim[n] = acs_n;
}

//NEW!
//Builds the set of alternative constraints and stores it in ac
int ac_generator(nodePair *pairs_t, int n_pairs_t, nodePair *pairs_h, int n_pairs_h, int* ac, int maxPh)
{
     int i, j, k, l, dim=0, dimt=0, dimh=0, temp[maxPh], comp;
     int** table_t = (int**)calloc(n_pairs_t, sizeof(int*));
     int* table_t_dim = (int*)calloc(n_pairs_t, sizeof(int));
     int** table_h = (int**)calloc(n_pairs_h, sizeof(int*));
     int* table_h_dim = (int*)calloc(n_pairs_h, sizeof(int));
     //Initialization
     for (i=0; i<n_pairs_t; i++)
     {
        table_t[i] = NULL;
        table_t_dim[i] = -1;
     }
     for (i=0; i<n_pairs_h; i++)
     {
        table_h[i] = NULL;
        table_h_dim[i] = -1;
     }
     //Each table has as many entries as the number of pairs
     //For each pair a set of constraints is stored
     for (i=0; i<n_pairs_t; i++)
     {
        get_ac(i, pairs_t, n_pairs_t, table_t, table_t_dim, maxPh);
        dimt = dimt + table_t_dim[i];
     }
     for (i=0; i<n_pairs_h; i++)
     {
        get_ac(i, pairs_h, n_pairs_h, table_h, table_h_dim, maxPh);
        dimh = dimh + table_h_dim[i];
     }
     int* ac_t = (int*)calloc(dimt*maxPh, sizeof(int));
     int* ac_h = (int*)calloc(dimh*maxPh, sizeof(int));
     dimt = 0;
     dimh = 0;
     //A set is built from the constraints in a table, so that no constraint is duplicated
     for (i=0; i<n_pairs_t; i++)
         for (j=0; j<table_t_dim[i]; j++)
         {
             for (l=0; l<maxPh; l++)
                 if (table_t[i][j*maxPh+l] != 0)
                    break;
             if (l == maxPh)
                continue;
             //Constraint is not null
             for (k=0; k<dimt; k++)
             {
                 for (l=0; l<maxPh; l++)
                     if (table_t[i][j*maxPh+l]!=ac_t[k*maxPh+l])
                        break;
                 if (l==maxPh)
                    break;
             }
             if (k==dimt)
             {
                //Constraint is not duplicated
                for (l=0; l<maxPh; l++)
                    ac_t[dimt*maxPh+l] = table_t[i][j*maxPh+l];
                dimt++;
             }
         }
     for (i=0; i<n_pairs_h; i++)
         for (j=0; j<table_h_dim[i]; j++)
         {
             for (l=0; l<maxPh; l++)
                 if (table_h[i][j*maxPh+l] != 0)
                    break;
             if (l == maxPh)
                continue;
             //Constraint is not null
             for (k=0; k<dimh; k++)
             {
                 for (l=0; l<maxPh; l++)
                     if (table_h[i][j*maxPh+l]!=ac_h[k*maxPh+l])
                        break;
                 if (l==maxPh)
                    break;
             }
             if (k==dimh)
             {
                //Constraint is not duplicated
                for (l=0; l<maxPh; l++)
                    ac_h[dimh*maxPh+l] = table_h[i][j*maxPh+l];
                dimh++;
             }
         }
     //Freeing memory from the tables
     for (i=0; i<n_pairs_t; i++)
         if (table_t[i]!=NULL)
            free(table_t[i]);
     free(table_t);
     free(table_t_dim);
     for (i=0; i<n_pairs_h; i++)
         if (table_h[i]!=NULL)
            free(table_h[i]);
     free(table_h);
     free(table_h_dim);
     //The AC set is obtained by the pairwise unification of constraints for text and hypo
     for (i=0; i<dimt; i++)
         for (j=0; j<dimh; j++)
             if (unify_constraints(&ac_t[i*maxPh], &ac_h[j*maxPh], temp, maxPh) >= 0)
             {
                for (l=0; l<dim; l++)
                {
                    for (k=0; k<maxPh; k++)
                        if (temp[k]!=ac[l*maxPh+k] && temp[k]!=0)
                           break;
                    if (k==maxPh)
                       break;
                }
                if (l == dim)
                {
                    for (k=0; k<maxPh; k++)
                        ac[dim*maxPh+k] = temp[k];
                    dim++;
                }
             }
     //Constraints which did not unify with any other are added to AC
     for (i=0; i<dimt; i++)
     {
         for (j=0; j<dim; j++)
         {
             for (k=0; k<maxPh; k++)
                 if (ac_t[i*maxPh+k]!=ac[j*maxPh+k] && ac_t[i*maxPh+k]!=0)
                    break;
             if (k==maxPh)
                break;
         }
         if (j==dim)
         {
            for (k=0; k<maxPh; k++)
                ac[dim*maxPh+k] = ac_t[i*maxPh+k];
            dim++;
         }
     }
     for (i=0; i<dimh; i++)
     {
         for (j=0; j<dim; j++)
         {
             for (k=0; k<maxPh; k++)
                 if (ac_h[i*maxPh+k]!=ac[j*maxPh+k] && ac_h[i*maxPh+k]!=0)
                    break;
             if (k==maxPh)
                break;
         }
         if (j==dim)
         {
            for (k=0; k<maxPh; k++)
                ac[dim*maxPh+k] = ac_h[i*maxPh+k];
            dim++;
         }
     }
     free(ac_t);
     free(ac_h);
     //Every constraint that is subset or equal to another constraint in AC is removed
     for (i=0; i<dim; i++)
     {
         for (j=i+1; j<dim; j++)
         {
             comp = unify_constraints(&ac[i*maxPh], &ac[j*maxPh], temp, maxPh);
             if (comp>0)
             {
                for (k=0; k<maxPh; k++)
                    ac[i*maxPh+k] = temp[k];
                break;
             }
         }
         if (j<dim)
         {
            dim--;
            for (k=0; k<maxPh; k++)
                 ac[j*maxPh+k] = ac[dim*maxPh+k];
            i--;
         }
     }
     //If no valid constraint was found, we consider the null constraint
     if (dim == 0)
     {
        for (i=0; i<maxPh; i++)
            ac[i] = 0;
        dim = 1;
     }
     return dim;
}

//NEW!
//Adds to ac the set of all possible intersections of elements in ac, and returns the new size
int set_of_constraints_intersections(int *ac, int ac_size, int maxPh)
{
     if (ac_size <= 1)
        return ac_size;
     hash_table table;
     hash_construct_table(&table,ac_size*ac_size);
     char key[maxPh*3], *p;
     int i, j, k, more = ac_size, total_size = ac_size, temp_size;
     int *temp = ac;
     //Each iteration adds a new set of constraints to ac, as the intersections of constraints in the previous set
     //The initial set is the original ac
     while(more > 1)
     {
         //temp points to the previous set of constraints, its size is temp_size
         temp = &ac[(total_size-more)*maxPh];
         temp_size = more;
         more = 0;
         //Intersecting pairs of constraints in the previous set
         for (i=0; i<temp_size; i++)
             for (j=i+1; j<temp_size; j++)
                 if(intersect_constraints(&temp[i*maxPh], &temp[j*maxPh], &ac[total_size*maxPh], maxPh) == 0)
                 {
                       //The intersection is not null and different from both the original constraints
                       //It is stored in the next free position of ac
                       for (k=0; k<maxPh*3; k++)
                           key[k] = '\0';
                       p = key;
                       for (k=0; k<maxPh; k++)
                           p = p + sprintf(p, "%d-", ac[total_size*maxPh+k]);
                       if (hash_lookup(key, &table) != 1)
                       {
                           hash_insert(key, 1, &table);
                           total_size++;
                           more++;
                       }
                 }
     }
     //The null constraint is added to ac
     for (i=0; i<maxPh; i++)
         ac[total_size*maxPh+i] = 0;
     total_size++;
     hash_free_table(&table);
     return total_size;
}

//NEW!
//Computes and stores the multiplier for the constraint in position pos of ac
int compute_cardinality(int pos, int* ac, int* card, int size, int maxPh)
{
    //If the multiplier has already been computed, its value is returned
    if (card[pos] > INT_MIN)
       return card[pos];
    //Otherwise, it is computed according to the simple equation
    int i, j, res = 1;
    for (i=0; i<size; i++)
    {
        //Checking if the constraint is subset of constraint i
        for (j=0; j<maxPh; j++)
            if (ac[pos*maxPh+j]!=0 && ac[pos*maxPh+j]!=ac[i*maxPh+j])
               break;
        if (i!=pos && j==maxPh)
           res = res - compute_cardinality(i, ac, card, size, maxPh);
    }
    card[pos] = res;
    return res;
}
//


double Delta_Ent( TreeNode * Nx, TreeNode * Nz, hash_table table_l, unsigned long long c, int C[]){
    int i;
    //int k;
    double prod=1;
    double delta;
    unsigned long long c_proj;

    //double num_of_variables;

//printf("[delta](%s , %s)\n",Nx->production,Nz->production);
//for(i=0;i<7;i++)printf("%d ",C[i]);
//printf("\nc :%d flag: %d c_proj %d\n\n",c, pair_flag,c_proj);
//fflush(stdout);



    c_proj = c_projection(c, Nx->projection , Nz->projection);

    if((delta=hash_lookup(make_key_ent(Nx->nodeID,Nz->nodeID,c_proj), &table_l))>=0) {
//printf("Delta Matrix: %1.30lf � diverso da -1 boh \n",delta_matrix[Nx->nodeID][Nz->nodeID],Nx->sName,Nz->sName,LAMBDA,SIGMA);
  //        printf("Delta Matrix: %1.30lf\n",delta);
          //printf("REC: %s:%d %s:%d",Nx->sName,Nx->nodeID,Nz->sName,Nz->nodeID);
          return delta; // Case 0 (Duffy and Collins 2002);
       }
	else
	   if(Nx->pre_terminal || Nz->pre_terminal){
           delta = (prj(Nx->place_holder,C) == Nz->place_holder?LAMBDA:0);
           return (hash_insert(make_key_ent(Nx->nodeID,Nz->nodeID,c_proj),delta,&table_l)); // case 1
         } else{
           for(i=0;i<Nx->iNoOfChildren;i++) {
               //printf("P1: %s\nP2: %s\n", Nx->pChild[i]->production, Nz->pChild[i]->production);
               if ( prj(Nx->pChild[i]->place_holder,C) == Nz->pChild[i]->place_holder) {
               //printf("P1: %s %d\nP2: %s %d\n",
               //             Nx->pChild[i]->production,
               //             (Nx->pChild[i]->place_holder > 0 && !pair_flag? C[Nx->pChild[i]->place_holder-1]:Nx->pChild[i]->place_holder),
               //             Nz->pChild[i]->production,
               //             (Nz->pChild[i]->place_holder > 0 && pair_flag? C[Nz->pChild[i]->place_holder-1]:Nz->pChild[i]->place_holder));
	   	           if (strcmp(Nx->pChild[i]->production,Nz->pChild[i]->production)==0) {

                   prod*= (SIGMA+Delta_Ent( Nx->pChild[i], Nz->pChild[i], table_l,c,C)); // case 2
                   //printf(">>>>\n");
                   // Prova di fine estate (AGOSTO):
                   //num_of_variables=1;
                   //for (k=0;k<7;k++) {num_of_variables+=((vettore_temporaneo[k]&Nz->pChild[i]->projection)==0?0:1);}
                   //prod*= (num_of_variables*SIGMA+Delta_Ent( Nx->pChild[i], Nz->pChild[i], table_l,c,C)); // case 2
                   // AGOSTO


                   //prod*= (SIGMA+Delta_Ent( Nx->pChild[i], Nz->pChild[i], table_l,c,C)); // case 2

                 }
               }
               else
                   return (hash_insert(make_key_ent(Nx->nodeID,Nz->nodeID,c_proj),0,&table_l));
               }
               return (hash_insert(make_key_ent(Nx->nodeID,Nz->nodeID,c_proj),LAMBDA*prod,&table_l));
	     }
}

double Eval_Ent_ParseTreeKernel(nodePair *pairs, int n, hash_table table_in, unsigned long long c, int C[]){
    int i,j;
    double sum=0,contr;
		//printf("\n-----------\n");
	   for(i=0;i<n;i++){
        //printf("%s:%d-%s:%d --> ",
        //                    pairs[i].Nx->sName,prj(pairs[i].Nx->place_holder,C),
        //                    pairs[i].Nz->sName,pairs[i].Nz->place_holder);fflush(stdout);
        short remains = prj(pairs[i].Nx->place_holder,C) == pairs[i].Nz->place_holder;
        if (!pairs[i].Nx->pre_terminal && !pairs[i].Nz->pre_terminal){
            for (j=0; j < pairs[i].Nx->iNoOfChildren && j < pairs[i].Nz->iNoOfChildren; j++ ) {
                remains = remains &&
                (prj(pairs[i].Nx->pChild[j]->place_holder,C) == pairs[i].Nz->pChild[j]->place_holder);
                //printf("%s:%d-%s:%d ",
                  // pairs[i].Nx->pChild[j]->sName,prj(pairs[i].Nx->pChild[j]->place_holder,C),
                  // pairs[i].Nz->pChild[j]->sName,pairs[i].Nz->pChild[j]->place_holder);fflush(stdout);

            }
        }
        if (remains) {
           contr=Delta_Ent(pairs[i].Nx,pairs[i].Nz, table_in, c, C);
           sum+=contr;
           //printf("%s-%s:%f\n",pairs[i].Nx->production,pairs[i].Nz->production,contr);fflush(stdout);
           //printf("=%f",contr);fflush(stdout);
        }
        //printf("\n");
      }
	  return sum;
}


double compute_new_kernel(nodePair *intersect_t, int n_pairs_t, nodePair *intersect_h, int n_pairs_h,
                                   int num_max_of_placeholders, int num_min_of_placeholders, double norm)
{
    double kernel = 0;
    int i,k;
    unsigned long long c_int;
    int C_all[factorial(8)], * C = NULL;
    hash_construct_table(&table_t,(n_pairs_t+1)*50); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)
    hash_construct_table(&table_h,(n_pairs_h+1)*50); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)
    if (num_max_of_placeholders+num_min_of_placeholders>0) {
        /*int number_of_iter = 0;
        int j;
        int C_init[max_placeholders];
        for (j=0;j<num_max_of_placeholders;j++)
            C_init[j] = (j<num_min_of_placeholders?j+1:0);

        all_permutations_reduced(C_all,C_init,num_max_of_placeholders,&number_of_iter);
        for (i=0;i<number_of_iter;i++) {
            C = &C_all[num_max_of_placeholders*i];
            //printf("Constraint: ");
            //for (k=0;k<num_max_of_placeholders;k++)
            //    printf("%d ", C[k]);
            //printf("\n");
            c_int = 0;
            for (k=0;k<num_max_of_placeholders;k++)
                c_int += C[k]*((long long)pow(base,k));
            double k_h = Eval_Ent_ParseTreeKernel(intersect_h, n_pairs_h, table_h, c_int, C);
            double k_t = Eval_Ent_ParseTreeKernel(intersect_t, n_pairs_t, table_t, c_int, C);
	        if (k_h * k_t > kernel) kernel = k_h * k_t;
        }*/

        int ac_size = 0, size = 0, *C_mult;
        kernel = 0;
        //Generating the set of alternative constraints
        ac_size = ac_generator(intersect_t, n_pairs_t, intersect_h, n_pairs_h, C_all, num_max_of_placeholders);
        //Determining all possible intersections of alternative constraints
        size = set_of_constraints_intersections(C_all, ac_size, num_max_of_placeholders);
        //Determining the values of the multipliers
        C_mult = (int*)calloc(size, sizeof(int));
        for (i=0; i<size; i++)
            C_mult[i] = INT_MIN;
        for (i=0; i<size; i++)
            compute_cardinality(i, C_all, C_mult, size, num_max_of_placeholders);
        //Computing the kernel according to inclusion-exclusion property
        for (i=0;i<size;i++) {
            if (C_mult[i] == 0)
               continue;
            C = &C_all[num_max_of_placeholders*i];
            c_int = 0;
            for (k=0;k<num_max_of_placeholders;k++)
                c_int += C[k]*((long long)pow(base,k));
            double k_h = Eval_Ent_ParseTreeKernel(intersect_h, n_pairs_h, table_h, c_int, C);
            double k_t = Eval_Ent_ParseTreeKernel(intersect_t, n_pairs_t, table_t, c_int, C);
            kernel = kernel + (k_h * k_t * C_mult[i]);
        }
        free(C_mult);
    } else {
           //No placeholder in the tDAG, only the null constraint is considered
           C = C_all;
           for (i=0; i<num_max_of_placeholders; i++)
               C[i] = 0;
           double k_h = Eval_Ent_ParseTreeKernel(intersect_h, n_pairs_h, table_h, 0, C);
           double k_t = Eval_Ent_ParseTreeKernel(intersect_t, n_pairs_t, table_t, 0, C);
           kernel = k_h * k_t;
    }
    //Normalizing kernel value
    kernel = kernel / norm;
    hash_free_table( &table_t );
    hash_free_table( &table_h );
    return kernel;
}

double Entailment_tree_kernel_tdag(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int index)
{
       int n_pairs_t=0;
       int n_pairs_h=0;
       double k=0;
       nodePair intersect_h[MAX_NUMBER_OF_PAIRS];
       nodePair intersect_t[MAX_NUMBER_OF_PAIRS];
       if (b->num_of_tdags >= index && a->num_of_tdags >= index)
       {
          if (a->tdag_vec[index]->num_of_placeHolders >= b->tdag_vec[index]->num_of_placeHolders)
          {
             determine_sub_lists(a->tdag_vec[index]->text,b->tdag_vec[index]->text,intersect_t,&n_pairs_t);
             determine_sub_lists(a->tdag_vec[index]->hypo,b->tdag_vec[index]->hypo,intersect_h,&n_pairs_h);
          } else {
             determine_sub_lists(b->tdag_vec[index]->text,a->tdag_vec[index]->text,intersect_t,&n_pairs_t);
             determine_sub_lists(b->tdag_vec[index]->hypo,a->tdag_vec[index]->hypo,intersect_h,&n_pairs_h);
          }
          k = compute_new_kernel(intersect_t,n_pairs_t, intersect_h,n_pairs_h,
             max(a->tdag_vec[index]->num_of_placeHolders,b->tdag_vec[index]->num_of_placeHolders),
             min(a->tdag_vec[index]->num_of_placeHolders,b->tdag_vec[index]->num_of_placeHolders),
             sqrt(a->tdag_vec[index]->norm * b->tdag_vec[index]->norm));
       }
       else
          k = 0;
       return k;
}

double Entailment_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){
       return Entailment_tree_kernel_w(kernel_parm, a, b , 2, 3);
}

double Entailment_tree_kernel_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int text, int hypo){
  int n_pairs_t=0;
  int n_pairs_h=0;

  double k=0;

  nodePair intersect_h[MAX_NUMBER_OF_PAIRS];
  nodePair intersect_t[MAX_NUMBER_OF_PAIRS];
  //if (permutation_matrix == NULL) fill_permutation_matrix();

  clock_t start;
  start = clock();

  if (factorials == NULL) fill_factorial_matrix();
  if (b->num_of_trees > 3 && a->num_of_trees > 3){

//printf("(%s,%s)\n",a->forest_vec[0]->root->pChild[0]->sName,b->forest_vec[0]->root->pChild[0]->sName);

/*printf("\nText 1: <"); writeTreeString(a->forest_vec[text]->root);
printf(">\nHypo 1: <"); writeTreeString(a->forest_vec[hypo]->root);printf(">\n"); fflush(stdout);
printf("\nText 2: <"); writeTreeString(b->forest_vec[text]->root);
printf(">\nHypo 2: <"); writeTreeString(b->forest_vec[hypo]->root);printf(">\n"); fflush(stdout);
fflush(stdout);*/
         if (a->forest_vec[text]->num_of_placeHolders >= b->forest_vec[text]->num_of_placeHolders) {
             determine_sub_lists(a->forest_vec[text],b->forest_vec[text],intersect_t,&n_pairs_t);
             determine_sub_lists(a->forest_vec[hypo],b->forest_vec[hypo],intersect_h,&n_pairs_h);
         } else {
             determine_sub_lists(b->forest_vec[text],a->forest_vec[text],intersect_t,&n_pairs_t);
             determine_sub_lists(b->forest_vec[hypo],a->forest_vec[hypo],intersect_h,&n_pairs_h);
         }
         k = compute_new_kernel(intersect_t,n_pairs_t, intersect_h,n_pairs_h,
             max(a->forest_vec[text]->num_of_placeHolders,b->forest_vec[text]->num_of_placeHolders),
             min(a->forest_vec[text]->num_of_placeHolders,b->forest_vec[text]->num_of_placeHolders),
             sqrt(sqrt(a->forest_vec[text]->twonorm_PT * b->forest_vec[text]->twonorm_PT)*
             sqrt(a->forest_vec[hypo]->twonorm_PT * b->forest_vec[hypo]->twonorm_PT)));
         //printf("max = %f\n",k);
         //printf("(%s,%s)=%f\n",a->forest_vec[0]->root->pChild[0]->sName,b->forest_vec[0]->root->pChild[0]->sName,k);
         int index = (b->forest_vec[text]->num_of_placeHolders*a->forest_vec[text]->num_of_placeHolders)-1;
              tempi[index] += (clock()-start);
              volte[index]++;
  } else k=0;
  //if (a->vectors != NULL &&   b->vectors != NULL)
  //    k += basic_kernel(kernel_parm, a, b, 0, 0)/
  //   sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD);

  return k;
}


////--------------------------------------- END VERSION 2

/// VERSION 3: Approximation
/// New Delta with the Hash table as a parameter
double Delta_aug( TreeNode * Nx, TreeNode * Nz, hash_table table_new){
    int i;
    double prod=1;
    double delta;
   	if((delta=hash_lookup(make_key(Nx->nodeID,Nz->nodeID), &table_new))>=0) {
	      return delta; // Case 0 (Duffy and Collins 2002);
       }
	else
	   if(Nx->pre_terminal || Nz->pre_terminal)
	      return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),LAMBDA,&table_new)); // case 1
	     else{
		   for(i=0;i<Nx->iNoOfChildren;i++)
	   	       if(strcmp(Nx->pChild[i]->production,Nz->pChild[i]->production)==0)
		          prod*= (SIGMA+Delta_aug( Nx->pChild[i], Nz->pChild[i],table_new)); // case 2

       		   return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),LAMBDA*prod,&table_new));
	     }
}


double Entailment_tree_kernel_approx(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){
       return Entailment_tree_kernel_approx_w(kernel_parm, a, b,2,3);
}

double Entailment_tree_kernel_approx_w(KERNEL_PARM * kernel_parm, DOC * a, DOC * b,int pair_i, int pair_j){

  int n_pairs_t=0;
  int n_pairs_h=0;

  double k,a_b;
  //, norma_a,norma_b=0;

  nodePair intersect_h[MAX_NUMBER_OF_PAIRS];
  nodePair intersect_t[MAX_NUMBER_OF_PAIRS];

  if (b->num_of_trees > pair_j && a->num_of_trees > pair_j){

         determine_sub_lists(a->forest_vec[pair_i],b->forest_vec[pair_i],intersect_t,&n_pairs_t);
         determine_sub_lists(a->forest_vec[pair_j],b->forest_vec[pair_j],intersect_h,&n_pairs_h);
         a_b = compute_approx_entailment_kernel(intersect_t,n_pairs_t, intersect_h,n_pairs_h);

         //determine_sub_lists(a->forest_vec[2],a->forest_vec[2],intersect_t,&n_pairs_t);
         //determine_sub_lists(a->forest_vec[3],a->forest_vec[3],intersect_h,&n_pairs_h);
         //norma_a = compute_approx_entailment_kernel(intersect_t,n_pairs_t, intersect_h,n_pairs_h);

         //determine_sub_lists(b->forest_vec[2],b->forest_vec[2],intersect_t,&n_pairs_t);
         //determine_sub_lists(b->forest_vec[3],b->forest_vec[3],intersect_h,&n_pairs_h);
         //norma_b = compute_approx_entailment_kernel(intersect_t,n_pairs_t, intersect_h,n_pairs_h);

         //// DEBUG  printf("%f - %f\n", a->forest_vec[2]->twonorm_PT, a->forest_vec[3]->twonorm_PT );

         k =  a_b/
              sqrt(a->forest_vec[pair_i]->twonorm_PT * b->forest_vec[pair_i]->twonorm_PT * a->forest_vec[pair_j]->twonorm_PT * b->forest_vec[pair_j]->twonorm_PT)
//                  +
//              (evaluateParseTreeKernel(intersect_t,n_pairs_t)/sqrt(a->forest_vec[2]->twonorm_PT * b->forest_vec[2]->twonorm_PT)+
//              evaluateParseTreeKernel(intersect_h,n_pairs_h)/sqrt(a->forest_vec[3]->twonorm_PT * b->forest_vec[3]->twonorm_PT))/2
               ;
        //printf("max = %f\n",k);
         //printf("(%s,%s)=%f\n",a->forest_vec[0]->root->pChild[0]->sName,b->forest_vec[0]->root->pChild[0]->sName,k);

  } else k=0;


  return k;
}


double compute_approx_entailment_kernel(nodePair *pairs_t, int n_t, nodePair *pairs_h, int n_h ){

    int i,j;
    double sum=0;

//    double somma_h = 0;
//    double somma_t = 0;
    hash_construct_table(&table_t,n_t+1); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)
    hash_construct_table(&table_h,n_h+1); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)

    for(i=0;i<n_t;i++){
        //printf("T: %f %s\n",Delta_aug(pairs_t[i].Nx,pairs_t[i].Nz,table_t) , pairs_t[i].Nx->production);
        for(j=0;j<n_h;j++){
            //printf("H: %f %s\n",Delta_aug(pairs_h[j].Nx,pairs_h[j].Nz,table_h) , pairs_h[j].Nx->production);
            if (   ((pairs_t[i].Nx->place_holder == pairs_h[j].Nx->place_holder) &&  (pairs_t[i].Nz->place_holder == pairs_h[j].Nz->place_holder))
                  ||
                   ((pairs_t[i].Nx->place_holder != pairs_h[j].Nx->place_holder) &&  (pairs_t[i].Nz->place_holder != pairs_h[j].Nz->place_holder)))
            //{
            sum+= Delta_aug(pairs_t[i].Nx,pairs_t[i].Nz,table_t)*Delta_aug(pairs_h[j].Nx,pairs_h[j].Nz,table_h);
            //somma_t += Delta_aug(pairs_t[i].Nx,pairs_t[i].Nz,table_t);
            //somma_h += Delta_aug(pairs_h[j].Nx,pairs_h[j].Nz,table_h);
            //}

        }
    }
//    printf("PRE = %f %f\n",somma_t/n_h , somma_h/n_t);
    //     printf("\n\nFORM EVALUATE KERNEL = %f \n",sum);

    hash_free_table( &table_t );
    hash_free_table( &table_h );

    return sum;
}


double Delta( TreeNode * Nx, TreeNode * Nz){
    int i;
    double prod=1;
    double delta;

	if((delta=hash_lookup(make_key(Nx->nodeID,Nz->nodeID), &table))>=0) {
		return delta; // Case 0 (Duffy and Collins 2002);
    }
	else
    if(Nx->pre_terminal || Nz->pre_terminal)
		//NO LEXICALIZATION
		//return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),1,&table)); // case 1
		return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),0,&table)); // case 1
    else{
		for(i=0;i<Nx->iNoOfChildren;i++)
			if(strcmp(Nx->pChild[i]->production,Nz->pChild[i]->production)==0)
				prod*= (SIGMA+LAMBDA*Delta( Nx->pChild[i], Nz->pChild[i])); // case 2
		return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),prod,&table));
    }
}


// Tree Kernels





double evaluateParseTreeKernel(nodePair *pairs, int n){

    int i;
    double sum=0,contr;

    hash_construct_table(&table,n+1); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)

	   for(i=0;i<n;i++){
		contr=Delta(pairs[i].Nx,pairs[i].Nz);
//		printf("Score for the pairs (%s , %s): %f\n",pairs[i].Nx->sName,pairs[i].Nz->sName,contr);fflush(stdout);
		sum+=contr;
	   }
 //     printf("\n\nFORM EVALUATE KERNEL = %f \n",sum);

   hash_free_table( &table );

	   return sum;
}

void determine_sub_lists(FOREST *a, FOREST *b, nodePair *intersect, int *n){

   int i=0,j=0,j_old,j_final;
   int n_a,n_b;
   short cfr;
   OrderedTreeNode *list_a, *list_b;

   n_a = a->listSize;
   n_b = b->listSize;
   list_a=a->orderedNodeSet;
   list_b=b->orderedNodeSet;
   *n=0;


/*  TEST
    printf("\n\n\nLenghts %d %d\n",a->listSize , b->listSize);
    printf("LIST1:\n");
    for(i=0;i<a->listSize;i++) printf("%s\n",a->orderedNodeSet[i].sName);
    printf("\n\n\nLIST2:\n");
    for(i=0;i<b->listSize;i++) printf("%s\n",b->orderedNodeSet[i].sName);
    i=0;
    printf("Determining LISTS:\n");fflush(stdout);
*/
    while(i<n_a && j<n_b){
      if((cfr=strcmp(list_a[i].sName,list_b[j].sName))>0)j++;
      else if(cfr<0)i++;
	   else{
		j_old=j;
		do{
		  do{
		    intersect[*n].Nx=list_a[i].node;
		    intersect[*n].Nz=list_b[j].node;
		    (*n)++;
		 //   delta_matrix[list_a[i].node->nodeID][list_b[j].node->nodeID]=-1.0;
            //printf("Evaluating-Pair: (%s  ,  %s) i %d,j %d j_old%d \n",list_a[i].sName,list_b[j].sName,i,j,j_old);fflush(stdout);
		    j++;
		  }
		  while(j<n_b && strcmp(list_a[i].sName,list_b[j].sName)==0);
		  i++;j_final=j;j=j_old;
		}
	        while(i<n_a && strcmp(list_a[i].sName,list_b[j].sName)==0);
		j=j_final;
	      }
   }
   if (*n>MAX_NUMBER_OF_PAIRS) { printf ("ERROR: The number of identical parse nodes exceed the current capacityn\n"); exit(-1);}
}

/*-------------SUBPATH TREE KERNEL------------*/

void determine_full_sub_lists(FOREST *a, FOREST *b, nodePair *intersect, int *n){
   int i=0,j=0,j_old,j_final;
   int n_a,n_b;
   short cfr;
   OrderedTreeNode *list_a, *list_b;
   n_a = a->nodeSize;
   n_b = b->nodeSize;
   list_a=a->fullOrderedNodeSet;
   list_b=b->fullOrderedNodeSet;
   *n=0;
    while(i<n_a && j<n_b){
      if((cfr=strcmp(list_a[i].sName,list_b[j].sName))>0)j++;
      else if(cfr<0)i++;
	   else{
		j_old=j;
		do{
		  do{
		    intersect[*n].Nx=list_a[i].node;
		    intersect[*n].Nz=list_b[j].node;
		    (*n)++;
		    j++;
		  }
		  while(j<n_b && strcmp(list_a[i].sName,list_b[j].sName)==0);
		  i++;j_final=j;j=j_old;
		}
	        while(i<n_a && strcmp(list_a[i].sName,list_b[j].sName)==0);
		j=j_final;
	      }
   }
   if (*n>MAX_NUMBER_OF_PAIRS) { printf ("ERROR: The number of identical parse nodes exceed the current capacityn\n"); exit(-1);}
}

double subpathDelta( TreeNode * Nx, TreeNode * Nz){
    int i, j;
    double sum=1;
    double delta;
	if((delta=hash_lookup(make_key(Nx->nodeID,Nz->nodeID), &table))>=0) {
		return delta;
    }
	else {
		for(i=0;i<Nx->iNoOfChildren;i++)
            for(j=0;j<Nz->iNoOfChildren;j++)
                if(strcmp(Nx->pChild[i]->sName,Nz->pChild[j]->sName)==0)
                    sum += (subpathDelta(Nx->pChild[i], Nz->pChild[j]));
		return (hash_insert(make_key(Nx->nodeID,Nz->nodeID),LAMBDA*sum,&table));
    }
}

double evaluateSubpathTreeKernel(nodePair *pairs, int n) {
    int i;
    double sum=0,contr;
    hash_construct_table(&table,n+1);
    for(i=0;i<n;i++){
		contr=subpathDelta(pairs[i].Nx,pairs[i].Nz);
//        printf("Score for the pairs (%s , %s): %f\n",pairs[i].Nx->sName,pairs[i].Nz->sName,contr);fflush(stdout);
		sum+=contr;
    }
    hash_free_table( &table );
    return sum;
}

/*-------------ROUTE TREE KERNEL------------*/

double routeDelta( TreeNode * Nx, TreeNode * Nz, int depth){
    double delta;
	if((delta=hash_lookup(make_key_depth(Nx->nodeID,Nz->nodeID,depth), &table))>=0) {
		return delta;
    }
	else {
	    if (depth == 1)
            return (hash_insert(make_key_depth(Nx->nodeID,Nz->nodeID,depth),1,&table));
		else {
            if(Nx->childIndex[depth-2] >= 0 && Nz->childIndex[depth-2] >= 0 && Nx->childIndex[depth-2] == Nz->childIndex[depth-2])
                return (hash_insert(make_key_depth(Nx->nodeID,Nz->nodeID,depth),LAMBDA*routeDelta(Nx, Nz, depth-1),&table));
            else
                return (hash_insert(make_key_depth(Nx->nodeID,Nz->nodeID,depth),0,&table));
		}
    }
}

double evaluateRouteTreeKernel(nodePair *pairs, int n, int maxDepth) {
    int i,j;
    double sum=0,contr;
    hash_construct_table(&table,(n+1)*maxDepth);
    for (j=1; j<= maxDepth; j++) {
        contr = 0;
        for(i=0;i<n;i++){
            contr += routeDelta(pairs[i].Nx,pairs[i].Nz,j);
//            printf("Score for the pairs (%s, %s, %d): %f\n",pairs[i].Nx->sName,pairs[i].Nz->sName,j,contr);fflush(stdout);
        }
        sum+=contr;
        if (contr == 0)
            break;
    }
    hash_free_table( &table );
    return sum;
}

double tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int i, int j){

  int n_pairs=0;

  double k=0;

  nodePair intersect[MAX_NUMBER_OF_PAIRS];

  if (b->num_of_trees > j && a->num_of_trees > i){
      if (kernel_parm->custom[0]=='d') {
          determine_full_sub_lists(a->forest_vec[i],b->forest_vec[j],intersect,&n_pairs);
          k =(evaluateSubpathTreeKernel(intersect,n_pairs));
      }
      else if (kernel_parm->custom[0]=='e') {
          int minMaxDepth = a->forest_vec[i]->maxDepth;
          if (b->forest_vec[i]->maxDepth < minMaxDepth)
            minMaxDepth = b->forest_vec[i]->maxDepth;
          determine_full_sub_lists(a->forest_vec[i],b->forest_vec[j],intersect,&n_pairs);
          k =(evaluateRouteTreeKernel(intersect,n_pairs,minMaxDepth));
      }
      else {
          determine_sub_lists(a->forest_vec[i],b->forest_vec[j],intersect,&n_pairs);
          k =(evaluateParseTreeKernel(intersect,n_pairs));
      }
         //printf("\n\n(i,j)=(%d,%d)= Kernel :%f norm1,norm2 (%f,%f)\n",i,j,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[j]->twonorm_PT);
  }
  else{
       printf("\nERROR: attempting to access to a tree not defined in the data\n\n");
       exit(-1);
  }
  return k;
}

double basic_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b, int i, int j)
     /* calculate the kernel function */

{
  switch(kernel_parm->second_kernel) {
    case 0: /* linear */
            return(sprod_i(a, b, i, j));
    case 1: /* polynomial */
            return (double) pow(((double)kernel_parm->coef_lin)*(double)sprod_i(a, b, i, j)
                   +(double)kernel_parm->coef_const,(double) kernel_parm->poly_degree);
    case 2: /* radial basis function */
            return(exp(-kernel_parm->rbf_gamma*(a->vectors[i]->twonorm_sq-2*sprod_i(a, b, i, j)+b->vectors[i]->twonorm_sq)));
    case 3: /* sigmoid neural net */
            return(tanh(kernel_parm->coef_lin*sprod_i(a, b, i, j)+kernel_parm->coef_const));
    case 4: /* custom-kernel supplied in file kernel.h*/
            return(custom_kernel(kernel_parm,a,b));
    case 5: /* TREE KERNEL */
            return(tree_kernel(kernel_parm,a,b,i,j));

    default: printf("Error: The kernel function to be combined with the Tree Kernel is unknown\n"); exit(1);
  }
}


void evaluateNorma(KERNEL_PARM * kernel_parm, DOC * d){

    int n_pairs=0,
        i;

    double k=0;
    nodePair intersect[MAX_NUMBER_OF_PAIRS];

//printf("doc ID :%d \n",d->docnum);
//printf("num of vectors:%d \n",d->num_of_vectors);
//fflush(stdout);

    for(i=0;i < d->num_of_tdags;i++)
    {
             d->tdag_vec[i]->norm = 1;
             k = Entailment_tree_kernel_tdag(kernel_parm, d, d, i);
             if (k != 0 && (kernel_parm->normalization == 1 || kernel_parm->normalization == 3))
                d->tdag_vec[i]->norm = k;
    }
    if (kernel_parm->kernel_type==15){
       d->forest_vec[2]->twonorm_PT = 1;
       d->forest_vec[3]->twonorm_PT = 1;
       k = Entailment_tree_kernel(kernel_parm, d, d);
       if(k!=0 && (kernel_parm->normalization == 1 || kernel_parm->normalization == 3))
       {
               d->forest_vec[2]->twonorm_PT = k;
               d->forest_vec[3]->twonorm_PT = k;
       }
    }
    else
      for(i=0;i < d->num_of_trees;i++){
      /*  TESTS

          printf("\n\n\nnode ID: %d \n", d->forest_vec[i]->root->nodeID); fflush(stdout);

          printf("node list length: %d\n", d->forest_vec[i]->listSize);

          printf("doc ID :%d num of placeH: %d\n",d->docnum,d->forest_vec[i]->num_of_placeHolders);

          printf("tree: <"); writeTreeString(d->forest_vec[i]->root);printf(">");

          printf("\n\n"); fflush(stdout);
*/
          // this avoids to check for norm == 0
          //printf ("Norm %f\n",k1);

          //ORG
          determine_sub_lists(d->forest_vec[i],d->forest_vec[i],intersect,&n_pairs);
          //ORG k =(evaluateParseTreeKernel(intersect,n_pairs));

          if (d->forest_vec[i]->num_of_placeHolders>0 && !(strlen(kernel_parm->custom) >= 3 && kernel_parm->custom[2]=='2')) {

                int C[max_placeholders];

                int c = 0;
                int iiii=0;
                for (iiii=0;iiii < max_placeholders; iiii++)
                {
                    C[iiii] = iiii+1;
                    c += C[iiii]*((int)pow((float)base,iiii));
                }


               hash_construct_table(&table,(n_pairs+1)*50); // for the moment only node pair // multiply for 5040 assigments (7 placeholders)
               k = Eval_Ent_ParseTreeKernel(intersect, n_pairs, table, c, C);
               hash_free_table( &table );
               //printf("VETTORE [%s] NORMA [%d] = %f \n",d->forest_vec[0]->root->pChild[0]->sName, i, k);
          } else {
            if (kernel_parm->custom[0] == 'd' || kernel_parm->custom[0] == 'e')
                k = tree_kernel(kernel_parm, d, d, i, i);
            else
                k =(evaluateParseTreeKernel(intersect,n_pairs));
          }
          //printf("VETTORE [%s] NORMA [%d] = %f \n",d->forest_vec[i]->root->pChild[i]->sName, i, k);
          //ATTENZIONE!!!!
          //k =(evaluateParseTreeKernel(intersect,n_pairs));

          if(k!=0 && (kernel_parm->normalization == 1 || kernel_parm->normalization == 3))
               d->forest_vec[i]->twonorm_PT=k;
          else d->forest_vec[i]->twonorm_PT=1;


      }
  /* SECOND KERNEL NORM EVALUATION */

      for(i=0;i < d->num_of_vectors;i++){
//      for(i=0;i < 61 && i<d->num_of_vectors;i+=60){

          d->vectors[i]->twonorm_STD=1; // basic-kernel normalizes the standard kernels
                                        // this also avoids to check for norm == 0

          k = basic_kernel(kernel_parm, d, d, i, i);

          if(k!=0 && (kernel_parm->normalization == 2 || kernel_parm->normalization == 3))
               d->vectors[i]->twonorm_STD=k; // if selected normalization is applied

          d->vectors[i]->twonorm_sq=sprod_ss(d->vectors[i]->words,d->vectors[i]->words);
       }
      // maintain the compatibility with svm-light single linear vector
        if(d->num_of_vectors>0) d->twonorm_sq=sprod_ss(d->vectors[0]->words,d->vectors[0]->words);
        else d->twonorm_sq=0;
 }


/***************************************************************************************/
/*                           KERNELS OVER SET OF KERNELS                               */
/***************************************************************************************/



// sequence summation of trees

double sequence_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){//all_vs_all_tree_kernel

  int i;
  double k;
  int n_pairs=0;
  nodePair intersect[MAX_NUMBER_OF_PAIRS];

//   printf("\n\nDocum %ld and %ld, size=(%d,%d)\n",a->docnum,b->docnum,a->num_of_trees,b->num_of_trees);
   k=0;

   for(i=0; i< a->num_of_trees && i< b->num_of_trees; i++){
        /*
         printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
         printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
         */
   //      printf("\ntree 1: <"); writeTreeString(a->forest_vec[i]->root);

    //     printf(">\ntree 2: <"); writeTreeString(b->forest_vec[i]->root);printf(">\n"); fflush(stdout);

      if(a->forest_vec[i]!=NULL && b->forest_vec[i]!=NULL){
         determine_sub_lists(a->forest_vec[i],b->forest_vec[i],intersect,&n_pairs);
         k+= (evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[i]->twonorm_PT));
      }

 //        printf("\n\n(i,i)=(%d,%d)= Kernel-Sequence :%f norm1,norm2 (%f,%f)\n",i,i,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[i]->twonorm_PT);
  }
   return k;
}


// all vs all summation of trees


double AVA_tree_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){//all_vs_all_tree_kernel

  int i,
      j,
      n_pairs=0;

  double k=0;
  nodePair intersect[MAX_NUMBER_OF_PAIRS];

   //printf("first elements: %s  %s\n", a->orderedNodeSet->sName,b->orderedNodeSet->sName);
   //printf("\n\n---------------------------------------------------------\n\n");fflush(stdout);
   //printf("doc IDs :%d %d",a->docnum,b->docnum);

   if (b->num_of_trees == 0 || a->num_of_trees==0) return 0;

   for(i=0; i< a->num_of_trees; i++)
      for(j=0; j< b->num_of_trees;j++){

      if(a->forest_vec[i]!=NULL && b->forest_vec[j]!=NULL){

        /*
         printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
         printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
         */
        //printf("\ntree 1: <"); writeTreeString(a->forest_vec[i]->root);

        //printf(">\ntree 2: "); writeTreeString(b->forest_vec[j]->root);printf(">\n");
        //fflush(stdout);

         determine_sub_lists(a->forest_vec[i],b->forest_vec[j],intersect,&n_pairs);

         k+= (evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[j]->twonorm_PT));
         //printf("\n\n(i,j)=(%d,%d)= Kernel :%f norm1,norm2 (%f,%f)\n",i,j,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[j]->twonorm_PT);
      }
     }
    //printf("\n---------------------------------------------------------------\n"); fflush(stdout);


   return k;
}



// sequence summation of vectors


double sequence(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  int i;
  double k=0;

   for(i=0; i< a->num_of_vectors && i< b->num_of_vectors; i++)

      if(a->vectors[i]!=NULL && b->vectors[i]!=NULL){
         k+= basic_kernel(kernel_parm, a, b, i, i)/
         sqrt(a->vectors[i]->twonorm_STD * b->vectors[i]->twonorm_STD);
      }
   return k;
}


// all vs all summation of vectors

double AVA(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  int i,
      j;

  double k=0;

   for(i=0; i< a->num_of_vectors; i++)
      for(j=0; j< b->num_of_vectors;j++){

         if(a->vectors[i]!=NULL && b->vectors[j]!=NULL){
             k+= basic_kernel(kernel_parm, a, b, i, j)/
             sqrt(a->vectors[i]->twonorm_STD * b->vectors[j]->twonorm_STD);
         }
      }
  return k;
}


// kernel for entailments [Zanzotto and Moschitti, ACL 2005]

double ACL2006_Entailment_kernel(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  int i,
      n_pairs=0;

  double k=0,
         max=0;

  nodePair intersect[MAX_NUMBER_OF_PAIRS];

//   LAMBDA = kernel_parm->lambda; //faster access for lambda
//   SIGMA = kernel_parm->sigma;


   // printf("first elements: %s  %s\n", a->orderedNodeSet->sName,b->orderedNodeSet->sName);
   // printf("\n\n---------------------------------------------------------\n\n");fflush(stdout);
   // printf("doc IDs :%d %d",a->docnum,b->docnum);

 if (b->num_of_trees > 1 && a->num_of_trees>1){
   //kk=0;
   //for(i=0; i< 2; i++)
   //  for(j=0; j< 2;j++){
   //   if(a->forest_vec[i]!=NULL && b->forest_vec[j]!=NULL){
        /*
         printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
         printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
         */
        //printf("\ntree 1: <"); writeTreeString(a->forest_vec[i]->root);
        //printf(">\ntree 2: "); writeTreeString(b->forest_vec[j]->root);printf(">\n");
        //fflush(stdout);
         //determine_sub_lists(a->forest_vec[i],b->forest_vec[j],intersect,&n_pairs);
         //kk+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[j]->twonorm_PT));
         //printf("\n\n(i,j)=(%d,%d)= Kernel :%f norm1,norm2 (%f,%f)\n",i,j,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[j]->twonorm_PT);
    //  }
    // }
        // determine_sub_lists(a->forest_vec[0],b->forest_vec[0],intersect,&n_pairs);
        // kk+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT));
        // determine_sub_lists(a->forest_vec[1],b->forest_vec[1],intersect,&n_pairs);
        // kk+= (evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[1]->twonorm_PT));
   }
   //kk = 0; FMZ added to test max contribution

   max = 0;
   if (b->num_of_trees > 2  && a->num_of_trees > 2){
   if (b->num_of_trees >  a->num_of_trees) {
     for(i=2;i<b->num_of_trees ;i+=2){
      if(a->forest_vec[2]!=NULL && b->forest_vec[i]!=NULL){
          //  printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
        //  printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
          // printf("\ntree 1: "); writeTreeString(a->forest_vec[i]->root);
          //printf("\ntree 2: "); writeTreeString(b->forest_vec[i]->root);
         // fflush(stdout);
         determine_sub_lists(a->forest_vec[2],b->forest_vec[i],intersect,&n_pairs);
	 k= evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[2]->twonorm_PT * b->forest_vec[i]->twonorm_PT);
         determine_sub_lists(a->forest_vec[3],b->forest_vec[i+1],intersect,&n_pairs);
         k+= evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[3]->twonorm_PT * b->forest_vec[i+1]->twonorm_PT);
         if(max<k)max=k;
         //printf("\n\nKernel :%f \n",k);
       }
      }
     } else {

      for(i=2;i<a->num_of_trees ;i+=2){
       if(a->forest_vec[i]!=NULL && b->forest_vec[2]!=NULL){
          //  printf("\n\n\n nodes: %d  %d\n", a->forest_vec[i]->root->nodeID,b->forest_vec[i]->root->nodeID);
        //  printf("node list lenghts: %d  %d\n", a->forest_vec[i]->listSize,b->forest_vec[i]->listSize);
          // printf("\ntree 1: "); writeTreeString(a->forest_vec[i]->root);
          //printf("\ntree 2: "); writeTreeString(b->forest_vec[i]->root);
         // fflush(stdout);
         determine_sub_lists(a->forest_vec[i],b->forest_vec[2],intersect,&n_pairs);
	     k= evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[i]->twonorm_PT * b->forest_vec[2]->twonorm_PT);
         determine_sub_lists(a->forest_vec[i+1],b->forest_vec[3],intersect,&n_pairs);
         k+= evaluateParseTreeKernel(intersect,n_pairs)/
         sqrt(a->forest_vec[i+1]->twonorm_PT * b->forest_vec[3]->twonorm_PT);
         if(max<k)max=k;
         //printf("\n\nKernel :%f \n",k);
       }
      }
     }
    }
   //printf("\n---------------------------------------------------------------\n");fflush(stdout);
//printf("\n\nKernel :%f \n",max);

  if(kernel_parm->combination_type=='+' && (a->vectors!=NULL && b->vectors!=NULL))
       return basic_kernel(kernel_parm, a, b, 0, 0)/
              sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD)+
              kernel_parm->tree_constant*max;
  else return max;
}


// Kernel for re-ranking predicate argument structures, [Moschitti, CoNLL 2006]

double SRL_re_ranking_CoNLL2006(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){//all_vs_all_tree_kernel

  int n_pairs=0;

  double k1=0,k2=0;
  nodePair intersect[MAX_NUMBER_OF_PAIRS];

   //printf("first elements: %s  %s\n", a->orderedNodeSet->sName,b->orderedNodeSet->sName);
   //printf("\n\n---------------------------------------------------------\n\n");fflush(stdout);

 if(kernel_parm->kernel_type==11 || kernel_parm->kernel_type==12){

   if(a->num_of_trees!=0 && b->num_of_trees!= 0){

      if(a->forest_vec[0]==NULL || a->forest_vec[1]==NULL
         || b->forest_vec[0]==NULL || b->forest_vec[1]==NULL){
         printf("ERROR: two trees for each instance are needed");
         exit(-1);
      }

         determine_sub_lists(a->forest_vec[0],b->forest_vec[0],intersect,&n_pairs);
         k1+= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT);

         determine_sub_lists(a->forest_vec[1],b->forest_vec[1],intersect,&n_pairs);
         k1+= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[1]->twonorm_PT);

         determine_sub_lists(a->forest_vec[1],b->forest_vec[0],intersect,&n_pairs);
         k1-= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[0]->twonorm_PT);

         determine_sub_lists(a->forest_vec[0],b->forest_vec[1],intersect,&n_pairs);
         k1-= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[1]->twonorm_PT);

         if(kernel_parm->kernel_type==12)k1*=kernel_parm->tree_constant;

    //printf("\n\n(i,j)=(%d,%d)= Kernel :%f norm1,norm2 (%f,%f)\n",i,j,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[j]->twonorm_PT);
    //printf("\n---------------------------------------------------------------\n"); fflush(stdout);
   }
}

// to use all the local argument classifier features as in [Toutanova ACL2005]

if(kernel_parm->kernel_type==13 || kernel_parm->kernel_type==12){
          k2+=sequence_ranking(kernel_parm, a, b, 0, 0);
          k2+=sequence_ranking(kernel_parm, a, b, 1, 1);
          k2-=sequence_ranking(kernel_parm, a, b, 0, 1);
          k2-=sequence_ranking(kernel_parm, a, b, 1, 0);
          }


// use only 1 vector for each predicate argument structure

//     if(kernel_parm->kernel_type==13 || kernel_parm->kernel_type==12){
//       if(a->num_of_vectors>0 && b->num_of_vectors>0){
//         k2+=basic_kernel(kernel_parm, a, b, 0, 0)/
//          sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD);
//	     k2+=basic_kernel(kernel_parm, a, b, 1, 1)/
//          sqrt(a->vectors[1]->twonorm_STD * b->vectors[1]->twonorm_STD);
//         k2-=basic_kernel(kernel_parm, a, b, 0, 1)/
//          sqrt(a->vectors[0]->twonorm_STD * b->vectors[1]->twonorm_STD);
//         k2-=basic_kernel(kernel_parm, a, b, 1, 0)/
//          sqrt(a->vectors[1]->twonorm_STD * b->vectors[0]->twonorm_STD);
//          }
//       }


   // printf("kernel: %f\n",k2);

   return  k1+k2;//(k1*k2 + k2 + (k1+1)*(k1+1));
}


// ranking algorithm based on only trees. It can be used for parse-tree re-ranking

double tree_kernel_ranking(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){//all_vs_all_tree_kernel

  int n_pairs=0;

  double k=0;
  nodePair intersect[MAX_NUMBER_OF_PAIRS];

      //printf("first elements: %s  %s\n", a->orderedNodeSet->sName,b->orderedNodeSet->sName);
   //printf("\n\n---------------------------------------------------------\n\n");fflush(stdout);

   if(a->num_of_trees==0 || b->num_of_trees== 0) return 0;

      if(a->forest_vec[0]==NULL || a->forest_vec[1]==NULL
         || b->forest_vec[0]==NULL || b->forest_vec[1]==NULL){
         printf("ERROR: two trees for each instance are needed");
         exit(-1);
      }

         determine_sub_lists(a->forest_vec[0],b->forest_vec[0],intersect,&n_pairs);
         k+= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[0]->twonorm_PT);

         determine_sub_lists(a->forest_vec[1],b->forest_vec[1],intersect,&n_pairs);
         k+= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[1]->twonorm_PT);

         determine_sub_lists(a->forest_vec[1],b->forest_vec[0],intersect,&n_pairs);
         k-= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[1]->twonorm_PT * b->forest_vec[0]->twonorm_PT);

         determine_sub_lists(a->forest_vec[0],b->forest_vec[1],intersect,&n_pairs);
         k-= evaluateParseTreeKernel(intersect,n_pairs)/sqrt(a->forest_vec[0]->twonorm_PT * b->forest_vec[1]->twonorm_PT);

         //printf("\n\n(i,j)=(%d,%d)= Kernel :%f norm1,norm2 (%f,%f)\n",i,j,k,a->forest_vec[i]->twonorm_PT, b->forest_vec[j]->twonorm_PT);
    //printf("\n---------------------------------------------------------------\n"); fflush(stdout);

   return k;
}


// ranking algorithm based on only vectors. For example, it can be used for ranking documents wrt a query


double vector_ranking(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  double k=0;

         if(a->num_of_vectors==0 || b->num_of_vectors==0) return 0;

          k+=basic_kernel(kernel_parm, a, b, 0, 0);
          sqrt(a->vectors[0]->twonorm_STD * b->vectors[0]->twonorm_STD);
          k+=basic_kernel(kernel_parm, a, b, 1, 1)/
          sqrt(a->vectors[1]->twonorm_STD * b->vectors[1]->twonorm_STD);
          k-=basic_kernel(kernel_parm, a, b, 0, 1)/
          sqrt(a->vectors[0]->twonorm_STD * b->vectors[1]->twonorm_STD);
          k-=basic_kernel(kernel_parm, a, b, 1, 0)/
          sqrt(a->vectors[1]->twonorm_STD * b->vectors[0]->twonorm_STD);
   return k;
}


// ranking algorithm based on tree forests. In this case the ranked objetcs are described by a forest

double vector_sequence_ranking(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  double k=0;

  k+=sequence_ranking(kernel_parm, a, b, 0, 0); // ranking with sequences of vectors
  k+=sequence_ranking(kernel_parm, a, b, 1, 1);
  k-=sequence_ranking(kernel_parm, a, b, 0, 1);
  k-=sequence_ranking(kernel_parm, a, b, 1, 0);

  return k;
}


/* uses all the vectors in the vector set for ranking */
/* this means that there are n/2 vectors for the first pair and n/2 for the second pair */

double sequence_ranking(KERNEL_PARM * kernel_parm, DOC * a, DOC * b, int memberA, int memberB){//all_vs_all vectorial kernel

  int i;
  int startA, startB;

  double k=0;

   startA= a->num_of_vectors*memberA/2;
   startB= b->num_of_vectors*memberB/2;

   if(a->num_of_vectors==0 || b->num_of_vectors==0) return 0;

//   for(i=0; i< a->num_of_vectors/2 && i< b->num_of_vectors/2; i++)
  for(i=0; i<1 && i< a->num_of_vectors/2 && i< b->num_of_vectors/2 ; i++)
      if(a->vectors[i+startA]!=NULL && b->vectors[startB+i]!=NULL){
         k+= basic_kernel(kernel_parm, a, b, startA+i, startB+i)/
         sqrt(a->vectors[startA+i]->twonorm_STD * b->vectors[startB+i]->twonorm_STD);
      }
   return k;
}


/***************************************************************************************/
/*                                  KERNELS COMBINATIONS                               */
/***************************************************************************************/

// select the method to combine a forest of trees
// when will be available more kernel types, remeber to define a first_kernel option (e.g. -F)

double choose_tree_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b){
     /* calculate the kernel function */

  switch(kernel_parm->vectorial_approach_tree_kernel) {

    case 'S': /* TREE KERNEL Sequence k11+k22+k33+..+knn*/
            return sequence_tree_kernel(kernel_parm,a,b);;
    case 'A': /* TREE KERNEL ALL-vs-ALL k11+k12+k13+..+k23+k33+..knn*/
            return(AVA_tree_kernel(kernel_parm,a,b));
    case 'R': /* re-ranking kernel classic SST*/
            return((CFLOAT)tree_kernel_ranking(kernel_parm,a,b));
//    case 7: /* TREE KERNEL MAX of ALL-vs-ALL */
//            return(AVA_MAX_tree_kernel(kernel_parm,a,b));
//    case 8: /* TREE KERNEL MAX of sequence of pairs Zanzotto et all */
//            return(AVA_MAX_tree_kernel_over_pairs(kernel_parm,a,b));
    default: printf("Error: Unknown tree kernel function\n"); exit(1);
   }
}


// select the method to combine the set of vectors

double choose_second_kernel(KERNEL_PARM *kernel_parm, DOC *a, DOC *b)
     /* calculate the kernel function */
{
  switch(kernel_parm->vectorial_approach_standard_kernel) {

   case 'S':/* non structured KERNEL Sequence k11+k22+k33+..+knn*/
            return(sequence(kernel_parm,a,b));
   case 'A': /* Linear KERNEL ALL-vs-ALL k11+k12+k13+..+k23+k33+..knn*/
            return(AVA(kernel_parm,a,b));
   case 'R': /* re-ranking kernel*/
            return((CFLOAT)vector_ranking(kernel_parm,a,b));

//    case 13: /* Linear KERNEL MAX of ALL-vs-ALL */
//            return((CFLOAT)AVA_MAX(kernel_parm,a,b));
//    case 14: /* TREE KERNEL MAX of sequence of pairs Zanzotto et all */
//            return((CFLOAT)AVA_MAX_over_pairs(kernel_parm,a,b));
    default: printf("Error: Unknown kernel combination function\n"); exit(1);
   }
}


// select the data to be used in kenrels:
//            vectors, trees, their sum or their product

double advanced_kernels(KERNEL_PARM * kernel_parm, DOC * a, DOC * b){

  double k1,
         k2;
/* TEST
        tmp = (k1*k2);
     	printf("K1 %f and K2= %f NORMA= %f norma.a= %f  norma.b= %f\n",k1,k2,norma,a->twonorm_sq,b->twonorm_sq);
	printf("\nKernel Evaluation: %1.20f\n", tmp);
*/

 switch(kernel_parm->combination_type) {

    case '+': /* sum first and second kernels*/
              k1 = choose_tree_kernel(kernel_parm, a, b);
              k2 = choose_second_kernel(kernel_parm, a, b);
    	      return k2 + kernel_parm->tree_constant*k1;
    case '*': k1 = choose_tree_kernel(kernel_parm, a, b);
              k2 = choose_second_kernel(kernel_parm, a, b);
              return k1*k2;
    case 'T': /* only trees */
              return choose_tree_kernel(kernel_parm, a, b);
    case 'V': /* only vectors*/
              return choose_second_kernel(kernel_parm, a, b);
              // otherwise evaluate the vectorial kernel on the basic kernels
    default: printf("Error: Unknown kernel combination\n"); exit(1);
   }
}
