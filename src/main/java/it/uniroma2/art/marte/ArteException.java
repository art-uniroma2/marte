package it.uniroma2.art.marte;

public class ArteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ArteException(String message) {
		super("ARTE panic : \n\t" + message);
	}
}
