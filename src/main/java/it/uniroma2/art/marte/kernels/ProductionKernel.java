package it.uniroma2.art.marte.kernels;

import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.SimpleConst;

import java.util.HashMap;
import java.util.Vector;


public class ProductionKernel {
	
	public static double lambda = 1;
	private static HashMap <Pair,Double> delta_matrix = null; 
	
	public static double value(Constituent a,Constituent b) {
		delta_matrix = new HashMap <Pair,Double> ();
		double sum = 0;
		for (Constituent aa:allConstituents(a))
			for (Constituent bb:allConstituents(b))
				sum += delta(aa,bb);
		return sum;
	}

	public static double delta(Constituent a,Constituent b) {
		double k = 0;
		
		if (delta_matrix != null && delta_matrix.containsKey(new Pair(a,b))) {
			return delta_matrix.get(new Pair(a,b));
		}
		
		if (a instanceof SimpleConst && b instanceof SimpleConst) {
//			if (
//					a.getType().equals(b.getType())
//					&& a.getSurface().equals(b.getSurface())
//					
//			) k = 1;
		} else if (a instanceof ComplxConst && b instanceof ComplxConst) {
			
			if (production((ComplxConst) a).equals(production((ComplxConst) b))) {
				k = 1;
				
			} 
		}
		if (delta_matrix != null) delta_matrix.put(new Pair(a,b),k);
		return k;
	}
	
	public static String production(ComplxConst a) {
		String production = a.getType() + ":";
		for (Constituent c: (Vector <Constituent>) a.getSubConstituents()) 
			production += c.getType()+ " ";
		return production;
	} 

	private static class Pair {
		Constituent a;
		Constituent b;
		Pair(Constituent a,Constituent b) {this.a=a;this.b=b;}
		public boolean equals(Object p) {
			return ((Pair)p).a==a && ((Pair)p).b==b; 
		}
	}
	
	private static Vector <Constituent> allConstituents(Constituent c) {
		Vector <Constituent> all = new Vector<Constituent>();
		all.add(c);
		if (c instanceof ComplxConst) {
			for (Constituent sc:(Vector <Constituent>)((ComplxConst) c).getSubConstituents()) {
				all.addAll(allConstituents(sc));
			}
		}
		return all;
	}
	
}
