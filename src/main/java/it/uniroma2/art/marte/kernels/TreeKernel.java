package it.uniroma2.art.marte.kernels;

import java.util.HashMap;
import java.util.Vector;

import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.SimpleConst;

public class TreeKernel {
	
	public static double lambda = 1;
	public static boolean lexicalized = false;
	private static int consts = 0;
	private static HashMap <String,Double> delta_matrix = null; 
	private static HashMap <Constituent,Integer> const_indexes = null; 
	
	public static double value(Constituent a,Constituent b) {
		delta_matrix = new HashMap <String,Double> ();
		const_indexes = new HashMap <Constituent,Integer> ();
		consts = 0;
		double sum = 0;
		for (Constituent aa:allConstituents(a))
			for (Constituent bb:allConstituents(b))
				sum += delta(aa,bb);
		return sum;
	}

	public static double delta(Constituent a,Constituent b) {
		double k = 0;
		if (const_indexes !=null ) {
			if (!const_indexes.containsKey(a)) {
				const_indexes.put(a,consts);
				consts++;
			}
			if (!const_indexes.containsKey(b)) {
				const_indexes.put(b,consts);
				consts++;
			}
		}			
		if (delta_matrix != null && const_indexes !=null ) {
		if (delta_matrix.containsKey("" +const_indexes.get(a) + ":" +const_indexes.get(b))) {
//				System.out.print(".");
				return delta_matrix.get("" +const_indexes.get(a) + ":" +const_indexes.get(b));
			}
		}
		
		if (a instanceof SimpleConst && b instanceof SimpleConst) {
			if (	lexicalized &&
					a.getType().equals(b.getType())
					&& a.getSurface().equals(b.getSurface())
					
			) k = 1;
		} else if (a instanceof ComplxConst && b instanceof ComplxConst) {
			if (production((ComplxConst) a).equals(production((ComplxConst) b))) {
				k = 1;
				Vector<Constituent> A = ((ComplxConst) a).getSubConstituents();
				Vector<Constituent> B = ((ComplxConst) b).getSubConstituents();
				
				for (int i=0;i<((ComplxConst) a).getSubConstituents().size();i++) {
					k = k*(1+lambda*delta(A.elementAt(i),B.elementAt(i)));
				}
			} 
		}
		if (delta_matrix != null) delta_matrix.put("" +const_indexes.get(a) + ":" +const_indexes.get(b),k);
		return k;
	}
	
	public static String production(ComplxConst a) {
		String production = a.getType() + ":";
		for (Constituent c: (Vector <Constituent>) a.getSubConstituents()) 
			production += c.getType()+ " ";
		return production;
	} 

	
	private static Vector <Constituent> allConstituents(Constituent c) {
		Vector <Constituent> all = new Vector<Constituent>();
		all.add(c);
		if (c instanceof ComplxConst) {
			for (Constituent sc:(Vector <Constituent>)((ComplxConst) c).getSubConstituents()) {
				all.addAll(allConstituents(sc));
			}
		}
		return all;
	}
	
}
