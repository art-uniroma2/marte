package it.uniroma2.art.marte.dictionaries;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class GeneralizationDictionary {

private Hashtable dictionary = null;

public GeneralizationDictionary(String location) throws Exception {
	System.out.println("Loading dictionary: " + location);
	load(location);
}


private void load(String file) throws Exception {
	dictionary = new Hashtable();
	String line = "";
	int line_no = 0;
	try {
		BufferedReader in = new BufferedReader(new FileReader(file));
		line = in.readLine();
		line_no = 0;
		while (line != null) {
			StringTokenizer st = new StringTokenizer(line,",");
			String generalization = st.nextToken().trim();
			String verb = st.nextToken().trim();
			if (!dictionary.containsKey(verb)) dictionary.put(verb,new Vector());
			Vector gens = (Vector) dictionary.get(verb);
			gens.addElement(generalization);
			dictionary.put(verb,gens);
			line = in.readLine();
			line_no++;
		}
		in.close();
	} catch (IOException e) {
		System.err.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	} catch (NoSuchElementException e) {
		System.err.println("ERROR in line : " + line_no + " <" + line +">");
		throw e;
	}
}

public boolean contains(String verb){
	return dictionary.containsKey(verb);
}

public String firstGeneralization(String verb){
	String out = null;
	if (dictionary.containsKey(verb) && ((Vector)dictionary.get(verb)).size() > 0) {
		out = (String) ((Vector)dictionary.get(verb)).elementAt(0);
	}
	return out;
}

}