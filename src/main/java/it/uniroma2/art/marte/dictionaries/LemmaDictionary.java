package it.uniroma2.art.marte.dictionaries;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.POS;

import java.io.*;
import java.util.*;
//import it.uniroma2.chaos.xdg.*;
/**
 * This class implements the <code>LemmaDictionary</code>. It is used to store in a hashtable pairs of words-lemmas.
 * The key of the hashtable is a word with its Pos (e.g. "negotiating_V"). The value is its lemma (e.g. "negotiate".
 * The keys are read from the KB directory in the file specified in the initializer of the class (with ".w" as suffix).
 * The values are read from the KB directory in the file specified in the initializer of the class (with ".l" as suffix).
 */
public class LemmaDictionary {
	
private Hashtable lemmata = null;
private String lemmata_file = null;
 
 /** 
  * Constructor Loads hashtable from KB files.
  *
  * @param 	lemmata_file	KB file base name
  */
public LemmaDictionary(String lemmata_file) {
	this.lemmata_file = lemmata_file;
	loadLemmata();
}

 /** 
  * Given a Constituent, return its base lemma if present in hashtable.
  * otherwise returns the surface of the consituent.
  *
  * @param 	the input constituent
  * @return the lemma
  */
public String lemma(Constituent h) {
	String lemma = h.getSurface();
	String lemmaType = typeForLemma(h);
	if (lemmaType != null) {
		if (lemmata == null) loadLemmata();
		if (lemmata.containsKey(lemma+"_"+lemmaType)) 
			lemma = (String) lemmata.get(lemma+"_"+lemmaType);
		else writeLemmataLog(lemma,lemma+"_"+lemmaType);
	} 
	return lemma;
}


public void addLemma(XDG xdg) {
	Vector <Constituent> cs = xdg.getSetOfConstituents();
	for (Constituent c: cs) {
		Vector <Constituent> scs = c.getSimpleConstituentList();
		for (Constituent sc : scs) {
			sc.getFirstLemma().setSurface(lemma(sc));
		} 
	}
	
}


private void writeLemmataLog(String lemma, String lemmaPlusPos) {
	try {
		BufferedWriter err = new BufferedWriter(new FileWriter(lemmata_file + ".err",true));
		err.write(lemmaPlusPos + "\n");
		err.close();
		lemmata.put(lemmaPlusPos,lemma);
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	}
	
}


 /** 
  * Loads hashtable from KB files.
  *
  * @param 	lemmata_file	KB file base name
  */
private void loadLemmata() {
	lemmata = new Hashtable();
	try {
		if ((new File(lemmata_file + ".err")).exists()) (new File(lemmata_file + ".err")).delete();
		BufferedReader in_w = new BufferedReader(new FileReader(lemmata_file + ".w"));
		BufferedReader in_l = new BufferedReader(new FileReader(lemmata_file + ".l"));
		System.out.println("Loading lemmata");
		String line_w = in_w.readLine();
		String line_l = in_l.readLine();
		while (line_w != null) {
			lemmata.put(line_w,line_l);
			line_w = in_w.readLine();
			line_l = in_l.readLine();
		}
		in_w.close();
		in_l.close();
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
	}
}

private String typeForLemma(Constituent h) {
	if (POS.proper_noun(h)) return "NP";
	else if (POS.common_noun(h)) return "NN";
	else if (POS.verb(h)) return "V";
	return null;
}


}