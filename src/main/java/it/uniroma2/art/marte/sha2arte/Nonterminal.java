package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class Nonterminal{

	private String _id;
	private String _cat;
	private String _head;
	private Vector<String> _edges;
	private String _surface;

	public Nonterminal(String id, String cat, String head, Vector<String> edges){
		_id=id;
		_cat=cat;
		_head=head;
		_edges=edges;
	}

	public String getId(){return _id;}
	public String getCat(){return _cat;}
	public String getHead(){return _head;}
	public String getSurface(){return _surface;}
	public Vector<String> getEdges(){return _edges;}
	
	public void setSurface(String surf){_surface=surf;}
	
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(_id);sb.append("\t");
		sb.append(_cat);sb.append("\t");
		sb.append(_head);sb.append("\t");
		for (String s : _edges)
		sb.append(s);sb.append("\t");
		sb.append("\n");
		return sb.toString();
	}

}