package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class FrameElement{

	private String _id;
	private String _name;
	//private Vector<String> _idref;
	private String _idref;
	private String _idframe;
	private String _nameframe;
	private String _surface;
	
	//public FrameElement(String id, String name, Vector<String> idref, String idframe, String nameframe){
		public FrameElement(String id, String name, String idref, String idframe, String nameframe){
		_id=id;
		_name=name;
		_idref=idref;
		_idframe=idframe;
		_nameframe=nameframe;
		_surface="";
	}

	
	public String getId(){return _id;}
	public String getName(){return _name;}
	//public Vector<String> getIdref(){return _idref;}
	public String getIdref(){return _idref;}
	public String getIdframe(){return _idframe;}
	public String getNameframe(){return _nameframe;}
	public String getSurface(){return _surface;}
	
	public void setSurface(String surf){_surface=surf;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(_id);sb.append("\t");
		sb.append(_name);sb.append("\t");
		sb.append(_idref.toString());sb.append("\t");
		return sb.toString();
	}

}
