package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class Sentence{

	private String _id;			// "3_t_1"
	private int _num;				// "1"
	private int _pairId;		// "3"
	private String _th; 			// either "t" or "h"
	private String _surface;
	private Vector<Terminal> _terminals;
	private Vector<Nonterminal> _nonterminals;
	private	Vector<FNframe>  _FNframes;
	Hashtable<String,String> htEdges;

	public Sentence(String id, int num, String surface){
		_id=id;
		_num=num;
		int uno = id.indexOf('_');
		int due = id.indexOf('_',uno+1);
		_pairId = Integer.parseInt(id.substring(0,uno));
		_th = id.substring(uno+1,due);
		_surface=surface;
	}

	
	public Sentence(String id, int num, Vector<Terminal> terminals, Vector<Nonterminal> nonterminals, Vector<FNframe> FNframes){
		_id=id;
		_num=num;
		_surface=null;
		_terminals=terminals;
		_nonterminals=nonterminals;
		_FNframes=FNframes;
		int uno = id.indexOf('_');
		int due = id.indexOf('_',uno+1);
		_pairId = Integer.parseInt(id.substring(0,uno));
		_th = id.substring(uno+1,due);
		try{buildSurfaces();} catch (Exception e) {e.printStackTrace();System.exit(-1);}
		/*for (int j=0; j<_terminals.size(); j++)
			System.out.println("TERM:" + _terminals.elementAt(j).getWord());
		for (int j=0; j<_nonterminals.size(); j++)
			System.out.println("NONTERM:" + _nonterminals.elementAt(j).getSurface());
		System.out.println("\n\n");
		for (int i=0; i<_FNframes.size(); i++)												
			{
			FNframe f = _FNframes.elementAt(i);
			Vector<FrameElement> fes = f.getFems();
			for (int j=0; j<fes.size(); j++)
				{
				FrameElement fe = fes.elementAt(j);
				System.out.println(f.getName() + " :: " + fe.getName() + " :: " +  fe.getSurface());
				}
			}	*/
	}
	
	
	private void buildSurfaces() throws Exception{
		Hashtable<String,FrameElement>  fesLookUp = new Hashtable<String,FrameElement>();  //build lookup for all frames elements and targets  in the frames
		Hashtable<String,Target>  targetLookUp = new Hashtable<String,Target>();
		for (int i=0; i<_FNframes.size(); i++)												
			{
			FNframe f = _FNframes.elementAt(i);
			Vector<FrameElement> fes = f.getFems();
			for (int j=0; j<fes.size(); j++)
				{
				FrameElement fe = fes.elementAt(j);
				if (fe.getIdref()!=null)
					fesLookUp.put(fe.getIdref(),fe);
				}
			Target tg = f.getTarget();
			targetLookUp.put(tg.getIdref(),tg);	
			}
		int ntsSize=_nonterminals.size(), tsSize=_terminals.size(), ctr=0;
		htEdges = new Hashtable<String,String>();
		Hashtable<String,Nonterminal> nonTerminalsLookUp = new Hashtable<String,Nonterminal>();
		for (int i=0;i<tsSize;i++)
			{
			Terminal t = _terminals.elementAt(i);
			htEdges.put(t.getId(),t.getWord()+" ");
			}
		for (int i=0;i<ntsSize;i++)
			{
			Nonterminal nt = _nonterminals.elementAt(i);	
			nonTerminalsLookUp.put(nt.getId(),nt);
			}
		for (int i=0;i<ntsSize;i++)
			{
			Nonterminal nt = _nonterminals.elementAt(i);
			if (!htEdges.containsKey(nt.getId()))
				{
				String ntSurface = buildNonterminal(nt,nonTerminalsLookUp,fesLookUp,targetLookUp);
				htEdges.put(nt.getId(),ntSurface);
				nt.setSurface(ntSurface);
				_nonterminals.setElementAt(nt,i);
				}
			}
		for (int i=0; i<_FNframes.size(); i++)												
			{
			FNframe f = _FNframes.elementAt(i);
			Vector<FrameElement> fes = f.getFems();
			for (int j=0; j<fes.size(); j++)
				{
				FrameElement fe = fes.elementAt(j);
				String idRef = fe.getIdref();
				if (idRef!=null && htEdges.containsKey(idRef))
					fe.setSurface(htEdges.get(idRef));
				fes.setElementAt(fe,j);
				}
			f.setFes(fes);
			_FNframes.setElementAt(f,i);
			}
	}
	
	
	private String buildNonterminal(Nonterminal nt, Hashtable<String,Nonterminal> nonTerminalsLookUp,Hashtable<String,FrameElement> fesLookUp, Hashtable<String,Target> targetLookUp) throws Exception{
		Vector<String> edges = nt.getEdges();
		String ret="";
		for (int i=0;i<edges.size();i++)
			{
			String edgeId = edges.elementAt(i);
			String edgeSurface;
			if (!htEdges.containsKey(edgeId))
				edgeSurface = buildNonterminal(nonTerminalsLookUp.get(edgeId),nonTerminalsLookUp, fesLookUp, targetLookUp);
			else
				edgeSurface = (String)htEdges.get(edgeId);
			ret+=edgeSurface;
			}
		return ret;
	}
	

	public String getId(){return _id;}
	public int getNum(){return _num;}
	public int getPairId(){return _pairId;}
	public String getTh(){return _th;}
	public String getSurface(){return _surface;}
	public Vector<Terminal> getTerminals(){return _terminals;}
	public Vector<Nonterminal> getNonterminals(){return _nonterminals;}
	public Vector<FNframe> getFrames(){return _FNframes;}

	public void addTerminals(Vector<Terminal> terms){
		_terminals.addAll(terms);
	}
	
	
	public void addNonterminals(Vector<Nonterminal> terms){
		_nonterminals.addAll(terms);
	}
	
	
	public void addFrames(Vector<FNframe> terms){
		_FNframes.addAll(terms);
	}

	public String toString(){
		StringBuffer sb = new StringBuffer();
		int size = _terminals.size();
		for (int j=0;j<size;j++)
				sb.append((_terminals.elementAt(j)).getWord() + " ");
		return sb.toString();		
	}

}
