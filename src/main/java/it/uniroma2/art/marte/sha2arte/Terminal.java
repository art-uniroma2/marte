package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class Terminal{

	private String _id;
	private String _lemma;
	private String _pos;
	private String _word;

	public Terminal(String id, String lemma, String pos, String word){
		_id=id;
		_lemma=lemma;
		if (pos.length()>0)
			_pos=pos;
			else
				{
				_pos="NOPOS";
				if (word.equals("("))
					_pos="-LRB-";
				else if (word.equals(")"))
					_pos="-RRB-";
				}
		if (_pos.equals("-LRB-"))
			_word="-LRB-";
		else 	if (_pos.equals("-RRB-"))
			_word="-RRB-";
		else
			_word=word;
		if (_word.indexOf("(")!=-1)
			_word=_word.replace("(","");
		if (_word.indexOf(")")!=-1)
			_word=_word.replace(")","");
				
	}

	
	

	public String getId(){return _id;}
	public String getPos(){return _pos;}
	public String getLemma(){return _lemma;}
	public String getWord(){return _word;}
	
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(_id);sb.append("\t");
		sb.append(_lemma);sb.append("\t");
		sb.append(_pos);sb.append("\t");
		sb.append(_word);sb.append("\n");
		return sb.toString();
	}

}
