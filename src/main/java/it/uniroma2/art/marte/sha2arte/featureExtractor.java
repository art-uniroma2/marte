package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;




public class featureExtractor{

	private Document dom;
	private String _shaFile = null;
	private BufferedWriter _outFile = null;
	private String _outFileName = null;
	private boolean _detourFlag= false;
	private Vector<Sentence> _sentences=null;
	
	public featureExtractor(){}
	
	public featureExtractor(String shaFile, boolean detour){
		_detourFlag = detour;
		_shaFile = shaFile;
	}
	
	//Returns an hashtable which has as Integer the ID of the pair, and as String 
	//their features, in a String where each feature is separated by a TAB
	public Hashtable<Integer,String> apply() throws Exception{
		parseXmlSha();
		_sentences = parseDocument();
		_sentences = rebuildDataset(_sentences);
		return(extractFeatures(_sentences));
		
	}
	
	private void run() throws Exception{
		_outFile = new BufferedWriter(new FileWriter(_outFileName));
		parseXmlSha();
		_sentences = parseDocument();
		_sentences = rebuildDataset(_sentences);
		extractFeatures(_sentences);
		
		
		//writeFile(sents,_pairs);
		//try {_outFile.close();} catch (Exception e) {e.printStackTrace();System.exit(-1);}
		}
	
	
	






/*******************************************************************************/
/************* methods for evaluating Levenstein Similairty ********************/
/*******************************************************************************/



//Compute similarity among two consituents in form of string (raw text, no bracketing)

private float computeSimilarity(String str1, String str2) throws Exception{
			int levDist = computeLevenshteinDistance(str1.toCharArray(), str2.toCharArray());
			float similarity = 1- ((float)levDist / (float) (Math.max(str1.length(),str2.length())));
			return similarity;
}
	

	private int minimum(int a, int b, int c) 
    {
        if (a<=b && a<=c)
            return a;
        if (b<=a && b<=c)
            return b;
        return c;
    }


    private int computeLevenshteinDistance(char[] str1, char[] str2) {
        int[][] distance = new int[str1.length+1][];

        for(int i=0; i<=str1.length; i++)
        {
            distance[i] = new int[str2.length+1];
            distance[i][0] = i;
        }
        for(int j=0; j<str2.length+1; j++)
            distance[0][j]=j;

        for(int i=1; i<=str1.length; i++)
            for(int j=1;j<=str2.length; j++)
                distance[i][j]= minimum(distance[i-1][j]+1,
                    distance[i][j-1]+1, distance[i-1][j-1] + 
                    ((str1[i-1]==str2[j-1])?0:1));
       
        return distance[str1.length][str2.length];
    }
		
/*******************************************************************************/
/*******************************************************************************/


	
	//Givent the sentences extracted from the Sha file, it rebuilds the correct
	//dataset, by putting back together different pieces of H and T.
	
	private Vector<Sentence> rebuildDataset(Vector<Sentence> sents) throws Exception{
		int i=0;
		Vector<Sentence> sentsOut = new Vector<Sentence>();
		while (i<sents.size())
			{
			Sentence sent =	sents.elementAt(i);
			i++;
			Sentence newSent = null;
			int num = sent.getNum(); 
			Sentence sentOut = sent;
			while (i<sents.size() && (newSent=sents.elementAt(i)).getNum() == (num+1))
				{
				num++;i++;	
				sent.addTerminals(newSent.getTerminals());
				sent.addNonterminals(newSent.getNonterminals());
				sent.addFrames(newSent.getFrames());
				}
			sentsOut.add(sent);	
			}
		return sentsOut;
	}
	
	
	
	private Hashtable<Integer,String>  extractFeatures(Vector<Sentence> sents) throws Exception {
		Hashtable<Integer,String> ht = new Hashtable<Integer,String>();
		for (int i=0;i<sents.size();i++)
			{
			Sentence sent = sents.elementAt(i);
			//System.out.print("** SENT :" + sent.getId()+": " + sent.toString()+ "\n");
			Vector<FNframe> frames_t = sent.getFrames();
			int num_frames_t = frames_t.size();																							// feature 1 : num. frames t
			Vector<FrameElement> fes_t = extractFE(frames_t);
			int num_fe_t = fes_t.size();																										// feature 2 : num. frame elements t
			i++;
			sent = sents.elementAt(i);
			Vector<FNframe> frames_h = sent.getFrames();
			int num_frames_h = frames_h.size();																							// feature 3 : num. frames h 
			Vector<FrameElement> fes_h = extractFE(frames_h);
			int num_fe_h = fes_h.size();																										// feature 4 : num. frame elements t
			int matched_frame=0;																													
			for (int j=0;j<num_frames_h;j++)
				{
				FNframe frame_h = frames_h.elementAt(j);
				String name_frame_h=frame_h.getName();
				boolean trovato=false; int m=0;
				while (!trovato && m<num_frames_t)
					{
					if (frames_t.elementAt(m).getName().equals(name_frame_h))
						{
						trovato=true;
						matched_frame++;
						}
					m++;
					}
				}
			float perc_matched_frames = 0;
			if (num_frames_h!=0)
				perc_matched_frames = (float) matched_frame / (float) num_frames_h; 		// feature 5 : percentage of common frames
			float average_matched_fes = computeFeSimilarity(fes_t,fes_h);
			String features =  num_frames_t + "\t"+ num_frames_h + "\t"+ perc_matched_frames+"\t"+ average_matched_fes;
			System.out.println(sent.getPairId() + ":\t" + features);	
			ht.put(new Integer(sent.getPairId()),features);
			}	
	return ht;		
	}
	

	
	private float computeFeSimilarity(Vector<FrameElement> fes_t, Vector<FrameElement> fes_h) throws Exception{
		int feshs = fes_h.size();
		float totSim = 0;
		for (int i=0;i<fes_h.size();i++)
			{
			FrameElement fs_h = fes_h.elementAt(i);
			String fs_h_name = fs_h.getName(); 
			String fs_h_nameF = fs_h.getNameframe(); 
			String fs_h_surf = fs_h.getSurface(); 	
			int fests=fes_t.size();
			for (int j=0;j<fests;j++)
				{
				float maxSim=0;
				FrameElement fs_t = fes_t.elementAt(j);
				String fs_t_name = fs_t.getName(); 
				String fs_t_nameF = fs_t.getNameframe(); 
				String fs_t_surf = fs_t.getSurface(); 	
				if (fs_t_nameF.equals(fs_h_nameF) && fs_t_name.equals(fs_h_name))
					{
					float sim = computeSimilarity(fs_h_surf,fs_t_surf);
					if (sim > maxSim)
						maxSim=sim;
					//System.out.println(sim + "\t" + fs_h_name + "\t" +fs_h_nameF + "\t" +fs_h_surf + "\t" +fs_t_name + "\t" +fs_t_nameF+ "\t" +fs_t_surf);
					//System.out.println(sim);
					}
				totSim += maxSim; 
				}
			}			
		float avgSim = 0;
		if 	(feshs!=0)
			avgSim = (float) totSim / (float) feshs;
		//System.out.println("T:" + totSim + "\tA:" + avgSim + "\n");
		return avgSim;
		}



	// Given a vector of FNframes, return the vector of all their Frame Elements
	private Vector<FrameElement> extractFE(Vector<FNframe> frames){
		Vector<FrameElement> fes=new Vector<FrameElement>();
		for (int i=0;i<frames.size();i++)
				fes.addAll((frames.elementAt(i)).getFems());
		return fes;
	}



/*******************************************************************************/
/************* methods for reading sha xml file ********************************/
/*******************************************************************************/

	
	private void parseXmlSha() throws Exception{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.parse(_shaFile);
				}catch(ParserConfigurationException pce) {
				pce.printStackTrace();
			}catch(SAXException se) {
				se.printStackTrace();
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
}



	private Vector<Sentence> parseDocument() throws Exception {
		Vector<Sentence> sents = new Vector<Sentence>();
		Element docEle = dom.getDocumentElement();							//root node
		NodeList nl = docEle.getElementsByTagName("s");
		if(nl != null && nl.getLength() > 0)										// s nodes
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element el = (Element)nl.item(i);
				sents.add(parseSentence(el));
				}
		}
	return sents;
	}



	private Sentence parseSentence(Element sEl) throws Exception{
		String idSent = sEl.getAttribute("id");
		NodeList nl = sEl.getElementsByTagName("graph");
		Element graphEl = (Element)nl.item(0);										//graph node
		String root=graphEl.getAttribute("root");
		Vector<Terminal> terminals = parseTerminals(graphEl);
		Vector<Nonterminal> nonterminals = parseNonterminals(graphEl);
		Vector<FNframe>  FNframes = parseFNframes(sEl);
		int numSent = Integer.parseInt(idSent.substring(idSent.length()-1,idSent.length()));
		return new Sentence(idSent,numSent,terminals,nonterminals,FNframes);
	}	
		
		
	
		
		
	private Vector<Terminal> parseTerminals(Element graphEl) throws Exception{
		Vector<Terminal> ret = new Vector<Terminal>();
		NodeList nl = graphEl.getElementsByTagName("terminals");
		Element terminalsEl = (Element)nl.item(0);								//terminals node
		nl = terminalsEl.getElementsByTagName("t");
		if(nl != null && nl.getLength() > 0)										
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element el = (Element)nl.item(i);
				ret.add(parseTerminal(el));
				}
			}
		return 	ret;
		}	
		
		
		
	private Vector<Nonterminal> parseNonterminals(Element graphEl) throws Exception{
		Vector<Nonterminal> ret = new Vector<Nonterminal>();
		NodeList nl = graphEl.getElementsByTagName("nonterminals");
		Element nonterminalsEl = (Element)nl.item(0);							//nonterminals node
		nl = nonterminalsEl.getElementsByTagName("nt");
		if(nl != null && nl.getLength() > 0)										// s nodes
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element el = (Element)nl.item(i);
				ret.add(parseNonterminal(el));
				}
		}
	return ret;
	}


		
	private Vector<FNframe> parseFNframes(Element graphEl) throws Exception{
		Vector<FNframe> ret = new Vector<FNframe>();
		NodeList nl1 = graphEl.getElementsByTagName("sem");
		Element semEl = (Element)nl1.item(0);							//sem node
		NodeList nl = graphEl.getElementsByTagName("frames");
		Element framesEl = (Element)nl.item(0);							//frames node
		nl = framesEl.getElementsByTagName("frame");
		if(nl != null && nl.getLength() > 0)										// s nodes
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element el = (Element)nl.item(i);
				ret.add(parseFNframe(el));
				}
		}
	return ret;
	}
	
	
	
	
	private Terminal parseTerminal(Element tEl) throws Exception{
		String id=tEl.getAttribute("id");
		String lemma=tEl.getAttribute("lemma");
		String pos=tEl.getAttribute("pos");
		String word=tEl.getAttribute("word");
		Terminal term = new Terminal(id,lemma,pos,word);
		return term;
		}



	private Nonterminal parseNonterminal(Element ntEl) throws Exception{
		String id=ntEl.getAttribute("id");
		String cat=ntEl.getAttribute("cat");
		String head=ntEl.getAttribute("head");
		Vector<String> edges = new Vector<String>();
		NodeList nl = ntEl.getElementsByTagName("edge");
		if(nl != null && nl.getLength() > 0)
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element edge = (Element)nl.item(i);
				edges.add(edge.getAttribute("idref"));
				}
			}
		Nonterminal nterm = new Nonterminal(id,cat,head,edges);
		return nterm;
	}


	
	private FNframe parseFNframe(Element fnEl) throws Exception{
		String id=fnEl.getAttribute("id");
		String name=fnEl.getAttribute("name");
		Vector<FrameElement> fems = new Vector<FrameElement>();
		NodeList nl = fnEl.getElementsByTagName("target");
		Target target = parseTarget((Element)nl.item(0),id,name);	
		nl = fnEl.getElementsByTagName("fe");
		if(nl != null && nl.getLength() > 0)
			{
			for(int i=0; i<nl.getLength();i++)
				fems.add(parseFrameElement((Element)nl.item(i),id,name));
			}
		String detour=null;																				// detour 
		nl = fnEl.getElementsByTagName("flag");
		int j=0; boolean found=false;
		while(nl != null && j < nl.getLength() && !found)
			{
			String nm = ((Element)nl.item(j)).getAttribute("name");
			if (nm.indexOf("Detour")!=-1)
				{found=true;detour=nm;}
			j++;
			}
		FNframe fm = new FNframe(id,name,target,fems,detour);
		return fm;
		}
	
	
	
	private FrameElement parseFrameElement(Element feEl, String idframe, String framename) throws Exception{
		String id=feEl.getAttribute("id");
		String name=feEl.getAttribute("name");
		NodeList nl = feEl.getElementsByTagName("fenode");
		String ref=null;
		if(nl != null && nl.getLength() > 0)
			ref = ((Element)nl.item(0)).getAttribute("idref");
		/*
		Vector<String> ref=null;
		if(nl != null && nl.getLength() > 0)
			{
			for(int i=0; i<nl.getLength();i++)
				{
				Element edge = (Element)nl.item(i);
				ref.add(edge.getAttribute("idref"));
				}
			}
		FrameElement fem = new FrameElement(id,name,ref,idframe,framename);
		*/	
		FrameElement fem = new FrameElement(id,name,ref,idframe,framename);
		return fem;
		}



	private Target parseTarget(Element tarEl, String idframe, String framename) throws Exception{
		String id=tarEl.getAttribute("id");
		String lemma=tarEl.getAttribute("lemma");
		String pos=tarEl.getAttribute("pos");
		NodeList nl = tarEl.getElementsByTagName("fenode");
		String ref = ((Element)nl.item(0)).getAttribute("idref");
		Target targ = new Target(id,lemma,pos,ref,idframe,framename);
		return targ;
		}

/*******************************************************************************/
/*******************************************************************************/




 /**
  * Parse input parameters. 
  */
  private void parseParameters(String[] argv) throws Exception {
	 for (int i=0; i < argv.length ; i++) 
		{
		if (argv[i].equals("-shafile")) {i++; _shaFile = argv[i];}
		else if (argv[i].equals("-of")) {i++; _outFileName = argv[i];}
		else if (argv[i].equals("-d")) {_detourFlag=true;}
		//else if (argv[i].equals("-shafile")) {i++; _shaFile = argv[i];}
		else if (argv[i].equals("-?") || argv[i].equals("-h") || argv[i].equals("-help")) {usage();}
		else throw new Exception("Unknown option : " + argv[i]);
		}
	if (_shaFile==null || _outFileName==null) {usage();}		
	}



	private static void usage() {
		System.out.println("\n");
		System.out.println("Usage: featureExtractor -shafile <sha file> -of <output file>");
		System.out.println("                   [-d]");
		System.out.println(" where <d> activates also Detour frame and role annotation.");
		System.out.println("\n");
		System.exit(0);
		}




public static void main(String[] argv) throws Exception {
	if (argv.length == 0) usage();
	else {
		featureExtractor fe = new featureExtractor ();
		fe.parseParameters(argv);
		fe.run();	
	}                
		
}//end main




}
