package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class Target{

	private String _id;
	private String _lemma;
	private String _pos;
	private String _idref;
	private String _idframe;
	private String _nameframe;
	
	public Target(String id, String lemma, String pos, String idref, String idframe,String nameframe){
		_id=id;
		_lemma=lemma;
		_pos=pos;
		_idref=idref;
		_idframe=idframe;
		_nameframe=nameframe;
	}

	
	public String getId(){return _id;}
	public String getLemma(){return _lemma;}
	public String getPos(){return _pos;}
	public String getIdref(){return _idref;}
	public String getIdframe(){return _idframe;}
	public String getNameframe(){return _nameframe;}
	
	
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(_id);sb.append("\t");
		sb.append(_lemma);sb.append("\t");
		sb.append(_pos);sb.append("\t");
		sb.append(_idref);sb.append("\t");
		return sb.toString();
	}

}
