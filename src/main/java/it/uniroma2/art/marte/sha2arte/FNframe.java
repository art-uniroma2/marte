package it.uniroma2.art.marte.sha2arte;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class FNframe{

	private String _id;
	private String _name;
	private Target _target;
	private Vector<FrameElement> _fems;
	private String _detour;
	
	
	public FNframe(String id, String name, Target tg, Vector<FrameElement> fes, String detour){
		_id=id;
		_name=name;
		_target=tg;
		_fems=fes;
		if (_detour!=null)
				_detour=detour;
			else	
				_detour="none";
	}

	
	public String getId(){return _id;}
	public String getName(){return _name;}
	public Target getTarget(){return _target;}
	public Vector<FrameElement> getFems(){return _fems;}
	public String getDetour(){return _detour;}
	
	public void setFes(Vector<FrameElement> fes){_fems=fes;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer();
		sb.append(_id);sb.append("\t");
		sb.append(_name);sb.append("\nTARGET: ");
		sb.append(_target.toString());sb.append("\t");
		sb.append(_name);sb.append("\nDETOUR: ");
		sb.append(_detour);sb.append("\n\n");
		return sb.toString();
	}

}
