// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! -- > cercare nel file per togliere il disastro

package it.uniroma2.art.marte;

import it.uniroma2.art.marte.SVMwrappers.*;
import it.uniroma2.art.marte.dictionaries.*;
import it.uniroma2.art.marte.postprocessors.*;
import it.uniroma2.art.marte.processors.*;
import it.uniroma2.art.marte.processors.featureDetectors.*;
import it.uniroma2.art.marte.processors.lexSimilarity.*;
import it.uniroma2.art.marte.processors.treeRewriters.*;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.ChunkHighlighter;
import it.uniroma2.art.marte.temp.PropsFile;
import it.uniroma2.art.marte.utils.ArteConfigurationHandler;
import it.uniroma2.art.marte.utils.BitOperations;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


//import RTV.utils.SystemExt;
//import it.uniroma2.chaos.*;
//import it.uniroma2.chaos.XDG.*;
//import it.uniroma2.chaos.textstructure.*;

import org.apache.commons.cli.*;


@Deprecated
public class Arte {


	private ExplicitFirstOrderRuleFeatureSpace efs = null;
	
	
	
   BufferedWriter __bw; //LEGOLAS 10-03-2007
	private final static int EMPTY = 0;
	private final static int MERGEANDASSIGN = 1;
	private final static int ASSIGN = 2;
	private final static int MARK = 3;
	private final static int PLAIN = 4;
	private final static int PLACEHOLDERS = 5;
	private final static int PROPAGATED_PLACEHOLDERS = 6;

	private static String inFile = null;
	
	private static File dirOut = null;
	private static File input_file = null;

	private static File output_file = null;
	private static BufferedWriter output = null;
	
	// TEMPOR
	// TEMPOR private static File output_file_2 = null;
	// TEMPOR private static BufferedWriter output_2 = null;


	private static BufferedWriter list = null;
	private static BufferedWriter outputTex = null;
	private static boolean force_file_creation = false;

	

	private static boolean BOW = false;
	private static boolean new_version = false;
	private static boolean new_formulation = false;
	
	private static Vector<LexProcessor> listLexProc = null;
	private static Vector<TreeRewriter> listTreeRewriters = null;
	private static Vector<FeatureDetector> listFeatureDetectors = null;
	private static Vector <String> listOfTasks = null;

	private static boolean idfParam = false;	
	private static int maxvarsParam = -1;	
	private static boolean filter = true;

	private static boolean coref = false;
	private static boolean prune_tree = true;
	private static boolean flattener = false;

	private static boolean chunking = true;


	private static boolean vercommParam = false;		
	private static int varmarkingParam = -1;	
	private static String dir = "n";
	private static boolean createSVMBatch = false;
	private static boolean learningRun = false;
	private static String SVMmodelFile = null;
	private static String SVMparamsString = "";
	
	
	
	private boolean typedAnchors = false;
	private boolean propagateTypedAnchors = false;
	
 // Processes
	private RelationDetectorChain lexical_relation_detector = null;

	private LexicalAnchorer lex_anc = null;
	
	private TreeRewriterCascade initial_tree_rewriters = null;

	private TreeRewriterCascade second_tree_rewriters = null;

	private TreeRewriterCascade temp_tree_rewriters = null;

	private OldFeatureDetectorChain feature_detector_chain = null;
	
	
	private VariableHandler variable_handler = null;


	private BOW bow = null;	

	private WordExtractor we = new WordExtractor();
	private BufferedWriter word_list_file = null;


 /**
  * Enumeration used to represent all the possible lexical processors that can be 
  * applied by an Arte object.
  */ 	
 public enum LexProcessor{
	//these are available ARTE lexical processors for anchoring phase.
	FN("FrameNet-derived similarity", "describe"),
	SURF("surface processor", "describe"),
	PN("prop noun similarity detector", "describe"),
	PNLV("prop noun levenshtein processor", "describe"),
	DRMO("derivational morpho processor", "describe"),
	VBEN("verb entailment processor", "describe"),
	WNET("wordNet processor", "describe"),
	PATT("web pattern processor", "describe"),
	DATE("date relation detector", "describe"),
	ANT("antinomy detector", "describe"),
	POF("part of relation detector", "describe"),
	AUX("auxiliary verb relation detector", "describe"),
	WNFULL("forces all the wordnet similarity detectors in a different chain", "describe"),
	NU("number relation detector", "describe");
	
	private String _label, _description;

	LexProcessor(String label, String description) {
		_label = label;
		_description = description;
	}
	
	public String getLabel() {
		return _label;
	}
	
	public String getDescription() {
		return _description;
	}

	public static String listOfValues() {
		String result = "";
		for (LexProcessor p : LexProcessor.values()) {
			result += p.toString() + ",";
		}
		return result.substring(0, result.length() - 1);
	}
 }


 /**
  * Enumeration used to represent all the possible lexical processors that can be 
  * applied by an Arte object.
  */ 	
 public enum TreeRewriter{
	//these are available ARTE lexical processors for anchoring phase.
	RED("Heavy Reducer", "This processor substitues with ANC_# the complex constituents that H shares with T"),
	BWD("Bad Word Detector", "This processor detects bad words and other features"),
	EPT("no_processor", "an empty processor"),
	LEM("lemmatizer", "produces a lemmatized version of the tree"),
	LEMP("lemmatizer post", "produces a lemmatized version of the tree: it is applied in the second tree cascade"),
	VLEM("verb lemmatizer", "produces a lemmatized version of the verbs in the tree"),
	PHR("phrasal verb rewriter", "for each phrasal verb, generates a leaf containing the verb and the particle"),
	APP("apposition syntactic transformer rewriter", "transform trees applying apposition normalizations (uses the stanford NER)"),
	ANA("syntactic transformer rewriter", "transform trees applying normalizations as who and which clauses"),
	MOD("modal transformer rewriter", "indentifies modal expression and inserts new labels to tree according to modal class"),
	WD("word deleter", "deletes words in specific syntactic classes"),
	NG("named entity generalizer", "changes a named entity in its class"),
	DEP("dependency tree transformer", "transforms the constituency tree in a dependency tree");
	private String _label, _description;

	TreeRewriter(String label, String description) {
		_label = label;
		_description = description;
	}
	
	public String getLabel() {
		return _label;
	}
	
	public String getDescription() {
		return _description;
	}

	public static String listOfValues() {
		String result = "";
		for (TreeRewriter p : TreeRewriter.values()) {
			result += p.toString() + ",";
		}
		return result.substring(0, result.length() - 1);
	}
 }


 /**
  * Enumeration used to represent all the possible lexical processors that can be 
  * applied by an Arte object.
  */ 	
 public enum FeatureDetector {
	//these are available ARTE lexical processors for anchoring phase.
	WIKI("wiki reject feature detector", "boh"),
	SURF("surface feature detector", "boh"),
	APFD("apposition detector", "activates the feature if H is implied by an simple apposition expression in T"),
	SV("subject verb detector", "activates the feature if an SV sentence in H is implied by an equivalent sentence in T"),
	SVO("subject verb object detector", "activates the feature if an SVO sentence in H is implied by an equivalent sentence in T"),
	ANFD("anaphora detector", "activates the feature if H is implied by an simple anaphoric expression in T"),
	CTX("Contextual Similarity", "compute the similarity between the contexts of the text and the hypothesis"),
	FN("framenet detector", "activates the set of features derived from FrameNet as extracted from Shalmaneser output");
	private String _label, _description;

	FeatureDetector(String label, String description) {
		_label = label;
		_description = description;
	}
	
	public String getLabel() {
		return _label;
	}
	
	public String getDescription() {
		return _description;
	}

	public static String listOfValues() {
		String result = "";
		for (TreeRewriter p : TreeRewriter.values()) {
			result += p.toString() + ",";
		}
		return result.substring(0, result.length() - 1);
	}
 }



 /**
  * Static method that initializes an Arte onbject in input. Initialiation cosists in
  * initializing all the processors, lexical processor,dictionaries and parameters that 
  * will be used by the Arte object.
  *
  * @param a    the Arte object to initialize.
  */ 
	public static void arteInitializer(Arte a) {
		try {
			
			// Termporanea Danilo 
			// a.word_list_file = new BufferedWriter(new FileWriter("D:\\USERS\\FABIO\\LAVORO\\PROGETTI\\CVS\\ARTE\\data\\word_list.txt",true)); 
			// 
			
			
			if (new_formulation) BitOperations.setBase((long)16); //NEW KERNEL
			else BitOperations.setBase((long)8); //OLD KERNEL

			printParameters(a);
			
			System.out.println("--------- Initializing ---------");

			// RelationDetector	initialization
			float max_similarity = Float.parseFloat(System.getProperty("arte.anchor.maxsim"));
			
			boolean wn_full = a.listLexProc.contains(LexProcessor.WNFULL);
			Vector <ConstituentRelationDetector> wn_similarity_detectors = null;
			if (wn_full) wn_similarity_detectors = new Vector();
			
			Vector <ConstituentRelationDetectorBasic> similarity_detectors = new Vector();
			if (a.listLexProc.contains(LexProcessor.FN)) similarity_detectors.add(new FNSimilarity());
			if (a.listLexProc.contains(LexProcessor.AUX)) similarity_detectors.add(new ToBeRelation());
			if (a.listLexProc.contains(LexProcessor.DATE)) similarity_detectors.add(new DateRelationDetector());
			if (a.listLexProc.contains(LexProcessor.NU)) similarity_detectors.add(new NumberSimilarity());
			if (a.listLexProc.contains(LexProcessor.SURF))  similarity_detectors.add(new SurfaceSimilarity());
			if (a.listLexProc.contains(LexProcessor.PN)) similarity_detectors.add(new NamedEntitySimilarity(System.getProperty("arte.namedentitymatcher.rules")));
			if (a.listLexProc.contains(LexProcessor.PNLV))  similarity_detectors.add(new ProperNounLevSimilarity());
			if (a.listLexProc.contains(LexProcessor.DRMO))  similarity_detectors.add(new DerivationalMorphology(System.getProperty("arte.wn.deriv.file")));
			if (a.listLexProc.contains(LexProcessor.VBEN))  similarity_detectors.add(new VerbLexicalEntailment(System.getProperty("arte.wn.verbent.file")));
			
			if (a.listLexProc.contains(LexProcessor.ANT))  similarity_detectors.add(new SemanticRelationDetector("-",(float) 0.3,System.getProperty("arte.wn.antinomy.file")));

			//if (a.listLexProc.contains(LexProcessor.ANT) && !wn_full)  similarity_detectors.add(new SemanticRelationDetector("-",System.getProperty("arte.wn.antinomy.file")));
			if (a.listLexProc.contains(LexProcessor.POF) && !wn_full)  similarity_detectors.add(new SemanticRelationDetector("po",System.getProperty("arte.wn.part_of.file")));
			if (a.listLexProc.contains(LexProcessor.WNET) && !wn_full)  {WNSimilarity wn_similarity1 = new WNSimilarity(System.getProperty("arte.wn.sim.file"));
																											 max_similarity = wn_similarity1.max_similarity_value() ;
																											 similarity_detectors.add(wn_similarity1);}
			if (a.listLexProc.contains(LexProcessor.ANT) && wn_full)  wn_similarity_detectors.add(new SemanticRelationDetector("-",System.getProperty("arte.wn.antinomy.file")));
			if (a.listLexProc.contains(LexProcessor.POF) && wn_full)  wn_similarity_detectors.add(new SemanticRelationDetector("po",System.getProperty("arte.wn.part_of.file")));
			if (a.listLexProc.contains(LexProcessor.WNET) && wn_full)  {WNSimilarity wn_similarity1 = new WNSimilarity(System.getProperty("arte.wn.sim.file"));
																											 max_similarity = wn_similarity1.max_similarity_value() ;
																											 wn_similarity_detectors.add(wn_similarity1);}
			if (wn_full) similarity_detectors.add(new WNRelationDetectorChain(wn_similarity_detectors,max_similarity) );
																										 
			if (a.listLexProc.contains(LexProcessor.PATT))  {
				PatternBasedLexicalEntailmentDetector webCounts_l = new PatternBasedLexicalEntailmentDetector(System.getProperty("arte.webpatterns.file")); 
				webCounts_l.setPattern(System.getProperty("arte.lexsim.PATT.type"));
				similarity_detectors.add(webCounts_l);}
			
			a.lex_anc = new LexicalAnchorer(new RelationDetectorChain(similarity_detectors,max_similarity));


			// INITIAL TREE REWRITER CASCADE

			a.initial_tree_rewriters = new TreeRewriterCascade();
			a.initial_tree_rewriters.add(new InitialNormalizer());
			
			
			if (a.listTreeRewriters.contains(TreeRewriter.RED)) a.initial_tree_rewriters.add(new SuperMatcherAndReducer());  // ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
			
			a.initial_tree_rewriters.add(new NEExtractor());
			
			a.initial_tree_rewriters.add(new Lemmatizer(new LemmaDictionary(System.getProperty("arte.lemmata.file")),
			                             a.listTreeRewriters.contains(TreeRewriter.LEM)));

			
			if (a.listTreeRewriters.contains(TreeRewriter.BWD))  a.initial_tree_rewriters.add(new BadWordDetector("D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/data/kb/bad_word_dictionary/bad_terms.small"));
			if (a.listTreeRewriters.contains(TreeRewriter.MOD))  a.initial_tree_rewriters.add(new ModalSyntacticTransformer(System.getProperty("arte.modalsyntactictranformer.rules")));
//			if (a.listTreeRewriters.contains(TreeRewriter.APP))
//								{
//								DataSet ds = new DataSet(input_file.toString(),listOfTasks);
//								a.initial_tree_rewriters.add(new AppositionSyntacticTransformer(System.getProperty("arte.appositionsyntactictranformer.rules")));
//								}
			if (a.listTreeRewriters.contains(TreeRewriter.ANA))   a.initial_tree_rewriters.add(new SyntacticTransformer(System.getProperty("arte.anaphorasyntactictranformer.rules")));
			if (a.listTreeRewriters.contains(TreeRewriter.PHR))  a.initial_tree_rewriters.add(new SyntacticTransformer(System.getProperty("arte.phrasalverb.rules"),true));
			if (a.listTreeRewriters.contains(TreeRewriter.VLEM))  a.initial_tree_rewriters.add(new VerbLemmatizer(new LemmaDictionary(System.getProperty("arte.lemmata.file")),true));
			if (!a.listTreeRewriters.contains(TreeRewriter.BWD))
			a.initial_tree_rewriters.add(new Lemmatizer(new LemmaDictionary(System.getProperty("arte.lemmata.file")),
			                             a.listTreeRewriters.contains(TreeRewriter.LEM)));
			if (!a.listTreeRewriters.contains(TreeRewriter.BWD))
			a.initial_tree_rewriters.add(new SemanticHeadMarker(System.getProperty("arte.semanticheadmarker.rules")));
			
			// SECOND TREE REWRITER CASCADE
			a.second_tree_rewriters = new TreeRewriterCascade();
			if (a.vercommParam) a.second_tree_rewriters.add(new LeafGeneralizer(new GeneralizationDictionary(System.getProperty("arte.wn.verbcomm.file"))));
			if (!a.listTreeRewriters.contains(TreeRewriter.BWD) && a.listTreeRewriters.contains(TreeRewriter.DEP))
				a.second_tree_rewriters.add(new ConstituencyTreeToDepencencyTree());
			
			
			// TEMPO PROVA 12/2/2009
			// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
			// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
			//a.second_tree_rewriters.add(new TreeMatchingDetector());
			//SVMpair.addFeature("confidence");
			// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
			// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 

			if (prune_tree) {
				a.second_tree_rewriters.add(new TextTreePruner());
				a.second_tree_rewriters.add(new HypoTreePruner());
			}
			if (flattener) a.second_tree_rewriters.add(new Flattener());
			if (a.listTreeRewriters.contains(TreeRewriter.LEMP)) a.second_tree_rewriters.add(new LemmaWriter());
			if (a.listTreeRewriters.contains(TreeRewriter.WD))
				a.second_tree_rewriters.add(new WordDeleter("NNP,NNPS"));
			if (a.listTreeRewriters.contains(TreeRewriter.NG))
				a.second_tree_rewriters.add(new NamedEntityGeneralizer());
			


			//a.temp_tree_rewriters = new TreeRewriterCascade();

			//a.temp_tree_rewriters.add(new ContentWordDeleter()); //// PROVA DISPERATA

			// FEATURE DETECTOR:
			// This is the standard distance feature:
			SVMpair.addFeature("distance");

			// IF YOU WANT TO ADD A FEATURE YOU MUST DEFINE A NEW GENERICFEATURE DETECTOR AND ADD IT TO THE CHAIN
			SVMpair.addFeatureSet("ent_detectors");						
			
			a.feature_detector_chain = new OldFeatureDetectorChain();
			
			if (a.listFeatureDetectors.contains(FeatureDetector.ANFD)) 
				a.feature_detector_chain.add(new EntailmentRuleDetector("ent_detectors","anaph",System.getProperty("arte.feature_detector.anaphora")));

			
			if (a.listFeatureDetectors.contains(FeatureDetector.APFD)) 
				a.feature_detector_chain.add(new EntailmentRuleDetector("ent_detectors","appo",System.getProperty("arte.feature_detector.apposition")));

			if (a.listFeatureDetectors.contains(FeatureDetector.SV)) 
				a.feature_detector_chain.add(new EntailmentRuleDetector("ent_detectors","sv",System.getProperty("arte.feature_detector.sv")));

			if (a.listFeatureDetectors.contains(FeatureDetector.SVO)) 
				a.feature_detector_chain.add(new EntailmentRuleDetector("ent_detectors","svo",System.getProperty("arte.feature_detector.svo")));

			if (a.listFeatureDetectors.contains(FeatureDetector.FN)) 
				a.feature_detector_chain.add(new EntailmentRuleDetector("framenet_detectors","fnfeature",System.getProperty("arte.feature_detector.anaphora")));

			if (a.listFeatureDetectors.contains(FeatureDetector.WIKI)) 
				a.feature_detector_chain.add(new WikiFeatureDetector("D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/data/kb/bad_word_dictionary/bad_terms.small"));


			if (a.listFeatureDetectors.contains(FeatureDetector.SURF)) 
				a.feature_detector_chain.add(new SurfaceSimilarityFeatureDetector());

			if (a.listFeatureDetectors.contains(FeatureDetector.CTX)) {
				//System.err.println("ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZz");
				a.feature_detector_chain.add(new ContextualSimilarityDetector());
			}

			a.variable_handler = new VariableHandler();

			ChunkHighlighter.setCoreConstituentTypes(a.selectList(System.getProperty("arte.tree.generalcuttypes")));
			
			if (BOW) a.bow = new BOW();
			
			
			
			// a.efs = new ExplicitFirstOrderRuleFeatureSpace();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	System.out.println("all done!\n--------------------------------\n");
	}

		private static void printParameters(Arte a){
			System.out.println("\n************ SETTING ************");
			System.out.println("Input File              : " + inFile);
			System.out.println("Output directory        : " + dirOut.toString());
			System.out.println("Dataset Tasks           : " + listOfTasks);
			System.out.println("Lexical Processors      : " + a.listLexProc.toString());
			System.out.println("Tree Rewriters          : " + a.listTreeRewriters.toString());
			System.out.println("Parameter IDF           : " + idfParam);
			System.out.println("Parameter Verb Comm     : " + vercommParam);
			System.out.println("Parameter Max Variables : " + maxvarsParam);
			System.out.println("Action                  : " + varmarkingParam);
			System.out.println("***********************************\n");
		}
		
		

 /**
  * Lunches Arte on a RTE dataset.
  *
  * @param ds    dataset to be analyzed
  * @return      analyzed dataset with predicted entailment scores.
  */ 
private DataSet treepairs(DataSet ds) throws Exception
	{
	System.out.println("\n------------------------------------");
	System.out.println("--------- Analyzing " + ds.size() + " pairs ------");
	System.out.println("------------------------------------\n");
	// to do :
	// 1. fare treeapirs(Pair p) returns SVMpair
	// 2. scrittura su file esterno!
	int unprocessed = 0;
	int written_examples = 0;
	for (int i=0; i<ds.getAllPairs().size();i++)
		{
		 	System.out.println("\n******** PAIR " + i + " ********");
		 	Pair pair = ds.getPair(i);
		 	SVMpair instance = null;
		 	try {
		 		instance = elaborate(pair);
			} catch (Exception e) {
				System.err.println("\n");
				e.printStackTrace();
				System.err.println("ERROR on the pair: " + i + "\n");
				//System.err.println(pair.toStringComplete());
			}

			//double f = (new Double(instance.getFeatureValue("distance"))).doubleValue();
			//TEMPOR if (f > 0.8) {
				System.err.print("." + i);
				list.write( "" + instance.getId() + "\n");
				instance.setId(written_examples);
				output.write("" + instance + "\n");
				output.flush();
				written_examples++;
			//TEMPOR } else output_2.write("" + instance + "\n");

		}//end for 
 	System.err.println(".");
	System.out.println("all done!\n--------------------------------\n");	
	System.out.println(" ANALYSIS STATISTICS : ");
	System.out.println(" ------------------------------------------------------- " );
	System.out.println("    Not processed in the variable style: " + unprocessed);
	System.out.println(lex_anc.getLexicalRelationDetector().reportActivations());
	return ds;
	}



	/**
	applies all the steps to generate an instance in the feature space.
	It does the following activities
	1) transforms the syntactic trees with the allowed transformations
	2) finds the anchors and determines the placeholders
	3) adds the placeholders in the trees
	*/
	public SVMpair elaborate(Pair pair) throws Exception {
		
			SVMpair instance = new SVMpair(pair.getId(),(pair.getOrigValue()? 1 : -1));
			instance.set_class_string(pair.get_original_value_string());
	// PHASE 0) Reworking syntactic trees

			initial_tree_rewriters.transform(pair);
			/////TEMP instance.addParseTreeWithPlaceholders(pair);

			if (BOW) { 
				instance.setBOWs(bow.extract(pair.getTextXDG()).toPennTree(),bow.extract(pair.getHypoXDG()).toPennTree());
			}

	
	// PHASE 1) LEXICAL ANCHORING
	//          phase 1 is carried out by a LexicalAnchorer (lex_anc)
			
	
			System.out.println("Lexical Anchoring....");
			Vector <Vector <Anchor>> anchors = lex_anc.findAnchorsWithRepetitions(pair);
			System.out.println("Lexical Anchoring....done!");
			
			pair.setAnchors(anchors);
			
	// PHASE 1.1) DETERMINE LEXICAL DISTANCE
			double f = 0;

			if (idfParam) f = lex_anc.globalAnchorBasedSimilarityWithRepetitions(anchors);
			else f = lex_anc.globalAnchorBasedSimilarityWithRepetitionsNoIDF(anchors);

			instance.setFeatureValue("distance","" + f);

			
				// FEATURE SPACE WRITING: Plain Syntactic Trees 
			
			//temp_tree_rewriters.transform(pair);

			if (varmarkingParam > 0 ) instance.setParseTrees(pair);
	
			//// TEMPOR
			
			//BufferedWriter provetta = new BufferedWriter(new FileWriter("PAROLE",true));
			
			//we.extract(pair,word_list_file);
			
			//provetta.close();
			
			////


	// PHASE 2.0) Reduction and propagation of the variables in the Text and the Hypothesis
	//            Setting of the relevance for the tree leaves  


			//// WIKI ZONE
			
			feature_detector_chain.computeFeatures(pair,instance);
			//instance.setFeatureValue("distance","" + 1);

			//// WIKI ZONE

			if (varmarkingParam  == MERGEANDASSIGN || varmarkingParam  == PLACEHOLDERS || varmarkingParam  == PROPAGATED_PLACEHOLDERS )
				{
				Vector variables = null;
				Vector selected_anchors = null;
				if (chunking) {
					Vector variables_and_anchors = variable_handler.mergeConstituentsAndAssignVariablesWithRepetitions(pair,anchors);
					variables = (Vector) variables_and_anchors.elementAt(0);
					selected_anchors = (Vector) variables_and_anchors.elementAt(1);
				} else {
					variables = variable_handler.metodoStupidoCheTantoNonFunzionaAccorpaAncore(anchors);
					selected_anchors = new Vector();
					for (Vector <Anchor> v:anchors) selected_anchors.addAll(v);
				}
				       
				                   
				System.out.println("********* ANCHORS: *****************");
				for (int i=0; i<selected_anchors.size();i++)
					System.out.println(selected_anchors.elementAt(i).toString());
				
				//// Tempor: fabio 22/8/2007
				if (coref) variables = variable_handler.expandVariablesWithAllTheAnchors((Vector <Vector>) variables,anchors);
				//// Tempor: fabio 22/8/2007
				
				variable_handler.setRelevancyOnTheVariableLeaves(variables);
				if (varmarkingParam  == PROPAGATED_PLACEHOLDERS) 
					variables = variable_handler.propagateVariables(variables, pair);

				if (typedAnchors) { 
					variable_handler.putAnchorTypesInNodes(selected_anchors);
					if (propagateTypedAnchors) variable_handler.propagateTheAnchorTypeInTheTrees(pair,variables);
				}
				

				
				if (!new_version) {
				
					variable_handler.putVariableNames(variables);
						
					//outputTex.write("" + pair.getId() + ";" + pair.getTextXDG().toLatexTreeFormat() + ";" );
					
					//outputTex.write("\n\n\\textbf{" + (pair.getOrigValue()? "TRUE" : "FALSE") + "}\\\\");
					//outputTex.write("\nOriginal T:\n" + pair.getTextXDG().toLatexTreeFormat() + "\n\n" );
					
		// PHASE 2.1) Second reworking of the syntactic trees
							
							
					second_tree_rewriters.transform(pair);
					
					//outputTex.write(pair.getHypoXDG().toLatexTreeFormat() + ";" + pair.getTextXDG().toLatexTreeFormat() +"\n");
					//outputTex.write("\nT:\n" + pair.getTextXDG().toLatexTreeFormat() + "\n\n" + 
					//                  "H:\n" + pair.getHypoXDG().toLatexTreeFormat() + "\n\\newpage");
															
															
		// PHASE 3.0) Permutations for the kernel computation
					if (variables.size() < maxvarsParam) 
						{
						Vector <Vector <Object>> permutations = (Vector <Vector <Object>>) variable_handler.permutations(variables);
						System.out.println("# permutation: " + permutations.size());
						for (Vector <Object> perm : permutations) 
							{
							variable_handler.putVariableNames(perm);

							
							instance.addParseTreeWithPlaceholders(pair);
	
							}
						}
					} else {
						if (variables.size() < maxvarsParam || !filter) {

							variable_handler.putVariableNamesAndComputePfunction(variables, pair, maxvarsParam);

							//second_tree_rewriters.transform(pair); //MAJOR ERRROR!!!


					// PHASE 1.2) DETERMINE FEATURES AND ADD IT TO THE SVM INSTANCE 
							feature_detector_chain.computeFeatures(pair,instance);
				
							// This must be before instance.addParseTreeWithPlaceholders(pair)
							second_tree_rewriters.transform(pair);//LEGOLAS 10-03-2007
							
							
							// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
							// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
							//instance.setFeatureValue("confidence","" + pair.getConfidence());
							// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
							// ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
							
							instance.addParseTreeWithPlaceholders(pair);
				
							
							//DecisionPostProcessor dpp = new //DecisionPostProcessor("D:/UNIV/PROGETTI/ENTAILMENT/TE_RECOGNIZER_2006/ARTE/src/arte/postprocessors/PrologLayer/NegationDetector.pl"); //LEGOLAS 10-03-2007
							//dpp.run(pair,instance,__bw); //LEGOLAS 10-03-2007
							//AntonymPostProcessor app = new AntonymPostProcessor(); //LEGOLAS 10-03-2007
							//app.run(pair,instance,__bw); //LEGOLAS 10-03-2007
							

				
						}
						
					}
					
					//efs.computeFeature(pair,instance);
										
					/// SYNTACTIC TREES WITH VARIABLES FOR THE NEW KERNEL
					//  Trees for the distance between T and H
					//variable_handler.moveVariableNamesToType(pair);
					//  instance.setParseTrees(pair);
				}
					
		System.out.println("\n**************************\n");
		return instance;
	}


private boolean open_files(String file, boolean clean) throws Exception {
		boolean status = clean || !output_file.exists();
		// OPENING FILES
		if (status) {
			output = new BufferedWriter(new FileWriter(output_file));
			//TEMPOR output_2 = new BufferedWriter(new FileWriter(output_file_2)); 
			list = new BufferedWriter(new FileWriter(new File(dirOut,(new File(file)).getName()  + ".lst")));
			outputTex = new BufferedWriter(new FileWriter(new File(dirOut,(new File(file)).getName() + ".tex")));
		}
		return status;
}

private void close_files() throws Exception {
	output.close();
	//TEMPOR output_2.close(); 
	list.close();
	outputTex.close();
}

 /**
  * Creates all the directories and files for this experiment
 */
private void createDirTree(String inFile) throws Exception {
	
		File fIn = new File(inFile);

		dirOut = new File(dirOut,dir);
		dirOut.mkdirs();

		File modDir = new File(dirOut,"models");
		File resultDir = new File(dirOut,"results");
		File svmDir = new File(dirOut,"SVMin");
		modDir.mkdir(); resultDir.mkdir(); svmDir.mkdir();

		(new File(svmDir,"bins")).mkdir();
		(new File(svmDir,"classic")).mkdir();
		(new File(svmDir,"splits")).mkdir();

		output_file = new File(dirOut , fIn.getName() + ".svm");
		
		// TEMPOR output_file_2 = new File(dirOut , fIn.getName() + "_2" + ".svm");

		
		// SETTING CURRENT EXPERIMENT OUT VARIABLES IN A FILE
	
		Experimenter.setCurrentExperiment("arte.pre_plus_processing",dir);		
	
		//creating SVM batch/sh files
		
		if (createSVMBatch)
		{
		 if (learningRun)
			{
			File modFile = new File(modDir , fIn.getName() + ".mod");
			batchPreparer.prepareLearnerWin(output_file,modFile,SVMparamsString);
			batchPreparer.prepareLearnerUnix(output_file,modFile,SVMparamsString);
			}
		 else
			{
			File resultFile = new File(resultDir , fIn.getName() + ".res");
			File modFile = new File(modDir , SVMmodelFile);
			batchPreparer.prepareClassifierWin(output_file, modFile ,resultFile,SVMparamsString);
			batchPreparer.prepareClassifierUnix(output_file, modFile ,resultFile,SVMparamsString);
			}
	  }		
	}


//Main method. Reads parameters, starts XML/NL parsing, starts pairs
//classifications in different DataSet and calls specific hunters on their
//DataSet.
private void run(String inFile) throws Exception {	    
	 //opening out files	
		createDirTree(inFile);
		//__bw = new BufferedWriter(new FileWriter(new File("D:/UNIV/PROGETTI/ENTAILMENT/TE_RECOGNIZER_2006/ARTE/bin/scarted.txt")));//LEGOLAS 10-03-2007
	// reading dataset and processing
		if (open_files(inFile,force_file_creation)) {
		DataSet ds = new DataSet(input_file.toString(),listOfTasks);
		DataSet dsSimple = treepairs(ds);
		DataSet outDs = new DataSet();
		outDs.addDataSet(dsSimple);
		close_files();
		//__bw.close();//LEGOLAS 10-03-2007
		} else {
			System.err.println("EXPERIMENT FILES: \n   " + output_file + "\n   are alreary up to date");
		}
}//end Run

	


 /**
  * Returns a vector of the lexical processors to be used for anchoring,
  * from a comma separated string containing the processors names
  */
 private Vector<LexProcessor> selectLexProc(String listOfProcessors){
		Vector<LexProcessor> processors = new Vector<LexProcessor>();
		if (listOfProcessors != null) 
			{
			String[] list = listOfProcessors.split(",");
			for (String s : list) 
				{
				LexProcessor p = LexProcessor.valueOf(s);
				if (p != null)
					processors.add(p);
				}
			}
		return processors;
	}


 /**
  * Returns a vector of tree rewriters
  */
 private Vector<TreeRewriter> selectTreeRewriters(String listOfProcessors){
		Vector<TreeRewriter> processors = new Vector<TreeRewriter>();
		if (listOfProcessors != null) 
			{
			String[] list = listOfProcessors.split(",");
			for (String s : list) 
				{
				TreeRewriter p = TreeRewriter.valueOf(s);
				if (p != null)
					processors.add(p);
				}
			}
		return processors;
	}


 /**
  * Returns a vector of feature detectors 
  */
 private Vector<FeatureDetector> selectFeatureDetectors(String listOfProcessors){
		Vector<FeatureDetector> processors = new Vector<FeatureDetector>();
		if (listOfProcessors != null) 
			{
			String[] list = listOfProcessors.split(",");
			for (String s : list) 
				{
				FeatureDetector p = FeatureDetector.valueOf(s);
				if (p != null)
					processors.add(p);
				}
			}
		return processors;
	}

 /**
  * Returns a vector of the tasks/gencuttype to be included in the dataset,
  * from a comma separated string containing the processors names. If the
  * string is "ALL", all tasks will be included
  */
 private Vector<String> selectList(String listOfTasks){
		Vector<String> tasks = new Vector<String>();
		if (listOfTasks != null) 
			{
			String[] list = listOfTasks.split(",");
			for (String s : list) 
				{
				if (s.equals("ALL"))
					{
					tasks = new Vector<String>();
					tasks.add(s);
					break;
					}
				else
				 tasks.add(s);
				}
			}
		return tasks;
	}
		
		

 /**
  * Parse input parameters. If not defined by user,
  * reads them from configuration file. 
  */
  private void parseParameters(String[] argv) throws Exception {
	 String lexProc=null,tasks=null,treeRewriters=null;
	 boolean ctr = false;
	for (int i=0; i < argv.length ; i++) 
		{
		System.out.println("Argomento : " + argv[i]);
		if (argv[i].equals("-if"))       {i++; inFile = argv[i]; if (inFile.endsWith(".xml")) inFile = inFile.substring(0,inFile.length()-4);
													System.setProperty("arte.input.file", (new File(inFile)).getName() );}
		else if (argv[i].equals("-svmlearn")) {i++; learningRun = Boolean.parseBoolean(argv[i]);createSVMBatch=true;}
		else if (argv[i].equals("-svmmodel")) {i++; SVMmodelFile = argv[i];}
		else if (argv[i].equals("-svmparams")) {i++; SVMparamsString = argv[i];}
		else if (argv[i].equals("-od")) {i++; dirOut = new File(argv[i]);}
		else if (argv[i].equals("-idf")) {idfParam = true;}
		else if (argv[i].equals("-cv"))  {vercommParam = true;}
		else if (argv[i].equals("-p"))   {i++; lexProc = argv[i];}
		else if (argv[i].equals("-trw"))   {i++; treeRewriters = argv[i];}
		else if (argv[i].equals("-fd"))   {i++; listFeatureDetectors = selectFeatureDetectors(argv[i]);}
		else if (argv[i].equals("-t"))   {i++; tasks = argv[i];}
		else if (argv[i].equals("-tap"))   {typedAnchors = true; propagateTypedAnchors= true;}
		else if (argv[i].equals("-ta"))   {typedAnchors = true;}
		else if (argv[i].equals("-force"))   {force_file_creation = true;}
		else if (argv[i].equals("-new_version"))   {new_version = true;}
		else if (argv[i].equals("-new_forumlation"))   {new_formulation = true;}
		else if (argv[i].equals("-bow"))   {BOW = true;}
		else if (argv[i].equals("-prune_tree"))   {i++; prune_tree = argv[i].equals("yes");}
		else if (argv[i].equals("-filter"))   {i++; filter = argv[i].equals("yes");}
		else if (argv[i].equals("-chunking"))   {i++; chunking = argv[i].equals("yes");}
		else if (argv[i].equals("-maxvars"))   {i++; maxvarsParam = Integer.parseInt(argv[i]);}
		else if (argv[i].equals("-coref"))   {coref = true;}
		else if (argv[i].equals("-flat"))   {flattener = true;}
		else if (argv[i].equals("-action")) 
			{
			i++;
			if (argv[i].equals("merge"))       varmarkingParam = MERGEANDASSIGN;
			else if (argv[i].equals("plac")) varmarkingParam = PLACEHOLDERS;
			else if (argv[i].equals("propplac")) varmarkingParam = PROPAGATED_PLACEHOLDERS;
			else if (argv[i].equals("assign")) varmarkingParam = ASSIGN;
			else if (argv[i].equals("mark"))   varmarkingParam = MARK;
			else if (argv[i].equals("plain"))  varmarkingParam = PLAIN;
			else if (argv[i].equals("empty"))  varmarkingParam = EMPTY;
			else throw new Exception("Unknown action : " + argv[i]);
			}
		else if (argv[i].equals("-?") || argv[i].equals("-h") || argv[i].equals("-help")) {usage();}
		else throw new Exception("Unknown option : " + argv[i]);
		}
		if ((inFile==null)) usage();
		//if (!learningRun && SVMmodelFile==null) {System.out.println("ERROR: with '-learn' option set to 'false', a model file must be indicated!\n");usage();}
		if (dirOut==null) dirOut = new File(System.getProperty("arte.default.output.dir"));
		if ((listLexProc=selectLexProc(lexProc)).size()==0) 
			listLexProc=selectLexProc(System.getProperty("arte.lexsim.processors"));
		if ((listTreeRewriters=selectTreeRewriters(treeRewriters)).size()==0) 
			listTreeRewriters=selectTreeRewriters(System.getProperty("arte.tree.processors"));
		if (listFeatureDetectors==null) listFeatureDetectors = new Vector();	
		if (!idfParam) idfParam= Boolean.parseBoolean(System.getProperty("arte.anchor.idf"));
		if (maxvarsParam==-1) maxvarsParam = Integer.parseInt(System.getProperty("arte.tree.maxvars"));
		if (!vercommParam) vercommParam = Boolean.parseBoolean(System.getProperty("arte.tree.verbcomm"));
		if (varmarkingParam==-1) varmarkingParam = Integer.parseInt(System.getProperty("arte.tree.varmarkingtype"));
		if ((listOfTasks = selectList(tasks)).size()==0) listOfTasks=selectList(System.getProperty("arte.tasks"));

		// OUTPUT DIRECTORY NAME SETTING
		dir = Experimenter.getCurrentExperiment("arte.preprocessing");
		
		
		if ((new File(inFile + ".xml")).exists())  { input_file = new File(inFile + ".xml"); dir = "unk_";}
		else input_file = new File(new File(new File(System.getProperty("preprocessing.default.output.dir")),dir),inFile + ".xml");
		
		if (idfParam) {dir+="_idf";}
		if (typedAnchors) {dir+="_ta";}
		if (propagateTypedAnchors) {dir+="p";}
		if (vercommParam) {dir+="_cv";}
		if (BOW) {dir+="_bow";}
		if (listLexProc != null) {
			dir += "_" + listLexProc.toString().replace(" ","").replace("[","").replace("]","").replace(",","_");
		}
		if (listTreeRewriters != null) {
			dir += "_" + listTreeRewriters.toString().replace(" ","").replace("[","").replace("]","").replace(",","_");
		}
		if (listFeatureDetectors != null) {
			dir += "_" + listFeatureDetectors.toString().replace(" ","").replace("[","").replace("]","").replace(",","_");
		}
		dir = dir + "_varMark" + varmarkingParam;
  }
  
  
 /** 
  * Explains input parameters
  */
	private static void usage()
		{
		System.out.println("\n");
		System.out.println("Usage: Arte -if <input file>");
		System.out.println("                   [-svmlearn <true|false>]");
		System.out.println("                   [-svmmmodel <svm model file (exclude path)>]");
		System.out.println("                   [-svmparams <svm parameters string>]");
		System.out.println("                   [-od <output directory>]");
		System.out.println("                   [-new_formulation]");
		System.out.println("                   [-idf]");
		System.out.println("                   [-cv]");
		System.out.println("                   [-nl]");
		System.out.println("                   [-p <list of lex processors>]");
		System.out.println("                   [-trw <list of tree rewriters>]");
		System.out.println("                   [-fd <list of feature detectors>]");
		System.out.println("                   [-t <list of tasks>]");
		System.out.println("                   [-maxvars <INT>]");
		System.out.println("                   [-prune_tree <yes|no>]");
		System.out.println("                   [-filter <yes|no>]");
		System.out.println("                   [-action <action for variable marking>]");
		System.out.println("                   [-patt <pattern for web based similarity>]");
		System.out.println("                   [-force]");
		System.out.println("                   [-ta|-tap]");
		System.out.println("                   [-coref]");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" where <input file> is a file containing a syntactically parsed (with Charniak) RTE dataset in XML format.");
		System.out.println("                    This file is obtained by: 1)lunching preProcessing; 2)running Charniak; 2)lunching CharniakParserWrapper");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" Options are:");
		System.out.println("\n");
		System.out.println("   -svmlearn <true|false>  if 'true' creates batch/sh files to run SVM-learn on the input dataset file, if 'false' creates batch/sh files to run SVM-classify on the input dataset file, if not specified does nothing");  
		System.out.println("\n");
		System.out.println("   -svmparams <svm parameters>  String containing specific the parameters for either SVM-learn or SVM-classify. ");  
		System.out.println("\n");
		System.out.println("   -svmmodel <svm model file>  file containing the svm learn model to be applied in batch/sh creation. This parameter must be set if '-svmlearn' is set to 'false'");  
		System.out.println("\n");
		System.out.println("   -od <output directory>  directory where to store output files (svm, latex files) [" + System.getProperty("arte.default.output.dir") + "]");  
		System.out.println("\n");
		System.out.println("   -idf                    use of IDF to compute overall anchoring similiarity score (each anchor is weighted by the");
		System.out.println("                           IDF of the H-constituent extracted from a large corpus [" + System.getProperty("arte.anchor.idf") + "]");  
		System.out.println("\n");
		System.out.println("   -cv                     use communication verb heuristics  during tree reworking process. [" + System.getProperty("arte.tree.verbcomm") + "]");  
		System.out.println("\n");
		System.out.println("   -patt <pattern>         pattern to be used for web based lexical similarity  module (PATT). Possible choices are: nom,hb,pe,nom_pe,hb_pe,hb_nom_pe,hbinv_pe,hbinv_nom_pe.  [" + System.getProperty("arte.lexsim.patt.type") + "]");  
		System.out.println("\n");
		System.out.println("   -maxvars <INT>          maximum number of variables to carry out kernel permutation in variable handling phase.  [" + System.getProperty("arte.tree.maxvars") + "]");  
		System.out.println("\n");
		System.out.println("   -filter <yes|no>        flag for the filter of examples that exceeds the maximum number of variable.");  
		System.out.println("                           An active filter will eliminate trees on these examples.");  
		System.out.println("\n");
		System.out.println("   -flat                   gives a flat tree a root with all the leaves that are words");  
		System.out.println("\n");
		System.out.println("   -action <action>        action to be done with variable during variable handling phase. Possible choices are: merge (1) , assign (2) , mark (3) , plain (4) , empty (0) [" + System.getProperty("arte.tree.varmarkingtype") + "]");  
		System.out.println("\n");
		System.out.println("   -t  <tasks>             restricts the dataset to a specific task. Possible choices are: ALL, QA, IE, MT, IR, SUM, etc.[" + System.getProperty("arte.tasks") + "]");  
		System.out.println("\n");
		System.out.println("   -p  <list of lex proc>  ordered list of lexical processors (in comma separated format) to be applied during lexical anchoring");
		System.out.println("                           Order should follow the importance precedence of processors. Possible processors are: SURF,LEVE,DRMO,VBEN,WNET,PATT. ["  + System.getProperty("arte.lexsim.processors") + "]"); 
		System.out.println("                    	      - SURF (SurfaceSimilarity) : word overlap between constituents in lower case");
		System.out.println("                    	      - PNLV (ProperNounLevSimilarity) : similarity between proper noun constituents using Levensthein distance");
		System.out.println("                    	      - (to be implemented) PNFS (ProperNounFiniteStateSimilarity) : similarity between proper noun constituents using a Finite State matching");
		System.out.println("                    	      - DRMO (DerivationalMorphology) : similarity between constituents looking at derivational morphology in WordNet");
		System.out.println("                    	      - VBEN (VerbLexicalEntailment) : similarity between verbal constituents looking at verb entailments in WordNet");
		System.out.println("                    	      - WNET (WNSimilarity) : similarity between constituents using the Pedersen distance in WordNet");
		System.out.println("                    	      - PATT (PatternBasedLexicalEntailmentDetector) : similarity between verb constituents using web based patterns (specified by 'patt' parameter)");
		System.out.println("   -trw  <list of tree rewriters>  ordered list of tree rewriters (in comma separated format). Possible rewriters are: EPT,LEM,VLEM,ANA,APP,PHR,MOD");
		System.out.println("   -force  forces the creation of new output files");  
		System.out.println("   -ta   type anchors ");  
		System.out.println("   -tap  type anchors and propagate in the tree");  
		System.out.println("   -chunking <no|yes>  chunking is applied to determine placeholds (the default is yes)");  
		System.out.println("   -coref  assigns the same placeholder to similar words");  
		System.out.println("   -prune_tree <yes|no> prunes the T syntactic tree (the default is yes)");
		System.out.println("   -new_formulation produces data for the kernel EMNLP 2009 (tDAG)");
		System.out.println("\n");
		System.out.println("\n\n");
		System.exit(0);
		}		

		
public static void main(String[] argv) throws Exception {
	if (argv.length == 0) usage();
	else {
		Arte art = new Arte();
		ArteConfigurationHandler.initialize();
		art.parseParameters(argv);
		Arte.arteInitializer(art);
		art.run(inFile);	
	}                
		
}//end main

}//end class

