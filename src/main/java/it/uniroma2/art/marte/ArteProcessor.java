/**
 * 
 */
package it.uniroma2.art.marte;

import it.uniroma2.art.marte.SVMwrappers.Experimenter;
import it.uniroma2.art.marte.dictionaries.GeneralizationDictionary;
import it.uniroma2.art.marte.dictionaries.LemmaDictionary;
import it.uniroma2.art.marte.processors.LexicalAnchorer;
import it.uniroma2.art.marte.processors.VariableHandler;
import it.uniroma2.art.marte.processors.cds.CDSExtractor;
import it.uniroma2.art.marte.processors.featureDetectors.FeatureDetectorChain;
import it.uniroma2.art.marte.processors.lexSimilarity.ConstituentRelationDetector;
import it.uniroma2.art.marte.processors.lexSimilarity.DateRelationDetector;
import it.uniroma2.art.marte.processors.lexSimilarity.DerivationalMorphology;
import it.uniroma2.art.marte.processors.lexSimilarity.DistributionalSimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.FNSimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.NamedEntitySimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.NumberSimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.PatternBasedLexicalEntailmentDetector;
import it.uniroma2.art.marte.processors.lexSimilarity.ProperNounLevSimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.RelationDetectorChain;
import it.uniroma2.art.marte.processors.lexSimilarity.SemanticRelationDetector;
import it.uniroma2.art.marte.processors.lexSimilarity.SurfaceSimilarity;
import it.uniroma2.art.marte.processors.lexSimilarity.ToBeRelation;
import it.uniroma2.art.marte.processors.lexSimilarity.VerbLexicalEntailment;
import it.uniroma2.art.marte.processors.lexSimilarity.WNRelationDetectorChain;
import it.uniroma2.art.marte.processors.lexSimilarity.WNSimilarity;
import it.uniroma2.art.marte.processors.treeRewriters.BadWordDetector;
import it.uniroma2.art.marte.processors.treeRewriters.ConstituencyTreeToDepencencyTree;
import it.uniroma2.art.marte.processors.treeRewriters.Flattener;
import it.uniroma2.art.marte.processors.treeRewriters.GenericTreeRewriter;
import it.uniroma2.art.marte.processors.treeRewriters.InitialNormalizer;
import it.uniroma2.art.marte.processors.treeRewriters.LeafGeneralizer;
import it.uniroma2.art.marte.processors.treeRewriters.LemmaWriter;
import it.uniroma2.art.marte.processors.treeRewriters.Lemmatizer;
import it.uniroma2.art.marte.processors.treeRewriters.ModalSyntacticTransformer;
import it.uniroma2.art.marte.processors.treeRewriters.NEExtractor;
import it.uniroma2.art.marte.processors.treeRewriters.SemanticHeadMarker;
import it.uniroma2.art.marte.processors.treeRewriters.SuperMatcherAndReducer;
import it.uniroma2.art.marte.processors.treeRewriters.SyntacticTransformer;
import it.uniroma2.art.marte.processors.treeRewriters.TextTreePruner;
import it.uniroma2.art.marte.processors.treeRewriters.TreeRewriterCascade;
import it.uniroma2.art.marte.processors.treeRewriters.VerbLemmatizer;
import it.uniroma2.art.marte.structures.AnalysedTextPair;
import it.uniroma2.art.marte.structures.Anchor;
import it.uniroma2.art.marte.structures.DataSet;
import it.uniroma2.art.marte.structures.Formatter;
import it.uniroma2.art.marte.structures.Pair;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;
import it.uniroma2.art.marte.structures.SVMFormatter;
import it.uniroma2.art.marte.structures.SVMpair;
import it.uniroma2.art.marte.utils.ArteConfigurationHandler;
import it.uniroma2.art.marte.utils.BitOperations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Properties;
import java.util.Vector;


import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

//import arte.processors.treeRewriters.AppositionSyntacticTransformer;


/**
 * @author Fabio Massimo Zanzotto
 * This class governs the principal activities of the ARTE system.
 */
public class ArteProcessor extends GlobalProcessor {

	 // Processes
	private TreeRewriterCascade initial_tree_rewriters = null;
	private RelationDetectorChain lexical_relation_detector = null;
	private LexicalAnchorer lex_anc = null;
	private VariableHandler variable_handler = null;
	private TreeRewriterCascade second_tree_rewriters = null;
//	private OldFeatureDetectorChain feature_detector_chain = null;
	private FeatureDetectorChain feature_detector_chain = null; 

	private boolean produce_dck = false;
	private CDSExtractor cds = null;
	private Double cdsLambda = null;
	private Integer cdsSize = null;
	private String cdsType = null;
	private String cdsOp = null;
	private Double cdsMu = null;
	
	// Parameters 
	// Generic input/output options
	private CommandLine commands = null;
	private Options options = null;
	// Processing chain options 
	//private static CommandLine processing_chain_commands = null;
	//private static Options options = null;
	
	
	// Parameters variables
	private static int max_variables = 7;
	

	// Input & Output
	File outputDirectory = null;
	File inputFile = null;
	File outputFile = null;
	
	
	public ArteProcessor() throws Exception {
		define_intial_tree_rewrites();
		define_second_tree_rewrites();
		define_lexical_similarity_detectors();
		defineFeatureDetectors();
	}
	
	public ArteProcessor(String[] argv) throws Exception {
		this();
		createOptions();
		parseOptions(argv);
		initialize();
		setFilesAndCreateDirectoriesForTheExperiment();
		feature_detector_chain.initialize(commands);
	}

	public ArteProcessor(CommandLine commands) throws Exception {
		this();
		this.commands = commands;
		analizeCommands(this.commands);
		initialize();
		setFilesAndCreateDirectoriesForTheExperiment();
		feature_detector_chain.initialize(commands);
	}

	
	public void initialize() throws Exception {
		
		initial_tree_rewriters.setActiveProcessors(commands.getOptionValues("trw1"));
		second_tree_rewriters.setActiveProcessors(commands.getOptionValues("trw2"));
		feature_detector_chain.setActiveProcessors(commands.getOptionValues("feat"));
		
		lexical_relation_detector.setActiveProcessors(commands.getOptionValues("p"));
		
		lex_anc = new LexicalAnchorer(lexical_relation_detector);

		if (commands.hasOption("new")) BitOperations.setBase((long)16); //NEW KERNEL
		else BitOperations.setBase((long)8); //OLD KERNEL
		
		variable_handler = new VariableHandler();

	}

	private void define_intial_tree_rewrites() throws Exception {
		initial_tree_rewriters = new TreeRewriterCascade();
		initial_tree_rewriters.add(new InitialNormalizer(false));
		initial_tree_rewriters.add(new SuperMatcherAndReducer(false));  // ATTENZIONE ____ FMZ TENTATIVO BISLACCO! 
		initial_tree_rewriters.add(new NEExtractor(false));
		initial_tree_rewriters.add(new Lemmatizer(false,new LemmaDictionary(System.getProperty("arte.lemmata.file")),false));
		initial_tree_rewriters.add(new BadWordDetector(false,"D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/data/kb/bad_word_dictionary/bad_terms.small"));
		initial_tree_rewriters.add(new ModalSyntacticTransformer(false,System.getProperty("arte.modalsyntactictranformer.rules")));
//		initial_tree_rewriters.add(new AppositionSyntacticTransformer(false,System.getProperty("arte.appositionsyntactictranformer.rules")));
		initial_tree_rewriters.add((GenericTreeRewriter) (new SyntacticTransformer(false,System.getProperty("arte.anaphorasyntactictranformer.rules"))).setNameAndDescription("ANA", "transforms trees applying normalizations as who and which clauses"));
		initial_tree_rewriters.add((GenericTreeRewriter) (new SyntacticTransformer(false,System.getProperty("arte.phrasalverb.rules"),true)).setNameAndDescription("PHR", "for each phrasal verb, generates a leaf containing the verb and the particle"));
		initial_tree_rewriters.add(new VerbLemmatizer(false,new LemmaDictionary(System.getProperty("arte.lemmata.file")),true));
		initial_tree_rewriters.add(new SemanticHeadMarker(false,System.getProperty("arte.semanticheadmarker.rules")));
		
	}

	private void define_second_tree_rewrites() throws Exception {
		/*		
		LEM("lemmatizer", "produces a lemmatized version of the tree"),
		 */
		second_tree_rewriters = new TreeRewriterCascade();
		second_tree_rewriters.add((GenericTreeRewriter) (new LeafGeneralizer(false,new GeneralizationDictionary(System.getProperty("arte.wn.verbcomm.file")))).setNameAndDescription("VGEN", "generalizes verbs (at the leaf level) according to a dictionary"));
		second_tree_rewriters.add(new ConstituencyTreeToDepencencyTree(false));
		second_tree_rewriters.add(new TextTreePruner(false));
		second_tree_rewriters.add(new Flattener(false));
		second_tree_rewriters.add(new LemmaWriter(false));

	}
	
	private void define_lexical_similarity_detectors() throws Exception {
		lexical_relation_detector = new RelationDetectorChain();
		
		lexical_relation_detector.add(new FNSimilarity(false));
		lexical_relation_detector.add(new ToBeRelation(false));
		lexical_relation_detector.add(new DateRelationDetector(false));
		lexical_relation_detector.add(new NumberSimilarity(false));
		lexical_relation_detector.add(new SurfaceSimilarity(false));
		lexical_relation_detector.add(new NamedEntitySimilarity(false,System.getProperty("arte.namedentitymatcher.rules")));
		lexical_relation_detector.add(new ProperNounLevSimilarity(false));
		lexical_relation_detector.add(new DerivationalMorphology(false,System.getProperty("arte.wn.deriv.file")));
		lexical_relation_detector.add(new VerbLexicalEntailment(false,System.getProperty("arte.wn.verbent.file")));
		
		WNRelationDetectorChain wn_processors = new WNRelationDetectorChain(false);
		wn_processors.add((ConstituentRelationDetector) (new SemanticRelationDetector(false,"-",System.getProperty("arte.wn.antinomy.file"))).setNameAndDescription("ANT", "links antonimous words"));
		wn_processors.add((ConstituentRelationDetector) (new SemanticRelationDetector(false,"po",System.getProperty("arte.wn.part_of.file"))).setNameAndDescription("POF", "links words in part-of relation "));
		wn_processors.add(new WNSimilarity(false,System.getProperty("arte.wn.sim.file")));
		lexical_relation_detector.add(wn_processors);
		lexical_relation_detector.add(new PatternBasedLexicalEntailmentDetector(false,System.getProperty("arte.webpatterns.file")).setPattern(System.getProperty("arte.lexsim.PATT.type")));	
		lexical_relation_detector.add(new DistributionalSimilarity(false,"single-target.dm"));	
																											 

	}

	public void defineFeatureDetectors() {
		feature_detector_chain = new FeatureDetectorChain();

	}

	
	public EntailmentPair elaborate(EntailmentPair pair) throws Exception {
		pair.setActiveAnalysedPair("raw_text:cpw");

		Pair p = new Pair(0, false, "", 
				pair.getActiveAnalysedPair().getText(), 
				pair.getActiveAnalysedPair().getHypothesis(),
				DataSet.newText(pair.getActiveAnalysedPair().getText()),
				DataSet.newText(pair.getActiveAnalysedPair().getHypothesis())
				);

		//System.out.println("T: " + pair.getActiveAnalysedPair().getText() + "\nH:" 
		//		+ pair.getActiveAnalysedPair().getHypothesis());

		elaborate(p,pair);

		
		/// CDS analysis
		if (produce_dck) {
			cds.extract(pair);
		}

		// Feature Extraction 
		pair = this.feature_detector_chain.computeFeature(pair);
		
		return pair;
	}
	/**
	applies all the steps to generate an instance in the feature space.
	It does the following activities
	1) transforms the syntactic trees with the allowed transformations
	2) finds the anchors and determines the placeholders
	3) adds the placeholders in the trees
	*/
	public SVMpair elaborate(Pair pair,EntailmentPair new_pair) throws Exception {
		
			SVMpair instance = new SVMpair(pair.getId(),(pair.getOrigValue()? 1 : -1));
			instance.set_class_string(pair.get_original_value_string());

			//new_pair.addFeature("distance");
			// PHASE 0) Reworking syntactic trees
			initial_tree_rewriters.transform(pair);
			
			// PHASE 1) LEXICAL ANCHORING
			//          phase 1 is carried out by a LexicalAnchorer (lex_anc)
			if (ArteParameters.verbous()) System.out.println("Lexical Anchoring....");
			Vector <Vector <Anchor>> anchors = lex_anc.findAnchorsWithRepetitions(pair);
			if (ArteParameters.verbous()) System.out.println("Lexical Anchoring....done!");
			pair.setAnchors(anchors);

			// PHASE 1.1) DETERMINE LEXICAL DISTANCE
			double f = 0;
			if (!commands.hasOption("idf_off")) f = lex_anc.globalAnchorBasedSimilarityWithRepetitions(anchors);
			else f = lex_anc.globalAnchorBasedSimilarityWithRepetitionsNoIDF(anchors);
			
//			instance.setFeatureValue("distance","" + f);/// OBSOLETE
			instance.setParseTrees(pair);/// OBSOLETE
			

			new_pair.addFeatureValue("distance", f);

			// PHASE 2.0) Reduction and propagation of the variables in the Text and the Hypothesis
			//            Setting of the relevance for the tree leaves  

			Vector variables = null;
			Vector<Anchor> selected_anchors = null;
			if (commands.hasOption("chk")) {
				Vector variables_and_anchors = variable_handler.mergeConstituentsAndAssignVariablesWithRepetitions(pair,anchors);
				variables = (Vector) variables_and_anchors.elementAt(0);

				selected_anchors = (Vector<Anchor>) variables_and_anchors.elementAt(1);
			} else {
				variables = variable_handler.metodoStupidoCheTantoNonFunzionaAccorpaAncore(anchors);
				selected_anchors = new Vector<Anchor>();
				for (Vector <Anchor> v:anchors) selected_anchors.addAll(v);
			}
			       
			                   
			if (ArteParameters.verbous()) { 
				System.out.println("********* ANCHORS: *****************");
				for (int i=0; i<selected_anchors.size();i++)
					System.out.println(selected_anchors.elementAt(i).toString());
			}
			
			if (commands.hasOption("coref")) variables = variable_handler.expandVariablesWithAllTheAnchors((Vector <Vector>) variables,anchors);
			
			variable_handler.setRelevancyOnTheVariableLeaves(variables);

			if (commands.hasOption("pc_all")) 
				variables = variable_handler.propagateVariables(variables, pair);

			if (commands.hasOption("ta") || commands.hasOption("tap")) {  
				variable_handler.putAnchorTypesInNodes(selected_anchors);
				if (commands.hasOption("tap")) variable_handler.propagateTheAnchorTypeInTheTrees(pair,variables);
			}
			if (variables.size() < max_variables || commands.hasOption("filter")) {

				variable_handler.putVariableNamesAndComputePfunction(variables, pair, max_variables);


				// PHASE 1.2) DETERMINE FEATURES AND ADD IT TO THE SVM INSTANCE 
				//feature_detector_chain.computeFeatures(pair,instance);
	
				// This must be before instance.addParseTreeWithPlaceholders(pair)
				
				second_tree_rewriters.transform(pair);//LEGOLAS 10-03-2007
				
				instance.addParseTreeWithPlaceholders(pair);
	
			}
			if (ArteParameters.verbous()) System.out.println("\n**************************\n");
			
			
			
			
			AnalysedTextPair a = new AnalysedTextPair(pair.getTextXDG().toPennTree(), pair.getHypoXDG().toPennTree(),"tdag");
			a.setHypothesisText(pair.getHypoText());
			a.setTextText(pair.getTextText());
			new_pair.addAnalysis(a);
			
			new_pair.setActiveAnalysedPair("tdag");
			

			return instance;
	}
	
	@SuppressWarnings({ "static-access", "deprecation" })
	private void createOptions(){
		options = new Options();
		options.addOption(new Option("h",  false, "print this message"));
		options.addOption("v",  false, "verbose: print control messages");
		Option o = new Option("i", true ,"name of already parsed input set (RTE format)");
		o.setArgName("RTE_DATA_SET_NAME");
		OptionGroup temp = new OptionGroup();
		temp.addOption(o);
		o = new Option("if", true ,"file of an already parsed input set (RTE format)");
		o.setArgName("RTE_DATA_SET_FILE");
		temp.addOption(o);
		options.addOptionGroup(temp);
		options.addOption("od",true,"output directory for the experiment. The defalut (arte.default.output.dir) is defined in the configuration file.");

		options.addOption("dck", false ,"activate the production of the distributed convolution vectors");
		
		//processing_chain_options = new Options();
		options.addOption("new",  false, "produces files that can be used by the kernel EMNLP,2009");
		options.addOption(new Option("pc_all", false , "placeholders are put on all the nodes except the leaves (by default placeholders are only on preterminals)"));
		options.addOption("coref",  false, "applies a simple coreference method: text words related with the same hypothesis word are coindexed");
		options.addOption("filter", false ,"does not include tDAGs for pairs having more than the max allowed placeholders");
		options.addOption("idf_off", false ,"does not use idf in the computation of the lexical distance");
		options.addOption("maxvars", true ,"maximum allowed number of placeholders");
		temp = new OptionGroup();
		temp.addOption(new Option("ta", false , "adds the type of the anchors on the preterminals"));
		temp.addOption(new Option("tap", false, "adds the type of the anchors on the preterminals and propagate it in all the nodes"));
		options.addOptionGroup(temp);
		options.addOption("chk", false ,"chunks are used to assign placeholder names: placeholders in the same chunk will have the same name");
		o = new Option("trw1", true ,"first tree rewriters cascade - " + initial_tree_rewriters);
		o.setValueSeparator(',');
		o.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(o);
		o = new Option("trw2", true ,"second tree rewriters cascade - " + second_tree_rewriters);
		o.setValueSeparator(',');
		o.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(o);
		o = new Option("p", true ,"lexical anchorers " + lexical_relation_detector);
		o.setValueSeparator(',');
		o.setArgs(Option.UNLIMITED_VALUES);
		options.addOption(o);

		options.addOption(OptionBuilder
				.hasArgs()
				.withValueSeparator(',')
				.withDescription("feature detectors " + this.feature_detector_chain.toString())
				.create("feat"));
				

		options.addOption(OptionBuilder.withArgName("PropertaryEntailmentSet")
				.hasArg()
				.withDescription("specification of a propertary entailment set: if the name does not contains dots, it.uniroma2.art.marte.structures.extension is added to the name")
				.create("set"));
		
		for (Option opt : (new SVMFormatter()).getOptions()) options.addOption(opt);
		for (Option opt : (new CDSExtractor()).getOptions()) options.addOption(opt);
	}
	
	
	public void analizeCommands(CommandLine commands) throws Exception {
    	if (commands.hasOption("maxvars")) try {
    		max_variables = new Integer(commands.getOptionValue("maxvars"));
    	} catch (Exception e) {
    		throw new ParseException("maxvars has to be an integer: " + commands.getOptionValue("maxvars"));
    	}
    	if (commands.hasOption("h")) help();
    	if (commands.hasOption("v")) ArteParameters.setVerbosity(1);
		if (commands.hasOption("od")) {outputDirectory = new File(commands.getOptionValue("od"));} 
		else outputDirectory = new File(System.getProperty("arte.default.output.dir"));
		
		if (commands.hasOption("i")) inputFile = new File(new File(new File(System.getProperty("preprocessing.default.output.dir")),Experimenter.getCurrentExperiment("arte.preprocessing")),commands.getOptionValue("i") + ".xml");
		else if (commands.hasOption("if")) inputFile = new File(commands.getOptionValue("if"));
		else throw new ArteException("Specify the input file (-if) or the input data set name (-i)");

		//processing_chain_commands =  parser.parse( options, argv );
		if (commands.hasOption("dck")) {
			produce_dck = true;
			cds = new CDSExtractor(commands);
			cds.initialize();
		}
		
	}
	
	private void parseOptions(String [] argv) throws Exception {
		CommandLineParser parser = new GnuParser();
	    try {
	    	commands =  parser.parse( options, argv );
			analizeCommands(commands);
	    }
	    catch(ParseException exp) {
	        System.err.println( "ARTE unexpected parameter:\n" + exp.getMessage() + "\n\n");
	        help();
	    }
	}

	private void process() throws Exception {
		initializeACorpus(inputFile, new File(outputDirectory , inputFile.getName()), commands.getOptionValue("set"));
		getCorpusToProcess().addFeature("distance");
		getCorpusToProcess().writeFeatureNames();
		processTheCorpus();
		finalizeTheCorpus();
	}

	private void help(){
	    HelpFormatter h = new HelpFormatter();
	    h.printHelp("Arte (-if FILE|-i DATA_SET) OPTIONS","Options", options,"");
        System.exit(0);
	}

	public String parametrization () throws Exception {
		String parameters =Experimenter.getCurrentExperiment("arte.preprocessing") + "_";
		for(Option o : commands.getOptions()) {
				if (!(o.getOpt().equals("h") || o.getOpt().equals("v")
				|| o.getOpt().equals("i") || o.getOpt().equals("if") ||
				o.getOpt().equals("od")))
				parameters += o.getOpt() + "_" + (o.getValues()!=null?Arrays.toString(o.getValues()).replace("[", "").replace("]", "").replace(",", "_").replace(" ", ""):""); 
		}
		return parameters;
	}

	 /**
	  * Creates all the directories and files for this experiment
	 */
	private void setFilesAndCreateDirectoriesForTheExperiment() throws Exception {
			outputDirectory = new File(outputDirectory,parametrization());
			outputDirectory.mkdirs();
			outputFile = new File(outputDirectory , inputFile.getName() + ".svm");
		}
	
	
	public static void main(String [] argv) throws Exception {
		try {
			
			ArteConfigurationHandler.initialize();
			ArteProcessor p = new ArteProcessor(argv);
			p.process();

			
			Formatter f = new SVMFormatter(p.commands);
			f.process(new EntailmentPairSet(new File(p.outputDirectory , p.inputFile.getName()),new File(p.outputDirectory , p.inputFile.getName() + ".out")),
					  new File(p.outputDirectory , p.inputFile.getName() + ".svm"));

			
		} catch (ArteException e) {
			System.err.println(e.getMessage());
		}
	}

}
