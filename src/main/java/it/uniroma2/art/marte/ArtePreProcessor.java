/**
 * 
 */
package it.uniroma2.art.marte;

import java.io.File;


import it.uniroma2.art.marte.structures.AnalysedTextPair;
import it.uniroma2.art.marte.structures.EntailmentPair;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.Tree;

/**
 * @author Fabio Massimo Zanzotto
 *
 */
public class ArtePreProcessor extends GlobalProcessor {
	
    LexicalizedParser lp = null;
    
    
    ArtePreProcessor() {
    	lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz");
    }
    
	public EntailmentPair elaborate(EntailmentPair p) throws Exception {
		Tree text = lp.parse(p.getRawPair().getText());
		Tree hype = lp.parse(p.getRawPair().getHypothesis());
		p.addAnalysis(new AnalysedTextPair(text.toString(), hype.toString(), "raw_text:cpw"));
		return p;
	}
	
	
	public static void main(String [] argv) throws Exception {
		File base_dir = new File("C:\\USERS_DATA\\FABIO\\LAVORO\\PROGETTI\\SAG_SVN\\Data\\RTE_sick\\sick_test");
		ArtePreProcessor p = new ArtePreProcessor();
		p.processACorpus(new File(base_dir,"SICK_test.txt"), new File(base_dir,"SICK_test.xml"), "SickEntailmentPairSet");
	}

}
