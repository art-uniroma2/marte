/**
 * 
 */
package it.uniroma2.art.marte;

/**
 * @author Fabio Massimo Zanzotto
 * This class contains the static parameters for the ArteProcessor
 */
public class ArteParameters {
	
	private static int verbosity = 0;

	public static void setVerbosity(int verbosity) {
		ArteParameters.verbosity = verbosity;
	}

	public static int getVerbosity() {
		return verbosity;
	}
	
	public static boolean verbous(){
		return (verbosity > 0);
	}
	

}
