package it.uniroma2.art.marte.utils;

public class BitOperations {
	
	
	private static long base = 3;//OLD KERNEL VERSION
	//private static long base = 8;//NEW KERNEL VERSION

	public static void setBase(long b) {base = b;}

	public static long mask(boolean [] mask) {
		long out = 0;
		for (int i = 0; i < mask.length; i++) {
			out += (long) ((mask[i]?1:0) * (base - 1) * Math.pow(base,i)); 
		}
		return out;
	}
	

}