package it.uniroma2.art.marte.utils;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


/**
 * Contains methods for splitting strings into sentences 
 */
public class SimpleSentenceSplitter {


	private boolean isTerminator(int pos, String input) {
		return 	separator(input.charAt(pos)) 
					&& !aSigle( input , 5 , pos) 
					//&& thereAreMoreThanXFullCharBefore(4,pos,input) 
					&& nextCharIsAtLeastBlank(pos,input);
	}

	private boolean aSigle(String input, int dis, int pos ) {
		
		if (pos + 1 < input.length() && pos - dis >= 0  ) {
			//System.out.println("--->" + input.substring(pos-dis,pos) + "<---");
			return input.substring(pos-dis,pos).matches(".* [A-Za-z\\.]{0,4}");
		} else return true;
	}

	private boolean nextCharIsAtLeastBlank(int pos, String input) {
		if (pos + 1 < input.length()) return input.charAt(pos+1) == ' ' || input.charAt(pos+1) == '\n' || input.charAt(pos+1) == '\t';
		return false; 
	}
	private boolean separator(char c) {return c == '.' || c == ':' || c == '!' || c == ';' || c == '?'; }

	private boolean thereAreMoreThanXFullCharBefore(int dis, int pos, String input) {
		boolean no_space = true; 
		for (int j = pos; j>=0 && j > pos - dis; j--) {
			no_space = no_space && input.charAt(j) != ' ';
		}
		return no_space;
	}

	public Vector<String> findSentences(String input) {
		Vector<String> sentences = new Vector();
		int start = 0;
		for (int i = 0; i < input.length(); i++) {
			if (isTerminator(i,input)) {
				sentences.add(input.substring(start,i+1));
				start = i + 1;
			}			
		}
		if (!((input.substring(start,input.length())).trim().equals(""))) sentences.add(input.substring(start,input.length()));
		return sentences;
	}
	
	public static void main(String[] argv) {
		try {
			BufferedReader bf = new BufferedReader(new FileReader(argv[0]));
			String line = bf.readLine();
			String input = "";
			while (line != null) {
				input += line;
				line = bf.readLine();
			}
			bf.close();
			
			Vector <String> ss = (new SimpleSentenceSplitter()).findSentences(input);
			for (String s: ss) {
				System.out.println("<s> " + s + " </s>");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}