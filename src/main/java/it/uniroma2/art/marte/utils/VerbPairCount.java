package it.uniroma2.art.marte.utils;

import java.util.*;

public class VerbPairCount {

	String v_h = "", v_t = "";
	int nom,pe,hb,hNom,hAll,tSimple,tAll;

	public VerbPairCount(String pairAndCounts) {
		StringTokenizer st = new StringTokenizer(pairAndCounts,"\t");
		v_t=st.nextToken();
		st.nextToken(); st.nextToken();st.nextToken();
		v_h=st.nextToken();
		st.nextToken(); st.nextToken();st.nextToken();
		hAll = (new Integer(st.nextToken())).intValue();
		tAll = (new Integer(st.nextToken())).intValue();
		tSimple = (new Integer(st.nextToken())).intValue();
		hNom = (new Integer(st.nextToken())).intValue();
		nom = (new Integer(st.nextToken())).intValue();
		pe = (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue();
		st.nextToken(); st.nextToken(); st.nextToken();st.nextToken();
		hb = (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue() 
		+ (new Integer(st.nextToken())).intValue();

	}

	public String getVH() {return v_h;}
	public String getVT() {return v_t;}
	
	public String pair() {return getVH() + ":" + getVT();}

	public int nomCount() {return nom;}
	public int peCount() {return pe;}
	public int hbCount() {return hb;}
	public int hNomCount() {return hNom;}
	public int hAllCount() {return hAll;}
	public int tSimpleCount() {return tSimple;}
	public int tAllCount() {return tAll;}
	
}
