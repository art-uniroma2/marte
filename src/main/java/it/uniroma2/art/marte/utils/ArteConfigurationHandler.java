package it.uniroma2.art.marte.utils;

import it.uniroma2.art.marte.temp.PropsFile;

import java.io.*;
import java.util.regex.*;



/**
	Dinamically handle location of KB files and other configuration data

	@author Daniele Pighin (20041004) modified by Marco Pennacchiotti
*/
public abstract class ArteConfigurationHandler {


	public static void initialize()
	{
		// a different main configuration file can be suggested at
		// run time using -Dchaos.conf.mainfile=<myfile>
		if (System.getProperty("arte.conf.mainfile") == null) {
			System.setProperty("arte.conf.mainfile", System.getProperty("arte.home") + "/arte_configuration.xml");
		}
		//parse main configuration file
		PropsFile.parse(System.getProperty("arte.conf.mainfile"));
	}


	public static class UndefinedPropertyException extends RuntimeException {
		public UndefinedPropertyException(String cname) {
			super("The property [" + cname + "] hasn't been properly defined.");
		}
	}
	

	public static class UndefinedEnvironmentVariableException extends RuntimeException {
		public UndefinedEnvironmentVariableException(String cname) {
			super("The environment variable [" + cname + "] hasn't been properly set.");
		}
	}


	
}
