package it.uniroma2.art.marte.utils;

import java.io.File;


public class FileFactory {

	public static File find(String file, File base_dir, String extension) {
		System.out.println("FILE:"+ file);
		System.out.println("DIR: "+ base_dir);
		System.out.println("EXT:"+ extension);
		
		File out = new File(file);
		
		if (!out.exists()) out = new File(file + "." + extension);
		if (!out.exists()) out = new File(base_dir,file);
		if (!out.exists()) out = new File(base_dir,file + "." + extension);
		return out;
	}

}

