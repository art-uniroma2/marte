package it.uniroma2.art.marte.utils;

import java.util.*;
import java.io.*;


public class IDF {
	
	private String idf_repository = null;
	
	private Hashtable<String,Integer> [] idfs ;
	
	private int TOT_DOCS = 0;
	private int DOCS = 0;
	private int DIRS = 0;
	private int MAX_DOCS = 50;
	
	

	public IDF() throws Exception {
		idf_repository = System.getProperty("arte.idf.dir");
		loadIdfTables();
	}

	public IDF(File corpus) throws Exception {
		idf_repository = System.getProperty("arte.idf.dir");
		initCorpusCounts();
		examineCorpus(corpus);
		storeIdfTables(DIRS);
		mergeIdfTables();				
	}

	public IDF(String idf_repository) throws Exception {
		this.idf_repository = idf_repository;
		loadIdfTables();
	}

	public IDF(File corpus,String idf_repository) throws Exception {
		this.idf_repository = idf_repository;
		initCorpusCounts();
		examineCorpus(corpus);
		storeIdfTables(DIRS);
		mergeIdfTables();				
	}
	
	
	private void loadIdfTables() throws Exception {
		System.out.println(" Loading IDF table! ");
		BufferedReader in = new BufferedReader(new FileReader(idf_repository + "/rep"));
		String inline = in.readLine() ;
		if (inline != null) {
			TOT_DOCS = new Integer(inline.trim()).intValue();
		} else throw new Exception( idf_repository + " not well formed file");
		in.close();
		idfs = new Hashtable ['z' - 'a' + 1] ;
		for (int i = 'a'; i <= 'z'; i++) {
			in = new BufferedReader(new FileReader(idf_repository + "/rep_" + (char) i ));
			idfs[i-'a'] = new Hashtable<String,Integer>();
			inline = in.readLine();
			while (inline!=null) {
				StringTokenizer st = new StringTokenizer(inline," \t");
				idfs[i-'a'].put(st.nextToken(),new Integer(st.nextToken()));
				inline = in.readLine();
			}
			in.close();
		}	
	}


	private void loadIdfTables(int DIRS) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(idf_repository + "/idf_table/" + DIRS + "/rep" ));
		String inline = in.readLine() ;
		if (inline != null) {
			TOT_DOCS += new Integer(inline.trim()).intValue();
		} else throw new Exception( idf_repository + " not well formed file");
		in.close();
		for (int i = 'a'; i <= 'z'; i++) {
			in = new BufferedReader(new FileReader(idf_repository + "/idf_table/" + DIRS + "/rep_" + (char) i ));
			inline = in.readLine();
			while (inline!=null) {
				StringTokenizer st = new StringTokenizer(inline," \t");
				String key = st.nextToken();
				int count = (new Integer(st.nextToken())).intValue();
				if (idfs[i-'a'].containsKey(key))
					idfs[i-'a'].put(key,new Integer( ((Integer)idfs[i-'a'].get(key)).intValue() + count ) );
				else idfs[i-'a'].put(key,new Integer(count) );
				inline = in.readLine();
			}
			in.close();
		}	
	}


	private void storeIdfTables() throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(idf_repository + "/rep"));
		out.write(""+TOT_DOCS);
		out.close();
		
		for (int i = 'a'; i <= 'z'; i++) {
			out = new BufferedWriter(new FileWriter(idf_repository + "/rep_" + (char) i ));
			Enumeration<?> e = idfs[i-'a'].keys();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				out.write(key + "\t" + idfs[i-'a'].get(key) + "\n");
			}
			out.close();
		}	
	}
	

	private void storeIdfTables(int DIRS) throws Exception {
		File dir = new File(idf_repository,""+DIRS);
		dir.mkdirs();
		System.out.println(" --> " + dir);
		BufferedWriter out = new BufferedWriter(new FileWriter(idf_repository + "/idf_table/" + DIRS + "/rep" ));
		out.write(""+DOCS);
		out.close();
		for (int i = 'a'; i <= 'z'; i++) {
			out = new BufferedWriter(new FileWriter(idf_repository + "/" + DIRS + "/idf_table/rep_" + (char) i ));
			Enumeration<String> e = idfs[i-'a'].keys();
			while (e.hasMoreElements()) {
				String key = e.nextElement();
				out.write(key + "\t" + idfs[i-'a'].get(key) + "\n");
			}
			out.close();
		}	
	}

	private void mergeIdfTables() throws Exception {
		initCorpusCounts();
		File dir = new File(idf_repository + "/idf_table");
		for (String f : dir.list()) {
			loadIdfTables((new Integer(f)).intValue());
		}
		storeIdfTables();
	}

	public float getIDF(String s) {
		float idf = (float) Math.log(((double)TOT_DOCS));
		s = s.toLowerCase();
		if (!s.equals("")) {
			char c = s.charAt(0);
			if ( c >= 'a' && c <= 'z' && idfs[c-'a'].containsKey(s) ) {
					idf = (float) Math.log(((double)TOT_DOCS)/((double)((Integer)idfs[c-'a'].get(s)).intValue()));
			}
		}
		return idf;
	}

	private void storeElement(String s, Vector<String> doc) {
		s = s.toLowerCase();
		if (!doc.contains(s)) {
			if (!s.equals("")) {
				char c = s.charAt(0);
				//System.out.println("C:"+c);
				if ( c >= 'a' && c <= 'z' ) {
					 if (idfs[c-'a'].containsKey(s)) {
					 	idfs[c-'a'].put(s,new Integer(((Integer)idfs[c-'a'].get(s)).intValue()+1));
					} else {
						idfs[c-'a'].put(s,new Integer(1));
					}
				}
			}
			doc.addElement(s);
		}
	}


	private void initCorpusCounts() {
		idfs = new Hashtable ['z' - 'a' + 1];
		for (int i = 0 ; i < idfs.length ; i++) {
			idfs[i] = new Hashtable<String,Integer>();
		}
		TOT_DOCS = 0 ;
	}

	private void examineCorpus(File f) throws Exception {
		if (f.isDirectory()) {
			for (String file :  f.list()) examineCorpus(new File(f,file));
		} else {
			BufferedReader in = new BufferedReader(new FileReader(f));
			System.out.println(" <-- " + f);
			Vector<String> doc = new Vector<String>();
			String inLine = in.readLine();
			while (inLine!= null) {
				StringTokenizer st = new StringTokenizer(inLine," .,:;?<>/=");
				while (st.hasMoreTokens()) storeElement(st.nextToken(),doc);				
				inLine = in.readLine();
			}
			TOT_DOCS++;
			DOCS++; 
		}
		if (DOCS == MAX_DOCS) {
			storeIdfTables(DIRS);
			DOCS = 0;
			DIRS++; 
			initCorpusCounts();
		}
	}

	public void test() throws Exception {
		System.out.println( "MUTATIONS  : " + getIDF("mutations"));
		System.out.println( "WOMAN      : " + getIDF("woman"));
	}

	public static void main(String [] argv) {
		try {
			if (argv.length == 1) {
				String corpus = argv[0];
				//IDF idf = 
				new IDF(new File(corpus));
				IDF idf1 = new IDF();
				idf1.test();
			} else if (argv.length == 0) {
				IDF idf = new IDF();
				idf.mergeIdfTables();				
				IDF idf1 = new IDF();
				idf1.test();
			} else System.out.println("Use: IDF <corpus>");
		} catch (Exception e) {
			System.out.println("Use: IDF <corpus>");
			e.printStackTrace();
		}
	}

	


}