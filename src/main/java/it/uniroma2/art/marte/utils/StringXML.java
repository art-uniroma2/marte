package it.uniroma2.art.marte.utils;

public class StringXML {
	public static String toXML(String input) {
		return input.replace("&","&amp;").replace("'","&apos;").replace("?","&apos;").replace("?","&apos;");
	} 
}