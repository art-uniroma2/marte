package it.uniroma2.art.marte.utils;

import java.util.HashMap;

public class VSM {
	
	public static HashMap<String,Double> textToBowVector(String text) {
		HashMap <String,Double> out = new HashMap<String,Double> ();
		String[] words = text.split(" ");
		for (String w:words) {
			if (!out.containsKey(w)) {
				out.put(w,new Double(0));
			}
			out.put(w,out.get(w)+1);
		}
		return out;
	}

	public static HashMap<String,Double> textToBowVector(String text,IDF idf) {
		HashMap <String,Double> out = new HashMap<String,Double> ();
		String[] words = text.split(" ");
		for (String w:words) {
			if (!out.containsKey(w)) {
				out.put(w,new Double(0));
			}
			out.put(w,out.get(w)+idf.getIDF(w));
			//System.err.println(w + ":" + (out.get(w)+idf.getIDF(w)));
		}
		return out;
	}
	
	public static HashMap<String,Double> add(HashMap<String,Double> v1,HashMap<String,Double> v2) {
		HashMap <String,Double> out = (HashMap<String, Double>) v1.clone();
		//System.err.print("\n------------------");
		for (String w:v2.keySet()) {
			if (!out.containsKey(w)) out.put(w,v2.get(w));
			else out.put(w,out.get(w)+ v2.get(w));
			//System.err.print("\nS " + w + " : " + v1.get(w)+ " + "+ v2.get(w) + " = " + out.get(w));
		}
		
		return out;
		
	}

	public static double dot(HashMap<String,Double> v1,HashMap<String,Double> v2) {
		double out = 0;
		// System.err.print("\nV1: ");
		//for (String w:v1.keySet()) {System.err.print(" " + w);}
		// System.err.print("\nV2: ");
		//for (String w:v2.keySet()) {System.err.print(" " + w);}
		// System.err.print("\n");
		for (String w:v2.keySet()) {
			if (v1.containsKey(w)) { out += v2.get(w)*v1.get(w);
			//System.err.print("\nS " + w + " : " + v1.get(w)+ " * "+ v2.get(w) + " = " + v2.get(w)*v1.get(w));
			}
		}
//		System.err.print("\n  = " + out + "\n");
		
		return out;
		
	}
	
	public static String vectorToString(HashMap<String,Double> v) {
		String out = "";
		for (String w:v.keySet()) out += w.replace(":", "#co#") + ":" + v.get(w) + " ";
		return out;
	}

	public static HashMap<String,Double> stringToVector(String v) {
		HashMap<String,Double> out = new HashMap<String,Double> ();
		for (String w:v.split(":")) out.put(w.split(":")[0].replace("#co#",":"), new Double(w.split(":")[1]));
		return out;
	}
	
}
