package it.uniroma2.art.marte.preprocessors;

import it.uniroma2.art.marte.utils.SimpleSentenceSplitter;

import java.util.*;
import java.io.*;



 /**
  * This class extends a PreProcessor for an RTE dataset. The processor can be
  * used in two ways (according to _preCharniak private parameter. 
  * Either to prepare a dataset for being analyzed by the Charniak 
  * parser, or for reading the output of the Charniak parser and building back the
  * dataset in parse tree format. Different methods
  * are implemented, to operate on XML files, on Dataset structures or on String.
  */	
  
public class CharniakParserWrapper extends PreProcessor {

	private SimpleSentenceSplitter sent_split = new SimpleSentenceSplitter();
	private Hashtable <String,String> parsedSentences = null;
	private String parsedSentencesFileNameBase = null;
	private int max_id = 0;
	private BufferedWriter charniak_input_file = null;
	private BufferedWriter id_file = null;
	private boolean _preCharniak; 						//option that is true if the module has to prepare the INPUT for Charniak, false if has to read the OUTPUT of Charniak
	
	
	
	public CharniakParserWrapper() throws Exception{
		if (_preCharniak)								// PRE CHARNIAK INVOCATION
			{	
      	id_file = new BufferedWriter(new FileWriter(parsedSentencesFileNameBase + ".idx"));
			charniak_input_file = new BufferedWriter(new FileWriter(parsedSentencesFileNameBase + ".chrin"));
			}
		else 														// POST CHARNIAK INVOCATION
	 		{
			try{load_parsed_sentences();
			}catch (Exception e) {
			 	e.printStackTrace();}
			}
		}
	
	
	
	public CharniakParserWrapper(boolean preCha, String baseChaDir) throws Exception{
		_preCharniak = preCha;
		parsedSentencesFileNameBase = (new File(System.getProperty("preprocessing.input.file"))).getName();
		if (parsedSentencesFileNameBase.endsWith(".xml")) 
					parsedSentencesFileNameBase = parsedSentencesFileNameBase.substring(0,parsedSentencesFileNameBase.length() - 4);
		parsedSentencesFileNameBase = baseChaDir + "/" + parsedSentencesFileNameBase;
		if (_preCharniak)								// PRE CHARNIAK INVOCATION
			{	
      	id_file = new BufferedWriter(new FileWriter(parsedSentencesFileNameBase + ".idx"));
			charniak_input_file = new BufferedWriter(new FileWriter(parsedSentencesFileNameBase + ".chrin"));
			}
	 else 														// POST CHARNIAK INVOCATION
	 		{
			try{load_parsed_sentences();
				}catch (Exception e) {
			 	e.printStackTrace();}
			}
	}
	
	
	public void charniakInvocator() throws Exception {
		boolean linux = (File.separatorChar == '/');
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(new File(new File(System.getProperty("arte.bin.dir")),"tmp"),"charniak_caller" + (linux?"":".bat"))));
		out.write((linux?"if [ -a "+ (new File(parsedSentencesFileNameBase + ".chrout")) +" ];\n ":""));
		out.write((linux?"then\n\techo \"Charniak Parser has already processed the file\";\nelse\n\t":""));
		out.write((linux?"":"call ") + new File(System.getProperty("arte.charniak.runner")));
		out.write(" " + new File(System.getProperty("arte.charniak.model")) + File.separatorChar);
		out.write(" " + (new File(parsedSentencesFileNameBase + ".chrin")));
		out.write(" > " + (new File(parsedSentencesFileNameBase + ".chrout")));
		out.write((linux?";\nfi":""));
		out.write("\n");
		out.close();
	}
	
	
	public String getExtension() {return "chr";} 
	
	
	public void finalize() throws Exception {
		if (id_file != null) id_file.close();
		if (charniak_input_file != null) charniak_input_file.close();
		charniakInvocator();
	}



	public void load_parsed_sentences() throws Exception {
		// LOAD INDEXES
		Hashtable <Integer,String> indexes = new Hashtable<Integer,String> ();
		BufferedReader in = new BufferedReader(new FileReader(parsedSentencesFileNameBase + ".idx"));
		System.out.println("Charniak Wrapper: loading index");
		String line = in.readLine();
		while (line!=null) {
			if (line.indexOf("\t") > 0) { 
				indexes.put( new Integer(line.substring(0,line.indexOf("\t"))), line.substring(line.indexOf("\t"),line.length()) );
			} else {
				System.out.println("ERROR: " + line);
			}
			line = in.readLine();
		}	
		in.close();		
		
		// LOAD PARSED SENTENCES
		in = new BufferedReader(new FileReader(parsedSentencesFileNameBase + ".chrout"));
		line = in.readLine();
		
		System.out.println("Charniak Wrapper: loading parsed sentences");
		String parsedSentence="";
		parsedSentences = new Hashtable<String,String>();
		//FIRST ID
		while (line!=null && !isId(line)) { 
			line = in.readLine();
			//System.out.println(line);
		}
		Integer id = new Integer(getId(line));
		line = in.readLine();
		int sentences = 0;
		while (line!=null) {
			//System.out.println(id + " - " + line);
			if (isId(line)) {
				if (sentences > 1) parsedSentence = "(S " + parsedSentence + ")";
				else if (sentences==0) parsedSentence ="(S no_parsed)";
				parsedSentences.put(indexes.get(id).trim(),parsedSentence);
				//System.out.println(">>>>>>>>>>" + indexes.get(id) + " ---- " + parsedSentence);
				id = new Integer(getId(line));
				parsedSentence ="";
				sentences = 0;
			} else {
				if (!line.trim().equals("")) {
					parsedSentence += " " + removeS1(line);
					//parsedSentence += " " + line;
					sentences++;
				}
			}
			line = in.readLine();
			
		}
		if (!parsedSentence.equals("")) {
			if (sentences > 1) parsedSentence = "(S " + parsedSentence + ")";
			parsedSentences.put(indexes.get(id).trim(),parsedSentence);
			//System.out.println(">>>>>>>>>>" + indexes.get(id) + " ---- " + parsedSentence);
		}
	}

	private String removeS1(String in){
		in = in.trim();
		String out = in;
		if (in.startsWith("(S1 ")) {
			out = in.substring(3,in.length()-1);
		}
		return out;
	}

	private boolean isId(String tree) {
		return tree.startsWith("(S1 (NP (NN ID) (CD ");
	}
	
	
	private String getId(String tree) {
		return tree.substring(20,tree.indexOf(")",21));
	}
	
	
	
	public String run(String sentence) throws Exception {
		String out = null;
		String sentence_in = sentence.replace('\n',' ').trim();
		if (!_preCharniak) 	// POST CHARNIAK INVOCATION : returns the parsed sentence
			out = parsedSentences.get(sentence_in);
		 else				// PRE CHARNIAK INVOCATION : writes the input for Charniak
		 	{
			save_id(max_id,sentence_in);
			save_sentence_in_charniak_file(max_id,sentence_in);
			max_id++;
			}
		if (out == null) out = "(S no_parsed)"; 
		return out;
	}	




	private void save_id(int id,String sentence) throws Exception {
		id_file.write("" + id + "\t" + sentence + "\n");
	}
	
	private void save_sentence_in_charniak_file(int id,String sentence) throws Exception {
		charniak_input_file.write("<s> ID " + id + " </s>\n");
		Vector<String> sents = sent_split.findSentences(removeQuotes(sentence));
		for (String s:sents) charniak_input_file.write( "<s> " + s + " </s>\n");
	}

	private String removeQuotes(String input) {
		return input.replace("\"","").replace("''","");
	} 



/*************************************/
/******** STAND-ALONE MODULES ********/
/*************************************/

 /**
  * Method to load stanfordNER specific parameters.
  */ 
	public void parseParameters(String [] argv) {
		for (int i=0; i < argv.length ; i++) {
			if (argv[i].equals("-if")) {
				i++;
				parsedSentencesFileNameBase = argv[i];
				if (parsedSentencesFileNameBase.endsWith(".xml")) 
					parsedSentencesFileNameBase = parsedSentencesFileNameBase.substring(0,parsedSentencesFileNameBase.length() - 4);
			} 
		}
		
	}
	/** 
  * Explains input parameters
  */
  public  void usage()	{
		System.out.println("\n");
		System.out.println("Usage: CharniakParserWrapper -if <INPUT_FILE> -of <OUTPUT_FILE> -action <ACTION_TO_DO> ");
		System.out.println("\n");
		System.out.println(" where <ACTION_TO_DO> is either <pre|post>. Use <pre> to create Charniak input files. Use <post> to read ");
		System.exit(0);
	}	
	


	public static void main(String [] argv) {
		try {
		PreProcessor p = new CharniakParserWrapper();
			p.main_method(argv);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
