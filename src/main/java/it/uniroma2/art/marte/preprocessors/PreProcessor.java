package it.uniroma2.art.marte.preprocessors;

import it.uniroma2.art.marte.structures.*;

import java.io.*;
import java.util.*;


 /**
  * This abstract class is used as a generic schema for a processor that reads each 
  * fragment (H or T) of a RTE dataset and makes some action or modification on it.
  * Different methods are implemented, to operate on XML files, on Dataset structures 
  or on String.
  */	
  
public abstract class PreProcessor {

	protected String input_file = null;
	protected String output_file = null;




	public abstract String getExtension(); 


 /**
  * Method to load preprocessor generic parameters.
  */ 
	public final void parseGenericParameters(String[] argv) {
		for (int i=0; i < argv.length ; i++) {
			if (argv[i].equals("-if")) {
				i++;
				input_file = argv[i];
				String base_file = input_file;
				if (input_file.endsWith(".xml")) base_file = input_file.substring(0,input_file.length() - 4);
				output_file = base_file + "-" + this.getExtension() + ".xml";
			} else 
			if (argv[i].equals("-of")) {
				i++;
				output_file = argv[i];
			}  
		}
	if (input_file==null || output_file==null)
		usage();	
	}

 
  /**
  * Abstact method to load preprocessor specific parameters.
  */ 
	public abstract void parseParameters(String[] argv);
		
	public abstract void usage();
	


	
 /**
  * finalizes the preprocessor
  */ 
	public abstract void finalize() throws Exception ;
	
	
	
/**
  * Abstract method to run the proprocessor on a single plain sentence.
  *
  * @param plain_sentences    sentence to be analized
  * @return                   analized sentence
  */ 
  public abstract String run(String plain_sentence) throws Exception ;


	
 /**
  * Runs the preprocessor over a dataset.
  *
  * @param ds    the dataset to be analyzed
  * @return      the processed datset
  */ 
	public final DataSet runOnDataset(DataSet ds) throws Exception {	    
		Vector <Pair> pairs = ds.getAllPairs();
		Pair p;
		int size = pairs.size();
		System.out.println("> Running pre-processor :" + this.getExtension());
		for (int i=0;i<size;i++) 
			{
			p = pairs.elementAt(i);
			p.setT(this.run(p.getText()));
			p.setH(this.run(p.getHypo()));
			ds.setPair(p,i);
			}
		System.out.println("done!");	
		return ds;
	}
	
	
	
  /**
  * Runs the preprocessor over a dataset read from an input file.
  * The modified dataset os written to an output file
  *
  * @param input_file     dataset input file in XML RTE format
  * @param output_file		dataset output file in XML RTE format
  */ 
	public final void runOnRTEFiles(String input_file,String output_file) throws Exception {	    
		DataSet ds = new DataSet(input_file);
		ds = runOnDataset(ds);
		BufferedWriter output = new BufferedWriter(new FileWriter(output_file));
		output.write(ds.toXML() + "\n");
		output.close();
	}


	
	public void main_method(String [] argv) throws Exception {

		// INITIALIZATION
		this.parseGenericParameters(argv);
		this.parseParameters(argv);
		// RUNNING
		this.runOnRTEFiles(input_file,output_file);
		
		// FINALIZATION
		this.finalize();
		
	}	



}