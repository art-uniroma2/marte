:- op(250,xfx,:).

iniziale(0).
finale(3).

arco(0,1,X:X):-lex(X).
arco(1,1,X:'#'):-lex(X).
arco(1,0,X:'.'):-space(X).
arco(1,0,X:Y):-space(X),space(Y).

arco(0,2,X:X):-lex(X).
arco(2,2,X:X):-lex(X).
arco(2,0,X:Y):-space(X),space(Y).
arco(2,0,'.':X):-space(X).
arco(2,0,'.':'.').

arco(0,4,X:'#'):-lex(X).
arco(4,4,X:'#'):-lex(X).
arco(4,0,X:'#'):-space(X).


arco(0,3,X:X):-lex(X).
arco(3,3,X:X):-lex(X).

lex(a).
lex(b).
lex(c).
lex(d).
lex(e).
lex(f).
lex(g).
lex(h).
lex(i).
lex(l).
lex(m).
lex(n).
lex(o).
lex(p).
lex(q).
lex(r).
lex(s).
lex(t).
lex(u).
lex(v).
lex(w).
lex(y).
lex(j).
lex(k).
lex(z).


space(' ').
space('-').

test(Ns1,Ns2) :- iniziale(X), trasduttore(X,Ns1,Ns2).

trasduttore(Stato,[],[]) :- finale(Stato).

trasduttore(Stato,Ns1,Ns2) :- arco(Stato,Stato_suc,Simbolo),
		traversa(Simbolo,Ns1,NNs1,Ns2,NNs2),
		trasduttore(Stato_suc,NNs1,NNs2).
		
traversa('#':'#',Ns1,Ns1,Ns2,Ns2) :- !.
traversa('#':L2,Ns1,Ns2,[L2|RestNs2],RestNs2) :- !.
traversa(L1:'#',[L1|RestNs1],RestNs1,Ns2,Ns2) :- !.
traversa(L1:L2,[L1|RestNs1],RestNs1,[L2|RestNs2],RestNs2) :- !.
