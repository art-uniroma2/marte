package it.uniroma2.art.marte.preprocessors;

/**
 * 
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * @author Noemi Scarpato
 *
 */
public class FredParser {
	// http://wit.istc.cnr.it:9191/fred?text=
	HttpURLConnection conn;	
	public static void main(String[] args) {
	 
		FredParser fp = new FredParser();
			
		String text="Korea states intention to restart reactors," +
				" throws out IAEA inspectors and takes down inspection cameras," +
				" announces immediate withdrawal from the Nuclear Non-Proliferation " +
				"Treaty and makes threats of war if the UN or the United States imposes sanctions.";		
		System.out.println("tree "+ fp.parse("this is a Test query",true));
		System.out.println("tree "+ fp.parse("Test query",true));
			
			 
			 
			 //System.out.println("tree "+tree);
	/*		
			//URL url = new URL("http://wit.istc.cnr.it:9191/fred?text="+URLEncoder.encode(text,"UTF8"));
			//URL url = new URL("http://wit.istc.cnr.it/stlab-tools/fred?semantic-subgraph=false&text="+URLEncoder.encode(text,"UTF8"));
URL url = new URL("http://wit.istc.cnr.it/stlab-tools/fred?text=UFO+is+a+British+television+science+fiction+series+created+by+Gerry+Anderson+and+Sylvia+" +
		"Anderson&semantic-subgraph=true&" +
		"format=text%2Ffunctional");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("GET");
		//	conn.setRequestProperty("Accept", "text/functional");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
	 
			BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
			String output;
			String tree = "";
			System.out.println("Output from Server .... \n");
			File out=new File("fred_out_false.txt");
			FileWriter fw = new FileWriter(out);
			boolean wr = false;
			while ((output = br.readLine()) != null) {
				if(output.contains("</pre>"))
					wr=false;
				if(wr){
					fw.write(output);
					fw.write("\n");	
					tree=tree+output;
					tree=tree+("\n");
					
				}
				if(output.contains("<pre>"))
					wr=true;
				
			}
			fw.flush();
			fw.close();
			conn.disconnect();*/
		  
	 
		}
		public String parse(String text,boolean semanticSubgraph){
			String tree = "";
			try {
				/*URL url = new URL("http://wit.istc.cnr.it/stlab-tools/fred?text="+text+
						"&semantic-subgraph="+semanticSubgraph
						+"&format=text%2Ffunctional");*/
				//URL url = new URL("http://wit.istc.cnr.it/stlab-tools/fred?text="+URLEncoder.encode(text,"UTF8") +
				URL url = new URL("http://wit.istc.cnr.it/stlab-tools/fred/?text="+URLEncoder.encode(text,"UTF8") +
						"&semantic-subgraph="+semanticSubgraph+
						"&format=text%2Ffunctional");
				System.out.println(url);
				conn = (HttpURLConnection) url.openConnection();
				
				conn.setRequestMethod("GET");
				
				System.out.println(conn.getResponseCode());
				if (conn.getResponseCode() != 200 && conn.getResponseCode() != 502) {
					
					throw new RuntimeException("Failed : HTTP error code : "
							+ conn.getResponseCode());
				}else if(conn.getResponseCode() == 502){
						System.err.println("Failed : HTTP error code : "
								+ conn.getResponseCode()+" waiting for FRED ....");
						System.err.println("Errore in query "+text);
						Thread.sleep(10000);	
						tree="<Errore></Errore>";
						throw new RuntimeException("Failed : HTTP error code : "
								+ conn.getResponseCode());
						
				}
		 
				BufferedReader br =
						new BufferedReader
						(new InputStreamReader((conn.getInputStream())));
				String output;
				
				boolean wr = false;
				while ((output = br.readLine()) != null) {
					if(output.contains("</pre>"))
						wr=false;
					if(wr){
						tree=tree+output;
					}
					if(output.contains("<pre>")){
						wr=true;
					}
				}
				
				conn.disconnect();
				return tree;
			} catch (MalformedURLException e) {
				System.out.println("MalformedURLException ");
				e.printStackTrace();
				return tree;
			} catch (IOException e) {
				System.out.println("IOException ");
				e.printStackTrace();
				return tree;
			} catch (InterruptedException e) {
				e.printStackTrace();
				return tree;
			}
			
		}
}
