package it.uniroma2.art.marte.structures;

import java.util.*;

import org.w3c.dom.Element;



import it.reveal.chaos.utils.xml.ElementExt;
import it.reveal.chaos.xdg.textstructure.Paragraph;
import it.reveal.chaos.xdg.textstructure.Text;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.ArteException;

@Deprecated
public class Pair {

	private int _id;
	private boolean _value;
	private String _original_value_string ="";
	private boolean _originalValue;
	private boolean _unknown = false;
	private float _confidence;
	private String _task;
	private String _t;
	private String _h;
	private String _question;
	private Text _hText;
	private Text _tText;
	private Vector <Vector <Anchor>> anchors = null;
	
	private GenericContext textContext = null; 
	private GenericContext hypothesisContext = null; 

	
	public Pair(int id, boolean value, String task, String t, String h) {
		_id = id;
		_value = value;
		_originalValue = value;
		_confidence = 0;
		_task = task;
		_t = t;
		_h = h;
		_tText = null;
		_hText = null;
		
	}

	public Pair(int id, boolean value, String task, String t, String h, Text tText, Text hText) {
		_id = id;
		_value = value;
		_originalValue = value;
		_confidence = 0;
		_task = task;
		_t = t;
		_h = h;
		_tText = tText;
		_hText = hText;
	}

	public Pair(Pair pr) {
		_id = pr.getId();
		_value = pr.getValue();
		_originalValue = pr.getOrigValue();
		_confidence = pr.getConfidence();
		_task = pr.getTask();
		_t = pr.getText();
		_h = pr.getHypo();
		_tText = pr.getTextText();
		_hText = pr.getHypoText();
	}


	public Pair(Element pair_el) throws Exception {
		ElementExt pair = new ElementExt(pair_el);
		_id = new Integer(pair_el.getAttribute("id")).intValue();
		
		if (	pair.getElementChildren("t") != null && pair.getElementChildren("t").size() > 0 &&
				pair.getElementChildren("t").elementAt(0) != null && 
				pair.getElementChildren("t").elementAt(0).getFirstChild() != null)
				_t = pair.getElementChildren("t").elementAt(0).getFirstChild().getNodeValue();
		else { _t = "empty"; System.out.println(" Empty text : " + _id); }
		if (	pair.getElementChildren("h") != null && pair.getElementChildren("h").size() > 0 && 
				pair.getElementChildren("h").elementAt(0) != null && 
				pair.getElementChildren("h").elementAt(0).getFirstChild() != null)
				_h = pair.getElementChildren("h").elementAt(0).getFirstChild().getNodeValue();
		else { _h = "empty"; System.out.println(" Empty hypo : " + _id); }

		if (pair.getElementChildren("contextofh")!=null && pair.getElementChildren("contextofh").size() > 0) {
			if (pair.getElementChildren("contextofh").elementAt(0).getElementsByTagName("c") == null) throw new ArteException("ERROR");
			this.setHypothesisContext(new GenericContext(pair.getElementChildren("contextofh").elementAt(0).getElementsByTagName("c")));
		}

		
		if (pair.getElementChildren("contextoft")!= null && pair.getElementChildren("contextoft").size() > 0) {
			if (pair.getElementChildren("contextoft").elementAt(0).getElementsByTagName("c") == null) throw new ArteException("ERROR");
			this.setTextContext(new GenericContext(pair.getElementChildren("contextoft").elementAt(0).getElementsByTagName("c")));
		}

		if (	pair.getElementChildren("q") != null && pair.getElementChildren("q").size() > 0 &&
				pair.getElementChildren("q").elementAt(0) != null && 
				pair.getElementChildren("q").elementAt(0).getFirstChild() != null)
				_question = pair.getElementChildren("q").elementAt(0).getFirstChild().getNodeValue();
		else { _question = "empty";}

		//pair_el.getAttribute("value").toUpperCase().equals("TRUE");
		if (pair_el.getAttribute("value")!=null) _original_value_string = pair_el.getAttribute("value").toUpperCase();
		if (pair_el.getAttribute("entailment")!=null) _original_value_string = pair_el.getAttribute("entailment").toUpperCase();
		
		if ( _original_value_string.equals("TRUE") ||
			 _original_value_string.equals("FALSE")||
			 _original_value_string.equals("ENTAILMENT") ||
			 _original_value_string.equals("CONTRADICTION") ||
			 _original_value_string.equals("YES") ||
			 _original_value_string.equals("NO")||
			 _original_value_string.equals("UNKNOWN") ) {
				_originalValue = _original_value_string.equals("TRUE") || _original_value_string.equals("YES") || _original_value_string.equals("ENTAILMENT") ;
				_unknown = _original_value_string.equals("UNKNOWN") ;
		/*} 
		else if ( pair_el.getAttribute("entailment").toUpperCase().equals("YES") ||
		     pair_el.getAttribute("entailment").toUpperCase().equals("NO") ||
		     pair_el.getAttribute("value").toUpperCase().equals("ENTAILMENT") ||
		     pair_el.getAttribute("value").toUpperCase().equals("CONTRADDICTION") 
			) {
				_originalValue = pair_el.getAttribute("entailment").toUpperCase().equals("YES") ||  pair_el.getAttribute("value").toUpperCase().equals("ENTAILMENT");
				//System.out.println("value :" + _originalValue);
		} else if (pair_el.getAttribute("entailment").equals("") && pair_el.getAttribute("value").equals("")) {
				_originalValue = false ;*/
		} else if (_original_value_string.equals("")) {
			_originalValue = false;
		} else throw new Exception ("Pair entailment value not compliant: " +  _id + "# " + _original_value_string);


		_value = _originalValue;
		_confidence = 0;
		_task = pair_el.getAttribute("task");
		_tText = null;
		_hText = null;
	}



	public String toXML() {
		//System.out.println(""+ _id );
		return 	
		"<pair id=\"" + _id + "\" entailment=\"" + 
		//(_originalValue? "YES" : "NO") 
		_original_value_string
		+ "\" task=\"" + _task + "\">\n" +
		"<t>" + it.uniroma2.art.marte.utils.StringXML.toXML(_t) + "</t>\n" +
		(this.getTextContext()!=null?
				"\t<contextoft>" +this.getTextContext().toXML() + "</contextoft>\n":"")+
		"<h>" + it.uniroma2.art.marte.utils.StringXML.toXML(_h) + "</h>\n" +
		(this.getHypothesisContext()!=null? 
				"\t<contextofh>" +this.getHypothesisContext().toXML() + "</contextofh>\n":"") +
		"<q>" + it.uniroma2.art.marte.utils.StringXML.toXML(_question) + "</q>\n" +
		"</pair>\n";
					
	}

	public boolean unknown() {return _unknown;}

	public boolean getValue() {
		return _value;
	}
	
	public String get_original_value_string() {
		return _original_value_string;
	}

	public void set_original_value_string(String originalValueString) {
		_original_value_string = originalValueString;
	}

	public boolean getOrigValue() {
		return _originalValue;
	}
	
	public float getConfidence() {
		return _confidence;
	}
	
	public String getTask() {
		return _task;
	}

	public int getId() {
		return _id;
	}

	public String getText() {
		return _t;
	}

	public String getQuestion() {
		return _question;
	}

	public String getHypo() {
		return _h;
	}


	public Text getTextText() {
		return _tText;
	}

	public Text getHypoText() {
		return _hText;
	}
	
	public XDG getHypoXDG(){
		Vector v = _hText.getParagraphs();
		Paragraph p = (Paragraph) v.elementAt(0);
		Vector x = p.getXdgs();
		XDG a = (XDG) x.elementAt(0);
		return a;	
	}
	
	public XDG getTextXDG(){
		Vector v = _tText.getParagraphs();
		Paragraph p = (Paragraph) v.elementAt(0);
		Vector x = p.getXdgs();
		XDG a = (XDG) x.elementAt(0);
		return a;	
	}


	public void setHypoXDG(XDG in){
		Vector v = _hText.getParagraphs();
		Paragraph p = (Paragraph) v.elementAt(0);
		p.getXdgs().setElementAt(in,0);
	}
	
	public void setTextXDG(XDG in){
		Vector v = _tText.getParagraphs();
		Paragraph p = (Paragraph) v.elementAt(0);
		p.getXdgs().setElementAt(in,0);
	}

	
	public String getHypoXDGProlog(){
		return getHypoXDG().toPrologPredicate().toPrologString();}


	public String getTextXDGProlog(){
		return getTextXDG().toPrologPredicate().toPrologString();}

	public String getHypoXDGXml(){
		return getHypoXDG().toXML();}


	public String getTextXDGXml(){
		return getTextXDG().toXML();}
	
	
	public void setConfidence(float confidence) {
		_confidence = confidence;
	}
	
	
	public void setValue(boolean value) {
		_value = value;
	}

	public void setOriginalValue(boolean value) {
		_originalValue = value;
	}


	public void setT(String t) {
		_t = t;
	}

	public void setH(String h) {
		_h = h;
	}


	public void setTText(Text t) {
		_tText = t;
	}

	public void setHText(Text h) {
		_hText = h;
	}

	public Vector <Vector <Anchor>> getAnchors() {
		return anchors;
	}
	
	public void setAnchors(Vector <Vector <Anchor>> anchors) {
		this.anchors = anchors;
	}
	

	public void setTextContext(GenericContext textContext) {
		this.textContext = textContext;
	}

	public GenericContext getTextContext() {
		return textContext;
	}

	public void setHypothesisContext(GenericContext hypothesisContext) {
		this.hypothesisContext = hypothesisContext;
	}

	public GenericContext getHypothesisContext() {
		return hypothesisContext;
	}

	public String toString() {
		String result = new String();
		result += "************************************************\n";
		result += " * id: " + getId() + "\n";
		result += "   * original value: " + getOrigValue() + "\n";
		result += "   * value: " + getValue() + "\n";
		result += "   * confidence: " + getConfidence() + "\n";
		result += "   * task: " + getTask() + "\n";
		result += "   * text: " + getText() + "\n";
		result += "   * hypo: " + getHypo() + "\n";
		result += "\n";
		return result;
	}
	
	
	public String toStringComplete() {
		String result = new String();
		result += "************************************************\n";
		result += " * id: " + getId() + "\n";
		result += "   * original value: " + getOrigValue() + "\n";
		result += "   * value: " + getValue() + "\n";
		result += "   * confidence: " + getConfidence() + "\n";
		result += "   * task: " + getTask() + "\n";
		result += "   * text: " + getText() + "\n";
		result += "   * hypo: " + getHypo() + "\n";
		result += "   * textChaos: " + getTextXDGXml() + "\n";
		result += "   * hypoChaos: " + getHypoXDGXml() + "\n";
		result += "\n";
		return result;
	}

	public static class SType {
		public static int text = 1;
		public static int hypo = 2;
	}
	
}
