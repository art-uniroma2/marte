package it.uniroma2.art.marte.structures;

import it.reveal.chaos.xdg.textstructure.Constituent;

public class Anchor {

		private int _id;
		private Constituent _hConst;
		private Constituent _tConst;
		private float _similarity; 
		private char _nominalization; 	
										// Nominalized sentence is supposed to be ONLY on the text.
										//
										// n= no nominalization; 
										//
										// a= action nominalization [-ion, -ing](Voice-Event+VP subcat.);	
										//		ex. Earthquake destructed the city --> The destruction of the city by the Earthquake
										//		ex. "		"		"		"	   --> The destructing of the city by the Earthquake
										//
										// b= agent nominalization [-er] (Voice-Event+VP subcat.);	
										//		ex. John mowed the lawn --> John, the mower of the lawn.
										//		ex. "	"	"	"	"	--> John is the the mower of the lawn.
										//
										// c= action nominalization [-th] (VP subcat.);
										//		ex. Tomatoes grew --> the growth of tomatoes.
										//
										// for more details:	"Event and Aspectual Structure in Derivational Morphology" (A.Van Hout, T.Roeper)
										//						"Entre syntaxe et semantique:Normalization de la sortie ..." (C.Hagege, C.Roux)
										//
																				 
		private char _passive; // n = no passive ; h = hypo verb is passive; t = text verb is passive
		private String _type; // type of anchor (=,entailment, etc.)
		private String _processor; // type of lexical processor used to obtain the anchor
	
	public Anchor(int id, Constituent hConst, Constituent tConst, float sim)  {
		_id = id;
		_hConst = hConst;
		_tConst = tConst;
		_similarity = sim;
		_nominalization = 'n';
		_passive = 'n';
		_type="unknow_type";
		_processor="unknow_proc";
	}
	
	
	public Anchor(int id, Constituent hConst, Constituent tConst, float sim, String type, String proc)  {
		_id = id;
		_hConst = hConst;
		_tConst = tConst;
		_similarity = sim;
		_nominalization = 'n';
		_passive = 'n';
		_type=type;
		_processor=proc;
	}
	
	public Anchor(Constituent hConst, Constituent tConst, float sim)  {
		_hConst = hConst;
		_tConst = tConst;
		_similarity = sim;
		_nominalization = 'n';
		_passive = 'n';
		_type="unknow_type";
		_processor="unknow_proc";
		}
	
	
	public Anchor(Constituent hConst, Constituent tConst, float sim, String type, String proc)  {
		_hConst = hConst;
		_tConst = tConst;
		_similarity = sim;
		_nominalization = 'n';
		_passive = 'n';
		_type=type;
		_processor=proc;
	}
	
	
	public Constituent getHConst() {
		return _hConst;
	}		

	public Constituent getTConst() {
		return _tConst;
	}		
		
	public int getId() {
		return _id;
	}		


	public float getSimilarity() {
		return _similarity;
	}		
	
	public char getNominalization() {
		return _nominalization;
	}
	
	public char getPassive() {
		return _passive;
	}				
	
	public float getAbsSimilarity() {
		return Math.abs(_similarity);
	}		
	
	
	public int getSignSimilarity() {
		if (_similarity<0)
			return -1;
		else return +1;
	}		
	
	public String getType(){
		return _type;
		}
	
	
	public String getProcessor(){
		return _processor;
		}
		
	public void setHConst(Constituent c) {
		_hConst = c;
	}		


	public void setTConst(Constituent c) {
		_tConst = c;
	}		
	
	public void setSimilarity(float s) {
		_similarity = s;
	}		

		public void setNominalization(char nom) {
		_nominalization = nom;
	}
	
		
	public void setId(int id) {
		_id = id;
	}
	
	public void setPassive(char p) {
		_passive = p;
	}
			
	public void setType(String t) {
		_type= t;
	}
			
	public void setProcessor(String proc) {
		_processor = proc;
	}
	
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		String out = (_hConst != null ? _hConst.getSurface() + ":" + _hConst.getId() : "null" );
		sb.append(out);
		sb.append(" , ");
		out= (_tConst != null ? _tConst.getSurface() + ":" + _tConst.getId()  : "null" );
		sb.append(out);
		sb.append(" , ");
		sb.append(_similarity);
		sb.append(" , ");
		sb.append(_nominalization);
		sb.append(" , ");
		sb.append(_passive);
		sb.append(" , ");
		sb.append(_type);
		sb.append(" , ");
		sb.append(_processor);
		sb.append(")");
		return sb.toString();
	}
	
}//end class