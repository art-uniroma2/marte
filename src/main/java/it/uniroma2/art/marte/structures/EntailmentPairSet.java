package it.uniroma2.art.marte.structures;


import java.io.*;
import java.util.Enumeration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.SAXParseException;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;

public class EntailmentPairSet {
	
	private BufferedReader input = null;
	private BufferedWriter output = null;
	DocumentBuilder builder = null;

	Instances feature_vector_space = null;
	
	public EntailmentPairSet() throws IOException, ParserConfigurationException {
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		feature_vector_space = new Instances("std", new FastVector(), 10000);
		EntailmentPair.setFeatureSpace(feature_vector_space);
	}

	public EntailmentPairSet(String input, String output) throws IOException, ParserConfigurationException {
		this(new File(input),new File(output));
	}

	public EntailmentPairSet(File input, File output) throws IOException, ParserConfigurationException {
		//System.out.print("Creating Entailment Pair Set...");
		this.input = new BufferedReader(new FileReader(input));
		this.output = new BufferedWriter(new FileWriter(output));
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		feature_vector_space = new Instances("std", new FastVector(), 10000);
		EntailmentPair.setFeatureSpace(feature_vector_space);
		readFeatureNames();
		//System.out.println("done");
	}


	public void setInput(String input) throws IOException  {
		this.input = new BufferedReader(new FileReader(input));
	}
	public void setOutput(String output) throws IOException {
		this.output = new BufferedWriter(new FileWriter(output));
	}
	
	public void closeInput() throws IOException {
		this.input.close();
	}

	public void closeOutput() throws IOException {
		this.output.close();
	}

	public void close() throws IOException {
		closeInput();
		closeOutput();
	}

	public EntailmentPair nextPair() throws Exception {
		String pair = null;
		String line = this.input.readLine();
		EntailmentPair out_pair = null;
		while (line != null && !line.trim().startsWith("<pair ")) line = this.input.readLine();
		if (line!=null) {
			pair = "";
			while (!line.trim().equals("</pair>") && line != null) {pair += line + "\n";line = this.input.readLine();}
			if (line!=null) pair += line;
			else pair = null;
		}
		if (pair != null) {
			try {
				Reader reader=new CharArrayReader(("<?xml version='1.0' encoding='utf-8'?>\n" +pair).toCharArray());
				Document doc = builder.parse(new org.xml.sax.InputSource(reader));
				out_pair = new EntailmentPair(doc.getDocumentElement());
			} catch (SAXParseException e) {
				System.err.print("ERROR IN PAIR: " + pair);
				throw e;
			}

		}
		return out_pair;
	}

	public void writeFeatureNames() throws Exception {
		this.output.write("<feature_names>");
		boolean first = true;
		Enumeration<Attribute> aa = feature_vector_space.enumerateAttributes();
		while (aa.hasMoreElements()) {
			Attribute a = aa.nextElement();
			this.output.write((first?"":",")+a.name());
			if (first) first = false;
		}
		this.output.write("</feature_names>\n");
	}

	private void readFeatureNames() throws IOException {
		String line = this.input.readLine();
		while (line != null && line.trim().equals("")) line = this.input.readLine();
		if (line.startsWith("<feature_names>") && line.endsWith("</feature_names>") ) {
			String [] featurenames = line.substring(line.indexOf(">")+1, line.lastIndexOf("<")).split(",");
			for (String f:featurenames) addFeature(f);
		}
	}

	
	public void writePair(EntailmentPair pair) throws Exception {
		this.output.write(pair.toXML() + "\n");
		this.output.flush();
	}

	public void addFeature(String name) {
		//System.out.println("ADDING FEATURE : " + name);
		feature_vector_space.insertAttributeAt(new Attribute(name), 0);
	}

}
