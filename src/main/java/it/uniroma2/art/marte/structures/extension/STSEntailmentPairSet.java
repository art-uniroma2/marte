package it.uniroma2.art.marte.structures.extension;

import it.uniroma2.art.marte.structures.AnalysedTextPair;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;
import it.uniroma2.art.marte.structures.GenericTextPair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;


public class STSEntailmentPairSet extends EntailmentPairSet {

	private BufferedReader plain_sentence1reader;
	private BufferedReader plain_sentence2reader;
	private BufferedReader synt_sentence1reader;
	private BufferedReader synt_sentence2reader;
	private BufferedReader similarity_reader;
	
	
	public STSEntailmentPairSet(File input, File output) throws IOException,
			ParserConfigurationException {
		super();
		String name = input.getName();
		setOutput(output.toString());
		plain_sentence1reader = new BufferedReader(new FileReader(new File(input,"STS.input1." + name + ".txt")));
		plain_sentence2reader = new BufferedReader(new FileReader(new File(input,"STS.input2." + name + ".txt")));
		synt_sentence1reader  = new BufferedReader(new FileReader(new File(new File(new File(input,"charniak"),"output"),"STS.charniak.output1." + name + ".txt")));
		synt_sentence2reader = new BufferedReader(new FileReader(new File(new File(new File(input,"charniak"),"output"),"STS.charniak.output2." + name + ".txt")));
		similarity_reader = new BufferedReader(new FileReader(new File(input,"STS.gs." + name + ".txt")));
	}
	
	public void closeInput() throws IOException {
		this.plain_sentence1reader.close();
		this.plain_sentence2reader.close();
		this.synt_sentence1reader.close();
		this.synt_sentence2reader.close();
		this.similarity_reader.close();
	}



	public EntailmentPair nextPair() throws Exception {

		EntailmentPair out = null;
		String plain_sentence1 = plain_sentence1reader.readLine();
		String plain_sentence2 = plain_sentence2reader.readLine();
		String synt_sentence1 = synt_sentence1reader.readLine();
		String synt_sentence2 = synt_sentence2reader.readLine();
		String similarity = similarity_reader.readLine();

		if (plain_sentence1 != null) {
			out = new EntailmentPair();
			out.setOriginalEntailmentValue(similarity);
			out.setRawPair(new GenericTextPair(plain_sentence1.replace("<", "&lt;").replace(">", "&gt;"),plain_sentence2.replace("<", "&lt;").replace(">", "&gt;")));
			out.addAnalysis(new AnalysedTextPair(synt_sentence1.replace("<", "&lt;").replace(">", "&gt;"), synt_sentence2.replace("<", "&lt;").replace(">", "&gt;"), "raw_text:cpw"));
		}
		
		return out;
	}
}
