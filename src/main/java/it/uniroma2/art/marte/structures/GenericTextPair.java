package it.uniroma2.art.marte.structures;


public class GenericTextPair {
	private String text = null;
	private String hypothesis = null;

	private GenericContext textContext = null; 
	private GenericContext hypothesisContext = null; 
	
	public GenericTextPair(String text,String hypothesis) {
		this.text = text;
		this.hypothesis = hypothesis;
	}

	public String getText() {
		return text;
	}

	public String getHypothesis() {
		return hypothesis;
	}

	public void setTextContext(GenericContext textContext) {
		this.textContext = textContext;
	}

	public GenericContext getTextContext() {
		return textContext;
	}

	public void setHypothesisContext(GenericContext hypothesisContext) {
		this.hypothesisContext = hypothesisContext;
	}

	public GenericContext getHypothesisContext() {
		return hypothesisContext;
	}
}
