package it.uniroma2.art.marte.structures;

import java.util.*;
import java.io.*;


/**
  * The class implements a structure to read and write instances of pairs 
  * from SVM.
  */
@Deprecated
public class SVMpair {
	
//	public static Vector <String> std_features = new Vector();


	// Specific 
	private int _id;                            			//pair identifiers to refer to the related Pair object
	private int _class;                         			//original classification value (1 or -1)
	private String _class_string;                         			//original classification value (1 or -1)

	// Predifined Features 
	private String _plainBOW_H =null;           			//Bag of word of the Hypothesis
	private String _plainBOW_T =null;           			//Bag of word of the of Text
	private String _plainParseTreeH =null;      			//parse tree of Hypothesis
	private String _plainParseTreeT =null;      			//parse tree of Text
	private Vector<String> _parseTreeWithPlaceholdersH =null;           //parse trees with placeholders of Hypothesis
	private Vector<String> _parseTreeWithPlaceholdersT =null;           //parse trees with placeholders of Text

	// Additional Features 
	// Features are divided in sets:
	//  - each sets has a name an correponds to a row of the matrix _features
	//  - the columns represents  of _features represent the actual feature in a set
	private String[][] _features =null; 					    //other features


	// Static variables that takes into account the feature names for each set	
	private static Vector <String> feature_set_names = new Vector();
	private static Vector <Vector <String>> feature_names_for_each_set = new Vector();
	private static int max_num_of_features = 5000;



	public SVMpair(int id, int cl){
		_id = id;
		_class = cl;

		_features = new String[feature_set_names.size()][max_num_of_features];
		for (int i=0;i < feature_set_names.size();i++) 
			for (int j=0;j<max_num_of_features;j++) 
				_features[i][j]=null;
	}
	
	/**
	 add a new feature on the possible features of all the SVMpair. It is better to add all the features before starting the creation of svm pairs.
	*/
	public static void addFeature(String feature) {
		addFeature(feature,"std");
	}


	public String get_class_string() {
		return _class_string;
	}

	public void set_class_string(String classString) {
		_class_string = classString;
	}

	/**
	adds a feature set.
	*/
	public static void addFeatureSet(String set) {
		if (!feature_set_names.contains(set)) {
			feature_set_names.add(set);
			feature_names_for_each_set.add(new Vector());
		}
	}
	

	/**
	 add a new feature on the possible features of all the SVMpair. It is better to add all the features before starting the creation of svm pairs.
	*/
	public static void addFeature(String feature, String set) {
		System.out.println("FD: Adding " + feature + " in " + set);
		addFeatureSet(set);
		Vector feat_vect = feature_names_for_each_set.elementAt(feature_set_names.indexOf(set));
		if (!feat_vect.contains(feature)) feat_vect.add(feature);
		
		int size = feature_names_for_each_set.elementAt(feature_set_names.indexOf(set)).size();
		if (size > max_num_of_features) max_num_of_features = size;
	}


	/**
	sets the specific value to the feature in position.
	*/
	public void setFeatureValue(int position, String value){
		setFeatureValue( position, value, "std");
	}

	/**
	sets the specific value to the feature name.
	*/
	public void setFeatureValue(String name, String value){
		setFeatureValue( name, value, "std");
	}

	/**
	gets the specific value to the feature name.
	*/
	public String getFeatureValue(String name){
		return getFeatureValue( name, "std");
	}


	/**
	sets the specific value to the feature in position.
	*/
	public void setFeatureValue(int position, String value, String set){
		int set_id = feature_set_names.indexOf(set);
		_features[set_id][position] = value;	
	}

	/**
	sets the specific value to the feature name.
	*/
	public void setFeatureValue(String name, String value, String set){
		int set_id = feature_set_names.indexOf(set);
		//System.out.println(set + ":" + set_id + "<>" + name + ":" + feature_names_for_each_set.elementAt(set_id).indexOf(name));
		_features[set_id][feature_names_for_each_set.elementAt(set_id).indexOf(name)] = value;	
	}

	/**
	sets the specific value to the feature name.
	*/
	public String getFeatureValue(String name, String set){
		int set_id = feature_set_names.indexOf(set);
		//System.out.println(set + ":" + set_id + "<>" + name + ":" + feature_names_for_each_set.elementAt(set_id).indexOf(name));
		return _features[set_id][feature_names_for_each_set.elementAt(set_id).indexOf(name)];	
	}


	public int getId() {
		return _id;
	}

	public void setId(int id) {
		_id = id;
	}
	
	public void setParseTrees(Pair p) {
		//ORG		
		setParseTrees(p.getTextXDG().toPennTree(),p.getHypoXDG().toPennTree());
		//IDS		setParseTrees("(S " + p.getId() + ")","(S " + p.getId() + ")");
		
	}
	
	public void setParseTrees(String parseTreeT,String parseTreeH) {
		_plainParseTreeT = parseTreeT;
		_plainParseTreeH = parseTreeH;
	}

	public void setBOWs(String parseTreeT,String parseTreeH) {
		_plainBOW_T = parseTreeT;
		_plainBOW_H = parseTreeH;
	}

	public void addParseTreeWithPlaceholders(Pair p) {
		
		addParseTreeWithPlaceholders(p.getTextXDG().toPennTree(),p.getHypoXDG().toPennTree());
	}

	public void addParseTreeWithPlaceholders(String parseTreeT,String parseTreeH) {
		if (_parseTreeWithPlaceholdersH == null) {
			_parseTreeWithPlaceholdersH = new Vector();
			_parseTreeWithPlaceholdersT = new Vector();
		}
		_parseTreeWithPlaceholdersT.add(parseTreeT);
		_parseTreeWithPlaceholdersH.add(parseTreeH);
	}


 /**
  * Returns the parse tree and the other features in a SVM 
  * readable format.
  *
  * @return    the SVM line 
  */ 
	public String toString() {
		
		// binary
		//ORIGINAL BEFORE TWITTER String out = "" + _class +	"\t|BT| " + _plainParseTreeT + " |BT| " + _plainParseTreeH;
		
		// multi-class
		//String out = "" + _class_string +	"\t|BT| " + _plainParseTreeT + " |BT| " + _plainParseTreeH;
		
		String out = "";
		/// ADDED FOR TWITTER
		//ORIGINAL BEFORE TWITTER 
		if (_plainBOW_T != null && _plainBOW_H != null) out = "" + _class +	"\t|BT| " + _plainBOW_T + " |BT| " + _plainBOW_H;
		else  out = "" +  _class +	"\t|BT| " + _plainParseTreeT + " |BT| " + _plainParseTreeH;
		
		if (_parseTreeWithPlaceholdersH != null) {
			for (int i=0; i < _parseTreeWithPlaceholdersH.size(); i++ )	{
				out += " |BT| " + _parseTreeWithPlaceholdersT.elementAt(i) + " |BT| " + _parseTreeWithPlaceholdersH.elementAt(i);
			}
		}
		//ORIGINAL 
		out += " |ET|";

		// with ID out += " |BT| (" +  _id + " *) |ET|";
		
		if (feature_set_names.size() > 0) {
			for (int i=0;i < feature_set_names.size();i++) {
				for (int j=0;j<max_num_of_features;j++) 
					if (_features[i][j]!=null) out += " " + (j+1) + ":" + (("" + _features[i][j]).equals("NaN")?
					0:_features[i][j]) ;
					
				if (i==feature_set_names.size()-1) out += " |EV|";	
				else out += " |BV|";	
			}
		}
		//if (_id == 4999) {
		//	System.out.println("Feature names");
		//	System.out.println(feature_names_for_each_set.elementAt(feature_set_names.indexOf("wiki")));
		//	//for (int i=0; i< feature_set_names.indexOf("wiki"); i++)
			
		//}
		return out;
	}		

}
