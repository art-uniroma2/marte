package it.uniroma2.art.marte.structures;

import java.io.File;
import java.io.IOException;


public interface Formatter {
	
	public String format(EntailmentPair p);

	public void process(EntailmentPairSet entailmentPairSet, File outputFile) throws Exception;
	
}
