package it.uniroma2.art.marte.structures;

import it.uniroma2.art.marte.ArteException;

import java.util.*;
import org.w3c.dom.*;

public class GenericContext {
	private Vector<String> leftContext = null;
	private Vector<String> rightContext = null;

	public GenericContext(NodeList context) throws ArteException {
		leftContext = new Vector<String>();
		rightContext = new Vector<String>();
		System.out.println("L : " + context.getLength());
		for (int i = 0 ; i < context.getLength(); i++) {
//			if (!context.item(i).getLocalName().equals("c")) 
//				throw new ArteException("Badly defined context - <c> missing " );
			System.out.println(((Element)context.item(i)).getAttribute("position") + " : " + context.item(i).getFirstChild().getNodeValue());
			if (((Element)context.item(i)).getAttribute("position").equals("AFTER")) 
				rightContext.add(context.item(i).getFirstChild().getNodeValue());
			else
				leftContext.add(context.item(i).getFirstChild().getNodeValue());
		}
		
	}
	
	public void setLeftContext(Vector<String> leftContext) {
		this.leftContext = leftContext;
	}
	public Vector<String> getLeftContext() {
		return leftContext;
	}

	public void setRightContext(Vector<String> rightContext) {
		this.rightContext = rightContext;
	}
	public Vector<String> getRightContext() {
		return rightContext;
	}
	
	public String toXML() {
		String out = "";
		for (String s:leftContext) {
			out += "<c position='BEFORE'>" +s.replace("&","&amp;")+"</c>";
		}
		for (String s:rightContext) {
			out += "<c position='AFTER'>" +s.replace("&","&amp;")+"</c>";
		}
		return out;
	}
}
