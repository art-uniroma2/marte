package it.uniroma2.art.marte.structures;

public interface SVMClassFormatter {

	public String format(String svm_class);
	
}
