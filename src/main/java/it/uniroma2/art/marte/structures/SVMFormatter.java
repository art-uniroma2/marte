package it.uniroma2.art.marte.structures;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import it.uniroma2.art.marte.GlobalProcessor;


public class SVMFormatter implements Formatter {
	
	private String [] pair_features = null;

	private String [] distributed_pair_features = null;
	
	private String [] similarity_features = null;
	
	private SVMClassFormatter class_formatter = new EntailmentSVMClassFormatter();

	private Options options = new Options();
	
	@SuppressWarnings("unchecked")
	public Collection<Option> getOptions() {
		return (Collection<Option>) options.getOptions();
	}


	@SuppressWarnings("static-access")
	public SVMFormatter() {
		options.addOption(OptionBuilder
				.hasArg()
				.withDescription("comma separated similarity features to appear in the final svm instance")
				.create("svmSimFeats"));
		options.addOption(OptionBuilder
				.hasArg()
				.withDescription("comma separated pair features to appear in the final svm instance")
				.create("svmPairFeats"));
		options.addOption(OptionBuilder
				.hasArg()
				.withDescription("comma separated pair distributed features to appear in the final svm instance")
				.create("svmPairDistFeats"));
		options.addOption(OptionBuilder
				.hasArg()
				.withDescription("producer of the final class or value in the classification example - default: it.uniroma2.art.marte.structures.EntailmentSVMClassFomatter")
				.create("svmClassFormatter"));
	}
	
	public SVMFormatter(CommandLine commands) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		this(commands.getOptionValue("svmPairFeats"),commands.getOptionValue("svmSimFeats"),commands.getOptionValue("svmPairDistFeats"),commands.getOptionValue("svmClassFormatter"));
	}
	
	public SVMFormatter(String pair_features, String similarity_features, String distributed_pair_features, String svm_class_formatter) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		this();
		if (pair_features!=null) this.pair_features = pair_features.split(",");
		if (similarity_features!=null) this.similarity_features = similarity_features.split(",");
		if (distributed_pair_features!=null)  this.distributed_pair_features = distributed_pair_features.split(",");
		if (svm_class_formatter != null) class_formatter = (SVMClassFormatter) Class.forName(svm_class_formatter).newInstance();
	}
	
	
	public String format(EntailmentPair p) {
		
		
		String out = class_formatter.format(p.getOriginalEntailmentValue()) + "\t";
		
		if (pair_features == null) {
			out += "|BT| " + p.getAnalysis("raw_text:cpw").getText().trim().replace(") (",")(") 
			+ " |BT| " + p.getAnalysis("raw_text:cpw").getHypothesis().trim().replace(") (",")(");
	
//			out += "|BT| " + p.getAnalysis("active_span:cpw").getText().trim().replace(") (",")(") 
//			+ " |BT| " + p.getAnalysis("active_span:cpw").getHypothesis().trim().replace(") (",")(");
			
			out += " |BT| " + p.getAnalysis("tdag").getText() 
				+ " |BT| " + p.getAnalysis("tdag").getHypothesis() + " |ET| " ;
		} else {

			for (String feat:pair_features) out += "|BT| " + p.getAnalysis(feat).getText().trim().replace(") (",")(") 
					+ " |BT| " + p.getAnalysis(feat).getHypothesis().trim().replace(") (",")(");
			out += " |ET|";
		}

		
		
		if (similarity_features == null) out += " |BV| " + " 1:" + p.getFeatureValue("distance") ;
		else {
			out += " |BV| "; 
			int i = 1;
			for (String feat:similarity_features) { out += " " + i + ":"  + p.getFeatureValue(feat); i++;}
		}
		
//		out +=  p.getAnalysis("cds:it.uniroma2.cds.SumCDS").getText() 
//		+ " |BV| " + p.getAnalysis("cds:it.uniroma2.cds.SumCDS").getHypothesis() + " |BV| " ;
//
//		out +=  p.getAnalysis("cds:it.uniroma2.cds.SumCDS").getText() 
//		+ " |BV| " + p.getAnalysis("cds:it.uniroma2.cds.SumCDS").getHypothesis() + " |BV| " ;
		
		if (distributed_pair_features != null)
//			out +=  p.getAnalysis("cds:it.uniroma2.cds.TreeSpectrumGenerator").getText() + " |BV| " 
//					+ p.getAnalysis("cds:it.uniroma2.cds.TreeSpectrumGenerator").getHypothesis() + " |EV| ";
//		else 
		{
			for (String feat:distributed_pair_features) { out += " |BV| " + p.getAnalysis(feat).getText() + " |BV| " 
					+ p.getAnalysis(feat).getHypothesis();}
		}
		out += " |EV| ";
		
		return out;
	}


	public void process(EntailmentPairSet corpus_to_process, File outputFile) throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
		int pair_an = 0;
		EntailmentPair pair;
		while ((pair = corpus_to_process.nextPair()) != null) {
			out.write(format(pair) +"\n");
			if (pair_an % 100 == 0) System.err.print("..." + pair_an); 
			pair_an++;
		}
		out.close();
		corpus_to_process.close();
	}

	
}