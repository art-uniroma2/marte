package it.uniroma2.art.marte.structures;

import it.reveal.chaos.xdg.textstructure.Constituent;


public class POS {

public static String type(Constituent h) {
	if (adj(h)) return "a";
	else if (noun(h)) return "n";
	else if (verb(h)) return "v";
	else if (adverb(h)) return "r";
	return "";
}


public static boolean adj(Constituent h){
	String POS = h.getType();
	return POS.equals("JJ") || POS.equals("JJS") || POS.equals("CD") ||
			 POS.startsWith("JJ:") || POS.startsWith("JJS:") || POS.startsWith("CD:") ||
			 POS.startsWith("JJ#") || POS.startsWith("JJS#") || POS.startsWith("CD#") ;
}

public static boolean cong(Constituent h){
	String POS = h.getType();
	return POS.equals("CC") || POS.equals(",") || POS.equals(".") ||
	       POS.startsWith("CC:") || POS.startsWith(",:") || POS.startsWith(".:") ||
	       POS.startsWith("CC#") || POS.startsWith(",#") || POS.startsWith(".#") ;
}

public static boolean noun(Constituent h){
	String POS = h.getType();
	return POS.equals("NN") || POS.equals("NNS") || POS.equals("NNP") || POS.equals("NNPS") ||
	       POS.startsWith("NN:") || POS.startsWith("NNS:") || POS.startsWith("NNP:") || POS.startsWith("NNPS:") ||
	       POS.startsWith("NN#") || POS.startsWith("NNS#") || POS.startsWith("NNP#") || POS.startsWith("NNPS#") ;
}

public static boolean proper_noun(Constituent h){
	String POS = h.getType();
	return POS.equals("NNP") || POS.equals("NNPS") ||
	       POS.startsWith("NNP:") || POS.startsWith("NNPS:") ||
	       POS.startsWith("NNP#") || POS.startsWith("NNPS#") ;
}

public static boolean common_noun(Constituent h){
	String POS = h.getType();
	return POS.equals("NN") || POS.equals("NNS") ||
	       POS.startsWith("NN:") || POS.startsWith("NNS:") ||
	       POS.startsWith("NN#") || POS.startsWith("NNS#") ;
}

public static boolean number(Constituent h){
	String POS = h.getType();
	return POS.equals("CD") ||
	       POS.startsWith("CD:") ||
	       POS.startsWith("CD#") ;
}

public static boolean verb(Constituent h){
	String POS = h.getType();
//RTE	
	return POS.equals("VB") || POS.equals("VBD") || POS.equals("VBG") || POS.equals("VBN") || POS.equals("VBP") || POS.equals("VBZ") ||
	       POS.startsWith("VB:") || POS.startsWith("VBD:") || POS.startsWith("VBG:") || POS.startsWith("VBN:") || POS.startsWith("VBP:") || POS.startsWith("VBZ:") ||
	       POS.startsWith("VB#") || POS.startsWith("VBD#") || POS.startsWith("VBG#") || POS.startsWith("VBN#") || POS.startsWith("VBP#") || POS.startsWith("VBZ#") ;
//AVE	return POS.equals("AUX") || POS.equals("VB") || POS.equals("VBD") || POS.equals("VBG") || POS.equals("VBN") || POS.equals("VBP") || POS.equals("VBZ") ;
}


public static boolean aux(Constituent h){
	String POS = h.getType();
//RTE	return POS.equals("VB") || POS.equals("VBD") || POS.equals("VBG") || POS.equals("VBN") || POS.equals("VBP") || POS.equals("VBZ") ;
//AVE	
	return POS.equals("AUX") ||
	       POS.startsWith("AUX:") ||
	       POS.startsWith("AUX#");
}

public static boolean adverb(Constituent h){
	String POS = h.getType();
	return POS.equals("RB") ||
	       POS.startsWith("RB:") ||
	       POS.startsWith("RB#") ;
}

public static boolean anchor(Constituent h){
	String POS = h.getType();
	return POS.equals("ANC") ||
	       POS.startsWith("ANC:") ||
	       POS.startsWith("ANC#") ;
}


public static boolean relevant(Constituent h) {
//AVE	return POS.adj(h) || POS.noun(h) || POS.verb(h) || POS.aux(h);
//RTE
	return POS.adj(h) || POS.noun(h) || POS.verb(h) || POS.anchor(h);
}


	
}