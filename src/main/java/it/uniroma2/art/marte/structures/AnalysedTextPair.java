package it.uniroma2.art.marte.structures;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.Text;
import it.reveal.chaos.xdg.textstructure.XDG;


public class AnalysedTextPair extends GenericTextPair  {
	
	private String type = "";
	Text textT = null;
	Text hypothesisT = null;
	
	public AnalysedTextPair(String text, String hypothesis, String typeOfAnalysis) {
		super(text, hypothesis);
		type = typeOfAnalysis;
	}
	
	public boolean isType(String typeOfAnalysis) {
		return type.equals(typeOfAnalysis);
	}

	public Text getTextText() {
		return textT;
	}

	public Text getHypothesisText() {
		return hypothesisT;
	}

	public void setTextText(Text t) {
		textT = t;
	}

	public void setHypothesisText(Text t) {
		hypothesisT = t;
	}

	
	public Constituent getTextTree() {
		return ((XDG)textT.getParagraphs().elementAt(0).getXdgs().elementAt(0)).getSetOfConstituents().getElementAt(0);
	}

	public Constituent getHypothesisTree() {
		return ((XDG)hypothesisT.getParagraphs().elementAt(0).getXdgs().elementAt(0)).getSetOfConstituents().getElementAt(0);
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
