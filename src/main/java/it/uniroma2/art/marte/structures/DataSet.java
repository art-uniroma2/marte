package it.uniroma2.art.marte.structures;

import it.reveal.chaos.utils.debug.Debugger;
import it.reveal.chaos.utils.xml.ElementExt;
import it.reveal.chaos.xdg.textstructure.Paragraph;
import it.reveal.chaos.xdg.textstructure.Text;
import it.reveal.chaos.xdg.textstructure.XDG;

import java.util.*;
import java.io.*;

import org.w3c.dom.Element;
import javax.xml.parsers.*;

@Deprecated
public class DataSet {

	Vector _pairs;

	public DataSet() {
		_pairs = new Vector();
	}

	
 /**
 	* Constructs a <code>DataSet</code> from a an original RTE dataset XML file.
 	* Pairs in  the XML file are synt parsed.
 	*/
	public  DataSet(String fileName) {
		try {
			System.out.print("Loading Dataset");
			_pairs = new Vector();
			Debugger.debug(1, "Parsing file: [" + fileName + "]");
			ElementExt root = ElementExt.parse(fileName);
			Vector<Element> pairs = root.getElementChildren("pair");
			for (int i=0; i<pairs.size(); i++) {
				Element _pair = pairs.elementAt(i);
				System.out.print(".");
				Pair p = new Pair(_pair );
				try {
					p.setHText(newText(p.getHypo()));
				} catch(Exception e) {
					System.err.println(p.getId());
				}
				try {
					p.setTText(newText(p.getText()));
				} catch(Exception e) {
					System.err.println(p.getId());
				}
				_pairs.add(p);
			}
		System.out.print("done!\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
 /**
 	* Constructs a <code>DataSet</code> from a an original RTE dataset XML file.
 	* Pairs in  the XML file are plain (plain=true) or syntactically parsed (plain=false).
 	*/
	public  DataSet(String fileName, boolean plain) {
		try {
			System.out.print("Loading Dataset");
			_pairs = new Vector();
			Debugger.debug(1, "Parsing file: [" + fileName + "]");
			ElementExt root = ElementExt.parse(fileName);
			Vector<Element> pairs = root.getElementChildren("pair");
			for (int i=0; i<pairs.size(); i++) {
				Element _pair = pairs.elementAt(i);
				System.out.print(".");
				Pair p = new Pair(_pair );
				if (!plain) p.setHText(newText(p.getHypo()));
				if (!plain) p.setTText(newText(p.getText()));
				_pairs.add(p);
			}
		System.out.print("done!\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
 /**
 	* Constructs a <code>DataSet</code> from a an original RTE dataset XML file.
 	* Only pairs of the specificed tasks are taken.
 	* Pairs in  the XML file are plain (not synt parsed) text.
 	*/
	public  DataSet(String fileName, Vector<String> tasks) {
		try {
			System.out.print("Loading Dataset");
			_pairs = new Vector();
			Debugger.debug(1, "Parsing file: [" + fileName + "]");
			ElementExt root = ElementExt.parse(fileName);
			Vector<Element> pairs = root.getElementChildren("pair");
			if (tasks.elementAt(0).equals("ALL"))
				{
				for (int i=0; i<pairs.size(); i++) 
					{
					Element _pair = pairs.elementAt(i);
					System.out.print(".");
					Pair p = new Pair(_pair );
				try {
					p.setHText(newText(p.getHypo()));
				} catch(Exception e) {
					System.err.println(p.getId());
				}
				try {
					p.setTText(newText(p.getText()));
				} catch(Exception e) {
					System.err.println(p.getId());
				}
					_pairs.add(p);
					}
				}	
			else
				{
				for (int i=0; i<pairs.size(); i++) 
					{
					Element _pair = pairs.elementAt(i);
					if (tasks.indexOf(_pair.getAttribute("task"))!=-1)
						{
						System.out.print(".");
						Pair p = new Pair(_pair );
						p.setHText(newText(p.getHypo()));
						p.setTText(newText(p.getText()));
						_pairs.add(p);
						}
					}
				}
		System.out.print("done!\n");		
		} catch (Exception e) {
			e.printStackTrace();
			}
	}
	


	/**
 	* Creates a <code>Text</code> structure from an input string
 	* of a Charniak analyzed fragment.
 	*/
	public static Text newText(String xdgPennTree) throws Exception {
		Text text = new Text();
		Vector pars = new Vector();
		text.setParagraphs(pars);
		Paragraph p = new Paragraph("empty");
		pars.add(p);
		Vector xdgs = new Vector();
		p.setXdgs(xdgs);
		XDG xdg = new XDG();
		xdgs.add(xdg);
		try {
			xdg.fromPennTree(xdgPennTree);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return text;
	}
	
	
	public void addPair(Pair p) {
		_pairs.add(p);
	}
	
	public void setPair(Pair p,int i) {
		_pairs.setElementAt(p,i);
	}
	
	
	public void addDataSet(DataSet ds) {
		Vector pairsVect = ds.getAllPairs();
		for (int i=0; i<pairsVect.size();i++)
			_pairs.add((Pair)(pairsVect.elementAt(i)));
	}
	

	public Pair getPair(int i) {
		return (Pair)_pairs.get(i);
	}

	public Pair getPairWithId(int id) {
		int sz = _pairs.size(), i=0;
		Pair p=null;
		boolean found = false;
		while (!found && i<sz)
			{
			if ((p=(Pair)_pairs.get(i)).getId()==id)
				found=true;
			i++;
			}		
		if (found) return p;
		return null;
	}

	public Vector getAllPairs() {
		return _pairs;
	}


	public int size() {
		return _pairs.size();
	}
	
	public String toXML(){
		String result = new String();
		result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
		//"<!DOCTYPE entailment-corpus SYSTEM \"RTE_1_2_3.dtd\">\n" + 
		"<entailment-corpus>\n";

		for (int i=0; i<_pairs.size(); i++) {
			result += getPair(i).toXML() + "\n";
		}
		
		result += "</entailment-corpus>\n";

		return result;
	}

	
	public String toString() {
		String result = new String();
		for (int i=0; i<_pairs.size(); i++) {
			result += getPair(i).toString();
		}
		return result;
	}




	public String toStringComplete() {
		String result = new String();
		for (int i=0; i<_pairs.size(); i++) {
			result += getPair(i).toStringComplete();
		}
		return result;
	}
	
	
	public void split(String dirName) {
		for (int i=0; i<_pairs.size(); i++) {
			Pair p = getPair(i);

			String tInName = getTextFileName(dirName, p.getId(), Pair.SType.text);
			String tOutName = getParsedFileName(dirName, p.getId(), Pair.SType.text);
			String hInName = getTextFileName(dirName, p.getId(), Pair.SType.hypo);
			String hOutName = getParsedFileName(dirName, p.getId(), Pair.SType.hypo);

			try {
				File f = new File(tInName);
				f = new File(f.getParent());
				if (!f.exists()) {
					f.mkdirs();
				}
				FileWriter fw = new FileWriter(tInName);
				fw.write(p.getText(), 0, p.getText().length());
				fw.close();
				fw = new FileWriter(hInName);
				fw.write(p.getHypo(), 0, p.getHypo().length());
				fw.close();

			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println(tInName);
			System.out.println(tOutName);
			System.out.println(hInName);
			System.out.println(hOutName);

			System.out.println("\n");

		}
	}

	public static String getParsedFileName(String dirName, int id, int sType) {
		return getFileName(dirName + "\\out", id, sType, "xml");
	}

	public static String getTextFileName(String dirName, int id, int sType) {
		return getFileName(dirName + "\\in", id, sType, "txt");
	}

	private static String getFileName(String dirName, int id, int sType, String ext) {
		String postfix = new String();
		switch (sType) {
			case 1:
				postfix = "_t";
				break;
			case 2:
				postfix = "_h";
				break;
		}
		if (postfix == null) {
			throw new BadSTypeException(sType);
		}
		return dirName + "\\" + id + postfix + "." + ext;
	}


	public static class BadSTypeException extends RuntimeException {
		public BadSTypeException(int sType) {
			super("No such SType defined: [" + sType + "]");
		}
	}
	


}

