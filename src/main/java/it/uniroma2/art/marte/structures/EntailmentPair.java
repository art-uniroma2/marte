package it.uniroma2.art.marte.structures;


import it.reveal.chaos.utils.xml.ElementExt;
import it.uniroma2.art.marte.ArteException;

import java.util.Enumeration;
import java.util.Vector;

import org.w3c.dom.*;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;


public class EntailmentPair {
	
	private String id = "0";
	private String originalEntailmentValue = null ;
	private String predictedEntailmentValue = "empty";
	private float confidence = 0;

	private GenericTextPair rawPair;
		
	private Vector<AnalysedTextPair> pairAnalyses;
	
	private AnalysedTextPair activeActiveAnalysedPair;
	
	private static Instances instances = null;
	
	private Instance instance = null;

	public static void setFeatureSpace(Instances fs) {
		instances = fs;
	}
	
	
	public EntailmentPair() throws Exception {
		//if (instances==null) instances = new Instances("std", new FastVector(), 10000);
		pairAnalyses = new Vector<AnalysedTextPair>();
	}

		
	public EntailmentPair(Element pair) throws Exception {
		this();
		pairAnalyses = new Vector<AnalysedTextPair>();
		ElementExt pair_w = new ElementExt(pair);
		id = pair.getAttribute("id");
		originalEntailmentValue = pair.getAttribute("entailment"); 
		predictedEntailmentValue = pair.getAttribute("predictedentailment");
		rawPair = new GenericTextPair(extractNodeValueFromElementChildren(pair,"t"),extractNodeValueFromElementChildren(pair,"h"));

		if (pair_w.getElementChildren("contextofh")!= null && pair_w.getElementChildren("contextofh").size() !=0) {
			if (pair_w.getElementChildren("contextofh").elementAt(0).getElementsByTagName("c") == null) throw new ArteException("ERROR");
			rawPair.setHypothesisContext(new GenericContext(pair_w.getElementChildren("contextofh").elementAt(0).getElementsByTagName("c")));
		}

		
		if (pair_w.getElementChildren("contextoft")!= null && pair_w.getElementChildren("contextoft").size() != 0) {
			if (pair_w.getElementChildren("contextoft").elementAt(0).getElementsByTagName("c") == null) throw new ArteException("ERROR");
			rawPair.setTextContext(new GenericContext(pair_w.getElementChildren("contextoft").elementAt(0).getElementsByTagName("c")));
		}
		
		if (pair_w.getElementChildren("analyses") != null && pair_w.getElementChildren("analyses").size() > 0) {
			NodeList analyses = pair_w.getElementChildren("analyses").elementAt(0).getElementsByTagName("analysis");
			for (int i=0;i<analyses.getLength();i++) {
				pairAnalyses.add(new AnalysedTextPair(
						extractNodeValueFromElementChildren((Element)analyses.item(i),"t"),
						extractNodeValueFromElementChildren((Element)analyses.item(i),"h"),
						((Element)analyses.item(i)).getAttribute("type")));
			}
		}
		//NScarpato 07/04/2014 added isEmpty clause in the if
		if (pair_w.getElementChildren("features") != null && !pair_w.getElementChildren("features").isEmpty()) {
			String features = pair_w.getElementChildren("features").firstElement().getTextContent();
			if (features != null && !features.trim().equals("")) {
				String [] f_vs = features.split(" ");
				for (String f_v:f_vs) addFeatureValue(f_v.split(":")[0],new Double(f_v.split(":")[1]));
			}
		}
	}

	@Deprecated
	public void addFeature(String name) {
		instances.insertAttributeAt(new Attribute(name), 0);
		
	}
	
	public void addFeatureValue(String name, double value) {
		if (instance == null) {
			instance = new SparseInstance(instances.numAttributes());
			instance.setDataset(instances);
		}
		instance.setValue(instances.attribute(name), value);
	}

	
	public double getFeatureValue(String name) {
		return instance.value(instances.attribute(name));
	}
	
	public AnalysedTextPair getActiveAnalysedPair() {
		return activeActiveAnalysedPair;
	}

	public void setActiveAnalysedPair(String typeOfAnalysis) throws ArteException {
		activeActiveAnalysedPair = null;
		if (pairAnalyses!= null) {
			for (AnalysedTextPair p:pairAnalyses) {
				if (p.isType(typeOfAnalysis)) {
					if (activeActiveAnalysedPair!=null) throw new ArteException("Pair " + getId() + ": Analysis " + typeOfAnalysis + " is present at least twice");
					activeActiveAnalysedPair = p;
				}
			}
		}
		if (activeActiveAnalysedPair==null) throw new ArteException("Pair " + getId() + ": Analysis " + typeOfAnalysis + " unavailable");
	}
	
	public void addAnalysis(AnalysedTextPair p) {
		pairAnalyses.add(p);
	}

	public AnalysedTextPair getAnalysis(String type) {
		AnalysedTextPair p = null;
		for (AnalysedTextPair p1:pairAnalyses) 
			if (p1.isType(type)) p = p1;
		return p;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return id;
	}
	public void setOriginalEntailmentValue(String originalEntailmentValue) {
		this.originalEntailmentValue = originalEntailmentValue;
	}
	public String getOriginalEntailmentValue() {
		return originalEntailmentValue;
	}
	public void setPredictedEntailmentValue(String predictedEntailmentValue) {
		this.predictedEntailmentValue = predictedEntailmentValue;
	}
	public String getPredictedEntailmentValue() {
		return predictedEntailmentValue;
	}
	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}
	public float getConfidence() {
		return confidence;
	}

	public void setRawPair(GenericTextPair rawPair) {
		this.rawPair = rawPair;
	}

	public GenericTextPair getRawPair() {
		return rawPair;
	}
	
	public String toXML() {
		String out = "<pair id=\"" + id + "\" entailment=\"" + originalEntailmentValue + "\" " + "predictedentailment=\"" + predictedEntailmentValue + "\" "+ "confidence=\"" + confidence + "\">\n";
		out += "\t<t>" +rawPair.getText().replace("&","&amp;") + "</t>\n";
		if (rawPair.getTextContext()!=null) 
			out += "\t<contextoft>" +rawPair.getTextContext().toXML() + "</contextoft>\n";
		out += "\t<h>" +rawPair.getHypothesis().replace("&","&amp;") + "</h>\n";
		if (rawPair.getHypothesisContext()!=null) 
			out += "\t<contextofh>" +rawPair.getHypothesisContext().toXML() + "</contextofh>\n";
		if (pairAnalyses != null  && pairAnalyses.size() != 0) {
			out += "\t<analyses>\n"; 
			for (AnalysedTextPair p:pairAnalyses) {
				out += "\t\t<analysis type=\"" + p.getType() +"\">\n";
				out += "\t\t\t<t>" +p.getText().replace("&","&amp;")+ "</t>\n";
				out += "\t\t\t<h>" +p.getHypothesis().replace("&","&amp;")+ "</h>\n";
				out += "\t\t</analysis>\n";
			}
			out += "\t</analyses>\n";
		}
		if (instances != null) {
			out += "\t<features name=\"" + instances.relationName()+ "\">";
			Enumeration<Attribute> atts = (Enumeration<Attribute>) instances.enumerateAttributes();
			while (atts.hasMoreElements()) {
				Attribute a = atts.nextElement();
				out += a.name() + ":" + instance.value(a) + " ";
			}
			out += "</features>\n";
		}
		out += "</pair>";
		
		return out;
	}
	

	public String toOldXMLVersion(int newid) {
		String out = "<pair id=\"" + newid + "\" orgid=\"" + id + "\" entailment=\"" + originalEntailmentValue + "\" " + "predictedentailment=\"" + predictedEntailmentValue + "\" "+ "confidence=\"" + confidence + "\">\n";
		out += "\t<t>" +activeActiveAnalysedPair.getText().replace("&", "&amp;")+ "</t>\n";
		if (rawPair.getTextContext()!=null) 
			out += "\t<contextoft>" +rawPair.getTextContext().toXML() + "</contextoft>\n";
		out += "\t<h>" +activeActiveAnalysedPair.getHypothesis().replace("&", "&amp;")+ "</h>\n";
		if (rawPair.getHypothesisContext()!=null) 
			out += "\t<contextofh>" +rawPair.getHypothesisContext().toXML() + "</contextofh>\n";
		out += "</pair>";
		
		return out;
	}

	public String extractNodeValueFromElementChildren(Element e,String childName) throws ArteException {
		ElementExt pair_w = new ElementExt(e);
		String output;
		if (	pair_w.getElementChildren(childName) != null && pair_w.getElementChildren(childName).size() > 0 &&
				pair_w.getElementChildren(childName).elementAt(0) != null && 
				pair_w.getElementChildren(childName).elementAt(0).getFirstChild() != null)
				output = pair_w.getElementChildren(childName).elementAt(0).getFirstChild().getNodeValue();
		else throw new ArteException(" " + childName + " does not exist or is empty");
		return output;
	}
	
}
