package it.uniroma2.art.marte.structures;

public class EntailmentSVMClassFormatter implements SVMClassFormatter {

	public String format(String svm_class) {
		return  (svm_class.equals("TRUE") || 
						svm_class.equals("YES") || 
						svm_class.equals("ENTAILMENT") ?"1":"-1") + "\t";
	}

}
