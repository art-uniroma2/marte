package it.uniroma2.art.marte;

import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;

import java.io.File;
import java.io.IOException;

public abstract class GlobalProcessor {

	EntailmentPairSet corpusToProcess = null;

	public EntailmentPairSet getCorpusToProcess() {
		return corpusToProcess;
	}

	public void setCorpusToProcess(EntailmentPairSet corpusToProcess) {
		this.corpusToProcess = corpusToProcess;
	}

	public void initializeACorpus(File inputFile, File outputFile, String setreader) throws Exception {
		if (setreader == null) {
			corpusToProcess = new EntailmentPairSet(inputFile,outputFile);
		} else {
			if (!setreader.contains("."))  setreader = "it.uniroma2.art.marte.structures.extension." + setreader;
			corpusToProcess = (EntailmentPairSet) Class.forName(setreader).getConstructor(File.class,File.class).newInstance(inputFile,outputFile);
		}
		
	}
	
	public void processTheCorpus() throws Exception {
		EntailmentPair pair = corpusToProcess.nextPair();
		int pair_an = 0;
		while (pair != null) {
			pair = elaborate(pair);
			corpusToProcess.writePair(pair);
			pair = corpusToProcess.nextPair();
			if (pair_an % 100 == 0) System.err.print("..." + pair_an); 
			pair_an++;
		}
		System.err.println("..." + pair_an);
	}
	
	public void finalizeTheCorpus() throws IOException {
		corpusToProcess.close();
	}

	public void processACorpus(File inputFile, File outputFile, String setreader) throws Exception {
		initializeACorpus(inputFile,outputFile,setreader);
		processTheCorpus();
		finalizeTheCorpus(); 
    }
    
    public abstract EntailmentPair elaborate(EntailmentPair pair) throws Exception; 

}
