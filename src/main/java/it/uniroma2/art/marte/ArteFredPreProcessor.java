package it.uniroma2.art.marte;


import java.io.File;
import java.util.Vector;

import it.uniroma2.art.marte.structures.AnalysedTextPair;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.utils.SimpleSentenceSplitter;
import it.uniroma2.art.marte.preprocessors.FredParser;

/**
 *@author Noemi Scarpato
 * */

public class ArteFredPreProcessor extends GlobalProcessor{
	 FredParser fp = new FredParser();
	 SimpleSentenceSplitter split = new SimpleSentenceSplitter(); 
	  ArteFredPreProcessor() {
		  
	    }
	    
		public EntailmentPair elaborate(EntailmentPair p) throws Exception {
			String text="";
			String hypo="";
			if(p.getRawPair().getText().length()<=150 && p.getRawPair().getHypothesis().length()<=150)
			{
				
				String s= p.getRawPair().getText();
				s=sanitise(s);
				text = fp.parse(s,true);
				Thread.sleep(10000);
				s= p.getRawPair().getHypothesis();
				s=sanitise(s);
				hypo = fp.parse(s,true);
			}else if(p.getRawPair().getText().length()>150 && p.getRawPair().getHypothesis().length()<=150){
				Vector<String> sentences = split.findSentences(p.getRawPair().getText());
				
				for (int i=0;i<sentences.size();i++) {
					String s = sentences.get(i);
					s=sanitise(s);
				//	if(s.length()<=100)
					text = text+ fp.parse(s,true);
					System.out.println("2 text "+text);	
					Thread.sleep(10000);
					
					}
				
				text="(&amp;lt;http://www.ontologydesignpatterns.org/ont/fred/domain.owl#splitted_tree&amp;gt;"+text+")";
				String s= p.getRawPair().getHypothesis();
				s=sanitise(s);
				hypo = fp.parse(s,true);
				Thread.sleep(10000);
			}else if(p.getRawPair().getText().length()<=150 && p.getRawPair().getHypothesis().length()>150){
				Vector<String> sentences = split.findSentences(p.getRawPair().getHypothesis());
				String s= p.getRawPair().getText();
				s=sanitise(s);
				text = fp.parse(s,true);
				Thread.sleep(10000);
				for (int i=0;i<sentences.size();i++) {
					s = sentences.get(i);
					s=sanitise(s);
				//	if(s.length()<=100)
					hypo = hypo+ fp.parse(s,true);
					Thread.sleep(10000);
					System.out.println(hypo);
					}
				hypo="(&amp;lt;http://www.ontologydesignpatterns.org/ont/fred/domain.owl#splitted_tree&amp;gt;"+hypo+")";
			}else{
				Vector<String> sentences = split.findSentences(p.getRawPair().getText());
				System.out.println(sentences);
				for (int i=0;i<sentences.size();i++) {
					String s = sentences.get(i);
					s=sanitise(s);
					text = text+ fp.parse(s,true);
					Thread.sleep(10000);
					System.out.println("3text"+text);
					}
				
				text="(&amp;lt;http://www.ontologydesignpatterns.org/ont/fred/domain.owl#splitted_tree&amp;gt;"+text+")";
				Thread.sleep(10000);
				 sentences = split.findSentences(p.getRawPair().getHypothesis());
				 for (int i=0;i<sentences.size();i++) {
						System.out.println(hypo);
						String s = sentences.get(i);
						s=sanitise(s);
						hypo = hypo+ fp.parse(s,true);
						Thread.sleep(10000);
						System.out.println("hypo"+hypo);
						}
					hypo="(&amp;lt;http://www.ontologydesignpatterns.org/ont/fred/domain.owl#splitted_tree&amp;gt;"+hypo+")";
				
			} 
			System.out.println("Text :"+text);
			System.out.println("Hypo :"+hypo);
			p.addAnalysis(new AnalysedTextPair(text, hypo, "raw_text:fred"));
			return p;
			
		}
		
		
		
		private String sanitise(String s) {
			s=s.replace("'"," ");
			s=s.replace("\""," ");
			return s;
		}

		public static void main(String [] argv) throws Exception {
			File base_dir = new File("C:\\Users\\Noemi\\Desktop\\Noemi\\CollaborazioneConFabio\\EclipseWorkspace\\Arte\\data\\experiments\\preprocessing\\CPW\\");
			//File base_dir = new File("C:\\Users\\Francesco\\Desktop\\Noemi\\CollaborazioneConFabio\\EclipseWorkspace\\Arte\\data\\datasets");
			ArteFredPreProcessor fpp = new ArteFredPreProcessor();
			fpp.processACorpus(new File(base_dir,"RTE3_dev_processedSanitised.xml"), new File(base_dir,"RTE3_dev_processedSanitised_FRED.xml"),null);
			
		}
}
