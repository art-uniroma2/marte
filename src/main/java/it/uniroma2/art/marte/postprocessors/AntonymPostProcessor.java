package it.uniroma2.art.marte.postprocessors;


import java.io.*;

import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import alice.tuprolog.*;

public class AntonymPostProcessor extends PostProcessor{
	
	private Theory _theory;
	private Prolog _engine;

	 
	public AntonymPostProcessor() throws Exception{
			}
	 
	
	public Pair run(Pair p){return null;}
	 
	/** 
		Simply inverts the value of prediction if the rule has success
	*/
	public Pair run(Pair p,SVMpair instance, BufferedWriter bw) throws Exception {
		if (instance.toString().indexOf("#-#")!=-1)
			{
			bw.write("CAPTURED BY ANTONYM POST PROCESSOR  IN false\n");
			p.setValue(false);
			bw.write(p.toXML()+"\n\n");
			}
		return p;
		}	

}
