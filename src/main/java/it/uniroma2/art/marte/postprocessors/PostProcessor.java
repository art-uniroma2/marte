package it.uniroma2.art.marte.postprocessors;


import it.uniroma2.art.marte.structures.Pair;

import java.io.*;


public abstract class PostProcessor{

	
	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public abstract Pair run(Pair p) throws Exception ;

}