package it.uniroma2.art.marte.postprocessors;


import it.uniroma2.art.marte.structures.Pair;

import java.io.*;


public class InversionPostProcessor extends PostProcessor{

	
	/** 
		Simply iverts the value of prediction 
	*/
	public Pair run(Pair p) throws Exception {
		p.setValue(!p.getValue());
		return p;
		}

}