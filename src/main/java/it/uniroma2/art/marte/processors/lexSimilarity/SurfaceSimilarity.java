package it.uniroma2.art.marte.processors.lexSimilarity;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class SurfaceSimilarity extends ConstituentRelationDetector {

private String type = "=";
public SurfaceSimilarity() {}
public SurfaceSimilarity(boolean initialization) {
	this.setNameAndDescription("SURF", "simply compares the tokens");
}

public String relation_type() {return type;}

public float relation_strength(Constituent h, Constituent t) throws Exception {

	float f = 0;
	if (((POS.relevant(h) && POS.relevant(t)) || (POS.adverb(h) && POS.adverb(t))) && 
			(h.getFirstLemma().getSurface().toLowerCase().equals(t.getFirstLemma().getSurface().toLowerCase()) 
			||  h.getSurface().toLowerCase().equals(t.getSurface().toLowerCase()))) {
		f = 1;
	}
	return f;

}

@Override
public void initialize() throws Exception {
	// TODO Auto-generated method stub
	
}


}
