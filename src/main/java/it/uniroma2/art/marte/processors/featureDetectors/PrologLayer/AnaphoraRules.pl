%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
entails(T,H):-
%	tell(paperino),
%	write(T),nl,
%	write(H),nl,
%	told,
	entails_r(T,H).


entails_r(N,N):-
  atom(N).

entails_r(N,N):-
  number(N).


entails_r(T,H):-
  rule(T,H),!.
  
  
entails_r(T,H):-
  T =.. [_,_,_,R1],
  !,
  %write(R1),nl,
  a_sub_part_entails_r(R1,H).
  
a_sub_part_entails_r([],_):- fail.

a_sub_part_entails_r([A1|_],H):-
  entails_r(A1,H),
  !.
  

a_sub_part_entails_r([_|R1],H):-
  !,
  a_sub_part_entails_r(R1,H).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  rule(TEXT_SUBPART,HYPOTHESIS).


%% NOTE:
%% 1. Regole annidate non possono essere catturate (the book which was turined into a script which was done in TV) (vedi ES (**) piu' avanti)
%% 2. Ovviamente va creato un diverso file PL per ogni fenomeno (apposizioni, who, which, whose...)
%
%          
%% *********
%% WHO RULES
%% *********
%

% John McEnroe, who plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                ','(_,_,_,_,l),'SBAR'(_,_,['WHNP'(_,_,['WP'(_,_,who,_,l)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.

% John McEnroe who plays ....  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                'SBAR'(_,_,['WHNP'(_,_,['WP'(_,_,who,_,l)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.


%% *********
%% WHICH RULES
%% *********
%

% John McEnroe, who plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                ','(_,_,_,_,l),'SBAR'(_,_,['WHNP'(_,_,['WDT'(_,_,which,_,l)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.

% John McEnroe who plays ....  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                'SBAR'(_,_,['WHNP'(_,_,['WDT'(_,_,which,_,l)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.



%% *********
%% WHOSE RULES
%% *********
%

% John McEnroe, whose son plays ... , goes... --> John McEnrone goes ... John McEnroe's son plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                ','(_,_,_,_,l),'SBAR'(_,_,['WHNP'(_,_,['WP$'(_,_,whose,_,l),'NP'(Z,_,_)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(_,_,['NP'(X,_,_),'POS'(_,_,_,_,l),'NP'(Z,_,_)]),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.

rule( 'NP'(_,_,['NP'(X,_,_),
                'SBAR'(_,_,['WHNP'(_,_,['WP$'(_,_,whose,_,l),'NP'(Z,_,_)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(_,_,['NP'(X,_,_),'POS'(_,_,_,_,l),'NP'(Z,_,_)]),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.

% John McEnroe, whose son plays ... , goes... --> John McEnrone goes ... The son of John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                ','(_,_,_,_,l),'SBAR'(_,_,['WHNP'(_,_,['WP$'(_,_,whose,_,l),'NP'(Z,_,_)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(_,_,['NP'(Z,_,_),'PP'(X,_,['IN'(_,_,of,of,l)|_])]),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.

rule( 'NP'(_,_,['NP'(X,_,_),
                'SBAR'(_,_,['WHNP'(_,_,['WP$'(_,_,whose,_,l),'NP'(Z,_,_)]),'S'(_,_,['VP'(Y,_,_)])])|_]),
     'S'(_,_,['NP'(_,_,['NP'(Z,_,_),'PP'(X,_,['IN'(_,_,of,of,l)|_])]),'VP'(Y,_,_)|_])):-X \= '-1', Y \= '-1'.



