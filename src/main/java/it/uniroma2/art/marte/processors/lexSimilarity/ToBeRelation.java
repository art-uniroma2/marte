package it.uniroma2.art.marte.processors.lexSimilarity;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class ToBeRelation extends ConstituentRelationDetector {


private String type = "=";


public ToBeRelation() {
}

public ToBeRelation(boolean initialization) {
	this.setNameAndDescription("AUX", "among auxiliary verbs");
}

public String relation_type() {return type;}

public float relation_strength(Constituent t, Constituent h) throws Exception {
	float f = 0;
	System.out.print(">> " +  t.getFirstLemma().getSurface() + " " + h.getFirstLemma().getSurface() + " ");
	if (POS.aux(h) && POS.aux(t) && t.getFirstLemma().getSurface().equals(h.getFirstLemma().getSurface()) ) {
		f = 1;
		//System.out.println("+");
	}
	//System.out.println();
	return f;
}

@Override
public void initialize() throws Exception {
	// TODO Auto-generated method stub
	
}

}