/**
 * 
 */
package it.uniroma2.art.marte.processors.treeRewriters;

import java.util.Vector;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ProperNoun;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.Pair;



/**
 * @author Fabio
 * This TreeRewriter changes a named entity in its class.  
 * It should be used only with the NER preprocessor
 */
public class NamedEntityGeneralizer extends GenericTreeRewriter {

	String [] d = null;
	/** 
	* 
	* 
	*/
	public NamedEntityGeneralizer(){
		this.setNameAndDescription("NG", "changes a named entity in its class");
	}
	
	
	/** 
	implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}
	
	/** 
		generalizes the leaf node according to the generalization dictionary.
		Change the surface form (token) of the leaf.
	*/
	public XDG transform(XDG xdg) {
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				if (sc.getFirstLemma() instanceof ProperNoun) 
					sc.setSurface(((ProperNoun) sc.getFirstLemma()).getNamedEntityCategory());
			} 
		}
	
		return xdg;
	}
	
	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
}


}
