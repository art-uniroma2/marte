package it.uniroma2.art.marte.processors.lexSimilarity;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;


public class NumberSimilarity extends ConstituentRelationDetector {


	private String type = "=";
	
	public NumberSimilarity() {}
	public NumberSimilarity(boolean initialization) {
		this.setNameAndDescription("NU", "determines the relation among numbers");
	}

	public String relation_type() {return type;}

	public float relation_strength(Constituent t, Constituent h) throws Exception {
		float f = 0 ;
		
		if (POS.number(h) && POS.number(t)) {
			try {
				double h_n = (new Double(h.getSurface())).doubleValue();
				double t_n = (new Double(t.getSurface())).doubleValue();
				double diff = h_n - t_n;
				if (diff==0) type = "=";
				else type = (diff>0?">":"<");
				f = (float) (1.0 - Math.abs(diff) /Math.max(h_n,t_n));
			} catch (NumberFormatException e) {} 
			
		}
		
		return f;
	
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
