package it.uniroma2.art.marte.processors;

import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.processors.lexSimilarity.RelationDetectorChain;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.utils.IDF;


 /**
  * This class implements methods for finding anchors between constituents in the Hypothesys
  * and constituents in the Text.
  *
  */
  
public class LexicalAnchorer {
	
	
private RelationDetectorChain lexical_relation_detector = null;
private IDF idf = null;


public LexicalAnchorer(RelationDetectorChain lexical_relation_detector) {
	this.lexical_relation_detector = lexical_relation_detector;
}	


public RelationDetectorChain getLexicalRelationDetector() {
	return lexical_relation_detector;
}


 /**
  * Given the XDG of the Hypothesis and the XDG of the Text, finds an anchor for each 
  * relevant constituent in the Hypothesis (H-constituent). A relevant constituent is
  * a verb, a noun or an adjective.
  *
  * @param hypothesis       the XDG of the hypothesis 
  * @param text             the XDG of the text
  * @return		              the vector of all anchors.
  */ 
public Vector <Anchor> findAnchors(XDG hypothesis,XDG text) throws Exception {
	Vector <Anchor> anchors_first_set = new Vector<Anchor>();
	Vector <Constituent> h_consts =  hypothesis.getSetOfConstituents().getSimpleConstituentList();
	Vector <Constituent> t_consts =  text.getSetOfConstituents().getSimpleConstituentList();
	for (Constituent h_c : h_consts)	
		if (POS.relevant(h_c)) 
			anchors_first_set.add(maxEnquivalentConstituent(h_c,t_consts));
	t_consts =  getTextConstituents(anchors_first_set);
	for (Constituent t_c : t_consts) 
		anchors_first_set = removeMinAnchoredConstituent(t_c,anchors_first_set);
	return anchors_first_set;
}


 /**
  * Given a H-constituent creates an anchor (H-constituent,T-constituent), by finding the
  * T-constituent which is in the most interesting relation with the H-constituent, among all
  * the constituents in the Text. The T-costituent and the strength score of the relation are 
  * found by applying the RelationDetector lexical processor to each constituent in the Text.
  *
  * @param h_c       the H-constituent
  * @param t_consts  the set of constituents in The Text.
  * @return		       the anchor (H-constituent, T-constituent)
  */ 
private Anchor maxEnquivalentConstituent(Constituent h_c, Vector <Constituent> t_consts) throws Exception{
	Anchor a = new Anchor(h_c.getId(),h_c,null,0);
	//System.out.println("H : " + h_c.getSurface());
	for (Constituent t_c : t_consts) {
		//if (POS.relevant(t_c)) {
			float f = lexical_relation_detector.relation_strength(t_c.getGov(),h_c.getGov());
			// ARCHEOLOGIA  float f = similarity(h_c.getGov(),t_c.getGov(),a.getSimilarity());
			if ( f > a.getSimilarity()) {System.out.println("CICCIO!");a.setTConst(t_c);a.setSimilarity(f);a.setType(lexical_relation_detector.getCurrentType());a.setProcessor(lexical_relation_detector.getCurrentProcessorName());}
		//}
	} 
	//System.out.println("Max anchor -> " + a.toString() );
	return a;
}


public Vector <Vector <Anchor>> findAnchorsWithRepetitions(Pair pair) throws Exception {
	return findAnchorsWithRepetitions(pair.getHypoXDG(),pair.getTextXDG());
}

public Vector <Vector <Anchor>> findAnchorsWithRepetitions(XDG hypothesis,XDG text) throws Exception {
	Vector <Vector <Anchor>> anchors_first_set = new Vector<Vector <Anchor>>();
	Vector <Constituent> h_consts =  hypothesis.getSetOfConstituents().getSimpleConstituentList();
	Vector <Constituent> t_consts =  text.getSetOfConstituents().getSimpleConstituentList();
	//System.err.println("QUESTO");
	for (Constituent h_c : h_consts)	if (POS.relevant(h_c)) 
		anchors_first_set.add(maxEnquivalentConstituentWithRepetitions(h_c,t_consts));
	//t_consts =  getTextConstituents(anchors_first_set);
	//
	//for (Constituent t_c : t_consts) anchors_first_set = removeMinAnchoredConstituent(t_c,anchors_first_set);
	return anchors_first_set;
}


private Vector <Anchor> maxEnquivalentConstituentWithRepetitions(Constituent h_c, Vector <Constituent> t_consts) throws Exception{
	Vector <Anchor> panchors = new Vector<Anchor>();
	float f_max = 0 ;
	for (Constituent t_c : t_consts) 
			{
			float f = lexical_relation_detector.relation_strength(t_c.getGov(),h_c.getGov());
			String type = lexical_relation_detector.getCurrentType();
			String processor = lexical_relation_detector.getCurrentProcessorName();
			if ( f > 0) panchors.add(new Anchor(h_c.getId(),h_c,t_c,f,type,processor));
			f_max = (f > f_max ? f : f_max );			
			} 
	Vector <Anchor> anchors = new Vector<Anchor>();
	for (Anchor a: panchors) 
		{if (a.getSimilarity() == f_max) anchors.add(a);}
	if (anchors.size()==0) anchors.add(new Anchor(h_c.getId(),h_c,null,0));
	return anchors;
}


private Anchor maxAnchoredConstituent(Constituent t_c, Vector <Anchor> anchors) throws Exception{
	Anchor na = new Anchor(t_c.getId(),null,t_c,0);
	for (Anchor a : anchors) {
		if (a.getTConst()!=null && a.getTConst().equals(t_c)) {
			float f = a.getSimilarity();
			if ( f > na.getSimilarity()) {na.setHConst(a.getHConst());na.setSimilarity(f);}
		}
	} 
	return na;
}


private Vector <Anchor> removeMinAnchoredConstituent(Constituent t_c,Vector <Anchor> anchors) {
	//System.out.println("------\n" + t_c.getSurface() +  "\n------" );
	Vector <Anchor> new_anchors = new Vector <Anchor>();
	Anchor na = new Anchor(t_c.getId(),null,t_c,0);
	for (Anchor a : anchors) {
		//System.out.print("Examining : " + a );
		if (a.getTConst()!=null && a.getTConst().equals(t_c)) {
			//System.out.print( "<" );
			if ( a.getSimilarity() > na.getSimilarity()) {
				//System.out.println("!");
				na.setHConst(a.getHConst());na.setSimilarity(a.getSimilarity());}
		} else new_anchors.add(a);
	}
	if (na.getHConst() != null) new_anchors.add(na);
	return new_anchors;
}



private Vector <Constituent> getTextConstituents(Vector <Anchor> anchors) throws Exception{
	Vector <Constituent> cs = new Vector<Constituent>();
	for (Anchor a : anchors) if (a.getTConst()!=null) cs.add(a.getTConst());
	return cs;
}



public double globalAnchorBasedSimilarity(Vector <Anchor> anchors) throws Exception {
	double f = 0;
	double f_numerator = 0;
	if (idf==null) idf = new IDF(); 
	for (Anchor a : anchors) {
		//System.out.println("Ancora: " + a.toString() + " IDF(" + a.getHConst().getSurface() + ")="  + idf.getIDF(a.getHConst().getSurface()) ) ; 
		f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) * idf.getIDF(a.getHConst().getSurface());
		f_numerator += (double)idf.getIDF(a.getHConst().getSurface());
		//f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) ;
		//f_numerator ++;
	}
	f = f / f_numerator;
	return f;
}


public double globalAnchorBasedSimilarityWithRepetitions(Vector <Vector <Anchor>> anchors) throws Exception {
	double f = 0;
	double f_numerator = 0;
	if (idf==null) idf = new IDF(); 
	for (Vector <Anchor> a_set : anchors) {
		Anchor a = a_set.elementAt(0);
		//System.out.println("Ancora: " + a.toString() + " IDF(" + a.getHConst().getSurface() + ")="  + idf.getIDF(a.getHConst().getSurface()) ) ; 
		f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) * idf.getIDF(a.getHConst().getSurface());
		f_numerator += (double)idf.getIDF(a.getHConst().getSurface());
		//f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) ;
		//f_numerator ++;
	}
	//f = f / (float) anchors.size();
	f = f / f_numerator;
	return f;
}


public double globalAnchorBasedSimilarityWithRepetitionsNoIDF(Vector <Vector <Anchor>> anchors) throws Exception {
	double f = 0;
	double f_numerator = 0;
	if (idf==null) idf = new IDF(); 
	for (Vector <Anchor> a_set : anchors) {
		Anchor a = a_set.elementAt(0);
		//System.out.println("Ancora: " + a.toString() + " IDF(" + a.getHConst().getSurface() + ")="  + idf.getIDF(a.getHConst().getSurface()) ) ; 
		//f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) * idf.getIDF(a.getHConst().getSurface());
		//f_numerator += (double)idf.getIDF(a.getHConst().getSurface());
		f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) ;
		f_numerator ++;
	}
	//f = f / (float) anchors.size();
	f = f / f_numerator;
	return f;
}


public double globalAnchorBasedSimilarityWithRepetitionsOnlyWords(Vector <Vector <Anchor>> anchors) throws Exception {
	double f = 0;
	double f_numerator = 0;
	if (idf==null) idf = new IDF(); 
	for (Vector <Anchor> a_set : anchors) {
		Anchor a = a_set.elementAt(0);
		//System.out.println("Ancora: " + a.toString() + " IDF(" + a.getHConst().getSurface() + ")="  + idf.getIDF(a.getHConst().getSurface()) ) ; 
		if (a.getSimilarity()>=1) {
			f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) * idf.getIDF(a.getHConst().getSurface());
		}
		f_numerator += (double)idf.getIDF(a.getHConst().getSurface());
		//f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) ;
		//f_numerator ++;
	}
	//f = f / (float) anchors.size();
	f = f / f_numerator;
	return f;
}


public double globalAnchorBasedSimilarityWithRepetitionsOnlyWordsNoIDF(Vector <Vector <Anchor>> anchors) throws Exception {
	double f = 0;
	double f_numerator = 0;
	if (idf==null) idf = new IDF(); 
	for (Vector <Anchor> a_set : anchors) {
		Anchor a = a_set.elementAt(0);
		//System.out.println("Ancora: " + a.toString() + " IDF(" + a.getHConst().getSurface() + ")="  + idf.getIDF(a.getHConst().getSurface()) ) ; 
		if (a.getSimilarity()>=1) {
			//f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) * idf.getIDF(a.getHConst().getSurface());
			//f_numerator += (double)idf.getIDF(a.getHConst().getSurface());
			f += ((double)(a.getSimilarity()<1 ? a.getSimilarity() : 1)) ;
		}
		f_numerator ++;
	}
	//f = f / (float) anchors.size();
	f = f / f_numerator;
	return f;
}



// ARCHEOLOGIA: se
// private float similarity(Constituent h, Constituent t, float max_similarity) throws Exception  {
// il metodo si trova nel file archeology.arc
}