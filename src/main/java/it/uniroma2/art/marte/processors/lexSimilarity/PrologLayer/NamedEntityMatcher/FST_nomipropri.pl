
test(NAME_A,NAME_B):-
	atom_chars(NAME_A,N_A),
	atom_chars(NAME_B,N_B),
	tokenize(N_A,T_N_A),
	tokenize(N_B,T_N_B),
	write(T_N_A),nl,
	write(T_N_B),nl,
	match(T_N_A,T_N_B).

/* RULES */

match([NAME,FAMILY_NAME],[FAMILY_NAME]).
match([NAME,FN,FAMILY_NAME],[FAMILY_NAME]).

match([NAME,FAMILY_NAME],[NAME_B,FAMILY_NAME]):-
	match_or_initials(NAME,NAME_B).
	
match([NAME,MIDDLE_NAME,FAMILY_NAME],[NAME_B,MIDDLE_NAME_B,FAMILY_NAME]):-
	match_or_initials(NAME,NAME_B),
	match_or_initials(MIDDLE_NAME,MIDDLE_NAME_B).

match_or_initials([A|REST],[A]).
match_or_initials([A|REST],[A,'.']).
match_or_initials(A,A).

/*   tokenize(WORDS,TOKENS)   */

tokenize([],[[]]).
tokenize([' '|REST],[[]|TOKENS]):-
	!,tokenize(REST,TOKENS).
tokenize(['.',' '|REST],[['.']|TOKENS]):-
	!,tokenize(REST,TOKENS).
tokenize(['.'|REST],[['.']|TOKENS]):-
	!,tokenize(REST,TOKENS).
tokenize([CHAR|REST],[[CHAR|TOKEN]|TOKENS]):-
	tokenize(REST,[TOKEN|TOKENS]).
	