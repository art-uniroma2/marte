package it.uniroma2.art.marte.processors.lexSimilarity;

import it.uniroma2.art.marte.processors.*;

 /**
  * This interface is used as a generic schema for processors that compute the 
  * strength  of a given relation between two constituent, according to some 
  * linguistic clue (eg, surface or WordNet similarity relation or WordNet
  * antonymy relation).
  */	

public abstract class ConstituentRelationDetector extends Processor implements  ConstituentRelationDetectorBasic  {
	
private float max_similarity = 1;

public float max_similarity_value() {
	return max_similarity;
}
 
}