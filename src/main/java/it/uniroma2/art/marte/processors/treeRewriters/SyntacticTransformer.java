package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import java.io.*;

import alice.tuprolog.*;



public class SyntacticTransformer extends GenericTreeRewriter {


	private Theory _theory;
	private Prolog _engine;
	private boolean tree_with_lemmas = false;	
	private String rule_file;

 /** 
  * Constructs a SyntacticTransformer object by setting private
  *	fields as specified in input parameters.
  *
  * @param rule_file    name and path of the prolog file
	*/
	public SyntacticTransformer(String rule_file) throws Exception {
			_theory = new Theory(new FileInputStream(rule_file));
			System.out.println("RULE FILE:" + rule_file);
			_engine=new Prolog();
		  _engine.setTheory(_theory);
		  
	}


 /** 
  * Constructs a SyntacticTransformer object by setting private
  *	fields as specified in input parameters.
  *
  * @param rule_file    name and path of the prolog file
	*/
	public SyntacticTransformer(String rule_file, boolean tree_with_lemmas) throws Exception {
			this.tree_with_lemmas = tree_with_lemmas;
			_theory = new Theory(new FileInputStream(rule_file));
			System.out.println("RULE FILE:" + rule_file);
			_engine=new Prolog();
		  _engine.setTheory(_theory);
	}

	public SyntacticTransformer(boolean b, String rule_file) {
	// TODO Auto-generated constructor stub
		this.rule_file = rule_file;
	}


	public SyntacticTransformer(boolean b, String rule_file, boolean tree_with_lemmas) {
		// TODO Auto-generated constructor stub
		this.rule_file = rule_file;
		this.tree_with_lemmas = tree_with_lemmas;
	}
	
	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		_theory = new Theory(new FileInputStream(rule_file));
		System.out.println("RULE FILE:" + rule_file);
		_engine=new Prolog();
	  _engine.setTheory(_theory);
		
	}

	


	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}
	
	/** 
	*/
	public XDG transform(XDG xdg) throws Exception{
		System.out.println("tree_with_lemmas:" + tree_with_lemmas);

		Constituent tree = (Constituent) xdg.getSetOfConstituents().elementAt(0);
		Struct input = null;
		if (tree_with_lemmas) input = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmas(tree);
		else input = ConstituentToTuPrologPennTree.toTuPrologPennTree(tree);
		Struct t = new Struct("transform",input,new Var("TRANFORMED_TREE"));
		SolveInfo info = _engine.solve(t);
		if (info.isSuccess()) {
			Struct solution = (Struct)info.getVarValue("TRANFORMED_TREE");
			if (tree_with_lemmas) tree = ConstituentToTuPrologPennTree.fromTuPrologPennTreeWithLemmas(solution);
			else tree = ConstituentToTuPrologPennTree.fromTuPrologPennTree(solution);
			xdg.getSetOfConstituents().setElementAt(tree,0);
		} else {Struct solution = (Struct)info.getVarValue("TRANFORMED_TREE"); System.out.println("SOLUTION:"+solution);}
		//throw new Exception("Error in : " + xdg.toPennTree().toString());
				
		return xdg;
	}



 /** 
  * Explains input parameters
  */
	private static void usage()
		{
		System.out.println("\n");
		System.out.println("Usage: SyntacticTransformer <rule file> ");
		System.out.println("                   [-if  <XML RTE charniak input file>]");
		System.out.println("                   [-it <charniak text>]");
		System.out.println("\n");
		System.out.println(" where <rule file> is the prolog file containing the transformation rules");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" Options are:");
		System.out.println("\n");
		System.out.println("   -it <input file> is the input file containing the RTE pairs in XML format and parsed by Charniak");
		System.out.println("   -if <charniak text> is an input text as a syntactic tree in Charniak format");
		System.out.println("\n\n");
		System.exit(0);
		}	
		


 /** 
  * \brief Lunches stand-alone SyntacticTransformer
  */
	public static void main(String [] argv) {
		try {
		 	if (argv.length!=3) usage();
			SyntacticTransformer st = new SyntacticTransformer(argv[0]);
			if (argv[1].equals("-if"))
				{
				DataSet ds = new DataSet(argv[2]);
				for (int i=0; i<ds.getAllPairs().size();i++)
					{
					Pair pair = ds.getPair(i);
					XDG xdg_t = pair.getTextXDG();	
					XDG xdg_t1 = st.transform(xdg_t);
					//if (xdg_t != xdg_t1)
					System.out.println("ORIGINAL TEXT:\n" + xdg_t.toPennTree() + "\nTRANSFORMED TEXT:" + xdg_t1.toPennTree());
					XDG xdg_h = pair.getHypoXDG();	
					XDG xdg_h1 = st.transform(xdg_h);
					//if (xdg_t != xdg_h1)
					System.out.println("ORIGINAL TEXT:\n" + xdg_h.toPennTree() + "\nTRANSFORMED TEXT:" + xdg_h1.toPennTree());
					}	
				}
			else if (argv[1].equals("-it"))
				{
				XDG xdg = new XDG();
				xdg.fromPennTree(argv[2]);
				xdg = st.transform(xdg);
				System.out.println("TRANSFORMED TEXT:\n" + xdg.toPennTree().toString());
				}
			else usage();	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
	}



}