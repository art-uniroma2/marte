package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class VerbLexicalEntailment extends ConstituentRelationDetector {

private Hashtable<String,Float> entailmentVerbs = null;

private String type = "e";
private String verb_entailment_file;

public VerbLexicalEntailment(String file) throws Exception {
	loadEntailmentVerbs(file);
}

public VerbLexicalEntailment(boolean initialize, String verb_entailment_file) {
	this.setNameAndDescription("VBEN", "links verbs in entailments");
	this.verb_entailment_file = verb_entailment_file;
}

@Override
public void initialize() throws Exception {
	loadEntailmentVerbs(verb_entailment_file);
}

public String relation_type() {return type;}

public float relation_strength(Constituent t, Constituent h) throws Exception {
	float f = 0;
	//System.out.print(">> " +  t.getFirstLemma().getSurface() + " " + h.getFirstLemma().getSurface() + " ");
	if (POS.verb(h) && POS.verb(t) && entailmentVerbs.containsKey(t.getFirstLemma().getSurface() + " " + h.getFirstLemma().getSurface())) {
		f = 1;
		//System.out.println("+");
	}
	//System.out.println();
	return f;
}


private void loadEntailmentVerbs(String file) throws Exception {
	entailmentVerbs = new Hashtable<String,Float>();
	System.out.println("Loading derivational verbs in entailment");
	String line = "";
	int line_no = 0;
	try {
		BufferedReader in = new BufferedReader(new FileReader(file));
		line = in.readLine();
		line_no = 0;
		while (line != null) {
			StringTokenizer st = new StringTokenizer(line,",");
			String pair = st.nextToken().trim() + " "+ st.nextToken().trim();
			//System.out.println(pair + " - " );
			Float f = new Float(1);
			entailmentVerbs.put(pair,f);
			line = in.readLine();
			line_no++;
		}
		in.close();
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	} catch (NoSuchElementException e) {
		System.out.println("ERROR in line : " + line_no + " <" + line +">");
		throw e;
	}
}


}