package it.uniroma2.art.marte.processors.featureDetectors;

import it.uniroma2.art.marte.sha2arte.*;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import java.util.*;
import java.io.*;




public class FramenetRuleDetector extends OldGenericFeaturesDetector {
		
		Hashtable<Integer,String> _sentences;
		int _num;
		
 /** 
  * Constructs a SyntacticTransformer object by setting private
  *	fields as specified in input parameters.
  *
  * @param rule_file    name and path of the prolog file
	*/
	public FramenetRuleDetector(String feature_set, String base_feature_name, int num_features, String shaFile, boolean detour) throws Exception {
		  super(feature_set, base_feature_name,num_features);
			_num = num_features;
		  featureExtractor fe = new featureExtractor(shaFile,detour);
			System.out.println("***INTIALIZING FRAMENET FEATURES DATABASE ***");
			System.out.println("SHALMANESER FILE:" + shaFile);
			System.out.println("Please wait, processing...");
			Hashtable<Integer,String> _sentences = fe.apply();
	}

	
	/** 
	*/
	public void computeFeatures(Pair p, SVMpair instance) throws Exception {
		String sentFeat = _sentences.get(new Integer(p.getId()));
		Vector<String> names = getNames();
		int i=0;
		if (sentFeat!=null)
			{	
			StringTokenizer st = new StringTokenizer(sentFeat,"\t");
			String feat=null;
			while (st.hasMoreTokens())
				{
				feat=st.nextToken();	
				instance.setFeatureValue(names.elementAt(i),feat,getSet());	
				i++;
				}
			}
		else {System.out.println("ERROR, CANNOT FIND SHALMANESER SENTENCE FOR ID: " +p.getId());System.exit(-1);}
		} 		




}