package it.uniroma2.art.marte.processors.featureDetectors;

import it.uniroma2.art.marte.processors.CascadeOfProcessors;
import it.uniroma2.art.marte.processors.Processor;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

@Deprecated
public class OldFeatureDetectorChain extends CascadeOfProcessors {
	
	private Vector <OldGenericFeatureDetector> fs = new Vector () ;
	private Vector <OldGenericFeaturesDetector> fss = new Vector () ;
	
	public OldFeatureDetectorChain() {}
	
	public void add(OldGenericFeatureDetector feature_detector) {
		fs.add(feature_detector);
	}
	
	public void add(OldGenericFeaturesDetector feature_detector) {
		fss.add(feature_detector);
	}

	@Deprecated
	public void computeFeatures(Pair p, SVMpair instance) {
		try{
		for (OldGenericFeatureDetector f: fs) f.computeFeature(p,instance);
		for (OldGenericFeaturesDetector f: fss) f.computeFeatures(p,instance);
		}catch (Exception e){
			e.printStackTrace();
			System.err.println("ERROR IN EXECUTING FEATURE COMPUTATION");System.exit(-1);}
	}

	public EntailmentPair computeFeature(EntailmentPair p) throws Exception {
		for (Processor proc:this.getActiveProcessors()) 
			p = ((OldGenericFeatureDetector)proc).computeFeature(p); 
		
		return p;
	}

}