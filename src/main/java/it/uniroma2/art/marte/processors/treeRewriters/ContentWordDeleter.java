package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;


import java.util.*;

/** 
	deletes nodes with content words (all expect DT, MD, IN)
*/
public class ContentWordDeleter extends GenericTreeRewriter {
	Vector<String> d = null;

	public ContentWordDeleter() {
		d = new Vector<String>();
		d.add("DT");d.add("MD");d.add("IN");
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		generalizes the leaf node according to the generalization dictionary.
		Change the surface form (token) of the leaf.
	*/
	public XDG transform(XDG xdg) {
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				boolean contains = false;
				for (String pos:d) if (sc.getType().equals(pos) || sc.getType().startsWith(pos+":")) contains = true ;
				if (!contains) sc.setSurface("x");
			} 
		}

		return xdg;
	}

	@Override
	public void initialize() throws Exception {
		
	}
	

}