/**
 * 
 */
package it.uniroma2.art.marte.processors;

/**
 * @author Fabio Massimo Zanzotto
 *
 */
public abstract class Processor {
	private String description;
	private String name = "#ACTIVE#";

	public abstract void initialize() throws Exception;
	
	public Processor setNameAndDescription(String name,String description) {
		setName(name);
		setDescription(description);
		return this;
	}
	
	public Processor setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Processor setName(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name + ": " + description;
	}
}
