package it.uniroma2.art.marte.processors.lexSimilarity;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class DateRelationDetector extends ConstituentRelationDetector {


	private String type = "=";
	private SimpleDateFormat date_estractor = new SimpleDateFormat("MM/dd/yy");
	
	
	public DateRelationDetector() {}
	public DateRelationDetector(boolean initialiazation) {
		this.setNameAndDescription("DATE", "determines relations among dates expressed in canonical formats (see the Date java class)");
	}

	public String relation_type() {return type;}

	public float relation_strength(Constituent t, Constituent h) throws Exception {
		float f = 0 ;
		
		if (POS.number(h) && POS.number(t)) {
			try {
				Date h_d = date_estractor.parse(h.getSurface());
				Date t_d = date_estractor.parse(t.getSurface());
				if (h_d != null && t_d != null) {
					int diff = h_d.compareTo(t_d);
					if (diff==0) {
						f = (float) 1.0;
						type = "=";
					} else {
						f = (float) 0.5;
						type = (diff>0?">":"<");
					}
				}
			} catch (ParseException e) {} 
			
		}
		
		return f;
	
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
