/**
 * 
 */
package it.uniroma2.art.marte.processors.treeRewriters;

import java.util.Vector;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.Pair;

/**
 * @author Fabio
 * This TreeRewriter removes from the trees the words in the leaves 
 * if these words belong to a specific syntactic class
 */
public class WordDeleter extends GenericTreeRewriter {

	String [] d = null;
	/** 
	* 
	* @param postags_to_be_deleted: a comma separated list of pos tags whose words should be 
	* deleted
	*/
	public WordDeleter(String postags_to_be_deleted){
		d = postags_to_be_deleted.split(",");
		this.setNameAndDescription("WD", "deletes words belonging to specific pos tags");
	}
	
	
	/** 
	implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}
	
	/** 
		generalizes the leaf node according to the generalization dictionary.
		Change the surface form (token) of the leaf.
	*/
	public XDG transform(XDG xdg) {
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				boolean contains = false;
				for (String pos:d) if (sc.getType().equals(pos) || sc.getType().startsWith(pos+":")) contains = true ;
				if (contains) sc.setSurface("x");
			} 
		}
	
		return xdg;
	}
	
	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
}


}
