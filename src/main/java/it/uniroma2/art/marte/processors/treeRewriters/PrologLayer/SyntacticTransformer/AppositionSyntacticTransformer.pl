
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transform(T,TT):-
  tell(pippo),write(T),nl,
  divide(T,TT_R,A),
  !,
  compose_sent(TT_R,A,TT),
  write(TT),nl,told.


compose_sent(T,[],T):-!.
compose_sent(TT_R,A,TT):-!,
	TT =.. ['S1',9999,TT_R|A].



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE ALGPRITHM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

divide(N,N,[]):-
  atom(N).

divide(N,N,[]):-
  number(N).


divide(N1,N,SENTENCES):-
  rule(N1,N,SENTENCES),!.
  
  
divide(N1,N2,SENTENCES):-
  N1 =.. [A,ID|R1],
  !,
  list_divided(R1,R2,SENTENCES),
  N2 =.. [A,ID|R2].
  

list_divided([],[],[]).

list_divided([A1|R1],[A2|R2],ALL_SENTECES):-
  divide(A1,A2,SENTS),
  !,
  list_divided(R1,R2,SENT_REST),
  append(SENTS,SENT_REST,ALL_SENTECES).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  rule(MATCH,WHAT_REMAINS_HERE,SENTENCE_TO_BE_PUT_AT_THE_TOP).


%% NOTE (Marco, 14-jan-2007):
%% 1. Rules for persons and ogranization are actually the same. We could maybe put them together
%% 2. We could do the same for Location, but is more risky, as they are often on top of the fragments as: "Roma, 15 April."
%% 3. We don't get (seldom) cases as the following: (S (NP (PRP He))(VP (VBD met)(NP (NNP_LOC U.S.)(NNP President)(, ,)(NNP_PER George)(NNP_PER W.)(NNP_PER Bush))(, ,)

%% *****************
%% PERSON APPO RULES
%% *****************


% John McEnroe, the tennis champion, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUB =.. ['NP'|REST4].
         


% the tennis champion, John McEnroe, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUP =.. ['NP'|REST4].

%(S (NP (NP (CD_ORG ECB)(NN spokeswoman))(, ,)(NP (NNP_PER Regina)(NNP_PER Schueller))(, ,))(VP (VBD declined)(S (VP (TO to)(VP (VB comment)(PP (IN on)(NP (NP (DT a)(NN report))(PP (IN in)(NP (NP (NNP_LOC Italy)(POS 's))(NNP_ORG La)(NNP_ORG Repubblica)(NN newspaper)))(SBAR (IN that)(S (NP (DT the)(NNP_ORG ECB)(NN council))(VP (MD will)(VP (VB discuss)(NP (NP (NP (NNP_PER Mr.)(NNP_PER Fazio)(POS 's))(NN role))(PP (IN in)(NP (DT the)(NN takeover)(NN fight))))(PP (IN at)(NP (PRP$ its)(NAC (NNP Sept.)(CD 15))(NN meeting)))))))))))))(. .))          


% John McEnroe, the tennis champion, PP wins ... --> John McEnrone PP wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),PP),
     'NP'(ID_MAIN,NP_SUP,PP),['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUB =.. ['NP'|REST4].    


% the tennis champion, John McEnroe, PP wins ... --> John McEnrone PP wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),PP),
     'NP'(ID_MAIN,NP_SUB,PP),['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUP =.. ['NP'|REST4].

% (S (NP (PRP He))(VP (VBD met)(NP (NNP_LOC U.S.)(NNP President)(, ,)(NNP_PER George)(NNP_PER W.)(NNP_PER Bush))(, ,)(PP (IN in)(NP (NP (NNP_LOC Washington))(CC and)(NP (NP (NNP_LOC British)(NNP Prime)(NNP Minister))(, ,)(NP (NNP_PER Tony)(NNP_PER Blair))(, ,)(PP (IN in)(NP (NNP_LOC London)))))))(. .))        



% ... John McEnroe, the tennis champion. --> ... John McEnrone. John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUB =.. ['NP'|REST4].      


% the tennis champion, John McEnroe.--> ... John McEnrone . John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
      NP_SUP =.. ['NP'|REST4]. 



%% John McEnroe, the tennis champion, who won ... --> John McEnrone who won ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
     'NP'(ID_MAIN,NP_SUP,SBAR),['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUB =.. ['NP'|REST2].
         


%% the tennis champion, John McEnroe, who won... --> John McEnrone who won ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
     'NP'(ID_MAIN,NP_SUB,SBAR),['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUP =.. ['NP'|REST2].


% John McEnroe (the tennis champion) wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,'-LRB-'(_,'-LRB-',l),NP_SUB,'-RRB-'(_,'-RRB-',l)),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUB =.. ['NP'|REST4].
         


% the tennis champion (John McEnroe) wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,'-LRB-'(_,'-LRB-',l),NP_SUB,'-RRB-'(_,'-RRB-',l)),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_PER'|REST3],
     NP_SUP =.. ['NP'|REST4].


%% ***********************
%% ORGANIZATION APPO RULES
%% ***********************


% John McEnroe, the tennis champion, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUB =.. ['NP'|REST4].
         


% the tennis champion, John McEnroe, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUP =.. ['NP'|REST4].

%(S (NP (NP (CD_ORG ECB)(NN spokeswoman))(, ,)(NP (NNP_PER Regina)(NNP_PER Schueller))(, ,))(VP (VBD declined)(S (VP (TO to)(VP (VB comment)(PP (IN on)(NP (NP (DT a)(NN report))(PP (IN in)(NP (NP (NNP_LOC Italy)(POS 's))(NNP_ORG La)(NNP_ORG Repubblica)(NN newspaper)))(SBAR (IN that)(S (NP (DT the)(NNP_ORG ECB)(NN council))(VP (MD will)(VP (VB discuss)(NP (NP (NP (NNP_PER Mr.)(NNP_PER Fazio)(POS 's))(NN role))(PP (IN in)(NP (DT the)(NN takeover)(NN fight))))(PP (IN at)(NP (PRP$ its)(NAC (NNP Sept.)(CD 15))(NN meeting)))))))))))))(. .))          


% John McEnroe, the tennis champion, PP wins ... --> John McEnrone PP wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),PP),
     'NP'(ID_MAIN,NP_SUP,PP),['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUB =.. ['NP'|REST4].    


% the tennis champion, John McEnroe, PP wins ... --> John McEnrone PP wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),PP),
     'NP'(ID_MAIN,NP_SUB,PP),['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUP =.. ['NP'|REST4].

% (S (NP (PRP He))(VP (VBD met)(NP (NNP_LOC U.S.)(NNP President)(, ,)(NNP_PER George)(NNP_PER W.)(NNP_PER Bush))(, ,)(PP (IN in)(NP (NP (NNP_LOC Washington))(CC and)(NP (NP (NNP_LOC British)(NNP Prime)(NNP Minister))(, ,)(NP (NNP_PER Tony)(NNP_PER Blair))(, ,)(PP (IN in)(NP (NNP_LOC London)))))))(. .))        





% ... John McEnroe, the tennis champion. --> ... John McEnrone. John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUB =.. ['NP'|REST4].      


% the tennis champion, John McEnroe.--> ... John McEnrone . John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
      NP_SUP =.. ['NP'|REST4]. 



%% John McEnroe, the tennis champion, who won ... --> John McEnrone who won ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
     'NP'(ID_MAIN,NP_SUP,SBAR),['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUB =.. ['NP'|REST2].
         


%% the tennis champion, John McEnroe, who won... --> John McEnrone who won ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
     'NP'(ID_MAIN,NP_SUB,SBAR),['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUP =.. ['NP'|REST2].

% John McEnroe (the tennis champion) wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,'-LRB-'(_,'-LRB-',l),NP_SUB,'-RRB-'(_,'-RRB-',l)),
     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
     NP_SUP =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUB =.. ['NP'|REST4].
         


% the tennis champion (John McEnroe) wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
rule( 'NP'(ID_MAIN,NP_SUP,'-LRB-'(_,'-LRB-',l),NP_SUB,'-RRB-'(_,'-RRB-',l)),
     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
     PROP_NAME =.. ['NNP_ORG'|REST3],
     NP_SUP =.. ['NP'|REST4].


%:-transform( 'S1'(82,'S'(81,'NP'(48,'NP'(2,'NNP'(1,'Helena',l),'NNP'(2,'Brighton',l)),','(4,',',l),'NP'(47,'NP'(6,'DT'(5,an,l),'NN'(6,attorney,l)),'PP'(11,'IN'(8,for,l),'NP'(10,'NNP'(9,'Eliza',l),'NNP'(10,'May',l))),'SBAR'(46,'WHNP'(13,'WP'(13,who,l)),'S'(45,'VP'(44,'VBD'(15,served,l),'PP'(43,'IN'(16,as,l),'NP'(42,'NP'(18,'JJ'(17,executive,l),'NN'(18,director,l)),'PP'(41,'IN'(20,of,l),'NP'(40,'NP'(29,'DT'(21,the,l),'ADJP'(23,'NNP'(22,'Texas',l),'JJ'(23,'Funeral',l)),'NNP'(25,'Services',l),'NNP'(26,'Commission',l),'NN'(27,'-the',l),'NN'(28,state,l),'NN'(29,agency,l)),'SBAR'(39,'WHNP'(31,'WDT'(31,that,l)),'S'(38,'VP'(37,'VBZ'(33,regulates,l),'NP'(36,'DT'(34,the,l),'JJ'(35,funeral,l),'NN'(36,'business-',l)))))))))))))),'VP'(79,'VBD'(50,claimed,l),'SBAR'(78,'IN'(51,that,l),'S'(77,'NP'(52,'PRP'(52,she,l)),'VP'(76,'AUX'(54,was,l),'VP'(75,'VBN'(55,fired,l),'PP'(60,'IN'(56,from,l),'NP'(59,'PRP$'(57,her,l),'NN'(58,state,l),'NN'(59,job,l))),'SBAR'(74,'IN'(62,because,l),'S'(73,'NP'(63,'PRP'(63,she,l)),'VP'(72,'VBD'(65,raised,l),'NP'(71,'NP'(66,'NNS'(66,questions,l)),'PP'(70,'IN'(68,about,l),'NP'(69,'NNP'(69,'SCI',l)))))))))))),'.'(81,'.',l))),T).
