package it.uniroma2.art.marte.processors.featureDetectors;

import org.apache.commons.cli.CommandLine;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;

public interface FeatureDetector {
	
	public abstract EntailmentPair computeFeature(EntailmentPair p) throws Exception;
	public abstract void addFeature(EntailmentPairSet p) throws Exception;
	public abstract void initialize(CommandLine commands);


}
