%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
entails(T,H):-
%	tell(paperino),
%	write(T),nl,
%	write(H),nl,
%	told,
	entails_r(T,H).


entails_r(N,N):-
  atom(N).

entails_r(N,N):-
  number(N).


entails_r(T,H):-
  rule(T,H),!.
  
  
entails_r(T,H):-
  T =.. [_,_,_,R1],
  !,
  %write(R1),nl,
  a_sub_part_entails_r(R1,H).
  
a_sub_part_entails_r([],_):- fail.

a_sub_part_entails_r([A1|_],H):-
  entails_r(A1,H),
  !.
  

a_sub_part_entails_r([_|R1],H):-
  !,
  a_sub_part_entails_r(R1,H).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  rule(MATCH,WHAT_REMAINS_HERE,SENTENCE_TO_BE_PUT_AT_THE_TOP).


%% NOTE (Marco, 14-jan-2007):
%% 1. Rules for persons and ogranization are actually the same. We could maybe put them together
%% 2. We could do the same for Location, but is more risky, as they are often on top of the fragments as: "Roma, 15 April."
%% 3. We don't get (seldom) cases as the following: (S (NP (PRP He))(VP (VBD met)(NP (NNP_LOC U.S.)(NNP President)(, ,)(NNP_PER George)(NNP_PER W.)(NNP_PER Bush))(, ,)

%% *****************
%% PERSON APPO RULES
%% *****************


% John McEnroe, who plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'NP'(_,_,['NP'(X,_,_),
                ','(_,_,_,_,l),'NP'(Y,_,_)|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,['AUX'(_,_,is,_,l),'NP'(Y,_,_)])|_])):- X \= '-1', Y \= '-1'.


rule( 'NP'(_,_,['NP'(Y,_,_),
                ','(_,_,_,_,l),'NP'(X,_,_)|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,['AUX'(_,_,is,_,l),'NP'(Y,_,_)])|_])):- X \= '-1', Y \= '-1'.



% John McEnroe (the tennis champion) wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
         
rule( 'NP'(_,_,['NP'(X,_,_),
                '-LRB-'(_,_,_,_,l),'NP'(Y,_,_)|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,['AUX'(_,_,is,_,l),'NP'(Y,_,_)])|_])):-X \= '-1', Y \= '-1'.


rule( 'NP'(_,_,['NP'(Y,_,_),
                '-LRB-'(_,_,_,_,l),'NP'(X,_,_)|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,['AUX'(_,_,is,_,l),'NP'(Y,_,_)])|_])):-X \= '-1', Y \= '-1'.

