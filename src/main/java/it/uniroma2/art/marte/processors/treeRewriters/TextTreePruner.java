package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

/** 
	eliminates irrelevant information in the syntactic tree of the Text.
	Relevant leaves must be marked a #r# in the morphological features.
	To mark a leaf as relevant, use the static method TextTreePruner.setRelevant.
*/
public class TextTreePruner extends GenericTreeRewriter {
	
	public TextTreePruner() {
	}


	public TextTreePruner(boolean intialiazation) {
		// TODO Auto-generated constructor stub
		this.setNameAndDescription("CUT", "prunes irreleant parts of the \"TEXT\" syntactic tree");
	}

	/** 
		eliminates irrelevant information in the Text syntactic tree.
		Relevant leaves must be marked a #r# in the morphological features.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		return p;
	}

	/** 
		eliminates irrelevant information in the syntactic tree.
		Relevant leaves must be marked a #r# in the morphological features.
	*/
	public XDG transform(XDG xdg) throws Exception{

		//System.out.println("Pruning....");
		Vector <Constituent> cs = xdg.getSetOfConstituents() ;
		for ( Constituent c : cs ) prune(c);

		return xdg;
	}


	private boolean prune(Constituent tree) {
		boolean keep = false;
		if ((tree instanceof SimpleConst) && isRelevant((SimpleConst) tree)) keep = true;
		else if (!(tree instanceof SimpleConst)) {
			Vector <Constituent> subs = ((ComplxConst)tree).getSubConstituents();
			ConstituentList newSubs = new ConstituentList();
			for (Constituent c : subs) {
				boolean keep_this = prune(c);
				//if (keep_this || (c instanceof SimpleConst && !POS.relevant(c)) ) newSubs.addElement(c);
				if (keep_this || (c instanceof SimpleConst) ) newSubs.addElement(c);
				keep = keep || keep_this;
			}
			((ComplxConst)tree).setSubConstituents(removeCongSeq(newSubs));
		}
		return keep;
	}

	private ConstituentList removeCongSeq(Vector <Constituent> consts) {
		ConstituentList new_cs = new ConstituentList();
		Constituent last_coord = null;
		for (Constituent c : consts) {
			if (new_cs.size() == 0) {
				//System.out.println("T : " + c.getType());
				if (!POS.cong(c)) new_cs.add(c); 
			} else {
				if (POS.cong(c)) {
					last_coord = c;				
				} else {
					if (last_coord != null) {new_cs.add(last_coord); last_coord=null;}
					new_cs.add(c);
				}			
			}
		}
		return new_cs;
	}


	public static boolean isRelevant(SimpleConst tree) {
		return tree.getFirstLemma().getMorphologicalFeatures().equals("#r#");
	}

	public static void setRelevant(SimpleConst tree) {
		tree.getFirstLemma().setMorphologicalFeatures("#r#");
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}


}