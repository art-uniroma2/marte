package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

public class SuperMatcherAndReducer extends GenericTreeRewriter {
	
	int anchors = 0;
	public SuperMatcherAndReducer() {
		// TODO Auto-generated constructor stub
	}

	public SuperMatcherAndReducer(boolean initialiazation) {
		// TODO Auto-generated constructor stub
		this.setNameAndDescription("RED", "substitues with ANC_# the complex constituents that H shares with T");
	}

	public Pair transform(Pair p) {
		anchors = 0;
		Constituent txt = (Constituent) p.getTextXDG().getSetOfConstituents().elementAt(0);
		Constituent hyp = (Constituent) p.getHypoXDG().getSetOfConstituents().elementAt(0);
		reduce(txt,hyp);
		return p;
	}

	public XDG transform(XDG p) {
		return p;
	}

	public void reduce(Constituent txt, Constituent hyp) { 
		if (hyp instanceof ComplxConst && txt instanceof ComplxConst) {
			//System.out.println("H : " + hyp.getSurface());
			//System.out.println("T : " + txt.getSurface());
			if (hyp.getSurface().indexOf(txt.getSurface()) >= 0) {
				//System.out.println("YEEE");
				find(txt,hyp);
			} else {
				Vector <Constituent> subs = ((ComplxConst) txt).getSubConstituents() ;
				for (Constituent s:subs) reduce(s,hyp);
			}
		}
	}
	
	private void find(Constituent txt,Constituent hyp) {
		if (hyp instanceof ComplxConst ){
			if (txt instanceof ComplxConst && hyp.getSurface().equals(txt.getSurface())) {
				SimpleConst c = new SimpleConst("ANC"+anchors,"ANC",anchors);
				ConstituentList l = new ConstituentList();
				l.add(c);
				((ComplxConst) hyp).setSubConstituents(l);
				c = new SimpleConst("ANC"+anchors,"ANC",anchors);
				l = new ConstituentList();
				l.add(c);
				((ComplxConst) txt).setSubConstituents(l);
				anchors++;
			} else {
				Vector <Constituent> subs = ((ComplxConst) hyp).getSubConstituents();
				for (Constituent s:subs) {
					find(txt,s);
				}
			}
		}
	}
	
	
	public static void main(String [] argv) throws Exception {
		XDG xdg1 = new XDG();
		XDG xdg2 = new XDG();
		xdg1.fromPennTree("(S1 (S (NP (NP (CD ECB) (NN spokeswoman)) (, ,) (NP (NNP Regina) (NNP Schueller)) (, ,)) (VP (VBD declined) (S (VP (TO to) (VP (VB comment) (PP (IN on) (NP (NP (DT a) (NN report)) (PP (IN in) (NP (NP (NNP Italy) (POS &apos;s)) (NNP La) (NNP Repubblica) (NN newspaper))) (SBAR (IN that) (S (NP (DT the) (NNP ECB) (NN council)) (VP (MD will) (VP (VB discuss) (NP (NP (NP (NNP Mr.) (NNP Fazio) (POS &apos;s)) (NN role)) (PP (IN in) (NP (DT the) (NN takeover) (NN fight)))) (PP (IN at) (NP (PRP$ its) (NAC (NNP Sept.) (CD 15)) (NN meeting))))))))))))) (. .)))");
		xdg2.fromPennTree("(S1 (S (NP (NNP Regina) (NNP Schueller)) (VP (VBZ works) (PP (IN for) (NP (NP (NNP Italy) (POS &apos;s)) (NNP La) (NNP Repubblica) (NN newspaper)))) (. .)))");
		SuperMatcherAndReducer sr = new SuperMatcherAndReducer();
		sr.reduce((Constituent) xdg1.getSetOfConstituents().elementAt(0),(Constituent) xdg2.getSetOfConstituents().elementAt(0));
		System.out.println(xdg1.toPennTree());
		System.out.println(xdg2.toPennTree());
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}
}