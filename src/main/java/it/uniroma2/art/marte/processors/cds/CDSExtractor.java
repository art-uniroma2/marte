package it.uniroma2.art.marte.processors.cds;

import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.ArteException;
import it.uniroma2.art.marte.processors.Processor;
import it.uniroma2.art.marte.structures.AnalysedTextPair;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.dsk.DS;
import it.uniroma2.dtk.dt.DT;
import it.uniroma2.exp.tools.TreeFlattenerForSK;
import it.uniroma2.util.math.ArrayMath;
import it.uniroma2.util.tree.Tree;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.Vector;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.CommandLine;

import existing.HeadMarker;



@Deprecated
public class CDSExtractor extends Processor {

	private HeadMarker hm = null;
	private String head_marker_rule_file = null;
	private Double cdsLambda = null;
	private Integer cdsSize = null;
	private String cdsType = null;
	private String cdsOp = null;
	private Double cdsMu = null;

	private Options options = null;
	
	
	public CDSExtractor() {
		options = new Options();
		options.addOption("cdsLambda", true ,"dtk lambda parameter [0.0 to 1.0]");
		options.addOption("cdsDim", true ,"dtk vector size");
		options.addOption("cdsType", true ,"dtk type class");
		options.addOption("cdsOp", true ,"dtk composition operation class");
		options.addOption("cdsMu", true ,"dptk mu parameter [0.0 to 1.0]");
	}

	
	public CDSExtractor(CommandLine commands) throws ArteException {
		this.head_marker_rule_file = System.getProperty("arte.semanticheadmarker.rules");
		if (commands.hasOption("cdsLambda"))
    		cdsLambda = new Double(commands.getOptionValue("cdsLambda"));
		else 
			throw new ArteException("Specify the CDS lambda");
		if (commands.hasOption("cdsDim"))
    		cdsSize = new Integer(commands.getOptionValue("cdsDim"));
		else 
			throw new ArteException("Specify the CDS vector size");
		if (commands.hasOption("cdsType"))
    		cdsType = new String(commands.getOptionValue("cdsType"));
		else 
			throw new ArteException("Specify the CDS type class");	
		if (commands.hasOption("cdsOp"))
    		cdsOp = new String(commands.getOptionValue("cdsOp"));
		else 
			throw new ArteException("Specify the CDS operation class");
		if (commands.hasOption("cdsMu"))
    		cdsMu = new Double(commands.getOptionValue("cdsMu"));
	}
	
	public CDSExtractor(String head_marker_rule_file, Double cdsLambda, Integer cdsSize, String cdsType, String cdsOp, Double cdsMu) {
		this.head_marker_rule_file = head_marker_rule_file;
		this.cdsLambda = cdsLambda;
		this.cdsSize = cdsSize;
		this.cdsType = cdsType;
		this.cdsOp = cdsOp;
		this.cdsMu = cdsMu;
	}

	@SuppressWarnings("unchecked")
	public Collection<Option> getOptions() {
		return options.getOptions();
	}


	public void setOptions(Options options) {
		this.options = options;
	}

	private Vector<Object> cds_processors = null;

	@SuppressWarnings("unchecked")
	@Override
	public void initialize() throws Exception {
		cds_processors = new Vector<Object>();
		hm = new HeadMarker();
		hm.initialize(head_marker_rule_file,HeadMarker.Target.GOVERNOR);

		DT dt = null;
		DS<String> ds = null;
		if (cdsMu == null) {
			Constructor<?> constructor = Class.forName(cdsType).getConstructor(Integer.TYPE, Integer.TYPE, Double.TYPE, Class.class);
			dt = (DT) constructor.newInstance(0, cdsSize, cdsLambda, Class.forName(cdsOp));
		}
		else {
			try {
				Constructor<?> constructor = Class.forName(cdsType).getConstructor(Integer.TYPE, Integer.TYPE, Double.TYPE, Double.TYPE, Class.class);
				dt = (DT) constructor.newInstance(0, cdsSize, cdsLambda, cdsMu, Class.forName(cdsOp));
			}
			catch (Exception ex) {
				Constructor<?> constructor = Class.forName(cdsType).getConstructor(Integer.TYPE, Integer.TYPE, Double.TYPE, Integer.TYPE, Class.class);
				ds = (DS<String>) constructor.newInstance(0, cdsSize, cdsLambda, cdsMu.intValue(), Class.forName(cdsOp));
			}
		}
		if (dt != null)
			cds_processors.add(dt);
		else
			cds_processors.add(ds);
	}

	public EntailmentPair extract(EntailmentPair p) throws Exception {
		XDG text = new XDG();
		String text_s = p.getActiveAnalysedPair().getText();
		if (text_s.trim().endsWith(" )")) text_s = text_s.replace(" )", " (-N-O- Empty))");
		text.fromPennTree(text_s);
		XDG hypo = new XDG();
		String hypo_s = p.getActiveAnalysedPair().getHypothesis();
		if (hypo_s.trim().endsWith(" )")) hypo_s = hypo_s.replace(" )", " (-N-O- Empty))");
		hypo.fromPennTree(hypo_s);

		System.out.println(hypo_s);
		removeAnchors((Constituent)text.getSetOfConstituents().elementAt(0));
		removeAnchors((Constituent)hypo.getSetOfConstituents().elementAt(0));

		p.addAnalysis(new AnalysedTextPair(text.toPennTree(),hypo.toPennTree(),"active_span:cpw"));

		hm.run(text);
		hm.run(hypo);

		int i = 0;
		for (Object c:cds_processors) {
			double [] t_cds = null, h_cds = null;
			if (c instanceof DT) {
				DT dt = (DT) c;
				t_cds =dt.dt(Tree.fromPennTree(text.toPennTree()));
				h_cds =dt.dt(Tree.fromPennTree(hypo.toPennTree()));
			}
			else if (c instanceof DS) {
				@SuppressWarnings("unchecked")
				DS<String> ds = (DS<String>) c;
				t_cds =ds.ds(TreeFlattenerForSK.flattenTree(Tree.fromPennTree(text.toPennTree())).toArray(new String[0]));
				h_cds =ds.ds(TreeFlattenerForSK.flattenTree(Tree.fromPennTree(hypo.toPennTree())).toArray(new String[0]));
			}
			p.addAnalysis(new AnalysedTextPair(vectorToString(t_cds),vectorToString(h_cds),"cds:it.uniroma2.cds.TreeSpectrumGenerator"));
			double cds_feat = (t_cds != null && h_cds != null? ArrayMath.cosine(t_cds, h_cds): 0);
			p.addFeatureValue("cds-feat-" + i, cds_feat);
			i++;
		}
		return p;
	}

	private String vectorToString(double [] d) {
		String out = "";
		if (d!=null) {
			for (int i = 0; i < d.length ; i++) {
				out += " " + (i+1) + ":" +d[i];
			}
		}
		return out;
	}

	@SuppressWarnings("unchecked")
	private Constituent removeAnchors(Constituent in) {
		in.setType(in.getType().split(":")[0]);
		if (in instanceof ComplxConst) 
			for (Constituent c:(Vector <Constituent>) ((ComplxConst) in).getSubConstituents()) removeAnchors(c);
		return in;		
	}

}
