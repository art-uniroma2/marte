
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transform(T,TT):-
	rewritten_in(T,TT).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE ALGPRITHM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rewritten_in(N,N):-
	atom(N).

rewritten_in(N,N):-
	number(N).


rewritten_in(N1,N2):-
	rule(N1,N2),!.
	
	
rewritten_in(N1,N2):-
	N1 =.. [A|R1],
	!,
	list_rewritten_in(R1,R2),
	N2 =.. [A|R2].
	

list_rewritten_in([],[]).

list_rewritten_in([A1|R1],[A2|R2]):-
	rewritten_in(A1,A2),
	!,
	list_rewritten_in(R1,R2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%(S _NP (VP (VP (AUX does) (RB not) (VP (VB solve) (NP (DT any) (JJ social) (NNS problems)))) (, 

%'S'(ID1,_NP,'VP'(ID2,_AUX,'RB'(ID3,not,l),VP_NOT),'.'(36,'.',l))

%S(36,'NP'_,'VP'(34,'MD'(21,should,l),'RB'(22,not,l),'VP'_),'.'(36,'.',l))

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,AUX,'RB'(ID3,'not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|not|'(ID2,AUX,'RB'(ID3,'not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|not|',ID1|REST_S_NOT].

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,AUX,'RB'(ID3,'n''t',l),VP)|REST],REST_S),
	append(X,[NP,'VP|not|'(ID2,AUX,'RB'(ID3,'n''t',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|not|',ID1|REST_S_NOT].
	

	

 