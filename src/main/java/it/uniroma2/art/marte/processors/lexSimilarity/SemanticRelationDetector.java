package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class SemanticRelationDetector extends ConstituentRelationDetector {

private Hashtable<String,Float> semanticRelationDictionary = null;

private String type = "e";
private String file;

private float strength = 1;

public SemanticRelationDetector(String type,String file) throws Exception {
	this.type = type;
	load(file);
}

public SemanticRelationDetector(String type,float strength,String file) throws Exception {
	this.type = type;
	this.strength = strength;
	load(file);
}

public SemanticRelationDetector(boolean initialization, String type, String file) {
	this.setNameAndDescription("SEM", "determines a relation according to a file");
	this.type = type;
	this.file = file;
}

@Override
public void initialize() throws Exception {
	load(file);
}

public String relation_type() {return type;}

public float relation_strength(Constituent t, Constituent h) throws Exception {
	float f = 0;
	if (semanticRelationDictionary.containsKey(t.getFirstLemma().getSurface()+"#" + POS.type(t) + " " + h.getFirstLemma().getSurface()+"#" + POS.type(h))) {
		f = strength;
	}
	return f;
}


private void load(String file) throws Exception {
	semanticRelationDictionary = new Hashtable<String,Float>();
	String line = "";
	int line_no = 0;
	try {
		BufferedReader in = new BufferedReader(new FileReader(file));
		line = in.readLine();
		line_no = 0;
		while (line != null) {
			StringTokenizer st = new StringTokenizer(line," ");
			String pair = st.nextToken().trim() + " "+ st.nextToken().trim();
			//System.out.println(pair + " - " );
			Float f = new Float(1);
			semanticRelationDictionary.put(pair,f);
			line = in.readLine();
			line_no++;
		}
		in.close();
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	} catch (NoSuchElementException e) {
		System.out.println("FILE : " + file);
		System.out.println("ERROR in line : " + line_no + " <" + line +">");
		throw e;
	}
}


}