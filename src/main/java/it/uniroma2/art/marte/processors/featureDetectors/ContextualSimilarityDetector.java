package it.uniroma2.art.marte.processors.featureDetectors;

import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.Pair;
import it.uniroma2.art.marte.structures.SVMpair;
import it.uniroma2.art.marte.utils.IDF;
import it.uniroma2.art.marte.utils.VSM;

import java.util.HashMap;


public class ContextualSimilarityDetector extends OldGenericFeatureDetector {

	IDF idf = null;
	VSM v_ops = null;
	public ContextualSimilarityDetector() throws Exception  {
		System.err.println("Activating");
		SVMpair.addFeature("left_context_similarity"); 
		SVMpair.addFeature("right_context_similarity");
		v_ops = new VSM();
		idf = new IDF();
	}

	public void computeFeature(Pair p, SVMpair instance) throws Exception {
		//System.err.print("C");
		if (p.getHypothesisContext() != null) {
			HashMap <String,Double> hypo_v = null;
			HashMap <String,Double> text_v = null;
			if (p.getHypothesisContext().getLeftContext() != null) {
				hypo_v = new HashMap <String,Double>();
				text_v = new HashMap <String,Double>();
				for (String c:p.getHypothesisContext().getLeftContext())
					hypo_v = VSM.add(hypo_v,VSM.textToBowVector(c,idf));
				for (String c:p.getTextContext().getLeftContext())  
					text_v = VSM.add(text_v,VSM.textToBowVector(c,idf));
				instance.setFeatureValue("left_context_similarity", "" + 
						(VSM.dot(hypo_v,text_v)/Math.sqrt(VSM.dot(hypo_v,hypo_v)*VSM.dot(text_v,text_v))));
			}
			
			if (p.getHypothesisContext().getRightContext() != null) {
				hypo_v = new HashMap <String,Double>();
				text_v = new HashMap <String,Double>();
		
				for (String c:p.getHypothesisContext().getRightContext())  
					hypo_v = VSM.add(hypo_v,VSM.textToBowVector(c,idf));
				for (String c:p.getTextContext().getRightContext())  
					text_v = VSM.add(text_v,VSM.textToBowVector(c,idf));
				instance.setFeatureValue("right_context_similarity", "" + 
						(VSM.dot(hypo_v,text_v)/Math.sqrt(VSM.dot(hypo_v,hypo_v)*VSM.dot(text_v,text_v))));
			}
		}

	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}


}
