package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;

//import chaos.processors.HeadMarker;




import existing.HeadMarker;

/** 
	marks the semantic heads of the constituents in the trees. It uses the Chaos processors HeadMarker. 
	Rules are a revised set with respect to the Collins set of head rules.
*/
public class SemanticHeadMarker extends GenericTreeRewriter {


	
	HeadMarker p = null;
	
	/** 
		initilizes a semantic head marker using a the rules in rule_file.		
	*/
	public SemanticHeadMarker(String rule_file) throws Exception {
		
		p = new HeadMarker();
		p.initialize(rule_file,HeadMarker.Target.HEAD);
	}

	public SemanticHeadMarker(boolean b, String property) throws Exception {
		// TODO Auto-generated constructor stub
		this(property);
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		marks the semantic heads of the constituents in the trees. It uses the Chaos processors HeadMarker. 
		Rules are a revised set with respect to the Collins set of head rules.
	*/
	public XDG transform(XDG xdg) throws Exception {
		
		xdg = p.run(xdg);
		
		return xdg;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}


}