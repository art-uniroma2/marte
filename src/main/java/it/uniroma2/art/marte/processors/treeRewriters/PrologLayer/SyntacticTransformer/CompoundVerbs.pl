
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transform(T,TT):-
		tell(pippo2),write(T),nl,
	rewritten_in(T,TT),
	write(TT),nl,told.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE ALGPRITHM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rewritten_in(N,N):-
	atom(N).

rewritten_in(N,N):-
	number(N).


rewritten_in(N1,N2):-
	rule(N1,N2),!.
	
	
rewritten_in(N1,N2):-
	N1 =.. [A|R1],
	!,
	list_rewritten_in(R1,R2),
	N2 =.. [A|R2].
	

list_rewritten_in([],[]).

list_rewritten_in([A1|R1],[A2|R2]):-
	rewritten_in(A1,A2),
	!,
	list_rewritten_in(R1,R2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%'VP'(14,'VBN'(5,given,given,l),'PRT'(6,'RP'(6,up,up,l))

rule(VP,VPT):-
	VP =.. ['VP',ID,VERB,'PRT'(_,'RP'(_,COMPOUND_PARTICLE,COMPOUND_PART_L,l))|REST],
	VERB =.. [POS,ID_V,TOKEN,LEMMA,_],
	atom_concat(TOKEN,'_',TOKEN_1),
	atom_concat(TOKEN_1,COMPOUND_PARTICLE,COMPOUND_VERB),
	atom_concat(LEMMA,'_',LEMMA_1),
	atom_concat(LEMMA_1,COMPOUND_PART_L,COMPOUND_VERB_L),
	CV =.. [POS,ID_V,COMPOUND_VERB,COMPOUND_VERB_L,l],
	VPT =.. ['VB',ID,CV|REST].
 