package it.uniroma2.art.marte.processors.featureDetectors;

import it.uniroma2.art.marte.structures.*;

import java.util.*;

public abstract class OldGenericFeaturesDetector {
	
	private Vector<String> names;
	private String set = "std";

	
	public OldGenericFeaturesDetector(Vector<String> names) {
		this.names = names;
		for (int i=0;i<names.size();i++)
			SVMpair.addFeature(names.elementAt(i));
	} 

	public OldGenericFeaturesDetector(String feature_set, String base_feature_name, int num_feat) {
		String name=null;
		this.set = feature_set;
		for (int i=0;i<num_feat;i++)
		  	{
		  	name=	base_feature_name+"_"+i;
		  	names.add(name);
				SVMpair.addFeature(name,feature_set);
				}
	} 

	public Vector<String> getNames() {return names;}


	public String getSet() {return set;}

	@Deprecated
	public abstract void computeFeatures(Pair p, SVMpair instance) throws Exception  ;

}