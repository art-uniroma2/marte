package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.dictionaries.*;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

public class VerbLemmatizer extends GenericTreeRewriter {
	
	private LemmaDictionary lemma_dictionary = null;
	private boolean lemmatize = false;

	
	/**
		creates a lemmatizer that adds the lemmas in the SimpleConst 	
	*/
	public VerbLemmatizer(LemmaDictionary ld){
		lemma_dictionary = ld;
	}

	/**
		creates a lemmatizer that adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens 
	
	*/
	public VerbLemmatizer(LemmaDictionary ld,boolean lemmatize){
		lemma_dictionary = ld;
		this.lemmatize = lemmatize;
		System.out.println("Lemmatiser is " + lemmatize);
	}
	
	public VerbLemmatizer(boolean initialiazation, LemmaDictionary dictionary, boolean lemmatize) {
		// TODO Auto-generated constructor stub
		this.setNameAndDescription("VLEM", "produces a lemmatized version of the verbs in the tree");
		lemma_dictionary = dictionary;
		this.lemmatize = lemmatize;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens	
	*/
	public XDG transform(XDG xdg) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				sc.getFirstLemma().setSurface(lemma_dictionary.lemma(sc));
				if (lemmatize && POS.verb(sc)) sc.setSurface(lemma_dictionary.lemma(sc));
			} 
		}
		return xdg;
	}




}