transform(T,TT):-
	tell("pippo.xyz"),
	write(T),nl,
	told,
	rewritten_in(T,TT).

rewritten_in(N,N):-
	atom(N).

rewritten_in(N,N):-
	number(N).


rewritten_in(N1,N2):-
	rule(N1,N2),!.
	
	
rewritten_in(N1,N2):-
	N1 =.. [A|R1],
	!,
	list_rewritten_in(R1,R2),
	N2 =.. [A|R2].
	

list_rewritten_in([],[]).

list_rewritten_in([A1|R1],[A2|R2]):-
	rewritten_in(A1,A2),
	!,
	list_rewritten_in(R1,R2).


rule(s(V1,vp(be(is),V2,pp(in(by),V3))),s(V3,vp(V2,V1))):-
	V1 =.. ['NP'|_],
	V2 =.. ['VBZ'|_],
	V3 =.. ['NP'|_].
	


% s(np(det(the),nn(man)),vp(be(is),vbz(killed),pp(in(by),np(det(the),nn(cat))))).

% s(np(det(the),nn(man)),vp(be(is),vbz(killed),pp(in(with),np(det(the),nn(cat))))).
 