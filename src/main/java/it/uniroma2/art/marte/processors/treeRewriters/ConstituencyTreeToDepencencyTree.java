package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.dictionaries.LemmaDictionary;
import it.uniroma2.art.marte.structures.Pair;

import java.util.Vector;

public class ConstituencyTreeToDepencencyTree extends GenericTreeRewriter {

	private LemmaDictionary ld = null;
	
	public ConstituencyTreeToDepencencyTree() {
		// TODO Auto-generated constructor stub
	}
	
	public ConstituencyTreeToDepencencyTree(boolean initialization) {
		// TODO Auto-generated constructor stub
		this.setNameAndDescription("DEP", "transforms the constituency tree in a dependency tree");
	}

	public ConstituencyTreeToDepencencyTree(boolean initialization, LemmaDictionary ld) {
		// TODO Auto-generated constructor stub
		this.setNameAndDescription("DEP", "transforms the constituency tree in a dependency tree");
		this.ld = ld;
	}

	@Override
	public XDG transform(XDG xdg) throws Exception {
		Constituent c = augment_tags_with_lemma((Constituent) xdg.getSetOfConstituents().elementAt(0));
		xdg.getSetOfConstituents().setElementAt(c, 0);
		return xdg;
	}

	public Constituent augment_tags_with_lemma(Constituent c) {
		String type = (c.getType().indexOf(':')>0?c.getType().split(":")[0]:c.getType());
		String rest = (c.getType().split(":").length>1? ":" + c.getType().split(":")[1] + (c.getType().split(":").length>2? ":" + c.getType().split(":")[2] : "" ): "" );
		//if (type.equals("NP") || type.equals("VP") || type.startsWith("S") )
			c.setType(type + "#" + (ld==null ? c.getHead().getSurface() : ld.lemma(c.getHead()) ) + "$" + c.getHead().getType().substring(0,1).toLowerCase() + rest);
		if (c instanceof ComplxConst) {
			Vector <Constituent> vc = (Vector <Constituent>) ((ComplxConst)c).getSubConstituents();
			for (Constituent cc : vc)  
				augment_tags_with_lemma(cc);
		}
		return c;
	}
	@Override
	public Pair transform(Pair p) throws Exception {
		transform(p.getHypoXDG());
		transform(p.getTextXDG());
		return p;
	}

	@Override
	public void initialize() throws Exception {
		
	}

}
