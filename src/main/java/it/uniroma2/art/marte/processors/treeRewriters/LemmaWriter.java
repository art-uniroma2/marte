package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;


import java.util.*;

public class LemmaWriter extends GenericTreeRewriter {
	
	public LemmaWriter() {
	}
	public LemmaWriter(boolean initialization) {
		this.setNameAndDescription("LEMP", "produces a lemmatized version of the tree");
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens	
	*/
	public XDG transform(XDG xdg) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				sc.setSurface(sc.getFirstLemma().getSurface());
			} 
		}
		return xdg;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}



}