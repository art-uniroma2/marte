package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.IcdList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

public class TreeMatchingDetector extends GenericTreeRewriter {

	

	int anchors = 0;
	public Pair transform(Pair p) {
		anchors = 0;
		Constituent txt = (Constituent) p.getTextXDG().getSetOfConstituents().elementAt(0);
		Constituent hyp = (Constituent) p.getHypoXDG().getSetOfConstituents().elementAt(0);
		Triplet t = detect(txt,hyp);
		
		int s_t = size(txt);
		int s_h = size(hyp);
		int s_s = 0;
		if (t.same != null) s_s = size(t.same);
		
		//System.out.println("Sizes : " + s_t + " " + s_h + " " + s_s + " " + (Math.sqrt(((float)s_s*s_s)/((float)s_t*s_h))));

		p.setConfidence((float)(Math.sqrt(((float)s_s*s_s)/((float)s_t*s_h))));
		

		//System.out.println(" HYPO \n --- \n" +p.getHypoXDG().toPennTree());
		ConstituentList c2 = extractAll(hyp);

		
		//System.out.println(" TEXT \n --- \n" +p.getTextXDG().toPennTree());
		ConstituentList c1 = extractAll(txt);
		
		//XDG x1 = new XDG(c2,new IcdList());
		//System.out.println("HYPO : " + x1.toPennTree());
		//x1 = new XDG(c1,new IcdList());
		//System.out.println("TEXT : " + x1.toPennTree());

		
		if (c1 == null || c1.size() == 0) p.getTextXDG().getSetOfConstituents().setElementAt(new SimpleConst("S","no_parsed",0),0);
		else p.setTextXDG(new XDG(c1,new IcdList()));

		if (c2 == null || c2.size() == 0) p.getHypoXDG().getSetOfConstituents().setElementAt(new SimpleConst("S","no_parsed",0),0);
		else p.setHypoXDG(new XDG(c2,new IcdList()));
    
		return p;
	}

	public XDG transform(XDG p) {
		return p;
	}

	public static String production(Constituent a) {
		String production = a.getType().split(":")[0] + "->";
		if (a instanceof ComplxConst) {
			for (Constituent c: (Vector <Constituent>) ((ComplxConst)a).getSubConstituents()) 
				production += c.getType().split(":")[0] + " ";
		} else {
			production += a.getSurface();
		}
		return production;
	} 


	public Triplet detect(Constituent txt, Constituent hyp) { 
		Constituent new_c = null;
		ConstituentList residual_ts = new ConstituentList();
		ConstituentList residual_hs = new ConstituentList();
		boolean active = false;
		if (production(hyp).equals(production(txt))) {
			if (hyp instanceof ComplxConst && txt instanceof ComplxConst) {
				Vector<Constituent> hyp_sub = ((ComplxConst) hyp).getSubConstituents();
				Vector<Constituent> txt_sub = ((ComplxConst) txt).getSubConstituents();
				ConstituentList cl = new ConstituentList();
				for (int i = 0; i < hyp_sub.size(); i++) {
					Triplet triplet = detect(txt_sub.elementAt(i),hyp_sub.elementAt(i));
					Constituent actual_c ;
					if (triplet.same == null) {
						actual_c = new SimpleConst("*",txt_sub.elementAt(i).getType(),txt_sub.elementAt(i).getId());
					} else {
						actual_c = triplet.same;
					}
					cl.addElement(actual_c);
					if (triplet.residual_h != null && triplet.residual_h.size() > 0) {
						residual_hs.addAll(triplet.residual_h);
					} else {
						//hyp_sub.setElementAt(new SimpleConst("*",hyp_sub.elementAt(i).getType(),hyp_sub.elementAt(i).getId()), i); 
					}
					if (triplet.residual_t != null && triplet.residual_t.size() > 0 ) {
						residual_ts.addAll(triplet.residual_t);
					} else {
						//txt_sub.setElementAt(new SimpleConst("*",txt_sub.elementAt(i).getType(),txt_sub.elementAt(i).getId()), i); 
					}
					active = active || triplet.active;
				}
			 	new_c = new ComplxConst(txt.getType(),cl);
			} else {
				new_c = new SimpleConst(txt.getSurface(),txt.getType(),txt.getId());
			}
		} else {
			residual_ts.addElement(txt);
			residual_hs.addElement(hyp);
		}
		//if (!active) System.out.println(production(hyp));
		Triplet new_triplet = new Triplet();
		new_triplet.active = (new_c == null) || active;
		if (new_triplet.active) {txt.setType(txt.getType() + "-A");hyp.setType(hyp.getType() + "-A");}
		new_triplet.active = new_triplet.active && (!(txt.getType().equals("S-A") || (txt.getType().startsWith("S:") && txt.getType().endsWith("-A")) ));
		new_triplet.same = new_c;
		new_triplet.residual_h = residual_hs;
		new_triplet.residual_t = residual_ts;
		return new_triplet;
	}
	
	
	public void extract(Constituent txt, Constituent hyp) {
		ConstituentList c1 = extractAll(txt);
		ConstituentList c2 = extractAll(hyp);
		XDG x1 = new XDG(c1,new IcdList());
		XDG x2 = new XDG(c2,new IcdList());
		System.out.println(x1.toPennTree());
		System.out.println(x2.toPennTree());
	}

	public ConstituentList extractAll(Constituent c) {
		ConstituentList l = new ConstituentList();
		Constituent new_c = extract(c);
		if (new_c != null && !(new_c instanceof SimpleConst)) { l.add(new_c); System.out.println(" ADDING "  + new_c.getType() );}
			
		if (c instanceof ComplxConst) {
			Vector<Constituent> sub_cs = ((ComplxConst)c).getSubConstituents();
				for (Constituent cs:sub_cs) {
					for (Constituent ccs: (Vector<Constituent>)extractAll(cs)) if (!(ccs instanceof SimpleConst) )l.add(ccs);
				}
		}
		return l;
	}

	
	public Constituent extract(Constituent c) {
		Constituent new_c = null;
		if (c instanceof SimpleConst) {
			if (c.getType().endsWith("-A")) {
				c.setType(c.getType().replaceAll("-A",""));
			}
			new_c = c;
		} else {
			if (c.getType().endsWith("-A")) {
				//System.out.println(c.getType());
				Vector<Constituent> sub_cs = ((ComplxConst)c).getSubConstituents();
				ConstituentList cl = new ConstituentList();
				for (Constituent cs:sub_cs) {
					//System.out.println("  <  " + cs.getType());
					cl.add(extract(cs));
				}
				c.setType(c.getType().replaceAll("-A",""));
				new_c = new ComplxConst(c.getType(),cl);
			} else {
				new_c = new SimpleConst("*",c.getType(),c.getId());
			}
		}
		return new_c;
	}
	
	public static int size(Constituent c) {
		int size = 0;
		if (c instanceof SimpleConst) size++;
		else {
			size++;
			for (Constituent cs:(Vector<Constituent>) ((ComplxConst)c).getSubConstituents()) size += size(cs);
		}
		return size;
	}
	
	
	public static void main(String [] argv) throws Exception {
		XDG xdg1 = new XDG();
		XDG xdg2 = new XDG();
		 
//		xdg1.fromPennTree("(S (NP (DT The) (NN multitude)) (VP (VBZ plays) (NP (NP (DT a) (JJ prominent) (NN part)) (PP (PP (IN in) (NP (NNP Empire))) (, ,) (CC and) (RB not) (PP (IN in) (NP (DT the) (JJS least))))) (SBAR (IN for) (S (NP (PRP$ its) (NNP Revolution) (NN potential)) (VP (TO to) (VP (VB establish) (SBAR (WHNP (WP what)) (S (NP (NNP Baruch) (NNP Baruch) (NNP Spinoza)) (VP (VBD called) (S (NP (NP (DT an) (JJ absolute) (NN democracy)) (, ,) (SBAR (S (VP (TO to) (VP (VB subvert) (NP (NP (DT the) (JJ alleged) (JJ post-disciplinary) (NNS societies)) (PP (IN of) (NP (NP (NN control)) (CC and) (NP (PRP$ its) (JJ concomitant) (NN biopower))))))))) (, ,)) (PP (IN in) (ADJP (JJ short) (S (VP (TO to) (VP (VB attack) (NP (JJ post-industrial) (JJ capitalist) (NN hegemony)) (PP (IN with) (NP (JJ effective) (NN Information) (NN warfare))))))))))))))))) (. .))");
//		xdg2.fromPennTree("(S (NP (DT The) (NN multitude)) (VP (VBZ plays) (NP (NP (DT a) (JJ prominent) (NN part)) (PP (PP (IN in) (NP (NNP Empire))) (, ,) (CC and) (RB not) (PP (IN in) (NP (DT the) (JJS least))))) (SBAR (IN for) (S (NP (PRP$ its) (NNP Revolution) (NN potential)) (VP (TO to) (VP (VB establish) (SBAR (WHNP (WP what)) (S (NP (NNP Baruch) (NNP Spinoza)) (VP (VBD called) (S (NP (NP (DT an) (JJ absolute) (NN democracy)) (, ,) (SBAR (S (VP (TO to) (VP (VB subvert) (NP (NP (DT the) (JJ alleged) (JJ post-disciplinary) (NNS societies)) (PP (IN of) (NP (NP (NN control)) (CC and) (NP (PRP$ its) (JJ concomitant) (NN biopower))))))))) (, ,)) (PP (IN in) (ADJP (JJ short) (S (VP (TO to) (VP (VB attack) (NP (JJ post-industrial) (JJ capitalist) (NN hegemony)) (PP (IN with) (NP (JJ effective) (NN weaponry))))))))))))))))) (. .))");

		xdg1.fromPennTree("(S (NP (ANC ANC0))(VP (VBZ plays)(NP (ANC ANC1))(SBAR (IN for)(S (NP (ANC ANC2))(VP (TO to)(VP (VB establish)(SBAR (WHNP (ANC ANC3))(S (NP (NNP Baruch)(NNP Baruch)(NNP Spinoza))(VP (VBD called)(S (NP (ANC ANC4))(PP (IN in)(ADJP (JJ short)(S (VP (TO to)(VP (VB attack)(NP (ANC ANC5))(PP (IN with)(NP (JJ effective)(NN Information)(NN warfare)))))))))))))))))(. .))");
		xdg2.fromPennTree("(S (NP (ANC ANC0))(VP (VBZ plays)(NP (ANC ANC1))(SBAR (IN for)(S (NP (ANC ANC2))(VP (TO to)(VP (VB establish)(SBAR (WHNP (ANC ANC3))(S (NP (NNP Baruch)(NNP Spinoza))(VP (VBD called)(S (NP (ANC ANC4))(PP (IN in)(ADJP (JJ short)(S (VP (TO to)(VP (VB attack)(NP (ANC ANC5))(PP (IN with)(NP (JJ effective)(NN weaponry)))))))))))))))))(. .))");

		TreeMatchingDetector sr = new TreeMatchingDetector();
		Triplet tt = sr.detect((Constituent) xdg1.getSetOfConstituents().elementAt(0),(Constituent) xdg2.getSetOfConstituents().elementAt(0));
		ConstituentList cl = new ConstituentList();
		cl.add(tt.same);
		XDG x = new XDG(cl,new IcdList());
		XDG x1 = new XDG(tt.residual_t,new IcdList());
		XDG x2 = new XDG(tt.residual_h,new IcdList());
		System.out.println(xdg1.toPennTree());
		System.out.println(xdg2.toPennTree());
		System.out.println("---");
		System.out.println(x.toPennTree());
		System.out.println(x1.toPennTree());
		System.out.println(x2.toPennTree());
		System.out.println("-----------------------------");
		sr.extract((Constituent) xdg1.getSetOfConstituents().elementAt(0),(Constituent) xdg2.getSetOfConstituents().elementAt(0));
		
	}
	
	class Triplet {
		Constituent same = null;
		ConstituentList residual_h = null; 
		ConstituentList residual_t = null;
		boolean active = false;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}
}