package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.dictionary.MorphologicalProcessor;


/**
 * @author Noemi Scarpato
 *
 */
public class WordnetLemmatizer extends GenericTreeRewriter {
	 private MorphologicalProcessor morph;
	 private Dictionary dic;
	 private boolean lemmatize = false;
	  /** 
	   * @throws Exception 
	   */
	  public WordnetLemmatizer(boolean lemmatize) throws Exception  {
		 this.lemmatize=lemmatize;
		initialize();
	  }
	  
	  
	  /**
	   * It takes a word  and obtains a word's lemma from WordNet.
	   * 
	   * @param word
	   * @return lemma
	 * @throws JWNLException 
	   */
	  public String lemmatize(String word) throws JWNLException {
	    IndexWord lemma = null;
	    
	    try
		{
	    	lemma = morph.lookupBaseForm( POS.VERB, word );
			if ( lemma != null )
				return lemma.getLemma();
			lemma = morph.lookupBaseForm( POS.NOUN, word );
			if ( lemma != null )
				return lemma.getLemma();
			lemma = morph.lookupBaseForm( POS.ADJECTIVE, word );
			if ( lemma != null )
				return lemma.getLemma();
			lemma = morph.lookupBaseForm( POS.ADVERB, word );
			if ( lemma != null )
				return lemma.getLemma();
		} 
		catch ( JWNLException e )
		{
			return null;
		}
		return null;
		
	  }
	  public static void main(String[] args) {
			 WordnetLemmatizer lemm;
			try {
				lemm = new WordnetLemmatizer(true);
				 String res = lemm.lemmatize("coming");
				 System.out.println(res);
				 
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JWNLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JWNL.shutdown();
		}


	  /** 
		adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens	
	*/
	public XDG transform(XDG xdg) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				sc.getFirstLemma().setSurface(lemmatize(sc.getSurface()));
				if (lemmatize) sc.setSurface((lemmatize(sc.getSurface())));
			} 
		}
		return xdg;
	}


	@Override
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}


	@Override
	public void initialize() throws Exception {
		try
		{
			JWNL.initialize(new FileInputStream
				("config\\file_properties.xml"));
			dic = Dictionary.getInstance();
			morph = dic.getMorphologicalProcessor();
		}
		catch ( FileNotFoundException e )
		{
			System.out.println ( "Error initializing Lemmatizer: " +
					"JWNLproperties.xml not found" );
		}
		catch ( JWNLException e )
		{
			System.out.println ( "Error initializing Lemmatizer: " 
				+ e.toString() );
		} 
		
	}
	  

}
