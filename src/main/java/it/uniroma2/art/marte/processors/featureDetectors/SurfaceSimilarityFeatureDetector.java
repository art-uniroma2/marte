package it.uniroma2.art.marte.processors.featureDetectors;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.Anchor;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.POS;
import it.uniroma2.art.marte.structures.Pair;
import it.uniroma2.art.marte.structures.SVMpair;

import java.util.Vector;


public class SurfaceSimilarityFeatureDetector extends OldGenericFeatureDetector {

	public SurfaceSimilarityFeatureDetector() {
		SVMpair.addFeature("covered_h_verbs"); 
		SVMpair.addFeature("covered_h_proper_nouns"); 
		SVMpair.addFeature("covered_h_common_nouns"); 
		SVMpair.addFeature("covered_h_adjectives"); 
		
		SVMpair.addFeature("covered_with_similarity_h_verbs"); 
		SVMpair.addFeature("covered_with_similarity_h_common_nouns"); 
		SVMpair.addFeature("covered_with_similarity_h_adjectives"); 

		SVMpair.addFeature("covered_with_antinomy_h_verbs"); 
		SVMpair.addFeature("covered_with_antinomy_h_common_nouns"); 
		SVMpair.addFeature("covered_with_antinomy_h_adjectives"); 
		
	}
	
	@Override
	public void computeFeature(Pair p, SVMpair instance) throws Exception {
		// TODO Auto-generated method stub
		
		Vector <Vector<Anchor>> anchors = p.getAnchors();
		
		// Counts
		int tot_h_verbs = 0; int covered_h_verbs = 0; 
		int tot_h_proper_nouns = 0; int covered_h_proper_nouns = 0; 
		int tot_h_common_nouns = 0; int covered_h_common_nouns = 0; 
		int tot_h_adjectives = 0; int covered_h_adjectives = 0; 
		
		int covered_with_similarity_h_verbs = 0; 
		int covered_with_similarity_h_common_nouns = 0; 
		int covered_with_similarity_h_adjectives = 0; 

		int covered_with_antinomy_h_verbs = 0; 
		int covered_with_antinomy_h_common_nouns = 0; 
		int covered_with_antinomy_h_adjectives = 0; 

		for (Vector <Anchor> node_anchors:anchors) {
			Constituent h = node_anchors.elementAt(0).getHConst();
			if (POS.proper_noun(h)) tot_h_proper_nouns++;
			if (POS.common_noun(h)) tot_h_common_nouns++;
			if (POS.adj(h)) tot_h_adjectives++;
			if (POS.verb(h)) tot_h_verbs++;
			if (node_anchors.elementAt(0).getTConst()!=null) {
				if (node_anchors.elementAt(0).getType().equals("=")) {
					if (POS.proper_noun(h)) covered_h_proper_nouns++;
					if (POS.common_noun(h)) covered_h_common_nouns++;
					if (POS.adj(h)) covered_h_adjectives++;
					if (POS.verb(h)) covered_h_verbs++;
				} else if (node_anchors.elementAt(0).getType().equals("-") || node_anchors.elementAt(0).getProcessor().equals("e")) {
					if (POS.common_noun(h)) covered_with_antinomy_h_common_nouns++;
					if (POS.adj(h)) covered_with_antinomy_h_adjectives++;
					if (POS.verb(h)) covered_with_antinomy_h_verbs++;
				} else {
					if (POS.common_noun(h)) covered_with_similarity_h_common_nouns++;
					if (POS.adj(h)) covered_with_similarity_h_adjectives++;
					if (POS.verb(h)) covered_with_similarity_h_verbs++;
				}
			}
			instance.setFeatureValue("covered_h_verbs", "" + ((float)covered_h_verbs) /tot_h_verbs);
			instance.setFeatureValue("covered_h_proper_nouns", "" + ((float)covered_h_proper_nouns)/tot_h_proper_nouns);
			instance.setFeatureValue("covered_h_common_nouns", "" + ((float)covered_h_common_nouns)/tot_h_common_nouns);
			instance.setFeatureValue("covered_h_adjectives", "" + ((float)covered_h_adjectives)/tot_h_adjectives);

			instance.setFeatureValue("covered_with_similarity_h_verbs", "" + ((float)covered_with_similarity_h_verbs)/tot_h_verbs);
			instance.setFeatureValue("covered_with_similarity_h_common_nouns", "" + ((float)covered_with_similarity_h_common_nouns)/tot_h_common_nouns);
			instance.setFeatureValue("covered_with_similarity_h_adjectives", "" + ((float)covered_with_similarity_h_adjectives)/tot_h_adjectives);

			instance.setFeatureValue("covered_with_antinomy_h_verbs", "" + ((float)covered_with_antinomy_h_verbs)/tot_h_verbs);
			instance.setFeatureValue("covered_with_antinomy_h_common_nouns", "" + ((float)covered_with_antinomy_h_common_nouns)/tot_h_common_nouns);
			instance.setFeatureValue("covered_with_antinomy_h_adjectives", "" + ((float)covered_with_similarity_h_adjectives)/tot_h_adjectives);
		}
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
