package it.uniroma2.art.marte.processors.lexSimilarity;

import it.reveal.chaos.xdg.textstructure.Constituent;

public interface ConstituentRelationDetectorBasic {

	/**
	  * Given two constituent compute the strength of their relation, which is a value
	  * between 0 and 1.
	  *
	  * @param c1    input constituent
	  * @param c1    input constituent
	  * @return      strength score
	  */ 
	 public abstract float relation_strength(Constituent c1, Constituent c2) throws Exception;
		
		
	 /**
	  * Type of the relation between the constituent
	  * @return      relation type
	  */ 
		public abstract String relation_type();

		public abstract float max_similarity_value();
		

}
