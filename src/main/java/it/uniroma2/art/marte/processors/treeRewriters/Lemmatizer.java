package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.dictionaries.*;
import it.uniroma2.art.marte.structures.*;


import java.util.*;

public class Lemmatizer extends GenericTreeRewriter {
	
	private LemmaDictionary lemma_dictionary = null;
	private boolean lemmatize = false;
	
	/**
		creates a lemmatizer that adds the lemmas in the SimpleConst 	
	*/
	public Lemmatizer(LemmaDictionary ld){
		lemma_dictionary = ld;
	}

	/**
		creates a lemmatizer that adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens 
	
	*/
	public Lemmatizer(LemmaDictionary ld,boolean lemmatize){
		lemma_dictionary = ld;
		this.lemmatize = lemmatize;
		System.out.println("Lemmatiser is " + lemmatize);
	}
	
	public Lemmatizer(boolean initalization, LemmaDictionary dictionary, boolean lemmatize) {
		lemma_dictionary = dictionary;
		this.lemmatize = lemmatize;
		System.out.println("Lemmatiser is " + lemmatize);
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens	
	*/
	public XDG transform(XDG xdg) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				sc.getFirstLemma().setSurface(lemma_dictionary.lemma(sc));
				if (lemmatize) sc.setSurface(lemma_dictionary.lemma(sc));
			} 
		}
		return xdg;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}



}