
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transform(T,TT):-
  tell(pippo),write(T),nl,
  divide(T,TT_R,A),
  !,
  compose_sent(TT_R,A,TT),
  write(TT),nl,told.


compose_sent(T,[],T):-!.
compose_sent(TT_R,A,TT):-!,
	TT =.. ['S1',9999,TT_R|A].



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE ALGPRITHM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

divide(N,N,[]):-
  atom(N).

divide(N,N,[]):-
  number(N).


divide(N1,N,SENTENCES):-
  rule(N1,N,SENTENCES),!.
  
  
divide(N1,N2,SENTENCES):-
  N1 =.. [A,ID|R1],
  !,
  list_divided(R1,R2,SENTENCES),
  N2 =.. [A,ID|R2].
  

list_divided([],[],[]).

list_divided([A1|R1],[A2|R2],ALL_SENTECES):-
  divide(A1,A2,SENTS),
  !,
  list_divided(R1,R2,SENT_REST),
  append(SENTS,SENT_REST,ALL_SENTECES).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  rule(MATCH,WHAT_REMAINS_HERE,SENTENCE_TO_BE_PUT_AT_THE_TOP).

rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
    'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).

          


%% NOTE:
%% 1. Regole annidate non possono essere catturate (the book which was turined into a script which was done in TV) (vedi ES (**) piu' avanti)
%% 2. le regole per l'apposizione hanno un problema: vedi subito qui sotto.
%% 3. Ovviamente va creato un diverso file PL per ogni fenomeno (apposizioni, who, which, whose...)
%
%% *********
%% APPO RULES
%% *********
%% ATTENZIONE!
%%regole pericolose. E' necessario fare in modo che scattino solo quando almeno uno dei NP non sia un NNP. Infatti
%% nell'attuale versione viene catturato il caso:
%% Roma, Italia, leads the league --> Roma leads the league, Roma is Italia
%
%
%% the tennis champion, John McEnroe, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
%rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
%     NP_SUB,['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
%     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
%     PROP_NAME =.. ['NNP'|REST3].
%          
%
%% John McEnroe, the tennis champion, wins ... --> John McEnrone wins ... John McEnroe is the tennis champion.
%rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l)),
%     NP_SUP,['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
%     NP_SUB =.. ['NP'|REST2].
%          
%%'S1'(82,'S'(81,'NP'(48,'NP'(2,'NNP'(1,'Helena',l),'NNP'(2,'Brighton',l)),','(4,',',l),'NP'(47,'NP'(6,'DT'(5,an,l),'NN'(6,attorney,l)),'PP'(11,'IN'(8,for,l),'NP'(10,'NNP'(9,'Eliza',l),'NNP'(10,'May',l))),'SBAR'(46,'WHNP'(13,'WP'(13,who,l)),'S'(45,'VP'(44,'VBD'(15,served,l),'PP'(43,'IN'(16,as,l),'NP'(42,'NP'(18,'JJ'(17,executive,l),'NN'(18,director,l)),'PP'(41,'IN'(20,of,l),'NP'(40,'NP'(29,'DT'(21,the,l),'ADJP'(23,'NNP'(22,'Texas',l),'JJ'(23,'Funeral',l)),'NNP'(25,'Services',l),'NNP'(26,'Commission',l),'NN'(27,'-the',l),'NN'(28,state,l),'NN'(29,agency,l)),'SBAR'(39,'WHNP'(31,'WDT'(31,that,l)),'S'(38,'VP'(37,'VBZ'(33,regulates,l),'NP'(36,'DT'(34,the,l),'JJ'(35,funeral,l),'NN'(36,'business-',l)))))))))))))),'VP'(79,'VBD'(50,claimed,l),'SBAR'(78,'IN'(51,that,l),'S'(77,'NP'(52,'PRP'(52,she,l)),'VP'(76,'AUX'(54,was,l),'VP'(75,'VBN'(55,fired,l),'PP'(60,'IN'(56,from,l),'NP'(59,'PRP$'(57,her,l),'NN'(58,state,l),'NN'(59,job,l))),'SBAR'(74,'IN'(62,because,l),'S'(73,'NP'(63,'PRP'(63,she,l)),'VP'(72,'VBD'(65,raised,l),'NP'(71,'NP'(66,'NNS'(66,questions,l)),'PP'(70,'IN'(68,about,l),'NP'(69,'NNP'(69,'SCI',l)))))))))))),'.'(81,'.',l)))
%
%
%% le seguenti regole catturano i casi di apposiozione seguiti da una SBAR
%
%% the tennis champion, John McEnroe, who won... --> John McEnrone who won ... John McEnroe is the tennis champion.
%rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
%     'NP'(ID_MAIN,NP_SUB,SBAR),['S'(999,NP_SUB,'VP'('AUX'(999,is,l),NP_SUP))]):- 
%     NP_SUB =.. ['NP',_,PROP_NAME|REST2],
%     PROP_NAME =.. ['NNP'|REST3].
%
%%'S1'(88,'S'(87,'PP'(41,'IN'(1,'In',l),'NP'(40,'NP'(3,'DT'(2,a,l),'NN'(3,move,l)),'ADJP'(39,'JJ'(5,reminiscent,l),'PP'(38,'IN'(6,for,l),'NP'(37,'NP'(7,'DT'(7,some,l)),'PP'(36,'IN'(9,of,l),'NP'(35,'NP'(11,'DT'(10,another,l),'NN'(11,actor,l)),','(13,',',l),'NP'(15,'NNP'(14,'Ronald',l),'NNP'(15,'Reagan',l)),','(17,',',l),'SBAR'(34,'WHNP'(18,'WP'(18,who,l)),'S'(33,'VP'(32,'AUX'(20,was,l),'ADVP'(21,'RB'(21,twice,l)),'VP'(31,'VBN'(23,elected,l),'S'(30,'NP'(29,'NP'(24,'NN'(24,governor,l)),'PP'(28,'IN'(26,of,l),'NP'(27,'NNP'(27,'California',l)))))))))))))))),','(43,',',l),'NP'(44,'NNP'(44,'Schwarzenegger',l)),'VP'(85,'VBD'(46,said,l),'SBAR'(84,'S'(83,'NP'(47,'PRP'(47,he,l)),'VP'(82,'MD'(49,would,l),'VP'(81,'AUX'(50,be,l),'VP'(80,'VBG'(51,putting,l),'NP'(54,'PRP$'(52,his,l),'NN'(53,movie,l),'NN'(54,career,l)),'PP'(58,'IN'(56,on,l),'NP'(57,'NN'(57,hold,l))),'SBAR'(79,'IN'(60,so,l),'S'(78,'NP'(61,'PRP'(61,he,l)),'VP'(77,'MD'(63,can,l),'VP'(76,'VB'(64,devote,l),'NP'(66,'PRP$'(65,his,l),'NN'(66,time,l)),'PP'(75,'TO'(68,to,l),'S'(74,'VP'(73,'VBG'(69,running,l),'PP'(72,'IN'(70,for,l),'NP'(71,'NN'(71,governor,l)))))))))))))))),'.'(87,'.',l)))
%
%
%% John McEnroe, the tennis champion, who won ... --> John McEnrone who won ... John McEnroe is the tennis champion.
%rule( 'NP'(ID_MAIN,NP_SUP,','(_,',',l),NP_SUB,','(_,',',l),SBAR),
%     'NP'(ID_MAIN,NP_SUB,SBAR),['S'(999,NP_SUP,'VP'('AUX'(999,is,l),NP_SUB))]):- 
%     NP_SUB =.. ['NP'|REST2].
%         
%         
%         
%      
%          
%% *********
%% WHO RULES
%% *********
%
%
%% The shoes for John McEnroe, who plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 
%
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%%'S1'(89,'S'(88,'S'(67,'NP'(42,'NP'(27,'NP'(16,'JJ'(15,'-The',l),'NNS'(16,bodies,l)),'PP'(26,'IN'(18,of,l),'NP'(25,'NP'(20,'NNP'(19,'Hector',l),'NNP'(20,'Oqueli',l)),'CC'(22,and,l),'NP'(24,'NNP'(23,'Gilda',l),'NNP'(24,'Flores',l))))),','(29,',',l),'SBAR'(40,'WHNP'(30,'WP'(30,who,l)),'S'(39,'VP'(38,'AUX'(32,had,l),'VP'(37,'AUX'(33,been,l),'VP'(36,'VBN'(34,kidnapped,l),'NP'(35,'NN'(35,yesterday,l))))))),','(42,',',l)),'VP'(66,'AUX'(44,were,l),'VP'(65,'VBN'(45,found,l),'PP'(53,'IN'(46,in,l),'NP'(52,'NP'(47,'NNP'(47,'Cuilapa',l)),','(49,',',l),'NP'(50,'NNP'(50,'Guatemala',l)),','(52,',',l))),'PP'(64,'IN'(55,near,l),'NP'(63,'NP'(57,'DT'(56,the,l),'NN'(57,border,l)),'PP'(62,'IN'(59,with,l),'NP'(61,'NNP'(60,'El',l),'NNP'(61,'Salvador',l)))))))),','(69,',',l),'NP'(82,'NP'(71,'DT'(70,the,l),'NNS'(71,relatives,l)),'PP'(81,'IN'(73,of,l),'NP'(80,'NP'(74,'CD'(74,one,l)),'PP'(79,'IN'(76,of,l),'NP'(78,'DT'(77,the,l),'NNS'(78,victims,l)))))),'VP'(86,'AUX'(84,have,l),'VP'(85,'VBN'(85,reported,l))),'.'(88,'.',l)))
%
%
%
%% The shoes for John McEnroe, who plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%
%% The shoes for John McEnroe who plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays ....
%% NP1( NP2,NP4(PP(PREP,NP3)) , SBAR ) -->  ... (NP3,SBAR)
%
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%
%% The shoes for John McEnroe who plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays .... 
%% NP1( NP2(PP(PREP,NP3)) , SBAR ) -->  ... (NP3,SBAR)
%
%rule( 'NP'(ID_NP,NP_SUP,PP,'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP,PP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%
%
%
%% John McEnroe, who plays ... , goes... --> John McEnrone goes ... John McEnroe plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%%'S1'(87,'S'(86,'NP'(43,'NP'(2,'NNP'(1,'Linda',l),'NNP'(2,'Johnson',l)),','(4,',',l),'SBAR'(41,'WHNP'(5,'WP'(5,who,l)),'S'(40,'VP'(39,'VBZ'(7,lives,l),'PP'(21,'IN'(8,with,l),'NP'(20,'NP'(15,'NP'(10,'PRP'(9,her,l),'NN'(10,husband,l)),','(12,',',l),'NP'(13,'NNP'(13,'Charles',l)),','(15,',',l)),'CC'(17,and,l),'NP'(19,'CD'(18,two,l),'NNS'(19,cats,l)))),'PP'(38,'IN'(23,in,l),'NP'(37,'NP'(34,'NP'(27,'DT'(24,a,l),'VBN'(25,rented,l),'JJ'(26,'one-bedroom',l),'NN'(27,apartment,l)),'PP'(32,'IN'(29,in,l),'NP'(31,'NNP'(30,'San',l),'NNP'(31,'Francisco',l))),'POS'(34,'&apos;s',l)),'NNP'(36,'Mission',l),'NNP'(37,'District',l)))))),','(43,',',l)),'VP'(84,'VBD'(45,said,l),'SBAR'(83,'S'(82,'NP'(46,'NNP'(46,'Katrina',l)),'VP'(81,'AUX'(48,has,l),'VP'(80,'VBN'(49,made,l),'S'(79,'NP'(50,'PRP'(50,her,l)),'VP'(78,'VB'(52,realize,l),'SBAR'(77,'S'(76,'NP'(53,'PRP'(53,she,l)),'VP'(75,'VBZ'(55,needs,l),'S'(74,'VP'(73,'TO'(56,to,l),'VP'(72,'VB'(57,gather,l),'ADVP'(58,'RB'(58,together,l)),','(60,',',l),'PP'(65,'IN'(61,in,l),'NP'(64,'CD'(62,one,l),'JJ'(63,accessible,l),'NN'(64,place,l))),','(67,',',l),'NP'(71,'PRP$'(68,her,l),'JJ'(69,scattered,l),'NN'(70,disaster,l),'NNS'(71,supplies,l))))))))))))))),'.'(86,'.',l)))
%
%     
%     
%% John McEnroe, who plays ...  goes... --> John McEnrone goes ... John McEnroe plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%%'S1'(47,'SINV'(46,'S'(14,'NP'(3,'DT'(1,'A',l),'NN'(2,force,l),'NN'(3,majeure,l)),'VP'(13,'AUX'(5,is,l),'NP'(12,'NP'(7,'DT'(6,an,l),'NN'(7,act,l)),'PP'(11,'IN'(9,of,l),'NP'(10,'NNP'(10,'God',l)))))),','(16,',',l),'VP'(17,'VBD'(17,said,l)),'NP'(44,'NP'(21,'NN'(19,attorney,l),'NNP'(20,'Phil',l),'NNP'(21,'Wittmann',l)),','(23,',',l),'SBAR'(43,'WHNP'(24,'WP'(24,who,l)),'S'(42,'VP'(41,'VBZ'(26,represents,l),'NP'(40,'NP'(30,'DT'(27,the,l),'NNP'(28,'New',l),'NNP'(29,'Orleans',l),'NNS'(30,'Saints',l)),'CC'(32,and,l),'NP'(39,'NP'(36,'NN'(33,owner,l),'NNP'(34,'Tom',l),'NNP'(35,'Benson',l),'POS'(36,'&apos;s',l)),'JJ'(38,local,l),'NNS'(39,interests,l)))))
%
%     
%% people who play ... go... --> people go ... people play .... 
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WP'(_,who,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%%'S1'(47,'SINV'(46,'S'(14,'NP'(3,'DT'(1,'A',l),'NN'(2,force,l),'NN'(3,majeure,l)),'VP'(13,'AUX'(5,is,l),'NP'(12,'NP'(7,'DT'(6,an,l),'NN'(7,act,l)),'PP'(11,'IN'(9,of,l),'NP'(10,'NNP'(10,'God',l)))))),','(16,',',l),'VP'(17,'VBD'(17,said,l)),'NP'(44,'NP'(21,'NN'(19,attorney,l),'NNP'(20,'Phil',l),'NNP'(21,'Wittmann',l)),','(23,',',l),'SBAR'(43,'WHNP'(24,'WP'(24,who,l)),'S'(42,'VP'(41,'VBZ'(26,represents,l),'NP'(40,'NP'(30,'DT'(27,the,l),'NNP'(28,'New',l),'NNP'(29,'Orleans',l),'NNS'(30,'Saints',l)),'CC'(32,and,l),'NP'(39,'NP'(36,'NN'(33,owner,l),'NNP'(34,'Tom',l),'NNP'(35,'Benson',l),'POS'(36,'&apos;s',l)),'JJ'(38,local,l),'NNS'(39,interests,l)))))
%
%
%
%% *********
%% WHICH RULES
%% *********
%
%% The shoes for John McEnroe, which plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 
%
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%% The shoes for John McEnroe, which plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%% The shoes for John McEnroe which plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays ....
%% NP1( NP2,NP4(PP(PREP,NP3)) , SBAR ) -->  ... (NP3,SBAR)
%
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     NP_SUP =.. ['NP',IDA,NP_PRINC,PP],
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%% The shoes for John McEnroe which plays ...   are broken --> the shoes of John McEnrone are broken ... John McEnroe plays .... 
%% NP1( NP2(PP(PREP,NP3)) , SBAR ) -->  ... (NP3,SBAR)
%
%rule( 'NP'(ID_NP,NP_SUP,PP,'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP,PP),['S'(ID_SUB,NP_SUB,VP_SUB)]):-
%     PP =.. ['PP',IDB,PREP,NP_SUB].
%
%
%
%% people, which play, ... go... --> people go ... people play .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%
%
%% people, which play ... go... --> people go ... people play .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%
%% people which play ... go... --> people go ... people play .... 
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WDT'(_,which,l)),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,NP_SUP,VP_SUB)]).
%
%
%
%% *********
%% WHOSE RULES
%% *********
%
%
%% John McEnroe, whose son plays ... , goes... --> John McEnrone goes ... John McEnroe's son plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP$'(_,whose,l),NP_POSS),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,'NP'(997,'NP'(998,NP_SUP,'POS'(999,'&apos;s',l)),NP_POSS),VP_SUB)]).
%
%%'S1'(64,'S'(63,'NP'(22,'NP'(2,'NNP'(1,'Larry',l),'NNP'(2,'Estrada',l)),','(4,',',l),'SBAR'(20,'WHNP'(6,'WP$'(5,whose,l),'NN'(6,father,l)),'S'(19,'VP'(18,'VBD'(8,served,l),'PP'(13,'IN'(9,in,l),'NP'(12,'DT'(10,the,l),'NNP'(11,'Marine',l),'NNP'(12,'Corps',l))),'PP'(17,'IN'(15,in,l),'NP'(16,'NNP'(16,'Vietnam',l)))))),','(22,',',l)),'VP'(61,'VBD'(24,said,l),'SBAR'(60,'S'(59,'NP'(25,'PRP'(25,he,l)),'VP'(58,'AUX'(27,has,l),'ADVP'(28,'RB'(28,always,l)),'VP'(57,'VP'(40,'AUX'(30,had,l),'NP'(39,'NP'(33,'DT'(31,a,l),'JJ'(32,great,l),'NN'(33,appreciation,l)),'PP'(38,'IN'(35,for,l),'NP'(37,'DT'(36,the,l),'NN'(37,military,l))))),'CC'(42,and,l),'VP'(56,'VBD'(43,wanted,l),'S'(55,'VP'(54,'TO'(44,to,l),'VP'(53,'AUX'(45,do,l),'NP'(52,'PRP$'(46,his,l),'NN'(47,part,l),'S'(51,'VP'(50,'TO'(48,to,l),'VP'(49,'VB'(49,help,l)))))))))))))),'.'(63,'.',l)))
%
%
%% John McEnroe, whose son plays ...  goes... --> John McEnrone goes ... John McEnroe's son plays .... 
%rule( 'NP'(ID_NP,NP_SUP,','(_,',',l),'SBAR'(_,'WHNP'(_,'WP$'(_,whose,l),NP_POSS),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,'NP'(997,'NP'(998,NP_SUP,'POS'(999,'&apos;s',l)),NP_POSS),VP_SUB)]).
%
%
%% John McEnroe whose son plays ...,  goes... --> John McEnrone goes ... John McEnroe's son plays .... 
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WP$'(_,whose,l),NP_POSS),'S'(ID_SUB,VP_SUB)),','(_,',',l)),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,'NP'(997,'NP'(998,NP_SUP,'POS'(999,'&apos;s',l)),NP_POSS),VP_SUB)]).
%
%
%% John McEnroe whose son plays ...  goes... --> John McEnrone goes ... John McEnroe's son plays .... 
%rule( 'NP'(ID_NP,NP_SUP,'SBAR'(_,'WHNP'(_,'WP$'(_,whose,l),NP_POSS),'S'(ID_SUB,VP_SUB))),
%     'NP'(ID_NP,NP_SUP),['S'(ID_SUB,'NP'(997,'NP'(998,NP_SUP,'POS'(999,'&apos;s',l)),NP_POSS),VP_SUB)]).
%
%
%
%
%%(**) ESEMPIO ANNIDATO DI WHICH: 
%%'S1'(84,'S'(83,'NP'(13,'NP'(1,'JJS'(1,'Most',l)),'PP'(12,'IN'(3,of,l),'NP'(11,'NP'(5,'DT'(4,the,l),'NN'(5,life,l)),'PP'(10,'IN'(7,of,l),'NP'(9,'NNP'(8,'Petko',l),'NNP'(9,'Kiryakov',l)))))),'VP'(81,'AUX'(15,was,l),'RB'(16,not,l),'VP'(80,'VBN'(17,unveiled,l),'PP'(79,'PP'(20,'IN'(18,by,l),'NP'(19,'NNS'(19,historians,l))),'CC'(22,but,l),'PP'(78,'IN'(23,by,l),'NP'(77,'NP'(29,'DT'(24,the,l),'JJ'(25,prominent,l),'JJ'(26,'Bulgarian',l),'NN'(27,writer,l),'NNP'(28,'Nikolai',l),'NNP'(29,'Haitov',l)),','(31,',',l),'SBAR'(76,'WHNP'(32,'WP'(32,who,l)),'S'(75,'VP'(74,'VBD'(34,wrote,l),'NP'(73,'NP'(36,'DT'(35,a,l),'NN'(36,novel,l)),'CC'(38,and,l),'NP'(72,'NP'(40,'DT'(39,a,l),'NN'(40,script,l)),'SBAR'(71,'WHNP'(42,'WDT'(42,which,l)),'S'(70,'VP'(69,'AUX'(44,was,l),'VP'(68,'VBN'(45,turned,l),'PP'(67,'IN'(46,into,l),'NP'(66,'NP'(49,'DT'(47,a,l),'NN'(48,'TV',l),'NN'(49,series,l)),','(51,',',l),'SBAR'(65,'WHNP'(52,'WDT'(52,which,l)),'S'(64,'VP'(63,'VBD'(54,became,l),'NP'(62,'NP'(56,'DT'(55,a,l),'NN'(56,favourite,l)),'PP'(61,'IN'(58,of,l),'NP'(60,'JJS'(59,most,l),'NNPS'(60,'Bulgarians',l))))))))))))))))))))))),'.'(83,'.',l)))
%
%%:-transform( 'S1'(58,'S'(57,'NP'(51,'NP'(1,'NNP'(1,'Miller',l)),','(3,',',l),'NP'(49,'NP'(7,'DT'(4,the,l),'NNP'(5,'Pulitzer',l),'JJ'(6,'Prize-winning',l),'NN'(7,playwright,l)),'SBAR'(48,'WHNP'(31,'WHNP'(14,'WP$'(9,whose,l),'ADJP'(12,'RBS'(10,most,l),'JJ'(11,famous,l),'JJ'(12,fictional,l)),'NN'(14,creation,l)),','(16,',',l),'NP'(17,'NNP'(17,'Loman',l)),'PP'(30,'IN'(19,in,l),'FRAG'(29,'&apos;&apos;'(20,'&apos;',l),'NP'(28,'NP'(21,'NNP'(21,'Death',l)),'PP'(26,'IN'(23,of,l),'NP'(25,'DT'(24,a,l),'NN'(25,'Salesman',l))),'&apos;&apos;'(28,'&apos;',l))))),'S'(47,'VP'(46,'VBD'(33,came,l),'S'(45,'VP'(44,'TO'(34,to,l),'VP'(43,'VB'(35,symbolize,l),'NP'(38,'DT'(36,the,l),'NNP'(37,'American',l),'NN'(38,'Dream',l)),'VP'(42,'VBN'(40,gone,l),'ADVP'(41,'RB'(41,awry,l)))))))))),','(51,',',l)),'VP'(55,'AUX'(53,has,l),'VP'(54,'VBN'(54,died,l))),'.'(57,'.',l))),T).
%
%
