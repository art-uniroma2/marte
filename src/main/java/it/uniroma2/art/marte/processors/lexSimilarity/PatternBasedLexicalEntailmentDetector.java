package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.processors.lexSimilarity.ConstituentRelationDetector;
import it.uniroma2.art.marte.structures.POS;
import it.uniroma2.art.marte.utils.VerbPairCount;

public class PatternBasedLexicalEntailmentDetector extends ConstituentRelationDetector {


	private Hashtable<String,VerbPairCount> pair_counts = new Hashtable<String,VerbPairCount>();
	private Hashtable<String,VerbPairCount> false_friends = new Hashtable<String,VerbPairCount>();
	private long T_SIMPLE_FORMS         = 0 ;
	private long H_NOMINALIZATION_FORMS = 0 ;
	private long H_ALL_FORMS            = 0 ;
	private long T_ALL_FORMS            = 0 ;
	private long H_NOM_T_PAIRS          = 0 ;
	private long PE_PAIRS               = 0 ;
	private long HB_PAIRS               = 0 ;
	   
   private HashMap<String,Double> max = new HashMap<String,Double> ();
   private String google_file = null;
   private String count_dictionary;
   
   
   private String type = "#e#";
   private String selected_pattern = "nom";
                
	public PatternBasedLexicalEntailmentDetector(String count_dictionary) throws Exception {
		System.out.println("Google Intializiation");
		google_file = count_dictionary + ".missing";
		if ((new File(count_dictionary)).exists()) {
			this.loadCountDictionary(count_dictionary, pair_counts);
			try {     
				this.loadCountDictionary(count_dictionary+".false_friends", false_friends);
			} catch (FileNotFoundException e) {
				System.out.println(" No false friends of : " + count_dictionary);
				
			}
			this.loadTotalValue(count_dictionary+".counts");
		} else {
			initMaxTable();
			System.err.println("Missing Google File: " + count_dictionary );
		}
	} 

	
	public PatternBasedLexicalEntailmentDetector(boolean initialiazation, String count_dictionary) {
		this.count_dictionary = count_dictionary;
		this.setNameAndDescription("PATT", "uses patterns (on the web) to detect relations among verbs");
	}

	@Override
	public void initialize() throws Exception {

		System.out.println("Google Intializiation");
		google_file = count_dictionary + ".missing";
		if ((new File(count_dictionary)).exists()) {
			this.loadCountDictionary(count_dictionary, pair_counts);
			try {     
				this.loadCountDictionary(count_dictionary+".false_friends", false_friends);
			} catch (FileNotFoundException e) {
				System.out.println(" No false friends of : " + count_dictionary);
				
			}
			this.loadTotalValue(count_dictionary+".counts");
		} else {
			initMaxTable();
			System.err.println("Missing Google File: " + count_dictionary );
		}
	}

	public String relation_type() {return type;}
	
	public PatternBasedLexicalEntailmentDetector setPattern(String pattern) {
		this.selected_pattern = pattern;
		return this;
	}
	
	public float relation_strength(Constituent t, Constituent h) throws Exception {
		float f = 0;
		if (POS.verb(h) && POS.verb(t)) {
			f = (float) compute(selected_pattern,h.getFirstLemma().getSurface(),t.getFirstLemma().getSurface());
		}
		return f;
	}

	public void loadCountDictionary(String count_dictionary, Hashtable<String,VerbPairCount> dictionary) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(count_dictionary));
		String in_line = in.readLine();
		while (in_line != null) {
			VerbPairCount vpc = new VerbPairCount(in_line);
			pair_counts.put(vpc.pair(),vpc);
			in_line = in.readLine();
		}
	}
	
	public void initMaxTable() {
		max.put("nom"       		,0.0); 
		max.put("hb"        		,0.0); 
		max.put("pe"        		,0.0); 
		max.put("nom_pe"    		,0.0); 
		max.put("hb_pe"     		,0.0); 
		max.put("hb_nom_pe" 		,0.0); 
		max.put("hbinv_pe"  		,0.0); 
		max.put("hbinv_nom_pe"  ,0.0);
	}
	
	public void loadTotalValue(String count_dictionary) throws Exception {
		if ((new File(count_dictionary)).exists()) { 
			BufferedReader in = new BufferedReader(new FileReader(count_dictionary));
			String in_line = in.readLine();
			while (in_line != null) {
				StringTokenizer st = new StringTokenizer(in_line.trim(),"=");
				if (st.countTokens() == 2) {
					String type = st.nextToken();
					String value = st.nextToken();
					if (type.equals("T_SIMPLE_FORMS")) T_SIMPLE_FORMS=new Long(value);
					else if (type.equals("T_ALL_FORMS")) T_ALL_FORMS=new Long(value);
					else if (type.equals("H_ALL_FORMS")) H_ALL_FORMS=new Long(value);
					else if (type.equals("H_NOMINALIZATION_FORMS")) H_NOMINALIZATION_FORMS=new Long(value);
					else if (type.equals("H_NOM_T_PAIRS")) H_NOM_T_PAIRS=new Long(value);
					else if (type.equals("PE_PAIRS")) PE_PAIRS=new Long(value);
					else if (type.equals("HB_PAIRS")) HB_PAIRS=new Long(value);
					else if (type.equals("max[nom]"      )) max.put("nom",new Double(value));
					else if (type.equals("max[hb]"       )) max.put("hb",new Double(value));
					else if (type.equals("max[pe]"       )) max.put("pe",new Double(value));
					else if (type.equals("max[nom_pe]"   )) max.put("nom_pe"   , new Double(value));
					else if (type.equals("max[hb_pe]"    )) max.put("hb_pe"    , new Double(value));
					else if (type.equals("max[hb_nom_pe]")) max.put("hb_nom_pe", new Double(value));
					else if (type.equals("max[hbinv_pe]" )) max.put("hbinv_pe" , new Double(value));
					else if (type.equals("max[hbinv_nom_pe]" )) max.put("hbinv_nom_pe" ,new Double(value));
					else System.err.println("File: " + count_dictionary + "\nError in line: >" + in_line + "<");
				} else if (!in_line.trim().equals("")) {
					System.err.println("File: " + count_dictionary + "\nError in line: >" + in_line + "<");
				}
				in_line = in.readLine();
			}
			in.close();	
		} else {
			Vector <String> v_h = new Vector<String>();
			Vector <String> v_t = new Vector<String>();
			Enumeration<String> pairs = pair_counts.keys();
			initMaxTable();
			while (pairs.hasMoreElements()) {
				VerbPairCount vpc = (VerbPairCount) pair_counts.get(pairs.nextElement());
				H_NOM_T_PAIRS += vpc.nomCount();
				PE_PAIRS += vpc.peCount();
				HB_PAIRS += vpc.hbCount();
				if (!v_h.contains(vpc.getVH())) {
					v_h.add(vpc.getVH());
					H_NOMINALIZATION_FORMS += vpc.hNomCount();
					H_ALL_FORMS += vpc.hAllCount();
				} 
				if (!v_t.contains(vpc.getVT())) {
					v_t.add(vpc.getVT());
					T_SIMPLE_FORMS += vpc.tSimpleCount();
					T_ALL_FORMS += vpc.tAllCount();
				} 
				if (c(compute_abs("nom",vpc.getVH(),vpc.getVT())) > max.get("nom"))                   max.put("nom"         , c(compute_abs("nom",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("hb",vpc.getVH(),vpc.getVT())) > max.get("hb"))                     max.put("hb"          , c(compute_abs("hb",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("pe",vpc.getVH(),vpc.getVT())) > max.get("pe"))                     max.put("pe"          , c(compute_abs("pe",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("nom_pe",vpc.getVH(),vpc.getVT())) > max.get("nom_pe") )            max.put("nom_pe"      , c(compute_abs("nom_pe",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("hb_pe",vpc.getVH(),vpc.getVT())) > max.get("hb_pe")    )           max.put("hb_pe"       , c(compute_abs("hb_pe",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("hb_nom_pe",vpc.getVH(),vpc.getVT())) > max.get("hb_nom_pe") )      max.put("hb_nom_pe"   , c(compute_abs("hb_nom_pe",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("hbinv_pe",vpc.getVH(),vpc.getVT())) > max.get("hbinv_pe")    )     max.put("hbinv_pe"    , c(compute_abs("hbinv_pe",vpc.getVH(),vpc.getVT())));
				if (c(compute_abs("hbinv_nom_pe",vpc.getVH(),vpc.getVT())) > max.get("hbinv_nom_pe")) max.put("hbinv_nom_pe", c(compute_abs("hbinv_nom_pe",vpc.getVH(),vpc.getVT())));
			}
			BufferedWriter in = new BufferedWriter(new FileWriter(count_dictionary));
			in.write("T_SIMPLE_FORMS="         + T_SIMPLE_FORMS         +"\n");
			in.write("H_NOMINALIZATION_FORMS=" + H_NOMINALIZATION_FORMS +"\n");
			in.write("H_ALL_FORMS="            + H_ALL_FORMS            +"\n");
			in.write("T_ALL_FORMS="            + H_ALL_FORMS            +"\n");
			in.write("H_NOM_T_PAIRS="          + H_NOM_T_PAIRS          +"\n");
			in.write("PE_PAIRS="               + PE_PAIRS               +"\n");
			in.write("HB_PAIRS="               + HB_PAIRS               +"\n"); 
			in.write("max[nom]="               + max.get("nom"         )     +"\n");
			in.write("max[hb]="                + max.get("hb"          )     +"\n");
			in.write("max[pe]="                + max.get("pe"          )     +"\n");
			in.write("max[nom_pe]="            + max.get("nom_pe"      )     +"\n");
			in.write("max[hb_pe]="             + max.get("hb_pe"       )     +"\n");
			in.write("max[hb_nom_pe]="         + max.get("hb_nom_pe"   )     +"\n");
			in.write("max[hbinv_pe]="          + max.get("hbinv_pe"    )     +"\n");
			in.write("max[hbinv_nom_pe]="      + max.get("hbinv_nom_pe")         +"\n");
			
			in.close();
			
		}  
	}     
         
	      
	public double compute(String pattern,String v_h, String v_t) throws Exception {
		double result = Double.NEGATIVE_INFINITY ;
		//System.out.println("Computing: " + pattern + " - " + v_t + " --> " + v_h );
		if (pair_counts.containsKey(v_h + ":" +v_t)) { 
			try {
				result = ((Double) this.getClass().getMethod("compute_" + pattern , new Class[] {v_h.getClass(),v_t.getClass()}).invoke(this,new Object[] {v_h,v_t} )).doubleValue();
			} catch (NoSuchMethodException e) {
				throw new Exception("The pattern required does not exist: " + pattern + "\n"+ 
				                    "   Possible patterns are:" );
			}
		} else {
			BufferedWriter verbGooglePair = new BufferedWriter(new FileWriter(google_file,true));
			verbGooglePair.write("tex_ent("+ v_t +","+ v_h + ",0,0,0,0).\n");
			verbGooglePair.close();
		}
		return result / max.get(pattern);	
	}

	public double compute_abs(String pattern,String v_h, String v_t) throws Exception {
		double result = Double.NEGATIVE_INFINITY ;
		//System.out.println("Computing: " + pattern + " - " + v_t + " --> " + v_h );
		if (pair_counts.containsKey(v_h + ":" +v_t)) { 
			try {
				result = ((Double) this.getClass().getMethod("compute_" + pattern , new Class[] {v_h.getClass(),v_t.getClass()}).invoke(this,new Object[] {v_h,v_t} )).doubleValue();
			} catch (NoSuchMethodException e) {
				throw new Exception("The pattern required does not exist: " + pattern + "\n"+ 
				                    "   Possible patterns are:" );
			}
		} else {
			BufferedWriter verbGooglePair = new BufferedWriter(new FileWriter(google_file,true));
			verbGooglePair.write("tex_ent("+ v_t +","+ v_h + ",0,0,0,0).\n");
			verbGooglePair.close();
		}
		return result;	
	}


	public Double compute_nom(String v_h, String v_t) {
		VerbPairCount vpc = (VerbPairCount) pair_counts.get(v_h + ":" +v_t);
		double result = ((double)vpc.nomCount()*T_SIMPLE_FORMS*H_NOMINALIZATION_FORMS)/((double)H_NOM_T_PAIRS*vpc.tSimpleCount()*vpc.hNomCount());
		
		return new Double(Math.log(result)) ;
	}

	public Double compute_pe(String v_h, String v_t) {
		VerbPairCount vpc = (VerbPairCount) pair_counts.get(v_h + ":" +v_t);
		double result = ((double)vpc.peCount()*T_ALL_FORMS*H_ALL_FORMS)/((double)PE_PAIRS*vpc.tAllCount()*vpc.hAllCount());
		return new Double(Math.log(result)) ;
	}

	public Double compute_hb(String v_h, String v_t) {
		VerbPairCount vpc = (VerbPairCount) pair_counts.get(v_h + ":" +v_t);
		double result = (((double)vpc.hbCount()*T_ALL_FORMS*H_ALL_FORMS)/((double)HB_PAIRS*vpc.tAllCount()*vpc.hAllCount()));
		return new Double(Math.log(result));
	}

	public Double compute_hbinv(String v_h, String v_t) {
		return new Double(0);
	}

	public Double compute_nom_pe(String v_h, String v_t) {
		return new Double(c(compute_nom(v_h,v_t).doubleValue()) + c(compute_pe(v_h,v_t).doubleValue()))  ;
	}


	public Double compute_hb_pe(String v_h, String v_t) {
		return new Double(c(compute_hb(v_h,v_t).doubleValue())  + c(compute_pe(v_h,v_t).doubleValue()) ) ;
	}

	public Double compute_hb_nom_pe(String v_h, String v_t) {
		return new Double(c(compute_nom(v_h,v_t).doubleValue()) + c(compute_hb(v_h,v_t).doubleValue()) + c(compute_pe(v_h,v_t).doubleValue())) ;
	}


	public Double compute_hbinv_pe(String v_h, String v_t) {
		return new Double(c(compute_hbinv(v_h,v_t).doubleValue())+ c(compute_pe(v_h,v_t).doubleValue())) ;
	}

	public Double compute_hbinv_nom_pe(String v_h, String v_t) {
		return new Double(c(compute_nom(v_h,v_t).doubleValue())  + c(compute_hbinv(v_h,v_t).doubleValue()) + c(compute_pe(v_h,v_t).doubleValue()));
	}
	
	public double c(double d) {
		if (d == Double.NEGATIVE_INFINITY) return 0;
		else return d;
	}
	
	public static void main (String [] argv ) {
		try {
			PatternBasedLexicalEntailmentDetector count_dictionary = new PatternBasedLexicalEntailmentDetector(argv[0]);
			
			Enumeration<String> pairs = count_dictionary.pair_counts.keys();
			while (pairs.hasMoreElements()) {
				VerbPairCount vpc = (VerbPairCount) count_dictionary.pair_counts.get(pairs.nextElement());
				
				System.out.print(vpc.getVH() + " " +  vpc.getVT());
				try {  
					System.out.print(" " + count_dictionary.compute("nom",vpc.getVH(),vpc.getVT())  );
				} catch (Exception e) {System.out.print(" none");}
				try {  
					System.out.print(" " + count_dictionary.compute("hb",vpc.getVH(),vpc.getVT())  );
				} catch (Exception e) {System.out.print(" none");}
				try {  
					System.out.print(" " + count_dictionary.compute("hb_pe",vpc.getVH(),vpc.getVT())  );
				} catch (Exception e) {System.out.print(" none");}
				try {  
					System.out.print(" " + count_dictionary.compute("hb_nom_pe",vpc.getVH(),vpc.getVT())  );
				} catch (Exception e) {System.out.print(" none");}
				try {  
					System.out.print(" " + count_dictionary.compute("nom_pe",vpc.getVH(),vpc.getVT())  );
				} catch (Exception e) {System.out.print(" none");}
				System.out.println();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}



}