package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.dictionaries.*;
import it.uniroma2.art.marte.structures.*;


import java.util.*;

/** 
	generalizes the leaf node according to the generalization dictionary.
	Change the surface form (token) of the leaf.
*/
public class LeafGeneralizer extends GenericTreeRewriter {

	GeneralizationDictionary d = null;
	public LeafGeneralizer(GeneralizationDictionary d){
		this.d = d;
	}

	public LeafGeneralizer(boolean initialization,
			GeneralizationDictionary generalizationDictionary) {
		// TODO Auto-generated constructor stub
		this.d = generalizationDictionary;
		this.setNameAndDescription("GEN", "generalize leaves according to a dictionary");
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}
	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
		generalizes the leaf node according to the generalization dictionary.
		Change the surface form (token) of the leaf.
	*/
	public XDG transform(XDG xdg) {
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c: cs) {
			Vector <Constituent> scs = c.getSimpleConstituentList();
			for (Constituent sc : scs) {
				if (d.contains(sc.getFirstLemma().getSurface())) {
					sc.setSurface("#GEN#" + d.firstGeneralization(sc.getFirstLemma().getSurface()));
					TextTreePruner.setRelevant((SimpleConst) sc);
				}
			} 
		}

		return xdg;
	}

	

}