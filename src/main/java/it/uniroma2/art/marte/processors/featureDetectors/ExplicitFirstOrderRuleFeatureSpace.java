package it.uniroma2.art.marte.processors.featureDetectors;



import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import java.io.*;
import java.util.*;

import alice.tuprolog.*;



public class ExplicitFirstOrderRuleFeatureSpace {


	private Theory _theory;
	private Prolog _engine;


	private Hashtable features = new Hashtable();
	private int max_feature = 0;

 /** 
  * Constructs a SyntacticTransformer object by setting private
  *	fields as specified in input parameters.
  *
  * @param rule_file    name and path of the prolog file
	*/
	public ExplicitFirstOrderRuleFeatureSpace() throws Exception {
		/*
			_theory = new Theory(new FileInputStream("D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/src/arte/processors/featureDetectors/PrologLayer/subtree.pl"));
			_engine=new Prolog();
		  _engine.setTheory(_theory);
		*/
		BufferedWriter out = new BufferedWriter(new FileWriter("D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/una_prova.pl",false));
	}

	
	/** 
	*/
	public void computeFeature(Pair p, SVMpair instance) throws Exception {
		System.out.println("STARTING");
		//Struct t = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmasAndWithVariables((Constituent) //p.getTextXDG().getSetOfConstituents().elementAt(0));
		//Struct h = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmasAndWithVariables((Constituent) //p.getHypoXDG().getSetOfConstituents().elementAt(0));
		//Struct rule_detector = new Struct("instance",new Struct("" +p.getId()),new Struct((p.getOrigValue()?"YES":"NO")), t,h);
		
		BufferedWriter out = new BufferedWriter(new FileWriter("D:/USERS/FABIO/LAVORO/PROGETTI/CVS/ARTE/una_prova.pl",true));
		
		Vector<Constituent> pp = p.getTextXDG().getSetOfConstituents().getSimpleConstituentList();
		
		out.write(p.getId() + " - " + (p.getOrigValue()?"YES":"NO") + " # ");
		
		for (Constituent c: pp) {
			out.write(c.getSurface() + "/" + c.getType().split(":")[0] + (c.getType().split(":")[1].equals("-1")? "" : ":" + c.getType().split(":")[1]) + " ");
		}

		out.write(" # ");
		
		pp = p.getHypoXDG().getSetOfConstituents().getSimpleConstituentList();
		
		for (Constituent c: pp) {
			out.write(c.getSurface() + "/" + c.getType().split(":")[0] + (c.getType().split(":")[1].equals("-1")? "" : ":" + c.getType().split(":")[1]) + " ");
		}
		out.write("\n");
		out.close();
		
		/*
		Var lhs = new Var();
		Var rhs = new Var();
		Struct rule_detector = new Struct("a_rule",t,h,lhs,rhs);
		boolean more_solutions = true;
		SolveInfo info = _engine.solve(rule_detector);
		if (info.isSuccess()) {
			System.out.println(lhs.toStringFlattened()  + " --> " + rhs.toStringFlattened()  );
			while (more_solutions) {
				try {
					info = _engine.solveNext();
					System.out.println(lhs.toStringFlattened()  + " --> " + rhs.toStringFlattened()  );
				} catch (NoMoreSolutionException e) {
					more_solutions = false;
				}
			}
		}
			*/
 		
	}
}
