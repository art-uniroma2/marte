package it.uniroma2.art.marte.processors.featureDetectors;

import org.apache.commons.cli.CommandLine;

import it.uniroma2.art.marte.processors.CascadeOfProcessors;
import it.uniroma2.art.marte.processors.Processor;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;

public class FeatureDetectorChain extends CascadeOfProcessors implements
		FeatureDetector {

	public EntailmentPair computeFeature(EntailmentPair pair) throws Exception {
		if (this.getActiveProcessors() != null) {

			for (Processor p:this.getActiveProcessors()) {
				pair = ((GenericFeatureDetector) p).computeFeature(pair);
			}
		}
		return pair;
	}

	public void addFeature(EntailmentPairSet set) throws Exception {
		if (this.getActiveProcessors() != null) {
			for (Processor p:this.getActiveProcessors()) {
			((GenericFeatureDetector) p).addFeature(set);
			}
		}

	}

	public void initialize(CommandLine commands) {
		if (this.getActiveProcessors() != null)
			for (Processor p:this.getActiveProcessors()) {
				((GenericFeatureDetector) p).initialize(commands);
			}
	}

}
