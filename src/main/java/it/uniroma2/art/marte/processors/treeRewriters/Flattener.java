package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;


import java.util.*;

/**
 * flattens the tree 
*/
public class Flattener extends GenericTreeRewriter {


	public Flattener() {
		
	}

	public Flattener(boolean initalization) {
		this.setNameAndDescription("FLT", "builds flat trees using only the leaves");
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		p.setTextXDG(transform(p.getTextXDG()));
		p.setHypoXDG(transform(p.getHypoXDG()));
		return p;
	}

	/** 
		generalizes the leaf node according to the generalization dictionary.
		Change the surface form (token) of the leaf.
	*/
	public XDG transform(XDG xdg) {
		Vector <Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
		ConstituentList cl_sub = new ConstituentList();
		for (Constituent c: cs) {
			//c.setType("*");			
			cl_sub.add(c);
		}

		if (cl_sub.size()==0) cl_sub.add(new SimpleConst("EMPTY","EMPTY",1));
		
		Constituent c = new ComplxConst("Sequence",cl_sub);
		ConstituentList cl = new ConstituentList();
		cl.add(c);
		XDG xdg_out = new XDG();
		
		xdg_out.setSetOfConstituents(cl);		
		return xdg_out;
	}

	@Override
	public void initialize() throws Exception {
		
	}
	

}