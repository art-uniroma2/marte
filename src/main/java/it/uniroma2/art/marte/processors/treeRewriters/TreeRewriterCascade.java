package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.processors.*;
import it.uniroma2.art.marte.structures.*;

import java.util.*;

public class TreeRewriterCascade extends CascadeOfProcessors 
//implements GenericTreeRewriter  
{
	
	//private Vector <GenericTreeRewriter> tree_rewriters = null;


	/**
		creates a cascade of tree-rewriters	
	public TreeRewriterCascade(Vector <GenericTreeRewriter> tree_rewriters){
		//this.tree_rewriters = tree_rewriters;
		Vector<Processor> tree_rewriters_2 = (Vector<Processor>) tree_rewriters;
		this.setProcessors( tree_rewriters_2);
	}
	*/
	
	/**
		creates a cascade of tree-rewriters	
	*/
	public TreeRewriterCascade(){
		//this.tree_rewriters = new Vector<GenericTreeRewriter>();
		this.setDescription("possible modules are:");
		this.setProcessors(new Vector<Processor>());
	}

	/**
		adds a rewriter
	*/
	public void add(GenericTreeRewriter t){
		this.getProcessors().add((Processor)t);
		//tree_rewriters.add(t);
	}

	/** 
		applies the tree rewriters in sequence
	*/
	public Pair transform(Pair p) throws Exception{
		for (Processor t: this.getActiveProcessors())
			p =  ((GenericTreeRewriter) t).transform(p);
		return p;
	}



	/** 
		applies the tree rewriters in sequence
	*/
	public XDG transform(XDG xdg) throws Exception{
		XDG xdg_ = xdg;
		for (Processor t: this.getProcessors())
			xdg_ =  ((GenericTreeRewriter) t).transform(xdg_);
		return xdg_;
	}



}