package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;

import java.util.*;

public class BOW {


	/** 
		adds the lemmas in the SimpleConst 	
		and if lemmatize=true put the lemmas as surface of the tokens	
	*/
	public XDG extract(XDG xdg) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
		ConstituentList cl_sub = new ConstituentList();
		for (Constituent c: cs) {
			SimpleConst sc_new = new SimpleConst();
			
			sc_new.setType(c.getSurface());
			sc_new.setSurface("*");
			
			//sc_new.setSurface(c.getSurface());
			//sc_new.setType("*");
			
			cl_sub.add(sc_new);
		}


		Constituent c = new ComplxConst("BOW",cl_sub);
		ConstituentList cl = new ConstituentList();
		cl.add(c);
		XDG xdg_out = new XDG();
		
		xdg_out.setSetOfConstituents(cl);		
		return xdg_out;
	}



}
