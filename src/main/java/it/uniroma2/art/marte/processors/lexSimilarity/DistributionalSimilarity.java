package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;
import it.uniroma2.util.math.ArrayMath;
import it.uniroma2.util.vector.SemanticVectorProvider;

public class DistributionalSimilarity extends ConstituentRelationDetector {

private String similarity_file = null;


private SemanticVectorProvider dsDict = null;

private float max_similarity_value = 1;

private String type = "ds";


public DistributionalSimilarity(String similarity_file) throws Exception {
	this.similarity_file = similarity_file;
	this.initialize();
}


public DistributionalSimilarity(boolean initialization, String similarity_file) throws Exception {
	this.similarity_file = similarity_file;
	this.setNameAndDescription("DS", "links words according to a distributional similarity measure");
	if (initialization) initialize();
}

@Override
public void initialize() throws Exception {
	System.out.println("intializing distributional similarity matrix");
	dsDict = new SemanticVectorProvider(300, new File(similarity_file));
	
}


public String relation_type() {return type;}

public float relation_strength(Constituent c1, Constituent c2) throws Exception {
	float f = 0;
	if (compliantPOSTags(c1,c2)) {
		try { 
			f = (float) ArrayMath.cosine(
					dsDict.getVector(c1.getSurface().trim()+ "::" + POS.type(c1)),
					dsDict.getVector(c2.getSurface().trim()+ "::" + POS.type(c2)));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	return f;
}

public float max_similarity_value() {
	return max_similarity_value;
}




private boolean compliantPOSTags(Constituent h, Constituent t) {
	return (POS.noun(h) && POS.noun(t)) || (POS.verb(h) && POS.verb(t));
}




}
