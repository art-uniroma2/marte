%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
entails(T,H):-
%	tell(paperino),
%	write(T),nl,
%	write(H),nl,
%	told,
	entails_r(T,H).


entails_r(N,N):-
  atom(N).

entails_r(N,N):-
  number(N).


entails_r(T,H):-
  rule(T,H),!.
  
  
entails_r(T,H):-
  T =.. [_,_,_,R1],
  !,
  %write(R1),nl,
  a_sub_part_entails_r(R1,H).
  
a_sub_part_entails_r([],_):- fail.

a_sub_part_entails_r([A1|_],H):-
  entails_r(A1,H),
  !.
  

a_sub_part_entails_r([_|R1],H):-
  !,
  a_sub_part_entails_r(R1,H).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  rule(MATCH,WHAT_REMAINS_HERE,SENTENCE_TO_BE_PUT_AT_THE_TOP).

% John McEnroe, who plays ...,  are broken --> the shoes for John McEnrone are broken ... John McEnroe plays .... 

rule( 'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_]),
     'S'(_,_,['NP'(X,_,_),'VP'(Y,_,_)|_])):- X \= '-1', Y \= '-1'.




