package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

public class WNSimilarity extends ConstituentRelationDetector {

private String similarity_file = null;

private Hashtable<String,Float> similarity_table = null;

private float max_similarity_value = 1;

private String type = "s";


public WNSimilarity(String similarity_file) throws Exception {
	this.similarity_file = similarity_file;
	similarity_table = new Hashtable<String,Float>();
	loadSimilarityTable(similarity_file);
}


public WNSimilarity(boolean initialization, String similarity_file) {
	this.similarity_file = similarity_file;
	this.setNameAndDescription("WNET", "links words according to a wordnet similarity measure");
}

@Override
public void initialize() throws Exception {
	similarity_table = new Hashtable<String,Float>();
	loadSimilarityTable(similarity_file);
	System.out.println("intializing wn similarity");
	
}


public String relation_type() {return type;}

public float relation_strength(Constituent c1, Constituent c2) throws Exception {
	String key = c1.getSurface().trim()+ "#" + POS.type(c1) + " " + c2.getSurface().trim()+ "#" + POS.type(c2) ;
	String key_rev = c2.getSurface().trim()+ "#" + POS.type(c2) + " " + c1.getSurface().trim()+ "#" + POS.type(c1);
	float f = 0;
	if (compliantPOSTags(c1,c2)) {
		//System.err.println("\n<" +key + ">\n<" +key_rev + ">");

		if (similarity_table.containsKey(key)) f = ((Float) similarity_table.get(key)).floatValue();
		else if (similarity_table.containsKey(key_rev)) f = ((Float) similarity_table.get(key_rev)).floatValue();
		else writeErrorInLog(key);
	}
	return f;
}

public float max_similarity_value() {
	return max_similarity_value;
}

private void loadSimilarityTable(String similarityFile) throws Exception {
	String test_bed = System.getProperty("arte.input.file");
	if ((new File(similarity_file + (test_bed==null?"":"."+test_bed) + ".err")).exists()) (new File(similarity_file + (test_bed==null?"":"."+test_bed) + ".err")).delete();
	similarity_table = new Hashtable<String,Float>();
	System.out.println("Loading similarity table");
	try {
		BufferedReader in = new BufferedReader(new FileReader(similarity_file));
		String line = in.readLine();
		while (line != null) {
			String value = "0";
			String pair = line;
			StringTokenizer st = new StringTokenizer(line);
			pair = st.nextToken().trim() + " "+ st.nextToken().trim();
			String pair_with_value = in.readLine();
			String pair_in_lemma = null;
			if (!pair_with_value.equals("")) {
				st = new StringTokenizer(pair_with_value);
				String lemma1 = st.nextToken();
				String lemma2 = st.nextToken();
				value = st.nextToken().trim();
				//String empty = 
				in.readLine();
				pair_in_lemma = lemma1.substring(0,lemma1.lastIndexOf('#')) + " " + lemma2.substring(0,lemma2.lastIndexOf('#'));
			}
			//System.out.println("<" +pair + "> - " + (new Float(value)).toString());
			Float f = new Float(value);
			// PAIR IN TOKENS
			
			similarity_table.put(pair,f);
			// DERIVED PAIR IN LEMMAS
			if ((pair_in_lemma != null) && (!pair_in_lemma.equals(pair))) similarity_table.put(pair_in_lemma,f);
			
			if (f.floatValue() > max_similarity_value) max_similarity_value = f.floatValue();
			line = in.readLine();
		}
		in.close();
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	}
	
}

private void writeErrorInLog(String key) throws Exception {
	String test_bed = System.getProperty("arte.input.file");
	BufferedWriter err = new BufferedWriter(new FileWriter(similarity_file + (test_bed==null?"":"."+test_bed) + ".err",true));
	err.write(key + "\n");
	err.close();
	similarity_table.put(key,new Float(0));
}


private boolean compliantPOSTags(Constituent h, Constituent t) {
	return (POS.noun(h) && POS.noun(t)) || (POS.verb(h) && POS.verb(t));
}




}
