package it.uniroma2.art.marte.processors.featureDetectors;


import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.dictionaries.*;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.utils.*;

import java.util.*;
import java.io.*;
import java.util.regex.*;



public class WikiFeatureDetector extends OldGenericFeatureDetector {
	
	private BufferedWriter output_file = null;
	
	private Hashtable<String,Integer> bw = null;
	private IDF idf = null;
	
	
	public WikiFeatureDetector(String dictionary) throws Exception {
		load(dictionary);
		if (idf==null) idf = new IDF(); 
		SVMpair.addFeatureSet("wiki");
	}


	private void load(String dict) throws IOException {
		bw = new Hashtable();
		BufferedReader in = new BufferedReader(new FileReader(dict));
		String line = in.readLine();
		while (line!=null) {
			if (!line.trim().equals("")) bw.put(line,1);
			line = in.readLine();
		}
	}

	
	public void computeFeature(Pair p, SVMpair instance) throws Exception {
		output_file = new BufferedWriter(new FileWriter("proooova",true));

		Vector<String> v_t = tokens(p.getTextXDG());
		Vector<String> v_h = tokens(p.getHypoXDG());
		compare(v_t,v_h,instance);
		computeFeature(p.getTextXDG(),instance);
		computeFeature(p.getHypoXDG(),instance);
		output_file.write("\n");
		output_file.close();
		
	}


	private void compare(Vector<String> a, Vector<String> b, SVMpair instance) throws Exception {
		int a_i = 0, b_i = 0;
		Vector <String> a_miss = new Vector();
		Vector <String> b_miss = new Vector();
		for (String a_e:a) {
			if (!b.contains(a_e)) a_miss.add(a_e);
			output_file.write(a_e + " ");
		}
		output_file.write("^");
		for (String b_e:b) {
			if (!a.contains(b_e)) b_miss.add(b_e);
			output_file.write(b_e + " ");
		}
		output_file.write("^");
		Vector <String> miss = new Vector();
		miss.addAll(a_miss);		miss.addAll(b_miss);
		int len = 0, bw_n = 0;
		String bad_words = "";
		String cap_words = "";
		for (String e:miss) {
			if (Pattern.matches("\\p{Upper}{2,}+",e)) {
				len += e.length();
				SVMpair.addFeature("C","wiki");
				instance.setFeatureValue("C","1","wiki");
				cap_words += e;
			}
			if (bw.containsKey(e)) {
				SVMpair.addFeature("BW","wiki");
				bw_n++;
				instance.setFeatureValue("BW","1","wiki");
				System.err.print("-" + e);
				bad_words += e;
			}
		}
		if (len > 0) {
			SVMpair.addFeature("C_LEN","wiki");
			instance.setFeatureValue("C",""+len,"wiki");
		}
		if (bw_n > 0) {
			SVMpair.addFeature("N_BW","wiki");
			instance.setFeatureValue("N_BW",""+bw_n,"wiki");
		}
		output_file.write( bad_words + "^" + bw_n + "^" + cap_words + "^" + len );

		
	}

	private void computeFeature(XDG xdg, SVMpair instance) throws Exception {
		Vector <Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
		ConstituentList cl = new ConstituentList();
		int i = 1;
		String prev_tok = "";
		for (Constituent c: cs) {
			
			//if (!prev_tok.equals("") && prev_tok.length() > 2 && LevenshteinDistance.compute(c.getSurface(),prev_tok) < 2) {
			//	SVMpair.addFeature("SEQ","wiki");
			//	SVMpair.addFeature("SEQ_" + prev_tok, "wiki");
			//	instance.setFeatureValue("SEQ","1","wiki");
			//	instance.setFeatureValue("SEQ_" + prev_tok,"1","wiki");
			//	System.err.println("SEQ_" + prev_tok);
			//}
			//prev_tok = c.getSurface();

			//if (c.getSurface().length() > 3 && Pattern.matches("\\p{Upper}{4,}+",c.getSurface()) && bw.containsKey((c.getSurface().toLowerCase()))) {
			//	SVMpair.addFeature("BW_C","wiki");
			//	SVMpair.addFeature("BW_C_" + c.getSurface(), "wiki");
			//	instance.setFeatureValue("BW_C","1","wiki");
			//	instance.setFeatureValue("BW_C_" + c.getSurface(),"1","wiki");
			//}


			
			//if (bw.containsKey(c.getSurface()) && idf.getIDF(c.getSurface()) > 2) {
			//	SVMpair.addFeature("BW","wiki");
			//	SVMpair.addFeature("BW_" + c.getSurface(), "wiki");
			//	instance.setFeatureValue("BW","1","wiki");
			//	instance.setFeatureValue("BW_" + c.getSurface(),"1","wiki");
			//}
		}
		SVMpair.addFeature("TOP","wiki");
		instance.setFeatureValue("TOP",((Constituent)xdg.getSetOfConstituents().elementAt(0)).getType(),"wiki");
		output_file.write("^" + ((Constituent)xdg.getSetOfConstituents().elementAt(0)).getType());


		SimpleConst prp = leftmost_pronuon_finder((Constituent)xdg.getSetOfConstituents().elementAt(0));
		output_file.write("^" );
		if (prp != null && prp.getType().split("_").length < 5 ) {
			output_file.write(prp.getType());
			SVMpair.addFeature("PRP","wiki");
			instance.setFeatureValue("PRP",prp.getType(),"wiki");
		}
		SVMpair.addFeature("PRP_POS","wiki");
		instance.setFeatureValue("PRP_POS","" +position_PRP((Constituent)xdg.getSetOfConstituents().elementAt(0)),"wiki");
		output_file.write("^" + position_PRP((Constituent)xdg.getSetOfConstituents().elementAt(0)));
		
	}

	private Vector<String> tokens(XDG xdg) throws Exception {
		Vector <Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
		ConstituentList cl = new ConstituentList();
		int i = 1;
		Vector<String> toks = new Vector();
		for (Constituent c: cs) {
			toks.add(c.getSurface());
		}
		return toks;
	}


	private SimpleConst leftmost_pronuon_finder(Constituent c) {
		SimpleConst c_out = null;
		if (c instanceof SimpleConst ) {
			if (c.getType().equals("PRP")) 					c_out = new SimpleConst(c.getType(), "" ,0); 
		} else {
			ConstituentList cl = ((ComplxConst) c).getSubConstituents(); 
			for (int i = 0; i < cl.size() && c_out == null; i++) {
				SimpleConst c1 = leftmost_pronuon_finder((Constituent) cl.elementAt(i));
				if (c1 != null) {
					c_out = new SimpleConst(c1.getSurface(), c.getType() + "_" +c1.getType(),0); 
				}
			}
		}
		return c_out;
	}


	private int position_PRP(Constituent c) {
		int j = 1000;
		for (int i=0; i<c.getSimpleConstituentList().size() && j == 1000; i++) {
			if (((Constituent)c.getSimpleConstituentList().elementAt(i)).getType().equals("PRP")) j = i;
		}
		return j;
	}


	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}

}