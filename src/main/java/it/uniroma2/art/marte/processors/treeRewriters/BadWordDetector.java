package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.utils.*;

import java.io.*;
import java.util.*;
import java.util.regex.*;



public class BadWordDetector extends GenericTreeRewriter {
	private Hashtable<String,Integer> bw = null;
	private IDF idf = null;
	private ConstituentList cl_t = null, cl_h = null;
	private String dictionary;
	
	
	public BadWordDetector(String dictionary) throws Exception {
		load(dictionary);
		if (idf==null) idf = new IDF(); 
	}


	public BadWordDetector(boolean b, String dictionary) {
		// TODO Auto-generated constructor stub
		this.dictionary = dictionary;
		this.setNameAndDescription("BWD", "detects bad words and other features");
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		load(dictionary);
		if (idf==null) idf = new IDF(); 
	}


	private void load(String dict) throws IOException {
		bw = new Hashtable<String,Integer>();
		BufferedReader in = new BufferedReader(new FileReader(dict));
		String line = in.readLine();
		while (line!=null) {
			if (!line.trim().equals("")) bw.put(line,1);
			line = in.readLine();
		}
	}
	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {


		cl_t = new ConstituentList();
		cl_h = new ConstituentList();

		transform(p.getTextXDG());
		transform(p.getHypoXDG());

		if (cl_t.size() == 0) cl_t.add(new SimpleConst("inactive","no",0));
		if (cl_h.size() == 0) cl_h.add(new SimpleConst("inactive","no",0));

		Constituent c1 = new ComplxConst("F",cl_t);
		ConstituentList cl = new ConstituentList();
		cl.add(c1);
		XDG xdg_out = new XDG();
		xdg_out.setSetOfConstituents(cl);		
		p.setTextXDG(xdg_out);
		
		
		c1 = new ComplxConst("F",cl_h);
		cl = new ConstituentList();
		cl.add(c1);
		xdg_out = new XDG();
		xdg_out.setSetOfConstituents(cl);		
		p.setHypoXDG(xdg_out);
		return p;
	}

	/** 
	*/
	public XDG transform(XDG xdg) throws Exception{
		Vector<Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
//		ConstituentList cl = new ConstituentList();
		int i = 1;
		String prev_tok = "";
		for (Constituent c: cs) {
			
			if (!prev_tok.equals("") && prev_tok.length() > 2 && LevenshteinDistance.compute(c.getSurface(),prev_tok) < 2) {
				cl_t.add(long_tree(prev_tok,"SEQ",i));
				i++;
			}
			prev_tok = c.getSurface();

			if (c.getSurface().length() > 3 && Pattern.matches("\\p{Upper}{4,}+",c.getSurface()) && bw.containsKey((c.getSurface().toLowerCase()))) {
				cl_h.add(long_tree(c.getSurface(),"BW_C",i));
				i++;
			}


			
			if (bw.containsKey(c.getSurface()) && idf.getIDF(c.getSurface()) > 2) {
				cl_h.add(long_tree(c.getSurface(),"BW",i));
				i++;
			}
		}

		SimpleConst sc_new = new SimpleConst(((Constituent)xdg.getSetOfConstituents().elementAt(0)).getType(),"TOP",i);
		
		cl_t.add(sc_new);

		SimpleConst prp = leftmost_pronuon_finder((Constituent)xdg.getSetOfConstituents().elementAt(0));
		if (prp != null && prp.getType().split("_").length < 5 ) cl_t.add(prp);
		

		XDG xdg_out = new XDG();
		//System.err.println("Adding " + xdg_out.toPennTree());
		return xdg_out;
	}


	private SimpleConst leftmost_pronuon_finder(Constituent c) {
		SimpleConst c_out = null;
		if (c instanceof SimpleConst ) {
			if (c.getType().equals("PRP")) 					c_out = new SimpleConst(c.getType(), "" ,0); 
		} else {
			ConstituentList cl = ((ComplxConst) c).getSubConstituents(); 
			for (int i = 0; i < cl.size() && c_out == null; i++) {
				SimpleConst c1 = leftmost_pronuon_finder((Constituent) cl.elementAt(i));
				if (c1 != null) {
					c_out = new SimpleConst(c1.getSurface(), c.getType() + "_" +c1.getType(),0); 
				}
			}
		}
		return c_out;
	}
	private Constituent long_tree(String token, String type, int i) {
		SimpleConst sc_new = new SimpleConst(token,"*",i);
		ConstituentList cl_sub = new ConstituentList();
		cl_sub.add(sc_new);
		Constituent c1 = new ComplxConst(type,cl_sub);
		return c1;
	}



	

}
