package it.uniroma2.art.marte.processors.treeRewriters;


import it.uniroma2.art.marte.structures.Pair;

public class HypoTreePruner extends TextTreePruner {

	public HypoTreePruner() {
		
	}

	public HypoTreePruner(boolean intialiazation) {
	this.setNameAndDescription("CUTH", "prunes irreleant parts of the hypo syntactic tree using text tree pruner");
	}
	/** 
	eliminates irrelevant information in the Hypo syntactic tree.
	Relevant leaves must be marked a #r# in the morphological features.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getHypoXDG());
		return p;
	}
	
}
