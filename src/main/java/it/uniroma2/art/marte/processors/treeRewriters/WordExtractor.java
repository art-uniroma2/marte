package it.uniroma2.art.marte.processors.treeRewriters;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.Pair;

import java.io.*;
import java.util.*;


public class WordExtractor {


	public void extract(Pair p, BufferedWriter o)  throws Exception {
		extract(p.getTextXDG(),o);
		extract(p.getHypoXDG(),o);
	}


	public void extract(XDG xdg, BufferedWriter o) throws Exception{
		Vector <Constituent> cs = xdg.getSetOfConstituents().getSimpleConstituentList();
		for (Constituent c: cs) {
			o.write(c.getSurface() + "\n");
		}
	}

}