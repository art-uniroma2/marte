package it.uniroma2.art.marte.processors.lexSimilarity;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.JiangConrath;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;
import it.reveal.chaos.xdg.textstructure.Constituent;

/**
 * @author Noemi Scarpato
 *
 * **/
public class JCNSimilarity extends ConstituentRelationDetector{
	
	private static ILexicalDatabase db;
	private static RelatednessCalculator rc;
	private String type = "jcn";

	public JCNSimilarity() {
		this.setNameAndDescription("JCN", "links words according to a JiangConrath (JCN) similarity measure");
		try {
			this.initialize();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public float relation_strength(Constituent c1, Constituent c2)
			throws Exception {
		double s = rc.calcRelatednessOfWords(c1.getSurface(), c2.getSurface());
		System.out.println( rc.getClass().getName()+"\t"+s );
		return (float) s ;
	}
	
	public float relation_strength(String c1, String c2)
			throws Exception {
		WS4JConfiguration.getInstance().setMFS(true);
		double s = rc.calcRelatednessOfWords(c1, c2);
		System.out.println( "\t"+s );
		return (float) s ;
	}

	@Override
	public String relation_type() {
		return type;
	}

	@Override
	public void initialize() throws Exception {
		db = new NictWordNet();
		rc =  new JiangConrath(db);
	}
public static void main(String[] args){
	JCNSimilarity jcn = new JCNSimilarity();
	try {
		jcn.relation_strength("home", "house");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}
