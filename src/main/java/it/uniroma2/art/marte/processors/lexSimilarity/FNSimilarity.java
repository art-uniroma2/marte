package it.uniroma2.art.marte.processors.lexSimilarity;

import it.reveal.chaos.xdg.textstructure.Constituent;


public class FNSimilarity extends ConstituentRelationDetector {

private String type = "=";

public FNSimilarity() {
}

public FNSimilarity(boolean initialization) {
	this.setNameAndDescription("FN", "computes the similarity between frames - nodes should contain frames");
}

public String relation_type() {return type;}

public float relation_strength(Constituent h, Constituent t) throws Exception {

	float f = 0;
	String [] h_s = h.getSurface().split("@");
	String [] t_s = t.getSurface().split("@");
	//System.err.print("> " + h.getSurface() + ":" + t.getSurface() );
	if (t_s.length>1 && h_s.length>1 && t_s[1].equals(h_s[1])) {
		//System.err.print("...ok");
		f = 1;
		type = t_s[2];
	}
	//System.err.println();
	return f;

}

@Override
public void initialize() throws Exception {
}


}
