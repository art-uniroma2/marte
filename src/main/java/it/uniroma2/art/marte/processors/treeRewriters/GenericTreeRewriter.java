package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.processors.*;
import it.uniroma2.art.marte.structures.Pair;



public abstract class GenericTreeRewriter extends Processor {

	/** 
		implements the generic transformation of the module.
	*/
	public abstract XDG transform(XDG xdg) throws Exception;
	
	/** 
		implements the generic transformation of the module on a pair.
	*/
	public abstract Pair transform(Pair p) throws Exception ;

}