package it.uniroma2.art.marte.processors.featureDetectors;

import it.uniroma2.art.marte.processors.Processor;
import it.uniroma2.art.marte.structures.EntailmentPair;
import it.uniroma2.art.marte.structures.EntailmentPairSet;
import it.uniroma2.art.marte.structures.Pair;
import it.uniroma2.art.marte.structures.SVMpair;

//import it.uniroma2.art.marte.structures.*;

public abstract class OldGenericFeatureDetector extends Processor {
	
	private String name = "generic";
	private String set = "std";

	public OldGenericFeatureDetector() {
		SVMpair.addFeature(name); 
	} 
	
	
	public OldGenericFeatureDetector(String name) {
		this.name = name;
		SVMpair.addFeature(name);
	} 

	public OldGenericFeatureDetector(String feature_set, String feature_name) {
		this.name = feature_name;
		this.set = feature_set;
		SVMpair.addFeature(feature_name,feature_set);
	} 
	

	
	public String getName() {return name;}
	public String getSet() {return set;}

	@Deprecated
	public abstract void computeFeature(Pair p, SVMpair instance) throws Exception;
	
	public EntailmentPair computeFeature(EntailmentPair p) throws Exception {
		throw new Exception("NOT IMPLEMENTED YET!");
		// TODO Auto-generated method stub
		
	}

}