package it.uniroma2.art.marte.processors.lexSimilarity;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;
import it.uniroma2.art.marte.utils.LevenshteinDistance;


public class ProperNounLevSimilarity extends ConstituentRelationDetector {

private String type = "=";

public ProperNounLevSimilarity(boolean initialization) {
	this.setNameAndDescription("PNLV", "determines the similarity between Proper Names using an edit distance");
}

public ProperNounLevSimilarity() {
}

public String relation_type() {return type;}

public float relation_strength(Constituent h, Constituent t) throws Exception {

	float f = 0;
	if (POS.proper_noun(h) && POS.proper_noun(t) && 
		 h.getSurface().length() > 3 && t.getSurface().length() > 3  && 
		 LevenshteinDistance.compute(h.getSurface().toLowerCase(),t.getSurface().toLowerCase()) == 1 ) {
		f = 1;
	}
	return f;

}

@Override
public void initialize() throws Exception {
	// TODO Auto-generated method stub
	
}


}
