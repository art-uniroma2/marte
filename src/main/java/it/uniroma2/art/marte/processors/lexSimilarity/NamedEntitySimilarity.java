package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;

import alice.tuprolog.*;


public class NamedEntitySimilarity extends ConstituentRelationDetector {
	

	private String type = "=";
	private String rules;

	private Theory _theory;
	private Prolog _engine;	

	
	public NamedEntitySimilarity(String rules) throws Exception {
			_theory = new Theory(new FileInputStream(rules));
			System.out.println("Named Entity Relation Rules:" + rules);
			_engine=new Prolog();
		  _engine.setTheory(_theory);
	}
	
	public NamedEntitySimilarity(boolean initialization, String rules) {
		this.setNameAndDescription("PN", "determines the similarity or the entailment of two Proper Names");
		this.rules = rules;
	}

	@Override
	public void initialize() throws Exception {
		_theory = new Theory(new FileInputStream(rules));
		System.out.println("Named Entity Relation Rules:" + rules);
		_engine=new Prolog();
	  _engine.setTheory(_theory);
	}


	public String relation_type() {return type;}
	
	public float relation_strength(Constituent h, Constituent t) throws Exception {
	
		float f = 0;
		int value = -1;
		if (POS.proper_noun(h) && POS.proper_noun(t) && 
		    //((ProperNoun) h.getFirstLemma()).getNamedEntityCategory().equals(((ProperNoun) h.getFirstLemma()).getNamedEntityCategory()) && 
		    (value = properNounRelation(h.getSurface().replace('_',' '),t.getSurface().replace('_',' '))) >= 0
			 ) {
			f = 1;
			switch (value) {
				case 0: type = "=";break;
				case 1: type = "<";break;
				case 2: type = ">";break;
			}
			type = "="; //FMZ ATTEMPT
			
		}
		return f;
	
	}

 /** 
  * <b>MAIN NON-STATIC METHOD</b>: starts SWI fst
  */ 
	public void Run(Vector<String> args) throws Exception{
		if (args.size()!=2)
				{
				System.out.println("Wrong number of arguments for fst_nomipropri.pl: I need TWO arguments");
				System.exit(-1);
				}	
			String firstPN = args.elementAt(0);
			String secondPN = args.elementAt(1);
			int ret = properNounRelation(firstPN,secondPN);
			switch (ret) {
				case 0 : System.out.println("EQUALITY   : " + firstPN + " == " + secondPN);break;
				case 1 : System.out.println("ENTAILMENT : " + firstPN + " --> " + secondPN);break;
				case 2 : System.out.println("ENTAILMENT : " + firstPN + " <-- " + secondPN);break;
				case -1 :System.out.println("NO         : " + firstPN + " <> " +secondPN);
				}			
}



 /** 
  * Given two proper nouns verifies if textual entailment holds 
  * and the entailment verse. It uses a Prolog FST. 
  *
  * @param firstPN   the first proper noun
  * @param secondPN  the second proper noun
  * @return					 -1 if entailment does not hold, 0 for equality, 1 for first->second, 2 for second->first 
	*/
	public int properNounRelation(String firstPN, String secondPN) throws Exception{
	
		//System.out.println(firstPN + "<>" + secondPN);
//		int i=0, ret=-1;
//		boolean found=false;
		firstPN = firstPN.toLowerCase();
		secondPN = secondPN.toLowerCase();
		if (firstPN.equals(secondPN))
			return 0;
		if (properNounEntailment(firstPN,secondPN))
			return 1;
		if (properNounEntailment(secondPN,firstPN))
			return 2;
		return -1;		
		}


 /** 
  * Given a proper noun verifies if it textually entails a second proper
  * noun, by using a prolog FST.
  *
  * @param entailingPN  candidate entailing proper noun
  * @param entailedPN   candidate entailed proper noun
  * @return						  true if entailment holds
	*/
	public boolean properNounEntailment(String entailingPN, String entailedPN) throws Exception{
		boolean found=false;
//		int i=0;
//		String solution;
//		Vector<String> solutions = new Vector<String>();

		Struct t = new Struct("test",new Struct(entailingPN),new Struct(entailedPN));
		SolveInfo info = _engine.solve(t);

		found = info.isSuccess();
		
		return found;
	}


 




/*************************************/
/******** STAND-ALONE MODULES ********/
/*************************************/
	
 /** 
  * Explains input parameters
  */
	private static void usage()
		{
		System.out.println("\n");
		System.out.println("Usage: NamedEntitySimilarity <fst prolog file> <arguments of prolog main predicate>");
		System.out.println("\n");
		System.out.println(" where <fst prolog file> is the name of the prolog file containing a given transducer");
		System.out.println("\n");
		System.out.println(" where <arguments of prolog main predicate> are the arguments of the main predicate of the fst tobe called");
		System.out.println("\n\n");
		System.exit(0);
		}	
		
 /** 
  * \brief runs stand-alone NamedEntitySimilarity
  */
	public static void main(String[] argv) throws Exception {
		 String plFile = null;
		 Vector<String> plArgs = new Vector<String>();
		 if (argv.length!=3) usage();
		 plFile=argv[0];
		 for (int i=1; i<argv.length ; i++)
			plArgs.add(argv[i]);
		NamedEntitySimilarity sif = new NamedEntitySimilarity(plFile);
		sif.Run(plArgs);
	}


}
