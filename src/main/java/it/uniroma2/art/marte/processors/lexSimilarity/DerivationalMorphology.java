package it.uniroma2.art.marte.processors.lexSimilarity;

import java.io.*;
import java.util.*;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.POS;


public class DerivationalMorphology extends ConstituentRelationDetector {


private Hashtable<String,Float> derivational_morphology_table = null;

private String type = "=";
private String derivational_morphology_file;


public DerivationalMorphology(String file) throws Exception {
	loadDerMorphologyTable(file);
}

public DerivationalMorphology(boolean b, String derivational_morphology_file) {
	this.setNameAndDescription("DRMO", "uses a derivational morphology table from wordnet");
	this.derivational_morphology_file=derivational_morphology_file;
}
@Override
public void initialize() throws Exception {
	loadDerMorphologyTable(derivational_morphology_file);
}


public String relation_type() {return type;}



public float relation_strength(Constituent h, Constituent t) throws Exception {

	String key_lem = h.getFirstLemma().getSurface() + "#" + POS.type(h) + " " + t.getFirstLemma().getSurface() + "#" + POS.type(t);
	String key_rev_lem = t.getFirstLemma().getSurface() + "#" + POS.type(t) + " " + h.getFirstLemma().getSurface() + "#" + POS.type(h);
	float f = 0;
	if (derivational_morphology_table.containsKey(key_lem) || derivational_morphology_table.containsKey(key_rev_lem) ) {
		f = 1;
		System.out.println("Derivation : " + key_lem );
	} else if 		((POS.noun(h) && POS.adj(t) && t.getSurface().toLowerCase().startsWith(h.getSurface().toLowerCase()) )  ||
					(POS.noun(t) && POS.adj(h) && h.getSurface().toLowerCase().startsWith(t.getSurface().toLowerCase()) )  ) {
						f=1;
						System.out.println("non-std derivation: " + t.getSurface() + " - " + h.getSurface());

					}

	return f;

}

private void loadDerMorphologyTable(String file) throws Exception {
	derivational_morphology_table = new Hashtable<String,Float>();
	System.out.println("Loading derivational morphology table");
	String line = "";
	int line_no = 0;
	try {
		BufferedReader in = new BufferedReader(new FileReader(file));
		line = in.readLine();
		line_no = 0;
		while (line != null) {
			StringTokenizer st = new StringTokenizer(line);
			String pair = st.nextToken().trim() + " "+ st.nextToken().trim();
			//System.out.println(pair + " - " );
			Float f = new Float(1);
			derivational_morphology_table.put(pair,f);
			line = in.readLine();
			line_no++;
		}
		in.close();
	} catch (IOException e) {
		System.out.println("WARNING: " + e.getMessage());
		e.printStackTrace();
	} catch (NoSuchElementException e) {
		System.out.println("ERROR in line : " + line_no + " <" + line +">");
		throw e;
	}
}





}
