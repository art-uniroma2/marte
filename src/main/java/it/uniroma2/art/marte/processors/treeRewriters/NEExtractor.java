package it.uniroma2.art.marte.processors.treeRewriters;

import java.util.*;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.Lemma;
import it.reveal.chaos.xdg.textstructure.ProperNoun;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Token;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;

public class NEExtractor extends GenericTreeRewriter {


	public NEExtractor() {
		// TODO Auto-generated constructor stub
	}

	public NEExtractor(boolean b) {
		// TODO Auto-generated constructor stub
	}


	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}


	/** 
		implements the generic transfomation of the module.
	*/

	public XDG transform(XDG xdg) throws Exception {
		Vector <Constituent> cs = xdg.getSetOfConstituents();
		for (Constituent c : cs) {
			grabNamedEntityCategory(c);
		}
		return xdg;
		
	}

	public Constituent grabNamedEntityCategory(Constituent constituent) {
		Constituent out = constituent;
		if (constituent instanceof SimpleConst) { 
			
			if (POS.proper_noun(constituent)) {
				String category = "unknown";
				String surface = constituent.getSurface();
				if (surface.lastIndexOf('_')>=0) {
					category = surface.substring(surface.lastIndexOf('_')+1,surface.length());
					surface = surface.substring(0,surface.lastIndexOf('_'));
					System.out.println("NE " + category + ":" + surface);
				}
				Lemma l = constituent.getFirstLemma();
				ProperNoun p = new ProperNoun(l,category);
				p.setSurface(surface);
				((Token)constituent.getTokenSpan().elementAt(0)).setSurface(surface);
				constituent.setSurface(surface);
				constituent.getAlternativeLemmas().remove(l);
				constituent.getAlternativeLemmas().add(p);
			}
		} else {
			Vector <Constituent> cs = ((ComplxConst) constituent).getSubConstituents();			
			for (Constituent c:cs) grabNamedEntityCategory(c);
		}
		return out;			
	}


	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}




}
