package it.uniroma2.art.marte.processors.lexSimilarity;

import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.processors.Processor;
import it.uniroma2.art.marte.processors.lexSimilarity.ConstituentRelationDetector;

 /**
  * This class implements a special ConstituentRelationDetector. The processor
  * assign the type according to all the ConstituentRelationDetectors in the chain,
  * but assignes the value according to the last processor. This is used mainly for 
  * WordNet relations. 
  */	
  
public class WNRelationDetectorChain extends RelationDetectorChain  {

private String type = "=";
private String currentProcessorName = null;
private int [] activations = null;			// NOTE FOR FABIO: specificare il significato di questa variabile


	
 /**
  * Constructor which takes as input the set of ConstituentRelationDetectors processors
  * which will be used in order for finding the most important relation among two constituents.
  *
  * @param similarity_detectors    the vector of processors
  * @param max_value               max value of strength to be returned
  */ 
public WNRelationDetectorChain(Vector <ConstituentRelationDetector> similarity_detectors,float max_value) {
	this.similarity_detectors = similarity_detectors;
	activations = new int[similarity_detectors.size()];
	for (int i = 0; i < similarity_detectors.size(); i++) activations[i]=0;
}


public WNRelationDetectorChain(boolean initialization) {
	this.setNameAndDescription("WNFULL", "wordnet similarities");
}
public WNRelationDetectorChain() {
}


public String relation_type() {return type;}


private Vector <ConstituentRelationDetector> similarity_detectors = null;



public float relation_strength(Constituent t, Constituent h) throws Exception {
	float f = 0 ;
	float f_last = 0;
	boolean similar = false;
	Vector <Processor> ap = this.getActiveProcessors();
	
	type = ((ConstituentRelationDetector)ap.elementAt(ap.size() - 1)).relation_type();
	
	for (int i = 0 ; i < ap.size() - 1 && !similar; i++) {
		if ((f = ((ConstituentRelationDetector)ap.elementAt(i)).relation_strength(t,h)) > 0) {
			similar=true;
			type = ((ConstituentRelationDetector)ap.elementAt(i)).relation_type();
			currentProcessorName =  ((ConstituentRelationDetector)ap.elementAt(i)).getClass().getName();
			currentProcessorName =  currentProcessorName.substring(currentProcessorName.lastIndexOf(".")+1);
			System.out.println(currentProcessorName);
			activations[i]++;
		}
	}	
	
	if ((f_last = ((ConstituentRelationDetector)ap.elementAt(ap.size() -1)).relation_strength(t,h)) > 0.2) 
		f = f_last; 
	else f = 0;


	return f;
}


public String getCurrentProcessorName(){ return currentProcessorName;}

public String getCurrentType(){ return type;}


public String reportActivations() {
	String out = "";
	for (int i = 0 ; i < similarity_detectors.size(); i++) {
		out += similarity_detectors.elementAt(i).getClass().getName() + ": " + activations[i] + "\n";
	}
	return out;
}




}
