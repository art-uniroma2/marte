package it.uniroma2.art.marte.processors.lexSimilarity;

import java.util.*;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.processors.*;
import it.uniroma2.art.marte.processors.lexSimilarity.ConstituentRelationDetectorBasic;

 /**
  * This class implements a special ConstituentRelationDetector. The processor
  * computes the most important relation between two constituent, among all the
  * possible choices. These choices are produced by single ConstituentRelationDetector
  * which are called in order of importance by this class among those selected from the
  * arte.processors.lexSimilairty package.
  * ConstituentRelationDetectors are selected by means of a parameter vector for the 
  * constructor of this class.
  */	
  
public class RelationDetectorChain extends CascadeOfProcessors implements ConstituentRelationDetectorBasic  
{

private String type = "=";
private String currentProcessorName = null;
private float max_similarity = 0;								// NOTE FOR FABIO: specificare il significato di questa variabile	


public void setMax_similarity(float maxSimilarity) {
	max_similarity = maxSimilarity;
}

private int [] activations = null;			// NOTE FOR FABIO: specificare il significato di questa variabile


public void initialize() throws Exception {
	super.initialize();
	setMax_similarity(this.max_similarity_value());
	similarity_detectors = new Vector<ConstituentRelationDetectorBasic> ();
	for (Processor p: this.getActiveProcessors()) {
		similarity_detectors.add((ConstituentRelationDetectorBasic) p);
	}
	activations = new int[similarity_detectors.size()];
	for (int i = 0; i < similarity_detectors.size(); i++) activations[i]=0;
	
}


/**
  * Constructor which takes as input the set of ConstituentRelationDetectors processors
  * which will be used in order for finding the most important relation among two constituents.
  *
  * @param similarity_detectors    the vector of processors
  * @param max_value               max value of strength to be returned
  */ 
public RelationDetectorChain(Vector <ConstituentRelationDetectorBasic> similarity_detectors,float max_value) {
	this.similarity_detectors = similarity_detectors;
	max_similarity = max_value;
	Vector<Processor> processors = new Vector<Processor>();
	for (ConstituentRelationDetectorBasic detector : similarity_detectors)
		processors.add((Processor)detector);
	setProcessors(processors);
	activations = new int[similarity_detectors.size()];
	for (int i = 0; i < similarity_detectors.size(); i++) activations[i]=0;
}


public RelationDetectorChain() {
	this.setProcessors(new Vector<Processor>());
}


public String relation_type() {return type;}

public void add(ConstituentRelationDetectorBasic p) {
	this.getProcessors().add((Processor) p);
}

private Vector <ConstituentRelationDetectorBasic> similarity_detectors = null;



public float relation_strength(Constituent t, Constituent h) throws Exception {
	float f = 0 ;
	boolean similar = false;
	for (int i = 0 ; i < similarity_detectors.size() && !similar; i++) {
		if ((f = similarity_detectors.elementAt(i).relation_strength(t,h)) > 0) {
			similar=true;
			type = similarity_detectors.elementAt(i).relation_type();
			currentProcessorName =  similarity_detectors.elementAt(i).getClass().getName();
			currentProcessorName =  currentProcessorName.substring(currentProcessorName.lastIndexOf(".")+1);
			activations[i]++;
		}
	}	
	if (f == 1) f = max_similarity; 
	else if (f < 0.2) f = 0;
	return f;
}


public String getCurrentProcessorName(){ return currentProcessorName;}

public String getCurrentType(){ return type;}


public String reportActivations() {
	String out = "";
	for (int i = 0 ; i < similarity_detectors.size(); i++) {
		out += similarity_detectors.elementAt(i).getClass().getName() + ": " + activations[i] + "\n";
	}
	return out;
}

public float max_similarity_value() {
	float max = 0;
	for (Processor p: this.getActiveProcessors()) {
		float m = ((ConstituentRelationDetectorBasic) p).max_similarity_value();
		if (m > max) max = m;
	}
	return max;
}
}
