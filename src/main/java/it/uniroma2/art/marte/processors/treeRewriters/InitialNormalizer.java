package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.Lemma;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;


import java.util.*;



public class InitialNormalizer extends GenericTreeRewriter {

	public InitialNormalizer() {
		// TODO Auto-generated constructor stub
	}
	public InitialNormalizer(boolean b) {
		// TODO Auto-generated constructor stub
	}

	/** 
		implements the generic transfomation of the module on a pair.
	*/
	public Pair transform(Pair p) throws Exception {
		transform(p.getTextXDG());
		transform(p.getHypoXDG());
		return p;
	}

	/** 
	performs the basic tranformations:
	  1) remove the top nodes if irrelevant
	  2) splits tokens containing hyphen and square parenthesis 
	*/
	public XDG transform(XDG xdg) throws Exception{
		XDG out = removeTheTwoTopNodes(xdg);
		out = splitTokens(out);
		return out;
	}

	private XDG removeTheTwoTopNodes(XDG xdg) {
		if (xdg.getSetOfConstituents().size() == 1) {
			Constituent s = (Constituent) xdg.getSetOfConstituents().elementAt(0) ;
			if (s instanceof ComplxConst && ((ComplxConst)s).getSubConstituents().size() == 1 && 
					//(s.getType().equals("S1") || s.getType().endsWith(":TOP") )
					s.getType().equals("S1") 
					) 
					{
				Constituent first = (Constituent) ((ComplxConst)s).getSubConstituents().elementAt(0);
				xdg.getSetOfConstituents().setElementAt(first,0);
			}
		} 
		return xdg;
	}


	private XDG splitTokens(XDG tree) {
		Vector <Constituent> cs = tree.getSetOfConstituents() ;
		for ( Constituent c : cs ) splitTokens(c);
		return tree;
	}


	private Vector <Constituent> splitTokens(Constituent tree) {
		Vector <Constituent> out = new Vector <Constituent>();
		if ((tree instanceof SimpleConst)) {
			StringTokenizer st = new StringTokenizer(tree.getSurface(), "-[]", false);		
			if (st.countTokens() > 1) {
				while (st.hasMoreTokens()) {
					SimpleConst sc = new SimpleConst((SimpleConst) tree);
					String token = st.nextToken();
					sc.setSurface(token);
					Lemma ol = sc.getFirstLemma();
					Lemma l = new Lemma(ol.getId(),token,ol.getType(),ol.getMorphologicalFeatures(),new Vector());
					Vector v = new Vector();
					v.add(l);
					sc.setAlternativeLemmas(v);
					out.add(sc);
				}
			} else {
				out.add(tree);
			}
		} else {
			Vector <Constituent> subs = ((ComplxConst)tree).getSubConstituents();
			ConstituentList newSubs = new ConstituentList();
			for (Constituent c : subs) {
				Vector <Constituent> new_const = splitTokens(c);
				newSubs.addAll(new_const);
			}
			((ComplxConst)tree).setSubConstituents(newSubs);
			out.add(tree);
		}
		return out;
	}

	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}


}