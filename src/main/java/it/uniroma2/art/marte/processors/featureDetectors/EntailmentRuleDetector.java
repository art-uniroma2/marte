package it.uniroma2.art.marte.processors.featureDetectors;



import it.reveal.chaos.xdg.textstructure.Constituent;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import java.io.*;

import alice.tuprolog.*;



public class EntailmentRuleDetector extends OldGenericFeatureDetector {


	private Theory _theory;
	private Prolog _engine;


 /** 
  * Constructs a SyntacticTransformer object by setting private
  *	fields as specified in input parameters.
  *
  * @param rule_file    name and path of the prolog file
	*/
	public EntailmentRuleDetector(String feature_set, String feature_name, String rule_file) throws Exception {
		   super(feature_set, feature_name);
			_theory = new Theory(new FileInputStream(rule_file));
			System.out.println("RULE FILE:" + rule_file);
			_engine=new Prolog();
		  _engine.setTheory(_theory);
		  
	}

	
	/** 
	*/
	public void computeFeature(Pair p, SVMpair instance) throws Exception {
		Struct t = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmasAndWithVariables((Constituent) p.getTextXDG().getSetOfConstituents().elementAt(0));
		Struct h = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmasAndWithVariables((Constituent) p.getHypoXDG().getSetOfConstituents().elementAt(0));
		Struct rule_detector = new Struct("entails",t,h);
		SolveInfo info = _engine.solve(rule_detector);
		if (info.isSuccess()) {
			instance.setFeatureValue(getName(),"1",getSet());			
		} 		
	}


	@Override
	public void initialize() throws Exception {
		// TODO Auto-generated method stub
		
	}




}