package it.uniroma2.art.marte.processors;

import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.processors.treeRewriters.*;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;
import it.uniroma2.art.marte.utils.*;

import java.util.*;


public class VariableHandler {



public Vector <Vector <Constituent>> metodoStupidoCheTantoNonFunzionaAccorpaAncore(Vector<Vector<Anchor>> anchors ) {
	Vector <Vector <Constituent>> variables = new Vector();
	Vector <Constituent> variable = null;
	for (Vector <Anchor> ancs:anchors) {
		for (Anchor a:ancs) {
			//System.err.println(a.toString());
			if ((variable = containedIn(variables,a.getHConst()))!=null) {
				if (a.getTConst()!=null) { 
					variable.add(a.getTConst());
					//System.err.println("Adding " + a.getTConst().getSurface() );
				}
			} else if ((variable = containedIn(variables,a.getTConst()))!=null) {
				//System.err.println("Adding " + a.getHConst().getSurface() );
				variable.add(a.getHConst());
			} else {
				if (a.getTConst()!=null) {
					variable = new Vector();
					variable.add(a.getHConst());
					variable.add(a.getTConst());
					//System.err.println("Adding " + a.getTConst().getSurface() + " and " + a.getHConst().getSurface());
					variables.add(variable);
				}
			}
		}
	}
	return variables;
}



private int countInversions(Vector <SimpleConst> sc) {
	int real_i = 0 ;
	int i = -1;
	int inversions = 0;

	for (SimpleConst c : sc ) {
		try {
			String s =  c.getSurface();
			if (s.startsWith("[")) {
				int act_i = (new Integer(s.substring(1,s.length()-1))).intValue();
				if (act_i != i) {
					i = act_i;
					System.out.print(" " + act_i);
					if (real_i != act_i) inversions++;
					real_i++;
				}
			}
		} catch (Exception e) {}
	}
	System.out.println(" = " + inversions );
	//return (real_i>0 ? ((float) inversions)/real_i : 0);
	return inversions ;
}


private void putVariablesToAnchors(XDG h,XDG t,Vector <Anchor> anchors) {
	Vector <Constituent> h_consts =  h.getSetOfConstituents().getSimpleConstituentList();
	int variable = 0;
	for (Constituent h_c : h_consts) {
		Constituent t_c = null;
		if (( t_c = connectedTo(h_c,anchors))!= null) {
			System.out.println(" linking (" + t_c.getSurface() +","+ h_c.getSurface()+ ")");
			t_c.setSurface("[" + variable + "]");
			h_c.setSurface("[" + variable + "]");
			variable ++;
		}
	}
}

public Vector <Vector <Object>> permutations(Vector <Object> anchors) {
	Vector <Vector <Object>> permutations = new Vector();
	if (anchors.size() > 1 ) {
		for (Object a : anchors) {
			Vector <Object> remaining = new Vector();
			for (Object b : anchors) if (b != a) remaining.addElement(b);
			Vector <Vector <Object>> new_permutations = permutations(remaining);
			for (Vector permutation : new_permutations) {
				permutation.addElement(a);
				permutations.addElement(permutation);
			}
		}
	} else {
		permutations.addElement(anchors);
	}
	return permutations;
}

private void putVariablesToAnchors(Vector <Anchor> anchors) {
	int variable = 0;
	for (Anchor a : anchors) {
		Constituent t_c = null;
		if ( a.getTConst() != null && a.getHConst() != null ) {
			System.out.println(" linking (" + a.getTConst().getSurface() +","+ a.getHConst().getSurface()+ ")");
			a.getTConst().setSurface("[" + variable + "]");
			a.getHConst().setSurface("[" + variable + "]");
			variable ++;
		}
	}
}


public void putVariableNames(Vector <Object> anchors) {
	int variable = 0;
	for (Object constituents : anchors) {
		for (Constituent c : (Vector <Constituent> ) constituents) {
			// variables in the POS 
			String type = 
				c.getType().substring(0,(c.getType().indexOf(":") < 0? c.getType().length():c.getType().indexOf(":")));
			c.setType(type + ":" + variable);
			// variables in the token c.setSurface( "[" + variable + "]");
			// variables instead of the POS 	c.setType( "[" + variable + "]");
			//if (c instanceof ComplxConst) propagateVariableName((ComplxConst)c,variable);
		}
		variable ++;
	}
}


public void putVariableNamesToLeaves(Vector <Object> anchors) {
	int variable = 0;
	for (Object constituents : anchors) {
		for (Constituent c : (Vector <Constituent> ) constituents) {
			if (c instanceof SimpleConst) {
				// variables in the POS 
				Vector temp = new Vector();
				temp.add(c.getSurface());
				c.getFirstLemma().setAlternativeWordSenses(temp);
				// variables in the token 
				c.setSurface( "[" + variable + "]");
				// variables instead of the POS 	c.setType( "[" + variable + "]");
				//if (c instanceof ComplxConst) propagateVariableName((ComplxConst)c,variable);
			}
		}
		variable ++;
	}
}

public void removeVariableNamesFromLeaves(Vector <Object> anchors) {
	int variable = 0;
	for (Object constituents : anchors) {
		for (Constituent c : (Vector <Constituent> ) constituents) {
			if (c instanceof SimpleConst  && c.getSurface().startsWith("[") && c.getSurface().endsWith("]") ) {
				// variables in the POS 
				// variables in the token 
				c.setSurface( (String)c.getFirstLemma().getAlternativeWordSenses().elementAt(0));
				// variables instead of the POS 	c.setType( "[" + variable + "]");
				//if (c instanceof ComplxConst) propagateVariableName((ComplxConst)c,variable);
			}
		}
		variable ++;
	}
}



public void moveVariableNamesToType(Pair p) {
	moveVariableNamesToType(p.getHypoXDG());
	moveVariableNamesToType(p.getTextXDG());
}
public void moveVariableNamesToType(XDG xdg) {
	Vector<Constituent> cs = xdg.getSetOfConstituents();
	for (Constituent c:cs) moveVariableNamesToType(c);
}

public void moveVariableNamesToType(Constituent c) {
	if (c.getType().indexOf(':')>=0) {
		c.setType(c.getType().substring(0,c.getType().lastIndexOf(':')).replace(':','a'));
	}
	if (c instanceof ComplxConst) { 
		Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
		for (Constituent sc:subs) moveVariableNamesToType(sc);
	} 
	
}


/// 

public void putVariableNamesAndComputePfunction(Vector <Vector <Constituent>> variables,Pair p, int max_vars) {
	putVariableNamesAndComputePfunction(variables,p.getHypoXDG(), max_vars);
	putVariableNamesAndComputePfunction(variables,p.getTextXDG(), max_vars);
}
public void putVariableNamesAndComputePfunction(Vector <Vector <Constituent>> variables,XDG xdg, int max_vars) {
	Vector<Constituent> cs = xdg.getSetOfConstituents();
	for (Constituent c:cs) putVariableNamesAndComputePfunction(variables,c, max_vars);
}

public boolean [] putVariableNamesAndComputePfunction(Vector <Vector <Constituent>> variables,Constituent c, int max_vars) {
	boolean [] p = new boolean[max_vars]; 
	for (int i = 0; i < max_vars; i++) p[i]= false;
	Vector <Constituent> variable = containedIn(variables,c);
	

	if (c instanceof SimpleConst) {
		//System.out.println(c.getSurface() + ":" + c.getId() );		
		if (variable != null && (variables.indexOf(variable)+1)<max_vars) p[variables.indexOf(variable)] = true;
		
	} else {
		Vector <Constituent> cs = ((ComplxConst)c).getSubConstituents();
		for (Constituent c_sub:cs) {
			boolean [] p_temp = putVariableNamesAndComputePfunction(variables,c_sub,max_vars);
			p = or(p,p_temp);
		}
	}
	// DETERMINING NODE WITH VARIABLES
	// c.setType(c.getType() + ":" + (variable != null? variables.indexOf(variable) + 1 : 0) + ":" + boolToString(p));
	// DETERMINING NODE WITH VARIABLES (with bitmask)
	//if (c.getType().startsWith("ANC")) System.out.println(" " + c.getType() + " - " + c.getSurface() );
	//WITH BITMASK 
	//System.err.println("" + BitOperations.mask(p));
	c.setType(c.getType().replace(':','-') + ":" + (variable != null && (variables.indexOf(variable)+1)<max_vars? variables.indexOf(variable) + 1 : -1) + ":" + BitOperations.mask(p));

	//WITHOUT BITMASK 	c.setType(c.getType().replace(':','-') + ":" + (variable != null && (variables.indexOf(variable)+1)<max_vars? variables.indexOf(variable) + 1 : -1) );

	return p;
}



private boolean [] or(boolean [] p1, boolean [] p2) {
	for (int i = 0; i <p1.length && i < p2.length; i++) p1[i] = p1[i] || p2[i];
	return p1;
}

private String boolToString(boolean [] p) {
	String out = "";
	for (int i = 0; i <p.length; i++) out += (p[i]?"1":"0");
	return out;
}

/**
 sets the relevance for the TextTreePruner on the leaves containing variables
*/
public void setRelevancyOnTheVariableLeaves(Vector <Object> anchors) {
	for (Object constituents : anchors) {
		for (Constituent c : (Vector <Constituent> ) constituents) {
			if (c instanceof SimpleConst) TextTreePruner.setRelevant((SimpleConst) c);
		}
	}
}


private void propagateVariableName(ComplxConst c,int variable) {
	Vector <Constituent> v = c.getSimpleConstituentList();
}

private void mark(XDG h,XDG t,Vector <Anchor> anchors) {
	Vector <Constituent> h_consts =  h.getSetOfConstituents().getSimpleConstituentList();
	for (Constituent h_c : h_consts) {
		Constituent t_c = null;
		if (( t_c = connectedTo(h_c,anchors))!= null) {
			System.out.println(" linking (" + t_c.getSurface() +","+ h_c.getSurface()+ ")");
			t_c.setSurface("[" + "+" + "]");
			h_c.setSurface("[" + "+" + "]");
		}
	}
}

//private void mark(XDG h,XDG t,Vector <Anchor> anchors) {
//	Vector <Constituent> h_consts =  h.getSetOfConstituents().getSimpleConstituentList();
//	for (Constituent h_c : h_consts) {
//		Constituent t_c = null;
//		if (( t_c = connectedTo(h_c,anchors))!= null) {
//			System.out.println(" linking (" + t_c.getSurface() +","+ h_c.getSurface()+ ")");
//			
//			replace(h,h_c,newComplexConstituent((SimpleConst) h_c,"[+]"));
//			replace(t,t_c,newComplexConstituent((SimpleConst) t_c,"[+]"));
//			//t_c.setSurface("[" + "+" + "]");
//			//h_c.setSurface("[" + "+" + "]");
//		}
//	}
//}

private Constituent newComplexConstituent(SimpleConst sc,String type) {
	ComplxConst c = new ComplxConst();
	SimpleConst newsc = new SimpleConst(sc);
	newsc.setType(sc.getSurface());
	c.setType(sc.getType());
	newsc.setSurface(type);
	ConstituentList cl = new ConstituentList();
	//cl.add(sc);
	//cl.add(sc);
	cl.add(newsc);
	c.setSubConstituents(cl);
	return c;
}


private void replace(XDG xdg, Constituent old_c, Constituent new_c) {
	Vector <Constituent> xdg_constList =  xdg.getSetOfConstituents();
	for (Constituent c : xdg_constList) replace(c,old_c,new_c);
}

private Constituent replace(Constituent c,Constituent old_c,Constituent new_c) {
	if (c instanceof ComplxConst) {
		Vector <Constituent> cl = ((ComplxConst)c).getSubConstituents();	
		if (cl.contains(old_c)) cl.setElementAt(new_c, cl.indexOf(old_c));
		else for (Constituent ci: cl) replace(ci,old_c,new_c);
	}
	return c;
}

private Constituent connectedTo(Constituent h,Vector <Anchor> anchors) {
	for (Anchor a : anchors) {
		if (a.getHConst().equals(h)) return a.getTConst();
	}
	return null;
}


private Map mergeVariables(Vector <SimpleConst> scs_h,Vector <SimpleConst> scs_t, Vector <Vector> variables) {
	Hashtable t = new Hashtable();
	Vector v = new Vector();
	t.put("variables", v);

	Vector eliminate = new Vector();
	for (Vector <Constituent> v_cs : variables) {
		int max = 0;

		Constituent c_max = scs_h.elementAt(0);
		ConstituentList cls = new ConstituentList();
		for (Constituent c : v_cs) {
			if (scs_h.indexOf(c) > max) {c_max = c; max = scs_h.indexOf(c);}
			if (scs_h.contains(c)) {
				cls.add(c);
				eliminate.add(c);
			}
		}
	
	
		ComplxConst nc = new ComplxConst("var",cls);
		//Constituent nc = new SimpleConst((SimpleConst)cls.elementAt(cls.size()-1));
		//nc.setType("var");
		//nc.setSurface("var");
		t.put(c_max,nc);


		Vector var_pair = new Vector();
		var_pair.add(nc);

		max = 0;
		c_max = scs_t.elementAt(0);
		cls = new ConstituentList();

		for (Constituent c : v_cs) {
			if (scs_t.indexOf(c) > max) {c_max = c; max = scs_t.indexOf(c);}
			if (scs_t.contains(c)) {
				cls.add(c);
				eliminate.add(c);
			}
		}
		
		nc = new ComplxConst("var",cls);
		//nc = new SimpleConst((SimpleConst)cls.elementAt(cls.size()-1));
		//nc.setType("var");
		//nc.setSurface("var");
		t.put(c_max,nc);
		var_pair.add(nc);
		((Vector) t.get("variables")).add(var_pair);
	}
	t.put("eliminate",eliminate);
	return t;	
}



public Vector <Vector <Constituent>> propagateVariables(Vector <Vector <Constituent>> variables, Pair p) {
	Vector <Vector <Constituent>> variables_out = propagateVariables(variables, p.getTextXDG());
	variables_out = propagateVariables(variables_out, p.getHypoXDG());
	return variables_out;

}

public Vector <Vector <Constituent>> propagateVariables(Vector <Vector <Constituent>> variables, XDG xdg) {
	Vector <Constituent> consts = xdg.getSetOfConstituents();
	for (Constituent c: consts) 
		propagateVariables(variables, c);
	return variables;
	
}

private void propagateVariables(Vector <Vector <Constituent>> variables, Constituent c) {
	if (!(c instanceof SimpleConst)) {
		Vector <Constituent> sub_cs = ((ComplxConst)c).getSubConstituents();

		for (Constituent csub : sub_cs) {
			if (csub instanceof ComplxConst) propagateVariables(variables,csub);
			// NEW VERSION
			if (csub == c.getImmediateHead()) {
				Vector variable = containedIn(variables,csub);
			//	if (variable != null  && (csub instanceof SimpleConst || csub.getType().equals(c.getType()))) variable.add(c);
				if (variable != null) variable.add(c);
			}
			// ----

			/* ORIGINAL VERSION
			Vector variable = containedIn(variables,csub);
			if (variable != null && (csub instanceof SimpleConst || csub.getType().equals(c.getType()))) {
				variable.add(c);
			}
			*/

		} 
	}
}


private Vector <Constituent> containedIn(Vector <Vector <Constituent>> variables, Constituent c) {
	Vector <Constituent> out = null;
	for (Vector <Constituent> variable: variables) if (variable.contains(c)) out = variable;
	return out;
}

private void removeAndSubstitute(XDG xdg, Map to_substitue) {
	Vector <Constituent> consts = xdg.getSetOfConstituents();
	for (Constituent c: consts) 
		removeAndSubstitute(c, to_substitue);
	
}

private void removeAndSubstitute(Constituent c,  Map to_substitue) {
	Vector eliminate_const = (Vector) to_substitue.get("eliminate");
	if (!(c instanceof SimpleConst)) {
		Vector <Constituent> sub_cs = ((ComplxConst)c).getSubConstituents();
		ConstituentList sub_cs_new = new ConstituentList();
		for (Constituent csub : sub_cs) {
			if (to_substitue.containsKey(csub)) sub_cs_new.addElement((Constituent) to_substitue.get(csub));
			else if (!eliminate_const.contains(csub)) {
				removeAndSubstitute(csub, to_substitue);
				sub_cs_new.addElement(csub);
			}
		} 
		((ComplxConst)c).setSubConstituents(sub_cs_new);
	}
}


public void putAnchorTypesInNodes(Vector <Anchor> anchors) {
	for (Anchor a:anchors) {
		if ((a.getHConst() != null) && (a.getTConst() != null)) {
			if (!((SimpleConst)a.getHConst()).getType().endsWith("#")) {
				a.getHConst().setType(a.getHConst().getType() + "#" + a.getType() + "#");
			}
			if (!((SimpleConst)a.getTConst()).getType().endsWith("#")) {
				a.getTConst().setType(a.getTConst().getType() + "#" + a.getType()+ "#");
			}
		}
	}
	
}

public void propagateTheAnchorTypeInTheTrees(Pair p, Vector variables) {
	propagateTheAnchorTypeInTheTrees(p.getHypoXDG(),variables);
	propagateTheAnchorTypeInTheTrees(p.getTextXDG(),variables);
}

public void propagateTheAnchorTypeInTheTrees(XDG xdg, Vector variables) {
	Vector <Constituent> loc = xdg.getSetOfConstituents();
	for (Constituent c: loc) propagateTheAnchorTypeInTheTrees(c,variables);
}
public void propagateTheAnchorTypeInTheTrees(Constituent c, Vector variables) {
	Vector <String> precedence_table = new Vector();
	precedence_table.addElement("s");
	precedence_table.addElement("=");
	precedence_table.addElement(">");
	precedence_table.addElement("<");
	precedence_table.addElement("po");
	precedence_table.addElement("e");
	precedence_table.addElement("-");
	
	if (c instanceof ComplxConst) {
		Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
		String anchor_type = null;
		//System.out.println(c.getType() + "[");
		for (Constituent sc:subs) {
			if (sc instanceof ComplxConst) propagateTheAnchorTypeInTheTrees(sc,variables);
			String sub_type = sc.getType();
			//if (sc instanceof SimpleConst) System.out.println(sub_type + ":" + sc.getSurface());
			if (sub_type.endsWith("#") && sub_type.length() > 1 ) {
				//System.out.println(sub_type);
				String sub_type_a = sub_type.substring(0,sub_type.indexOf('#'));
				String anchor_type_a = sub_type.substring(sub_type.indexOf('#')+1,sub_type.length()-1);
				sub_type = sub_type_a;
				//System.out.println("type: " + sub_type + " , " + anchor_type_a);
				if (c.getType().equals("VP")) {
					if ((sub_type.charAt(0)=='V') && 
					    ((anchor_type == null) || 
					    (precedence_table.indexOf(anchor_type_a) > precedence_table.indexOf(anchor_type))))
						anchor_type = anchor_type_a;
				} else if ((anchor_type == null) || 
					(precedence_table.indexOf(anchor_type_a)) > precedence_table.indexOf(anchor_type))
					anchor_type = anchor_type_a;
				//System.out.println("type: " + anchor_type);
				//System.out.println("all_simple_const: " + all_simple_const);
			}
			
		}
		//System.out.println("]");
		if (containedIn(variables,c) != null)
			c.setType(c.getType() + "#" + anchor_type + "#");

	}
}

/** Given a set of <B>anchors</B> with repetitions (more anchors are defined for each element), this method:
<UL>
<LI>computes the chunks for both T and H</LI>
<LI>reduces the variables according to the locality principle</LI>
</UL>
*/
public Vector mergeConstituentsAndAssignVariablesWithRepetitions(
					Pair p,
					Vector<Vector <Anchor>> anchors) {

	return mergeConstituentsAndAssignVariablesWithRepetitions(
			ChunkHighlighter.relevantSequences(p.getHypoXDG()),
			ChunkHighlighter.relevantSequences(p.getTextXDG()), 
			anchors);
}

public Vector mergeConstituentsAndAssignVariablesWithRepetitions(
					Vector <Constituent> hs, 
					Vector <Constituent> ts, 
					Vector<Vector <Anchor>> anchors) {
						
	//System.out.println("----------\n" + anchors + "\n---------------");
	Vector <Vector <Anchor>> possible_anchor_sets = possible_anchor_sets(anchors);
	//System.out.println("----------\n" + possible_anchor_sets + "\n---------------");
	Vector min_set_of_variables = null;
	Vector min_set_of_anchors = null;
	for (Vector anchor_set : possible_anchor_sets) {
		Vector variables = mergeConstituentsAndAssignVariables(hs,ts,anchor_set);
		if (min_set_of_variables == null || min_set_of_variables.size() > variables.size()) {
			min_set_of_variables = variables;
			min_set_of_anchors = anchor_set;
		}
	}
	Vector output = new Vector();
	output.add(min_set_of_variables);
	output.add(min_set_of_anchors);
	return output;
}


//private Vector <Vector <Anchor>> possible_anchor_sets(Vector <Vector <Anchor>> anchors) {
//	Vector <Vector <Anchor>> pas = new Vector();
//	if (anchors.size() == 0) {
//		pas.add(new Vector());
//	} else {
//		Vector <Anchor> actual_anchors = anchors.elementAt(0);
//		anchors.removeElementAt(0);
//		for (Vector <Anchor> v : possible_anchor_sets(anchors)) {
//			for (Anchor a: actual_anchors) {
//				Vector <Anchor> anchors_set = new Vector();
//				anchors_set.addElement(a);
//				anchors_set.addAll(v);
//				pas.add(anchors_set);
//			}
//		}
//	}
//	return pas;
//}
//

private Vector <Vector <Anchor>> possible_anchor_sets(Vector <Vector <Anchor>> anchors) {
	Vector <Vector <Anchor>> pas = new Vector();
	int size = 1;
	pas.add(new Vector());
	for (Vector <Anchor> node_anchors : anchors) {
		if (size < 5000) {
			size = size*node_anchors.size();
			Vector <Vector <Anchor>> new_pas = new Vector();
			for (Anchor a : node_anchors) 
				for (Vector <Anchor> new_anchors : pas) {
					Vector <Anchor> actual_anchors = (Vector <Anchor>) new_anchors.clone();
					actual_anchors.add(a);
					new_pas.add(actual_anchors);
				}
			pas = new_pas;
		} else {
			for (Vector <Anchor> new_anchors : pas) 
				new_anchors.add(node_anchors.elementAt(0));
			
		}
	}
	return pas;
}


private Vector mergeConstituentsAndAssignVariables(Vector <Constituent> hs, Vector <Constituent> ts, Vector <Anchor> anchors) {
	int variable = 0;
	Hashtable variables = new Hashtable();
	for (Constituent c_h:hs) {
		Hashtable map = new Hashtable();
		Vector <Constituent> scs_h = c_h.getSimpleConstituentList();
		for (Constituent sc_h:scs_h) {
			Constituent sc_t = connectedTo(sc_h,anchors); 
			System.out.println( " -> (" + sc_h.getSurface() + ")");

			//Integer variableI_prec = null; 
			//int nodeLength = 1;
			if (sc_t != null) { 
				System.out.print( "(" + sc_h.getSurface() + " - " + sc_t.getSurface() + ")");
				Constituent c_t = getConstituentContaining(sc_t,ts);
				Integer variableI = null;
				if (map.containsKey(c_t)) { 
					variableI = (Integer) map.get(c_t);
					//if ( variableI_prec == variableI ) nodeLength++;
					//else {
					//	no
					//}
				} else {
					variableI = new Integer(variable);
					map.put(c_t,variableI);
					variable++;
					variables.put(variableI,new Vector());
				}
				((Vector)variables.get(variableI)).add(sc_t);
				((Vector)variables.get(variableI)).add(sc_h);
				System.out.println("[" + variableI + "]");
				//sc_t.setSurface("[" + variableI + "]");
				//sc_h.setSurface("[" + variableI + "]");
				// FMZ POSSIBLE CATASTROFIC MODIFICATION sc_t.getFirstLemma().setSurface(sc_t.getType()); // Si usa il firstLemma come variabile di 
				// FMZ POSSIBLE CATASTROFIC MODIFICATION sc_h.getFirstLemma().setSurface(sc_h.getType()); //   appoggio per il tipo del costituente
				//variableI_prec = variableI ;
			} 
		}
	}
	return new Vector(variables.values());
}


private Constituent getConstituentContaining(Constituent c,Vector <Constituent> constituents) {
	Constituent cr = null;
	for (Constituent crr : constituents) {
		Vector <Constituent> scl = crr.getSimpleConstituentList();		
		for (Constituent sc : scl) if (sc == c) cr = crr; 
	}
	return cr;
}


/** a first attempt to include co-references: here, the same variable is assigned to nouns and verbs that are anchored to the same node. 
		(variables is modified accordingly)
*/
public Vector expandVariablesWithAllTheAnchors(Vector <Vector> variables, Vector <Vector <Anchor>> grouped_anchors) {
	for (Vector <Anchor> a_group_of_anchors_for_a_h_word:grouped_anchors) {
		if (a_group_of_anchors_for_a_h_word.size()>1) {
			Constituent h_w = a_group_of_anchors_for_a_h_word.elementAt(0).getHConst();
			Vector related_variable = null;
			for (Vector actual_variable:variables) if (actual_variable.contains(h_w)) related_variable = actual_variable;
			for (Anchor a:a_group_of_anchors_for_a_h_word) {
					if (!related_variable.contains(a.getTConst())) related_variable.add(a.getTConst());
			}
		}
	}
	return variables;

}

}
