package it.uniroma2.art.marte.processors;

import it.uniroma2.art.marte.ArteException;

import java.util.Arrays;
import java.util.Vector;


public abstract class CascadeOfProcessors extends Processor {

	private Vector <Processor> activeProcessors;
	
	private Vector <Processor> processors;

	public Vector<String> setActiveProcessors(Vector <String> active_processors) throws Exception{
		if (processors != null) {

			this.activeProcessors = new Vector <Processor> ();
			for (Processor p: processors) {
				if (p instanceof CascadeOfProcessors) {
						//System.out.println("Setting active processors for : " + p.getClass().getName() + "\n  with " + active_processors);
						active_processors = ((CascadeOfProcessors) p).setActiveProcessors(active_processors);
						this.activeProcessors.add(p);
				} else if (p.getName().equals("#ACTIVE#") || active_processors.contains(p.getName())) {
					System.out.println("This is active : " + p.getName() + " - " + p.getClass().getCanonicalName());
					this.activeProcessors.add(p);
					if (active_processors.contains(p.getName())) active_processors.remove(p.getName());
				}
			}
		}
		return active_processors;
	}
	
	public void setActiveProcessors(String [] activeProcessorsArray) throws Exception{
		if (processors != null) {
	
			Vector<String> active_processors;
			if (activeProcessorsArray!=null) active_processors = new Vector<String>(Arrays.asList(activeProcessorsArray)); 
			else active_processors = new Vector<String>(); 
			Vector <String>unactivated = setActiveProcessors(active_processors);
			if (unactivated.size()!=0) throw new ArteException("These processes are unknown: " + unactivated);
			this.initialize();
		}
	}

	public Vector <Processor> getActiveProcessors() {
		return activeProcessors;
	}

	public void setProcessors(Vector <Processor> processors) {
		this.processors = processors;
		this.activeProcessors = processors;
	}

	public Vector <Processor> getProcessors() {
		return processors;
	}
	
	public String toString() {
		String output = getDescription() +"\n";
		if (processors != null) {
			for (Processor p: processors) {
				if (p instanceof CascadeOfProcessors) {
				  output += "---inner cascade---\n" + p.toString() + "--------------\n";  
				} else if (!p.getName().equals("#ACTIVE#"))
					output += "* " + p.toString() + "\n";
			}
		}
		return output;
	}

	@Override
	public void initialize() throws Exception {
		if (processors != null) {
		
			for (Processor p: this.getActiveProcessors()) {
				p.initialize();
			}
		}
	}

	
}
