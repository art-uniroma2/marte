
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PUBLIC INTERFACE 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transform(T,TT):-
	rewritten_in(T,TT).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIMPLE ALGPRITHM 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rewritten_in(N,N):-
	atom(N).

rewritten_in(N,N):-
	number(N).


rewritten_in(N1,N2):-
	rule(N1,N2),!.
	
	
rewritten_in(N1,N2):-
	N1 =.. [A|R1],
	!,
	list_rewritten_in(R1,R2),
	N2 =.. [A|R2].
	

list_rewritten_in([],[]).

list_rewritten_in([A1|R1],[A2|R2]):-
	rewritten_in(A1,A2),
	!,
	list_rewritten_in(R1,R2).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPECIFIC RULES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE MODALS, GRADE 1 (must, be to)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%be to
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'AUX'(ID3,A,'be',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S),
	append(X,[NP,'VP|mod+1|'(ID2,'AUX'(ID3,A,'be',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT].


%must
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+1|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	mod1(MODAL).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE MODALS, GRADE 2 (have to, have got to, supposed to)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%have to
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'AUX'(ID3,A,'have',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S),
	append(X,[NP,'VP|mod+2|'(ID2,'AUX'(ID3,A,'have',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+2|',ID1|REST_S_NOT].


%have got to
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'AUX'(ID3,A,'have',l),'VP'(ID4,'VBD'(ID5,'got','got',l),'S'(ID6,'VP'(ID7,'TO'(ID8,'to','to',l),VP))))|REST],REST_S),
	append(X,[NP,'VP|mod+2|'(ID2,'AUX'(ID3,A,'have',l),'VP'(ID4,'VBD'(ID5,'got','got',l),'S'(ID6,'VP'(ID7,'TO'(ID8,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+2|',ID1|REST_S_NOT].


%have gotta
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'AUX'(ID3,A,'have',l),'RB'(ID4,'gotta','gotta',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+2|'(ID2,'AUX'(ID3,A,'have',l),'RB'(ID4,'gotta','gotta',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+2|',ID1|REST_S_NOT].


%supposed to 
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID8,'AUX'(ID7,A,'be',l),'VP'(ID2,'VBN'(ID3,'supposed','supposed',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP))))|REST],REST_S),
	append(X,[NP,'VP|mod+2|'(ID8,'AUX'(ID7,A,'be',l),'VP'(ID2,'VBN'(ID3,'supposed','supposed',l),'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+2|',ID1|REST_S_NOT].



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE MODALS, GRADE 3 (should, ought to , have better)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FARE le FORME CONTRATTE 've better , 's better , oughta, shoulda

%have better
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID3,'AUX'(ID4,A,'have',l),'ADVP'(ID5,'RB'(ID6,'better','better',l)),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+3|'(ID3,'AUX'(ID4,A,'have',l),'ADVP'(ID5,'RB'(ID6,'better','better',l)),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT].

%'d better
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,'''d','''d',l),'ADVP'(ID4,'RB'(ID5,'better','better',l)),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+3|'(ID2,'MD'(ID3,'''d','''d',l),'ADVP'(ID4,'RB'(ID5,'better','better',l)),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT].


%should, ought
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+3|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	mod3(MODAL).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE MODALS, GRADE 4 (may, might, could, can)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod+4|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	mod4(MODAL).




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NEGATIVE MODALS, GRADE 1 (could not, can not)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-1|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-1|',ID1|REST_S_NOT],
	mod1_n(MODAL).


rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID3,'n''t','n''t',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-1|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID3,'n''t','n''t',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-1|',ID1|REST_S_NOT],
	mod1_n(MODAL).
	




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NEGATIVE MODALS, GRADE 2 (must not)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-2|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-2|',ID1|REST_S_NOT],
	mod2_n(MODAL).


rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-2|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-2|',ID1|REST_S_NOT],
	mod2_n(MODAL).
	



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NEGATIVE MODALS, GRADE 3 (have better not, should not)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%FARE le FORME CONTRATTE 've better not, 's better not, 'd better not

%have better not
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID7,'AUX'(ID2,A,'have',l),'ADJP'(ID3,'JJR'(ID4,'better','better',l)),'S'(ID5,'RB'(ID6,'not','not',l),VP))|REST],REST_S),
	append(X,[NP,'VP|mod-3|'(ID7,'AUX'(ID2,A,'have',l),'ADJP'(ID3,'JJR'(ID4,'better','better',l)),'S'(ID5,'RB'(ID6,'not','not',l),VP))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-3|',ID1|REST_S_NOT].
	

%'d better not
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,'''d','''d',l),'ADVP'(ID4,'RB'(ID5,'better','better',l)),'RB'(ID6,'not','not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-3|'(ID2,'MD'(ID3,'''d','''d',l),'ADVP'(ID4,'RB'(ID5,'better','better',l)),'RB'(ID6,'not','not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-3|',ID1|REST_S_NOT].

	

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-3|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-3|',ID1|REST_S_NOT],
	mod3_n(MODAL).


rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-3|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-3|',ID1|REST_S_NOT],
	mod3_n(MODAL).
	


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%NEGATIVE MODALS, GRADE 4 (may not, might not)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-4|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'not','not',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-4|',ID1|REST_S_NOT],
	mod4_n(MODAL).


rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S),
	append(X,[NP,'VP|mod-4|'(ID2,'MD'(ID3,MODAL,MODAL_L,l),'RB'(ID4,'n''t','n''t',l),VP)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod-4|',ID1|REST_S_NOT],
	mod4_n(MODAL).
	



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE ADVERBS, GRADE 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%adv...
%rule(S,S_NOT):-
%	S =.. ['S',ID1|REST_S],
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S),
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S_NOT),
%	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
%	adv_1(ADV).
	

%...,adv,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S),
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	adv_1(ADV).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE ADVERBS, GRADE 3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%adv...
%rule(S,S_NOT):-
%	S =.. ['S',ID1|REST_S],
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S),
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S_NOT),
%	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
%	adv_3(ADV).
	

%...,adv,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S),
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	adv_3(ADV).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE ADVERBS, GRADE 4 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%adv...
%rule(S,S_NOT):-
%	S =.. ['S',ID1|REST_S],
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S),
%	append(X,['ADVP'(ID2,'RB'(ID3,ADV,ADV,l))|REST],REST_S_NOT),
%	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
%	adv_4(ADV).
	

%...,adv,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S),
	append(X,[','(ID2,',',',',l),'ADVP'(ID3,'RB'(ID4,ADV,ADV,l)),','(ID5,',',',',l)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	adv_4(ADV).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE VERBS, GRADE 1 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%John said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S),
	append(X,[NP,'VP|mod+1|'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA).


%...,John said
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA),
	np(NP),
	sent(SENT).


%...,John said,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	VP =.. ['VP',_,EPI_VERB,','(_,',',',',l)|REST],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA),
	np(NP),
	sent(SENT).


%John said to...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S),
	append(X,[NP,'VP|mod+1|'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA).

	
%It is said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+1|'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA).


%It is said to...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'S'(ID7,'VP'(ID8,'TO'(ID10,'to','to',l),VP))))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+1|'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'S'(ID7,'VP'(ID8,'TO'(ID10,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA).
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE VERBS, GRADE 3 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%John said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S),
	append(X,[NP,'VP|mod+3|'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_3(LEMMA).


%...,John said
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_3(LEMMA),
	np(NP),
	sent(SENT).


%...,John said,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	VP =.. ['VP',_,EPI_VERB,','(_,',',',',l)|REST],
	lemma(EPI_VERB,LEMMA),
	epi_verb_1(LEMMA),
	np(NP),
	sent(SENT).
	

%John said to...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S),
	append(X,[NP,'VP|mod+3|'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_3(LEMMA).
	
	
%It is said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+3|'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+3|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_3(LEMMA).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE VERBS, GRADE 4 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%John said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S),
	append(X,[NP,'VP|mod+4|'(ID2,EPI_VERB,'SBAR'(ID3,'IN'(ID4,'that','that',l),SUB_SENT))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA).


%...,John said
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,'VP'(ID3,EPI_VERB)|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA),
	np(NP),
	sent(SENT).


%...,John said,...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S),
	append(X,[SENT,','(ID2,',',',',l),NP,VP|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	VP =.. ['VP',_,EPI_VERB,','(_,',',',',l)|REST],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA),
	np(NP),
	sent(SENT).
	
	

%John said to...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,[NP,'VP'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S),
	append(X,[NP,'VP|mod+4|'(ID3,EPI_VERB,'S'(ID4,'VP'(ID5,'TO'(ID6,'to','to',l),VP)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA).


%It is said that...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+4|'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'SBAR'(ID7,'IN'(ID8,'that','that',l),SUB_SENT)))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA).
	
	
%It is said to...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'S'(ID7,'VP'(ID8,'TO'(ID10,'to','to',l),VP))))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+4|'(ID4,'AUX'(ID5,A,'be',l),'VP'(ID6,EPI_VERB,'S'(ID7,'VP'(ID8,'TO'(ID10,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	lemma(EPI_VERB,LEMMA),
	epi_verb_4(LEMMA).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE ADJECTIVE CLAUSE, GRADE 1 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%it is necessary that/if/whether ...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l)),'SBAR'(ID8,'IN'(ID9,PREP,PREP,l),SUB_SENT))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+1|'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l)),'SBAR'(ID8,'IN'(ID9,PREP,PREP,l),SUB_SENT))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	epi_adj_1(EPI_ADJ),
	prep_sub(PREP).


%it is necessary to ...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l),'S'(ID8,'VP'(ID9,'TO'(ID10,'to','to',l),VP))))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+1|'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l),'S'(ID8,'VP'(ID9,'TO'(ID10,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+1|',ID1|REST_S_NOT],
	epi_adj_1(EPI_ADJ).	
	
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%POSITIVE ADJECTIVE CLAUSE, GRADE 4 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%it is necessary that/if/whether ...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l)),'SBAR'(ID8,'IN'(ID9,PREP,PREP,l),SUB_SENT))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+4|'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l)),'SBAR'(ID8,'IN'(ID9,PREP,PREP,l),SUB_SENT))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	epi_adj_4(EPI_ADJ),
	prep_sub(PREP).


%it is necessary to ...
rule(S,S_NOT):-
	S =.. ['S',ID1|REST_S],
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l),'S'(ID8,'VP'(ID9,'TO'(ID10,'to','to',l),VP))))|REST],REST_S),
	append(X,['NP'(ID2,'PRP'(ID3,'it','it',l)),'VP|mod+4|'(ID4,'AUX'(ID5,A,'be',l),'ADJP'(ID6,'JJ'(ID7,EPI_ADJ,EPI_ADJ,l),'S'(ID8,'VP'(ID9,'TO'(ID10,'to','to',l),VP))))|REST],REST_S_NOT),
	S_NOT =.. ['S|mod+4|',ID1|REST_S_NOT],
	epi_adj_4(EPI_ADJ).	
	
	
%%%%%%%%%%%%%%%%%%%
% USEFUL PREDICATES
%%%%%%%%%%%%%%%%%%%

%epi_construct(VP):-
%	VP =.. ['VP',_,EPI_VERB,','(_,',',',',l)|REST],
%	epi_verb_1(EPI_VERB).
	
np(NP):- NP =.. ['NP'|R]. 

sent(SENT):- SENT =.. ['S'|R]. 

prep_sub('that').
prep_sub('if').
prep_sub('wheter').

lemma(V,LEMMA):- 
	V =.. [_,_,LEMMA|R].
	
	
	

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  MODAL LEXICON   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%POSITIVE GRADES
mod1(must).
mod3(should).
mod3(ought).
mod4(may).
mod4(might).
mod4(could).
mod4(can).


%NEGATIVE GRADES
mod1_n(can).
mod1_n(could).
mod2_n(must).
mod3_n(should).
mod4_n(might).
mod4_n(may).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  DEONTIC VERB LEXICON   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
% grade 1 : VERY STRONG
epi_verb_1('need').
epi_verb_1('require').
epi_verb_1('demand').
epi_verb_1('postulate').
epi_verb_1('necessitate').
epi_verb_1('govern').
epi_verb_1('presuppose').
epi_verb_1('suppose').
epi_verb_1('assume').
epi_verb_1('presume').
epi_verb_1('expect').
epi_verb_1('posit').
epi_verb_1('command').
epi_verb_1('require').
epi_verb_1('compel').
epi_verb_1('order').
epi_verb_1('request').

% grade 4 : WEAK
epi_verb_4('let').
epi_verb_4('allow').
epi_verb_4('permit').
epi_verb_4('tolerate').
epi_verb_4('authorize').
epi_verb_4('authorise').
epi_verb_4('licence').
epi_verb_4('license').
epi_verb_4('legalize').
epi_verb_4('legalise').
epi_verb_4('decriminalize').
epi_verb_4('decriminalise').
epi_verb_4('legitimize').
epi_verb_4('legitimise').
epi_verb_4('legitimate').
epi_verb_4('legitimatize').
epi_verb_4('legitimatise').
epi_verb_4('grant').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  EPISTEMIC-EVIDENTIALITY VERB LEXICON   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
% grade 1 : VERY STRONG
epi_verb_1('add').
epi_verb_1('adjudge').
epi_verb_1('adjudicate').
epi_verb_1('admit').
epi_verb_1('admonish').
epi_verb_1('advise').
epi_verb_1('affirm').
epi_verb_1('agnise').
epi_verb_1('agnize').
epi_verb_1('alert').
epi_verb_1('allow').
epi_verb_1('animadvert').
epi_verb_1('announce').
epi_verb_1('answer').
epi_verb_1('anticipate').
epi_verb_1('append').
epi_verb_1('argue').
epi_verb_1('argue').
epi_verb_1('articulate').
epi_verb_1('ascertain').
epi_verb_1('assert').
epi_verb_1('assume').
epi_verb_1('assure').
epi_verb_1('attest').
epi_verb_1('aver').
epi_verb_1('avow').
epi_verb_1('bank').
epi_verb_1('blab').
epi_verb_1('blurt').
epi_verb_1('bridle').
epi_verb_1('buy').
epi_verb_1('cable').
epi_verb_1('calculate').
epi_verb_1('call').
epi_verb_1('cast').
epi_verb_1('catch').
epi_verb_1('caution').
epi_verb_1('chat').
epi_verb_1('chatter').
epi_verb_1('chitchat').
epi_verb_1('claim').
epi_verb_1('clarion').
epi_verb_1('cognise').
epi_verb_1('cognize').
epi_verb_1('comment').
epi_verb_1('confer').
epi_verb_1('confess').
epi_verb_1('confide').
epi_verb_1('confirm').
epi_verb_1('converse').
epi_verb_1('convey').
epi_verb_1('counsel').
epi_verb_1('credit').
epi_verb_1('declare').
epi_verb_1('decree').
epi_verb_1('demonstrate').
epi_verb_1('denounce').
epi_verb_1('designate').
epi_verb_1('dictate').
epi_verb_1('discern').
epi_verb_1('discover').
epi_verb_1('doom').
epi_verb_1('editorialise').
epi_verb_1('editorialize').
epi_verb_1('e-mail').
epi_verb_1('enact').
epi_verb_1('endorse').
epi_verb_1('enlighten').
epi_verb_1('enunciate').
epi_verb_1('examine').
epi_verb_1('exclaim').
epi_verb_1('explain').
epi_verb_1('explicate').
epi_verb_1('expostulate').
epi_verb_1('fate').
epi_verb_1('fax').
epi_verb_1('fend for').
epi_verb_1('ferret').
epi_verb_1('ferret out').
epi_verb_1('field').
epi_verb_1('figure').
epi_verb_1('find').
epi_verb_1('find out').
epi_verb_1('gab').
epi_verb_1('grant').
epi_verb_1('guarantee').
epi_verb_1('hold').
epi_verb_1('indicate').
epi_verb_1('indorse').
epi_verb_1('inspect').
epi_verb_1('instruct').
epi_verb_1('introduce').
epi_verb_1('investigate').
epi_verb_1('judge').
epi_verb_1('know').
epi_verb_1('lean').
epi_verb_1('learn').
epi_verb_1('look').
epi_verb_1('maintain').
epi_verb_1('mean').
epi_verb_1('mention').
epi_verb_1('netmail').
epi_verb_1('note').
epi_verb_1('notice').
epi_verb_1('observe').
epi_verb_1('pass').
epi_verb_1('phone').
epi_verb_1('point out').
epi_verb_1('preamble').
epi_verb_1('precede').
epi_verb_1('predestine').
epi_verb_1('preface').
epi_verb_1('premise').
epi_verb_1('present').
epi_verb_1('previse').
epi_verb_1('proclaim').
epi_verb_1('profess').
epi_verb_1('prologise').
epi_verb_1('prologize').
epi_verb_1('prologuize').
epi_verb_1('promise').
epi_verb_1('promulgate').
epi_verb_1('propose').
epi_verb_1('protest').
epi_verb_1('prove').
epi_verb_1('provide').
epi_verb_1('quote').
epi_verb_1('rake up').
epi_verb_1('rap').
epi_verb_1('read').
epi_verb_1('realise').
epi_verb_1('realize').
epi_verb_1('re-argue').
epi_verb_1('recognise').
epi_verb_1('recognize').
epi_verb_1('recount').
epi_verb_1('reenact').
epi_verb_1('regard').
epi_verb_1('reiterate').
epi_verb_1('rejoin').
epi_verb_1('relate').
epi_verb_1('rely').
epi_verb_1('remark').
epi_verb_1('repay').
epi_verb_1('repeat').
epi_verb_1('reply').
epi_verb_1('report').
epi_verb_1('represent').
epi_verb_1('reprobate').
epi_verb_1('reprove').
epi_verb_1('repute').
epi_verb_1('respond').
epi_verb_1('return').
epi_verb_1('reveal').
epi_verb_1('riposte').
epi_verb_1('sass').
epi_verb_1('savor').
epi_verb_1('say').
epi_verb_1('scan').
epi_verb_1('schmooze').
epi_verb_1('scrutinize').
epi_verb_1('see').
epi_verb_1('show').
epi_verb_1('sign').
epi_verb_1('signal').
epi_verb_1('slip in').
epi_verb_1('sneak in').
epi_verb_1('spatchcock').
epi_verb_1('speak').
epi_verb_1('speak up').
epi_verb_1('state').
epi_verb_1('stick in').
epi_verb_1('suggest').
epi_verb_1('sum').
epi_verb_1('sum up').
epi_verb_1('summarise').
epi_verb_1('summarize').
epi_verb_1('supply').
epi_verb_1('support').
epi_verb_1('survey').
epi_verb_1('swallow ').
epi_verb_1('swan').
epi_verb_1('swear').
epi_verb_1('take').
epi_verb_1('take the stand').
epi_verb_1('talk').
epi_verb_1('talk down').
epi_verb_1('teach').
epi_verb_1('telegraph').
epi_verb_1('telephone').
epi_verb_1('telex').
epi_verb_1('tell').
epi_verb_1('testify').
epi_verb_1('toss in').
epi_verb_1('trip up').
epi_verb_1('trumpet').
epi_verb_1('trust').
epi_verb_1('underquote').
epi_verb_1('undersign').
epi_verb_1('understand').
epi_verb_1('verify').
epi_verb_1('view').
epi_verb_1('vocalise').
epi_verb_1('vocalize').
epi_verb_1('vouch').
epi_verb_1('warn').
epi_verb_1('warrant').
epi_verb_1('watch').
epi_verb_1('wire').
epi_verb_1('wise up').
epi_verb_1('witness').
epi_verb_1('write').
epi_verb_1('yak').

 
% grade 3 : MIDDLE
epi_verb_3('believe').
epi_verb_3('calculate').
epi_verb_3('cite').
epi_verb_3('conjecture').
epi_verb_3('count').
epi_verb_3('depend').
epi_verb_3('depone').
epi_verb_3('depose').
epi_verb_3('descry').
epi_verb_3('espy').
epi_verb_3('eye').
epi_verb_3('fancy').
epi_verb_3('feel').
epi_verb_3('get a line').
epi_verb_3('get the goods').
epi_verb_3('get wind').
epi_verb_3('get word').
epi_verb_3('glimpse').
epi_verb_3('gossip').
epi_verb_3('hear').
epi_verb_3('infer').
epi_verb_3('narrate').
epi_verb_3('overhear').
epi_verb_3('perceive').
epi_verb_3('pick up').
epi_verb_3('plead').
epi_verb_3('pledge').
epi_verb_3('plight').
epi_verb_3('presume').
epi_verb_3('promise').
epi_verb_3('quote').
epi_verb_3('read').
epi_verb_3('reason').
epi_verb_3('recite').
epi_verb_3('reckon').
epi_verb_3('relay').
epi_verb_3('scent').
epi_verb_3('sense').
epi_verb_3('sight').
epi_verb_3('smell').
epi_verb_3('spot').
epi_verb_3('spy').
epi_verb_3('suppose').
epi_verb_3('surmise').
epi_verb_3('suspect').
epi_verb_3('swear').
epi_verb_3('taste').
epi_verb_3('think').
epi_verb_3('vow').


% grade 4 : WEAK
epi_verb_4('anticipate').
epi_verb_4('augur').
epi_verb_4('bet').
epi_verb_4('counter').
epi_verb_4('desire').
epi_verb_4('forebode').
epi_verb_4('forecast').
epi_verb_4('foreknow').
epi_verb_4('foresee').
epi_verb_4('foretell').
epi_verb_4('guess').
epi_verb_4('hazard').
epi_verb_4('hope').
epi_verb_4('imagine').
epi_verb_4('opine').
epi_verb_4('outguess').
epi_verb_4('preach').
epi_verb_4('predict').
epi_verb_4('prognosticate').
epi_verb_4('prophesy').
epi_verb_4('scry').
epi_verb_4('second-guess').
epi_verb_4('sound off').
epi_verb_4('speculate').
epi_verb_4('vaticinate').
epi_verb_4('venture').
epi_verb_4('wager').
epi_verb_4('wish').




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  ADVERBIAL LEXICON   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%EPISTEMIC GRADE 1
adv_1('extremely').
adv_1('likely').
adv_1('inevitably').
adv_1('inescapably').
adv_1('ineluctably').
adv_1('inevitably').
adv_1('unavoidably').
adv_1('obviously').
adv_1('evidently').
adv_1('Extremely').
adv_1('Likely').
adv_1('Inevitably').
adv_1('Inescapably').
adv_1('Ineluctably').
adv_1('Inevitably').
adv_1('Unavoidably').
adv_1('Obviously').
adv_1('Evidently').

%DEONTIC GRADE 1
adv_1('Necessarily').
adv_1('Needfully').
adv_1('Compulsorily').
adv_1('Obligatorily').
adv_1('Mandatorily').
adv_1('necessarily').
adv_1('needfully').
adv_1('compulsorily').
adv_1('obligatorily').
adv_1('mandatorily').


%EPISTEMIC GRADE 3
adv_3('Believably').
adv_3('Belike').
adv_3('Credibly').
adv_3('Likely').
adv_3('Plausibly').
adv_3('Probably').
adv_3('believably').
adv_3('belike').
adv_3('credibly').
adv_3('likely').
adv_3('plausibly').
adv_3('probably').

%EPISTEMIC GRADE 4
adv_4('maybe').
adv_4('mayhap').
adv_4('peradventure').
adv_4('perchance').
adv_4('perhaps').
adv_4('possibly').
adv_4('potentially').
adv_4('unsteadily').
adv_4('falteringly').
adv_4('uncertainly').
adv_4('Maybe').
adv_4('Mayhap').
adv_4('Peradventure').
adv_4('Perchance').
adv_4('Perhaps').
adv_4('Possibly').
adv_4('Potentially').
adv_4('Unsteadily').
adv_4('Falteringly').
adv_4('Uncertainly').

%EPISTEMIC GRADE NEGATIVE 1
adv_n('impossibly').

%DEONTIC GRADE NEGATIVE 1
adv_n('unnecessarily').

%EPISTEMIC GRADE NEGATIVE 3
adv_n('unlikely').

%EPISTEMIC GRADE NEGATIVE 4
adv_4_n('maybe').
adv_4_n('mayhap').
adv_4_n('peradventure').
adv_4_n('perchance').
adv_4_n('perhaps').
adv_4_n('possibly').
adv_4_n('potentially').
adv_4_n('unsteadily').
adv_4_n('falteringly').
adv_4_n('uncertainly').
adv_4_n('Maybe').
adv_4_n('Mayhap').
adv_4_n('Peradventure').
adv_4_n('Perchance').
adv_4_n('Perhaps').
adv_4_n('Possibly').
adv_4_n('Potentially').
adv_4_n('Unsteadily').
adv_4_n('Falteringly').
adv_4_n('Uncertainly').


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%  ADJECTIVE LEXICON   %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%EPISTEMIC GRADE 1
epi_adj_1('certain').
epi_adj_1('undisputable').
epi_adj_1('sure').
epi_adj_1('definite').
epi_adj_1('inevitable').
epi_adj_1('true').

%DEONTIC GRADE 1
epi_adj_1('compulsory').
epi_adj_1('mandatory').
epi_adj_1('required').
epi_adj_1('obligatory').
epi_adj_1('necessary').

%EPISTEMIC GRADE 4
epi_adj_1('uncertain').
epi_adj_1('incertain').
epi_adj_1('disputable').
epi_adj_1('unsure').
epi_adj_1('indefinite').
epi_adj_1('contingent').
epi_adj_1('unnecessary').
epi_adj_1('evitable').
epi_adj_1('possible').
epi_adj_1('imaginable').
epi_adj_1('conceivable').

%DEONTIC GRADE 4
epi_adj_1('allowable').
epi_adj_1('permissible').
epi_adj_1('tolerable').
