package it.uniroma2.art.marte.processors.treeRewriters;


import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.XDG;
import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.temp.*;

import alice.tuprolog.*;
import java.io.*;

/** 
	Simply prints the input pairs already analysed by Charniak, 
	in the internal parenthetical format.
*/
public class XDGprinter{

	private String inFile=null;
	private String outFile=null;
	private BufferedWriter out=null;
	private boolean withLemma=false;
	
	
	
	public void Run() throws Exception{
		out = new BufferedWriter(new FileWriter(new File(outFile)));
		DataSet ds = new DataSet(inFile);
		out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +	"<entailment-corpus>\n");
		System.out.println(ds.getAllPairs().size());
		for (int i=0; i<ds.getAllPairs().size();i++)
			{
			Pair pair = ds.getPair(i);
			transform(pair);
			}
		//out.write(ds.toXML());
		out.write("</entailment-corpus>\n");
		out.close();
	}
	
		/** 
		print a pair in internal format.
	*/
	public void transform(Pair p) throws Exception {
		out.write("<pair id=\"" + p.getId()+ "\" entailment=\"" + ((p.getOrigValue())? "YES" : "NO") + "\" task=\"" + p.getTask()+ "\">\n");
		out.write("<t>");
		transform(p.getTextXDG());
		out.write("</t>\n<h>");
		transform(p.getHypoXDG());
	 	out.write("</h>\n</pair>\n");
	}

	/** 
		print a XDG in internal format.
	*/
	public XDG transform(XDG xdg) throws Exception{
		Constituent tree = (Constituent) xdg.getSetOfConstituents().elementAt(0);
		Struct input;
		if (withLemma)
			input = ConstituentToTuPrologPennTree.toTuPrologPennTreeWithLemmas(tree);
		 else
		 	input = ConstituentToTuPrologPennTree.toTuPrologPennTree(tree);
		out.write(input.toString());
		xdg.getSetOfConstituents().setElementAt(tree,0);
		return xdg;
	}
	



 /**
  * Parse input parameters. If not defined by user,
  * reads them from configuration file. 
  */
  private void parseParameters(String[] argv) throws Exception {
	 for (int i=0; i < argv.length ; i++) 
		{
		if (argv[i].equals("-if"))       {i++; inFile = argv[i]; }
		else if (argv[i].equals("-of")) {i++; outFile = argv[i];}
		else if (argv[i].equals("-l")) {withLemma = true;}
		 }
	if ((inFile==null) || (outFile==null)) usage();
	}	 
  

	
	 /** 
  * Explains input parameters
  */
	private static void usage()
		{
		System.out.println("\n");
		System.out.println("Usage: XDGprinter -if <input file> -of <output file>");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" where <input file> is a file containing a syntactically parsed (with Charniak) RTE dataset in XML format.");
		System.out.println(" where <output file> is a file containing a syntactically parsed RTE dataset in XML internal format.");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" Options are:");
		System.out.println("\n");
		System.out.println("   -l if lemmas must be written in the final representation");  
System.out.println("\n\n");
		System.exit(0);
		}		

	
	public static void main(String[] argv) throws Exception {
	if (argv.length == 0) usage();
	else {
		XDGprinter p = new XDGprinter();
		p.parseParameters(argv);
		p.Run();	
	}                
		
}//end main


}