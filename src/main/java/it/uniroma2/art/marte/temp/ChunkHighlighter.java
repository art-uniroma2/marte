package it.uniroma2.art.marte.temp;

import java.util.*;

import it.reveal.chaos.xdg.textstructure.ComplxConst;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.XDG;

public class ChunkHighlighter {

	private static Vector <String> generalCutTypes = new Vector();

	public static void setCoreConstituentTypes(Vector <String> core_constituent_types) {
		generalCutTypes = core_constituent_types;
	}


	public static Vector <Constituent> relevantSequences(XDG xdg) {
		Vector <Constituent> nrc = new Vector();
		Vector <Constituent> consts = xdg.getSetOfConstituents();
		for (Constituent c:consts) nrc.addAll(relevantSequences(c,generalCutTypes));
		return nrc;
	}
	
	
	private static Vector <Constituent> relevantSequences(Constituent c, Vector <String> cutTypes ) {
		Vector <Constituent> vc = new Vector();
		ConstituentList cl = cleanedSequence(c,cutTypes);
		if (cl.size() != 0) {
			Constituent new_c = new ComplxConst(c.getType(),cl);
			vc.add(new_c);
			System.out.println("CS " + c.getType() + ":" + cl.size() + ":" + new_c.getSurface() );
		}
		Vector <Constituent> uncovered_consts = uncoveredConstituents(c,cutTypes);
		for (Constituent uc:uncovered_consts) {
			Vector <String> newCutTypes = new Vector();
			for (String type: generalCutTypes) if (!type.equals(uc.getType())) newCutTypes.add(type);
			vc.addAll(relevantSequences(uc,newCutTypes));
		}
		return vc;
	}
	private static ConstituentList cleanedSequence(Constituent c, Vector <String> cutTypes ) {
		ConstituentList cl = new ConstituentList();
		if (c instanceof ComplxConst) {
			Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
			for (Constituent sc:subs) {
				if (!cutTypes.contains(sc.getType())) cl.addAll(cleanedSequence(sc,cutTypes));
			}
		} else cl.add(c);
		return cl;
	}
	
	private static Vector <Constituent> uncoveredConstituents(Constituent c, Vector <String> cutTypes ) {
		Vector <Constituent> cl = new Vector();
		if (c instanceof ComplxConst) {
			Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
			for (Constituent sc:subs) {
				if (cutTypes.contains(sc.getType())) cl.add(sc);
				else cl.addAll(uncoveredConstituents(sc,cutTypes));
			}
		} 
		return cl;
	}
	
	
	private static Vector <Constituent> nonRecursiveKernels(XDG xdg) {
		Vector <Constituent> nrc = new Vector();
		Vector <Constituent> consts = xdg.getSetOfConstituents();
		for (Constituent c:consts) nrc.addAll(nonRecursiveKernels(c));
		return nrc;
	}
	
	
	private static Vector <Constituent> nonRecursiveKernels(Constituent c) {
		// MIGLIORABILE!!!! 
		Vector <Constituent> v = new Vector();
		if ((c.getType().equals("NP") || c.getType().equals("VP")) && !recursive(c))	{ 
			v.add(c);
			System.out.println("PK: " + (new XDG()).constBracketing(c));
		}
		
		if (c instanceof ComplxConst) {
			Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
			for (Constituent sc: subs) {
				v.addAll(nonRecursiveKernels(sc));
			}
		}
		return v;
	}
	
	private static boolean recursive(Constituent c) {
		boolean rec = false;
		if (c instanceof ComplxConst) {
			Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
			for (Constituent sc: subs) {
				rec = rec || recursive(sc,c.getType());
			}
		} 
		return rec;
	}
	
	private static boolean recursive(Constituent c, String type) {
		boolean rec = (c.getType().equals(type));
		if (!rec && c instanceof ComplxConst) {
			Vector <Constituent> subs = ((ComplxConst) c).getSubConstituents();
			for (Constituent sc: subs) {
				rec = rec || recursive(sc,type);
			}
		}
		return rec;
	}



} 