/*!
 * \file ElementExt.java
 *
 * \brief Extension to the org.w3c.dom.Element functionalities.
 *
 * \author Daniele Pighin 
 *
 * \date 2004-06-24
 */

package it.uniroma2.art.marte.temp;


import java.util.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

/*!
  * \class ElementExt
  *
  * \brief Facilities for accessing an Element children and properties.
  */
public class ElementExt {
	
	Element _element = null;

	public ElementExt(Element e) {
		_element = e;
	}

	/**
	  * \brief Return a <code>Vector</code> with all the <strong>direct</strong>
	  *        children of an <code>Element</code> that match a given name.
	  *
	  * \param childrenName the name of the children elements to be returned.
	  *
	  * \return a <code>Vector</code> whose elements are all the
	  *         direct children of the calling element whose name is 
	  *         <code>childrenName</code>.
	  */
	public Vector<Element> getElementChildren(String childrenName) {
		Vector<Element> result = new Vector();
		//fetch all descendants of e with the right name
		NodeList nl = _element.getElementsByTagName(childrenName);
		if (nl.getLength() == 0) {
			return result;
		} else {
			//select only the descendants that are direct children of e
			for (int i=0; i<nl.getLength(); i++) {
				if (nl.item(i).getParentNode().equals((Node)_element))	{
					result.add((Element)(nl.item(i)));
				}
			}
			return result;
		}
	}

	/*!
	 * \brief Parse some resource and return its root element incapsulated
	 *        whithin an ElementExt object.
	 *
	 * \param resource the URI (as a String) of the resource to be parsed.
	 *
	 * \return an ElementExt encapsulating the root of the parsed document, or
	 *         null if some error occurred.
	 */
	public static ElementExt parse(String resourceURI) {
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(resourceURI);
			return new ElementExt(doc.getDocumentElement());
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
}
