package it.uniroma2.art.marte.temp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

public class StatSigFileExtractor {
	
	public static void main(String [] argv) {
		try {
			BufferedReader gold = new BufferedReader(new FileReader(argv[0]));
			BufferedReader decisions = new BufferedReader(new FileReader(argv[1]));
			BufferedWriter acc = new BufferedWriter(new FileWriter(argv[1] + ".acc"));
			BufferedWriter fm = new BufferedWriter(new FileWriter(argv[1] + ".fm"));
			
			BufferedWriter pos = new BufferedWriter(new FileWriter(argv[1] + ".pos"));
			BufferedWriter neg = new BufferedWriter(new FileWriter(argv[1] + ".neg"));
			BufferedWriter aroc = new BufferedWriter(new FileWriter(argv[1] + ".aroc_file"));

			String gold_l = gold.readLine();
			String decision_l = decisions.readLine();
			
			while (gold_l != null) {
				String gold_d = gold_l.split("\t")[0].split(" ")[0];
				Double syst_d = new Double(decision_l.trim());
				if ((gold_d.equals("-1") && syst_d < 0) || (gold_d.equals("1") && syst_d > 0)) acc.write("1\n"); else acc.write("0\n");  				

				
				if (gold_d.equals("1") && syst_d > 0) fm.write("1 "); else fm.write("0 ");  
				if (syst_d > 0) fm.write("1 "); else fm.write("0 ");  
				if (gold_d.equals("1")) fm.write("1\n"); else fm.write("0\n");  

				if (gold_d.equals("1")) pos.write("" + syst_d + "\n"); else neg.write("" + syst_d + "\n");  
				aroc.write((gold_d.equals("1")?"Y":"N") + " " + syst_d + "\n");
				
				gold_l = gold.readLine();
				decision_l = decisions.readLine();
			}
			acc.close();fm.close();gold.close();decisions.close();pos.close();neg.close();aroc.close();
			
			
			
			
		} catch (Exception e) {
			System.out.println("Use: StatSigFileExtractor <SVM GoldStandard> <SVM Decision File>");
			System.out.println("       produces <SVM Decision File>.acc file for accuracies");
			System.out.println("                <SVM Decision File>.fm  file for f-measure");
		}
		
		
	}

}
