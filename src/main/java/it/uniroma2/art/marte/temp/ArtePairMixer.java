package it.uniroma2.art.marte.temp;

import it.uniroma2.art.marte.structures.*;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;

public class ArtePairMixer {

	public static void main(String [] argv) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

		BufferedReader in_newformat = new BufferedReader(new FileReader(argv[0]));
		BufferedReader in_oldformat = new BufferedReader(new FileReader(argv[1]));

		String layer_to_be_updated = (argv.length ==3 ? argv[2] : "active_span");

		BufferedWriter out = new BufferedWriter(new FileWriter(getFileNameNoExt(argv[0]) + "_processed.xml"));
		System.err.println("ACTIVE LAYER = " + layer_to_be_updated);

		String pair = nextPair(in_newformat);
		String pair_old = nextPair(in_oldformat);
		while (pair != null) {
			EntailmentPair p = new EntailmentPair(getElement(pair,builder));
			Pair p_old = new Pair(getElement(pair_old,builder));
			
			p.addAnalysis(new AnalysedTextPair(p_old.getText(),p_old.getHypo(),layer_to_be_updated));
			
			
			out.write(p.toXML() + "\n");

			pair = nextPair(in_newformat);
			pair_old = nextPair(in_oldformat);
		}
		out.close();
		in_newformat.close();
		in_oldformat.close();
	}

	private static String getFileNameNoExt(String file_name) {
		return file_name.substring(0,file_name.lastIndexOf('.'));
	}
	
	public static Element getElement(String pair,DocumentBuilder builder) throws Exception {
		//System.out.println("<?xml version='1.0' encoding='utf-8'?>\n" +pair);
		Reader reader=new CharArrayReader(("<?xml version='1.0' encoding='utf-8'?>\n" +pair).toCharArray());
		Document doc = builder.parse(new org.xml.sax.InputSource(reader));
		return doc.getDocumentElement();
	}

	public static String nextPair(BufferedReader in) throws Exception {
		String out = null;
		String line = in.readLine();
		while (line != null && !line.trim().startsWith("<pair ")) line = in.readLine();
		if (line!=null) {
			out = "";
			while (!line.trim().equals("</pair>") && line != null) {out += line + "\n";line = in.readLine();}
			if (line!=null) out += line;
			else out = null;
		}
		return out;
	}
}
