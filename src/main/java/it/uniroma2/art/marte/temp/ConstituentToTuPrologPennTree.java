package it.uniroma2.art.marte.temp;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;


import it.reveal.chaos.utils.StringTool;
import it.reveal.chaos.xdg.textstructure.Constituent;
import it.reveal.chaos.xdg.textstructure.ConstituentList;
import it.reveal.chaos.xdg.textstructure.SimpleConst;
import it.reveal.chaos.xdg.textstructure.ComplxConst;

import java.util.*;

/** this class is temporarily here. It transform a constituent in a tree represented in tuprolog. It is not the best solution.

*/


public class ConstituentToTuPrologPennTree {

/** tranforms the Constituent c in a tu prolog tree

*/
	public static Struct toTuPrologPennTree(Constituent c) {
		Struct s = null;
		if (c instanceof SimpleConst) {
			s = new Struct(c.getType(), new alice.tuprolog.Int(c.getId()) , new Struct(c.getSurface()), new Struct("l"));
		} else {
			Term [] subs = new Term[((ComplxConst)c).getSubConstituents().size()+1];
			subs[0] = (new alice.tuprolog.Int(c.getId()) );	
			Vector <Constituent> cs = ((ComplxConst)c).getSubConstituents();
			for (int i = 0; i < cs.size(); i++ ) subs[i+1] = toTuPrologPennTree(cs.elementAt(i)) ;
			s = new Struct(c.getType(), subs);
		}
		return s;
	}

/** tranforms the tu prolog tree s in a Constituent 

*/
	public static Constituent fromTuPrologPennTree(Struct s) {
		Constituent c = null;
		if (s.getArity() == 3 && s.getArg(2).toString().equals("l")) {
			Term id = s.getArg(0);
			if (id instanceof Var) id = id.getTerm();
			c = new SimpleConst(StringTool.removeQuotes(s.getArg(1).toString()), s.getName(), ((alice.tuprolog.Int) id).intValue());
		} else {
			ConstituentList cs = new ConstituentList();
			//System.out.println(s.getName());
			for (int i = 1; i < s.getArity(); i++) { 
				//System.out.println(s.getArg(i));
				//System.out.flush();
				Term t = s.getArg(i); 
				if (t instanceof Var) t = t.getTerm();
				cs.add(fromTuPrologPennTree((Struct) t));
			}
			c = new ComplxConst(s.getName(),cs);
		}
		return c;
	}



/** tranforms the Constituent c in a tu prolog tree with lemmas

*/
	public static Struct toTuPrologPennTreeWithLemmas(Constituent c) {
		Struct s = null;
		if (c instanceof SimpleConst) {
			s = new Struct(c.getType(), new alice.tuprolog.Int(c.getId()) , new Struct(c.getSurface()), new Struct(c.getFirstLemma().getSurface()), new Struct("l"));
		} else {
			Term [] subs = new Term[((ComplxConst)c).getSubConstituents().size()+1];
			subs[0] = (new alice.tuprolog.Int(c.getId()) );	
			Vector <Constituent> cs = ((ComplxConst)c).getSubConstituents();
			for (int i = 0; i < cs.size(); i++ ) subs[i+1] = toTuPrologPennTreeWithLemmas(cs.elementAt(i)) ;
			s = new Struct(c.getType(), subs);
		}
		return s;
	}

/** tranforms the tu prolog tree s in a Constituent with lemmas

*/
	public static Constituent fromTuPrologPennTreeWithLemmas(Struct s) {
		Constituent c = null;
		if (s.getArity() == 4 && s.getArg(3).toString().equals("l")) {
			Term id = s.getArg(0);
			if (id instanceof Var) id = id.getTerm();
			c = new SimpleConst(StringTool.removeQuotes(s.getArg(1).toString()), s.getName(), ((alice.tuprolog.Int) id).intValue());
			((SimpleConst)c).getFirstLemma().setSurface(StringTool.removeQuotes(s.getArg(2).toString()));
		} else {
			ConstituentList cs = new ConstituentList();
			//System.out.println(s.getName());
			for (int i = 1; i < s.getArity(); i++) { 
				//System.out.println(s.getArg(i));
				//System.out.flush();
				Term t = s.getArg(i); 
				if (t instanceof Var) t = t.getTerm();
				cs.add(fromTuPrologPennTreeWithLemmas((Struct) t));
			}
			c = new ComplxConst(s.getName(),cs);
		}
		return c;
	}

/** tranforms the Constituent c (that has variable marked) in a tu prolog tree with lemmas and with variables

*/
	public static Struct toTuPrologPennTreeWithLemmasAndWithVariables(Constituent c) {
		Struct s = null;
		StringTokenizer in = new StringTokenizer(c.getType(),":");
		StringTokenizer type_struct = new StringTokenizer(in.nextToken(),"#");
		String type = type_struct.nextToken();
		
		String variable = in.nextToken();
		if (c instanceof SimpleConst) {
			s = new Struct(type, new Struct(variable) , new alice.tuprolog.Int(c.getId()) , new Struct(c.getSurface()), new Struct(c.getFirstLemma().getSurface()), new Struct("l"));
		} else {
			Term [] subs = new Term[3];
			subs[0] = (new Struct(variable) );	
			subs[1] = (new alice.tuprolog.Int(c.getId()) );	
			Vector <Constituent> cs = ((ComplxConst)c).getSubConstituents();
			Term [] list_of_sons =  new Term[((ComplxConst)c).getSubConstituents().size()];
			for (int i = 0; i < cs.size(); i++ ) list_of_sons[i] = toTuPrologPennTreeWithLemmasAndWithVariables(cs.elementAt(i)) ;
			subs[2] = new Struct(list_of_sons);
			s = new Struct(type, subs);
		}
		return s;
	}


} 
