package it.uniroma2.art.marte.temp;

import it.uniroma2.art.marte.ArteException;
import it.uniroma2.art.marte.structures.*;

import java.io.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.util.Hashtable;

public class ArtePairRewriter {
	static Hashtable<String,String> class_maps = null;

	public static void main(String [] argv) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		BufferedReader in = new BufferedReader(new FileReader(argv[0]));
		String active_layer = (argv.length >= 2 ? argv[1] : "active_span");
		String mapping_file = null;
		if (argv.length ==3) {
			mapping_file = argv[2];
			class_maps = new Hashtable<String,String>();
			BufferedReader in2 = new BufferedReader(new FileReader(mapping_file));
			String line = in2.readLine();
			while (line != null) {
				//System.out.println("line = " + line);
				if (!line.trim().equals("")) class_maps.put(line.split(":")[0],line.split(":")[1]);
				line = in2.readLine(); 
			}
			in2.close();
		}
		BufferedWriter out = new BufferedWriter(new FileWriter(getFileNameNoExt(argv[0]) + "_" + active_layer.replace(':','-') + (mapping_file!=null?"_"+(new File(mapping_file)).getName():"")+ ".xml"));
		System.err.println("ACTIVE LAYER = " + active_layer);
		String pair = nextPair(in);
		int newid = 1;
		out.write("<?xml version='1.0' encoding='utf-8'?>\n<entailment-corpus>\n");
		while (pair != null) {
			EntailmentPair p = new EntailmentPair(getElement(pair,builder));
			try { p.setActiveAnalysedPair(active_layer);
				if (class_maps != null && class_maps.get(p.getOriginalEntailmentValue()) != null) 
						p.setOriginalEntailmentValue(class_maps.get(p.getOriginalEntailmentValue()));
				//if (p.getId().contains("-event-")) 
					out.write(p.toOldXMLVersion(newid) + "\n");
				//out.write("------\n" );
				//out.write((new Pair(getElement(p.toOldXMLVersion(newid),builder))).toXML()+"\n");
				//out.write(p.toXML() + "\n");
				newid++;
			} catch (ArteException e) {
				System.out.println(e.getMessage());
			}
			pair = nextPair(in);
		}
		out.write("</entailment-corpus>\n");
		out.close();
		in.close();
	}

	private static String getFileNameNoExt(String file_name) {
		return file_name.substring(0,file_name.lastIndexOf('.'));
	}
	
	public static Element getElement(String pair,DocumentBuilder builder) throws Exception {
		//System.out.println("<?xml version='1.0' encoding='utf-8'?>\n" +pair);
		Reader reader=new CharArrayReader(("<?xml version='1.0' encoding='utf-8'?>\n" +pair).toCharArray());
		Document doc = builder.parse(new org.xml.sax.InputSource(reader));
		return doc.getDocumentElement();
	}
	public static String nextPair(BufferedReader in) throws Exception {
		String out = null;
		String line = in.readLine();
		while (line != null && !line.trim().startsWith("<pair ")) line = in.readLine();
		if (line!=null) {
			out = "";
			while (!line.trim().equals("</pair>") && line != null) {out += line + "\n";line = in.readLine();}
			if (line!=null) out += line;
			else out = null;
		}
		return out;
	}

}
