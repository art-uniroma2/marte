package it.uniroma2.art.marte.temp;

import org.w3c.dom.*;
import javax.xml.parsers.*;

import java.util.*;

/*!
 \class PropsFile

 \brief A class that handles XML based configuration files.

 PropsFile is a powerful utility class used to handle XML based 
 configuration files that can be used to:
 <ul>
 	<li>set system properties (System.setProperty like);</li>
 	<li>add elements to the classpath at runtime;</li>
	<li>
		read the value of environment variables into a 
		system property;
	</li>
	<li>
		set the value of environment variables.
	</li>
 </ul>

 After running the parse method on a well-formed configuration
 file, defined properties will be accessible via
 System.getProperty() calls and environment variables will
 have changed accordingly to the user needs.

 <h2>Syntax of a Properties file</h2>

 The root of a properties file is a <b>directives</b> element;
 a <b>directives</b> element contains <b>directive</b> and
 <b>loadlib</b> elements.

 <h3>The <em>directive</em> element</h3>

 A <b>directive</b> element can be used to:
 <ul>
 	<li>set a system property;</li>
	<li>read the value of an environment variable into
	a system property</li>
	<li>set the value of a system property.</li>
 </ul>

 <h4>Setting a system property</h4>

 When setting a system property, the <b>define</b> attribute 
 is used to specify the name of the property and the text
 of the <b>directive</b> element is the value assigned to
 the property.

 For example, you can assign the value "MyValue" to the
 system property "my.system.property" with the directive

 <pre>
 &lt;directive define="my.system.property"&gt;MyValue&lt;/directive&gt;
 </pre>

 You can also use the <b>prefix</b> attribute to compose
 a property value: this is particularly useful for defining
 properties which are in a hierical relation, such as a
 tree structure.

 For example, suppose you have defined the "my.home.dir"
 property:

 <pre>
 &lt;directive define="my.home.dir"&gt;/myhome&lt;/directive&gt;
 </pre>

 You could define the "my.home.media.subdir" property as
 follows:

 <pre>
 &lt;directive 
    define="my.home.media.subdir"
    prefix="my.home.dir"&gt;/media&lt;/directive&gt;
 </pre>

 Thus, the return value of 
 <code>System.getProperty("my.home.media.subdir")</code>
 would be "/myhome/media".
 
 <h4>Reading an environment variable</h4>

 You can read the value of an environment variable into
 a system property using the <b>getenv</b> and <b>define</b>
 attributes.

 For example, to copy the value of the PATH environment
 variable into the "myenv.path" system property you would
 use the following directive:
 
 <pre>
 &lt;directive define="myenv.path" getenv="PATH"/&gt;
 </pre>

 <h4>Setting an environment variable</h4>

 Setting an environment variable is much like setting a
 system property, but you have to use the <b>setenv</b>
 attribute rather than <b>define</b>.

 For example, you can add "C:\\binaries" to your PATH
 with the following two directives:

 <pre>
 &lt;directive define="myenv.path" getenv="PATH"/&gt;
 &lt;directive
    setenv="PATH" 
    prefix="myenv.path"&gt;;C:\\binaries&lt;/directive&gt;
 </pre>

 <h3>The <em>loadlib</em> element</h3>

 The <b>loadlib</b> element allows you dinamically extend
 your classpath at runtime.

 For example, you can use

 <pre>
 &lt;loadlib&gt;C:\\stuff\\myjar.jar&lt;/loadlib&gt;
 </pre>
 
 to add "C:\\stuff\\myjar.jar" to your classpath.

 The <b>prefix</b> attribute can be used within a 
 <b>loadlib</b> element as well.

 For example:
 
 <pre>
 &lt;directive define="myhome"&gt;C:\\stuff&lt;/directive&gt;
 &lt;loadlib prefx="myhome"&gt;\\myjar.jar&lt;/loadlib&gt;
 </pre>

 \author Daniele Pighin
 
 \date 2004-10-05

 */
public class PropsFile
{
	private static void defineHandler(Element e) {
		String define, prefix, value;
		prefix = e.getAttribute("prefix");
		define = e.getAttribute("define");
		try {
			value = e.getFirstChild().getNodeValue();
		} catch (Exception ex) {
			value = "";
		}
		if (prefix.length() > 0) {
			value = System.getProperty(prefix) + value;
		}
		System.setProperty(define, value);
	}

	private static void getenvHandler(Element e) {
		String getenv, define;
		getenv = e.getAttribute("getenv");
		define = e.getAttribute("define");
		System.setProperty(define, System.getenv(getenv));
	}

	private static void setenvHandler(Element e) {
		String setenv, prefix, value;
		setenv = e.getAttribute("setenv");
		prefix = e.getAttribute("prefix");
		if (prefix.length() > 0) {
			prefix = System.getProperty(prefix);
		}
		try {
			value = e.getFirstChild().getNodeValue();
		} catch (Exception ex) {
			value = "";
		}
		//SystemExt.setenv(setenv, prefix + value);
	}

	private static void loadlibHandler(Element cpe) {
		String prefix, value;

		prefix = cpe.getAttribute("prefix");

		if (prefix.length() > 0) {
			prefix = System.getProperty(prefix);
		}
		
		try {
			value = cpe.getFirstChild().getNodeValue();
		} catch (Exception e) {
			value = "";
		}
		
		try {
			ClassPathHacker.addFile(prefix + value);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/*!
	 \brief Parse a directives file.

	 The file is parsed and all the directives are processed.

	 \param fname The name of the configuration file to parse.
	 */
	public static void parse(String fname) {
		try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(fname);
			ElementExt root = new ElementExt(doc.getDocumentElement());
			Element e;
			
			Vector<Element> props = root.getElementChildren("directive");
			
			for (int i=0; i<props.size(); i++) {
				e = props.get(i);
				if (e.getAttribute("define").length() > 0) {
					defineHandler(e);
				} else if (e.getAttribute("getenv").length() > 0) {
					getenvHandler(e);
				} else if (e.getAttribute("setenv").length() > 0) {
					setenvHandler(e);
				}
			}

			Vector<Element> cpes = root.getElementChildren("loadlib");
			for (int i=0; i<cpes.size(); i++) {
				Element cpe = cpes.get(i);
				loadlibHandler(cpe);
			}
			//useful for debugging purposes
			//System.getProperties().list(System.out);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Bad args!");
		} else {
			parse(args[0]);
		}
	}


}


