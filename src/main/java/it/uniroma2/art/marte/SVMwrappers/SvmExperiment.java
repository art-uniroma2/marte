package it.uniroma2.art.marte.SVMwrappers;

	public class SvmExperiment {
		public String name = null;
		public String parameters = null;
		public SvmExperiment(String name,String parameters) {
			this.name = name;
			this.parameters = parameters;
		}
	}
