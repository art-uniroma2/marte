package it.uniroma2.art.marte.SVMwrappers;


import java.io.*;
import java.util.*;

public class SvmBestClassifiedSelector2 {
	static Vector <String> classes;
	static Vector <Float> distribution;
	public static void main(String [] argv) throws Exception {
		
		try {
		
			System.out.println("------------------------------");
			BufferedReader decisions = new BufferedReader(new FileReader(argv[1]));
			BufferedReader instances = new BufferedReader(new FileReader(argv[2]));
			int max_allowed = new Integer(argv[0]);
			String l_decisions = decisions.readLine();
			String l_instances = instances.readLine();
			int id = 0;
			
			loadClasssesFromFile(argv[4]); 
			
			TreeSet<SVMDecision> bests[] = new TreeSet[classes.size()];
			
			for (int oo = 0; oo < classes.size(); oo++) bests[oo] = new TreeSet();
			
			
			while (l_decisions != null && l_instances != null) {
				
				String cl = l_decisions.split("\t")[0];
				l_instances =  cl +"\t" + l_instances.substring(l_instances.indexOf('\t')+1,l_instances.length());
				bests[classes.indexOf(cl)].add(new SVMDecision(id,new Double(l_decisions.split("\t")[1]),l_instances));
				//System.out.print("+" + cl );
				if (bests[classes.indexOf(cl)].size() > max_allowed * distribution.elementAt(classes.indexOf(cl))) {
					//System.out.println(" " + bests[classes.indexOf(cl)].size() + " " + max_allowed * distribution.elementAt(classes.indexOf(cl)));
					bests[classes.indexOf(cl)].remove(bests[classes.indexOf(cl)].first());
					//System.out.print("-" + cl);
				}
				l_decisions = decisions.readLine();
				l_instances = instances.readLine();
				id++;
			}
				//System.out.println();
			
			decisions.close();instances.close();
	
			for (TreeSet t: bests) {
				if (t.size() != max_allowed) System.exit(-1);
			}
	
			BufferedWriter n_best = new BufferedWriter(new FileWriter(argv[3]));
			BufferedWriter remove_ids = new BufferedWriter(new FileWriter(argv[3] + ".ids"));
			for (int j = 0; j< classes.size(); j++) {
				Iterator<SVMDecision> i = bests[j].descendingIterator();
				while (i.hasNext()) {
					SVMDecision d = i.next();
					n_best.write(d.getLine() +"\n");
					remove_ids.write(d.getId() +"\n");
				}
			}
			n_best.close();remove_ids.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("USE: command <step_for_the_cotraining> <decision_file> <instance_file> <selected_files> <training_file>");
		}
	}
	

	static void loadClasssesFromFile(String training) throws Exception {
		System.out.println("-------------------------------- Loading classes --------------------------------");
		BufferedReader instances = new BufferedReader(new FileReader(training));
		String l = instances.readLine();
		classes = new Vector();
		distribution = new Vector();
		while (l!=null) {
			String c = l.split("\t")[0];	
			if (!classes.contains(c)) { classes.add(c); distribution.add(new Float(0));}
			distribution.setElementAt(distribution.elementAt((classes.indexOf(c)))+1,classes.indexOf(c));
			l = instances.readLine();
		}
		float total = 0;
		for (Float f:distribution) total +=f;
		for (int i=0; i < distribution.size(); i++) {
			distribution.setElementAt(new Float(distribution.elementAt(i)/total),i);
			System.out.println(" --- " + distribution.elementAt(i));
		}
		
	}



}

