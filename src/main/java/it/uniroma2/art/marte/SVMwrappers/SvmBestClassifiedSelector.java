package it.uniroma2.art.marte.SVMwrappers;


import java.io.*;
import java.util.*;

public class SvmBestClassifiedSelector {

	public static void main(String [] argv) throws Exception {
		
		try {
		
			BufferedReader decisions = new BufferedReader(new FileReader(argv[1]));
			BufferedReader instances = new BufferedReader(new FileReader(argv[2]));
			boolean multiclass = (argv.length > 4) && argv[4].equals("mc");
			int max_allowed = new Integer(argv[0]);
			String l_decisions = decisions.readLine();
			String l_instances = instances.readLine();
			int id = 0;
			
			TreeSet<SVMDecision> bests = new TreeSet();
			
			while (l_decisions != null && l_instances != null) {
				
				if (multiclass) {
					l_instances = l_decisions.split("\t")[0] +"\t" + l_instances.substring(l_instances.indexOf('\t')+1,l_instances.length());
					bests.add(new SVMDecision(id,new Double(l_decisions.split("\t")[1]),l_instances));
				} else {
					l_instances = (new Double(l_decisions) > 0?"1\t":"-1\t") + l_instances.substring(l_instances.indexOf('\t')+1,l_instances.length());
					bests.add(new SVMDecision(id,Math.abs(new Double(l_decisions)),l_instances));
				}
				if (bests.size()>max_allowed) bests.remove(bests.first());
				l_decisions = decisions.readLine();
				l_instances = instances.readLine();
				id++;
			}
			
			decisions.close();instances.close();
	
	
			BufferedWriter n_best = new BufferedWriter(new FileWriter(argv[3]));
			BufferedWriter remove_ids = new BufferedWriter(new FileWriter(argv[3] + ".ids"));
			Iterator<SVMDecision> i = bests.descendingIterator();
			while (i.hasNext()) {
				SVMDecision d = i.next();
				n_best.write(d.getLine() +"\n");
				remove_ids.write(d.getId() +"\n");
			}
			n_best.close();remove_ids.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("USE: command <step_for_the_cotraining> <decision_file> <instance_file> <selected_files> [mc|2c]");
		}
	}
	



}

