package it.uniroma2.art.marte.SVMwrappers.datasetPreparer;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class PlainRandomSeparator {

	Random r = null;
	
	int split = 70;
	public PlainRandomSeparator(long seed) {
		r = new Random(seed);
	}

	public void separate(BufferedReader ds, BufferedWriter training, BufferedWriter testing) throws Exception {
		String line = ds.readLine();
		while (line != null) {
			if (r.nextInt(100) < split) training.write(line+ "\n");
			else testing.write(line + "\n");
			line = ds.readLine();
		}
	}


	public static void main( String [] argv ) throws Exception {
		if (argv.length == 3) {
			// 1) Input
			// 2) Name for output
			// 3) percentage of the split
			
			String inFile = argv[0];
			String addName = argv[1];
			int split = (new Integer(argv[2])).intValue();
			//boolean append = !(argv[1].equals("begin"));
			File f = new File(inFile);
			File fout = new File(f.getParent() + File.separator +  "new_split"+ File.separator  );
			fout.mkdirs();
			BufferedReader source = new BufferedReader(new FileReader(inFile));
			BufferedWriter training = new BufferedWriter(new FileWriter(fout.getPath() + File.separator + addName+"train.svm",true));
			BufferedWriter testing = new BufferedWriter(new FileWriter(fout.getPath() + File.separator + addName+"test.svm",true));
			PlainRandomSeparator rs = new PlainRandomSeparator(1);
			//PlainRandomSeparator rs = new PlainRandomSeparator(63748923);
			rs.split = split;
			rs.separate(source,training,testing);
			source.close();
			training.close();
			testing.close();
		} else {
			System.out.println("Arguments: <input> <name of the experiment> <percentage of the split>");
		}
	}	
	
	
}