package it.uniroma2.art.marte.SVMwrappers;

import java.util.*;
import java.io.*;

 /**
  * This static class is used to prepare batch/sh files for Windows/Unix to
  * run SVM on given datasets
  **/
public class batchPreparer {


public static void prepareLearnerWin(File exampleFile, File modelFile, String paramString) throws Exception
	{
	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(System.getProperty("arte.bin.dir") + "/current-SVM-learn.bat")));
	bw.write("svm_learn.exe" + " " + paramString + " " + exampleFile + " " +  modelFile);
	bw.close();	
	
	}


public static void prepareClassifierWin(File exampleFile, File modelFile, File resultFile, String paramString) throws Exception
	{
	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(System.getProperty("arte.bin.dir") + "/current-SVM-classify.bat")));	
	bw.write("svm_classify.exe" + " " + paramString + " " + exampleFile + " " + modelFile + " " + resultFile);
	bw.close();
	}



public static void prepareLearnerUnix(File exampleFile, File modelFile, String paramString) throws Exception
	{
	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(System.getProperty("arte.bin.dir") + "/current-SVM-learn")));	
	bw.write("$ARTE_HOME/bin/svm_learn" + " " + paramString + " " + exampleFile + " " + modelFile) ;
	bw.close();
	}


public static void prepareClassifierUnix(File exampleFile, File modelFile, File resultFile, String paramString) throws Exception
	{
	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(System.getProperty("arte.bin.dir") + "/current-SVM-classify")));	
	bw.write("$ARTE_HOME/bin/svm_classify" + " " + paramString + " " + exampleFile + " " + modelFile + " " + resultFile);
	bw.close();
	}

private static String toWin(String in){
	return in.replace("/","\\");
	}

private static String toUnix(String in){
	return in.replace("\\","/");
	}

}
