package it.uniroma2.art.marte.SVMwrappers;


import java.io.*;
import java.util.*;

public class SelectedRemover {

	public static void main(String [] argv) throws Exception {
		
		BufferedReader in = new BufferedReader(new FileReader(argv[0]));
		BufferedReader to_be_removed_f = new BufferedReader(new FileReader(argv[1]));
		BufferedWriter out = new BufferedWriter(new FileWriter(argv[0] + ".prg"));

		String line = to_be_removed_f.readLine();
		Vector<Integer> to_be_removed = new Vector();
		while (line != null) {
			try {
				to_be_removed.add(new Integer(line.trim()));
			} catch (NumberFormatException e) { 
				System.err.println(">" + line.trim() + "< this is not a number!" );
			}
			line = to_be_removed_f.readLine();
		}
		to_be_removed_f.close();
		int id = 0;
		line = in.readLine();
		while (line != null) {
			if (!to_be_removed.contains(new Integer(id)))  out.write(line + "\n");
			line = in.readLine();
			id++;
		}
		
		in.close();out.close();

	}

}

