package it.uniroma2.art.marte.SVMwrappers;


import java.io.*;
import java.util.*;

public class SvmInstanceMerger {

	public static void main(String [] argv) throws Exception {
		
		if (argv.length != 4) { 
			System.out.println("Usage : cmd file_from_arte file_from_arte_2 file_from_corpus_builder output");
			System.exit(-1);
		}
		
		BufferedReader one = new BufferedReader(new FileReader(argv[0]));  // FROM ARTE
		//BufferedReader one_half = new BufferedReader(new FileReader(argv[1]));  // FROM ARTE
		BufferedReader two = new BufferedReader(new FileReader(argv[2]));  // FROM EntailmentCorpusBuilder
		BufferedWriter out = new BufferedWriter(new FileWriter(argv[3]));  // OUTPUT FILE

		String line_one = one.readLine();
		String line_two = two.readLine();
		//String line_one_half = one_half.readLine();
		while (line_one != null && line_two != null) {
			String [] line_two_s = line_two.split(";");
			
			Float ch =  new Float(line_one.substring(line_one.lastIndexOf("|ET|")+4,line_one.lastIndexOf("|BV|")).trim().split(" ")[0].split(":")[1]);
			
			System.out.println("<" +line_one.substring(line_one.lastIndexOf("|ET|")+4,line_one.lastIndexOf("|BV|")).trim().split(" ")[0].split(":")[1] + ">");
			
			if (ch > 0.5) {
				out.write(line_two_s[1] + "\t");
				out.write(line_one.substring(line_one.indexOf('\t'),line_one.lastIndexOf("|ET|")) + "|BT|");
				//out.write(" " + line_one_half.split(";")[0] + " |BT| " + line_one_half.split(";")[1] + " |BT|");
				out.write(" " + line_two_s[2] + " |ET| ");
				out.write(line_one.substring(line_one.lastIndexOf("|ET|") + 4,line_one.length()) + "\n");
			}

			line_one = one.readLine();
			line_two = two.readLine();
			//line_one_half = one_half.readLine();
		}
		one.close();two.close();out.close();
		//one_half.close();

	}

}

