package it.uniroma2.art.marte.SVMwrappers.datasetPreparer;

import it.uniroma2.art.marte.utils.*;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class PlainBinRandomSeparator {

	Random r = null;
	
	int  noOfBins = 5;

	public PlainBinRandomSeparator(long seed) {
		System.out.println("Random Separator SEED: " + seed);
		r = new Random(seed);
	}

	public void separate(BufferedReader ds, BufferedWriter[] bins) throws Exception {
		String line = ds.readLine();
		while (line != null) {
			bins[r.nextInt(noOfBins)].write(line + "\n");;
			line = ds.readLine();
		}
	}

	public File [] generateBins(File base_ex_dir, String instance_set, int n_of_bins) throws Exception {

			File dir_out = new File(base_ex_dir, "bins");
			dir_out.mkdirs();
			noOfBins = n_of_bins;

			BufferedReader source = new BufferedReader(new FileReader(FileFactory.find(instance_set,base_ex_dir,"svm")));
		
			String instance_set_name = (new File(instance_set)).getName();

			File[] bin_files = new File[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bin_files[i] = new File(dir_out,instance_set_name + "_" + i +".svm");

			BufferedWriter[] bins = new BufferedWriter[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bins[i] = new BufferedWriter(new FileWriter(bin_files[i]));
			separate(source,bins);
			source.close();
			for (int i=0; i < noOfBins; i++) bins[i].close();
			return bin_files;
	} 


	public static void main( String [] argv ) throws Exception {
		if (argv.length == 3) {
			// 1) Input
			// 2) Name for output
			// 3) percentage of the split
			
			String inFile = argv[0];
			String addName = argv[1];
			int noOfBins = (new Integer(argv[2])).intValue();
			//boolean append = !(argv[1].equals("begin"));
			//// TO BE CONTINUED
		} else {
			System.out.println("Arguments: <input> <name of the experiment> <number of bins>");
		}
	}	
	
	
}