package it.uniroma2.art.marte.SVMwrappers;

import java.io.*;

public class PerformanceEvaluator {

	public static void main(String [] argv) throws Exception {
		BufferedReader oracle_choices = new BufferedReader(new FileReader(argv[0]));
		BufferedReader system_choices = new BufferedReader(new FileReader(argv[1]));
		
		String input_oracle = oracle_choices.readLine();
		String input_system = system_choices.readLine();
		input_system = system_choices.readLine();
		
		int cases = 0, correct_cases = 0;

		int union = 0;
		int system_positive = 0;
		int oracle_positive = 0;
		
				
		while (input_oracle!=null) {
			float oracle_choice = (new Float(input_oracle.split("\t")[0])).floatValue();
			float system_choice = (new Float(input_system.trim())).floatValue();
			cases++;
			if ((oracle_choice > 0 && system_choice > 0) || (oracle_choice < 0 && system_choice < 0)) correct_cases++;


			if (oracle_choice > 0) System.out.println(system_choice + " ; 1");			
			else System.out.println(system_choice + " ; 0");			
			
			
			if (oracle_choice > 0 && system_choice > 0) union++;
			if (oracle_choice > 0) oracle_positive++;
			if (system_choice > 0) system_positive++;
			input_oracle = oracle_choices.readLine();
			input_system = system_choices.readLine();	
		}

		System.out.println("Accuracy = " + ((float) correct_cases)/((float) cases));
		System.out.println("Precision/Recall = " + ((float) union)/((float) system_positive) + "/" + ((float) union)/((float) oracle_positive));

		

	}


}