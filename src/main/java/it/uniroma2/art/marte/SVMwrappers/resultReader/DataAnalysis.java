package it.uniroma2.art.marte.SVMwrappers.resultReader;

import java.io.*;
import java.util.*;

public class DataAnalysis {
	
	
	
	
	public static void main(String [] argv) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(argv[0]));
			BufferedWriter out = new BufferedWriter(new FileWriter(argv[0] + ".csv"));
			Vector experimentListTypes = new Vector();			
			Hashtable results = new Hashtable();
			Vector <String> display_results = new Vector();

			//display_results.add("tr:DEV:ts:AT"                         );
			//display_results.add("tr:AT:ts:Dev"                         );
			//display_results.add("tr:DEV+AT:ts:RTE2"                    );
			//display_results.add("tr:DEV:ts:RTE2"                       );
			//display_results.add("tr:RTE2:ts:DEV+AT"                    );
			//display_results.add("tr:DEV+AT+RTE2_7:ts:DEV+AT+RTE2_3"  );
			//display_results.add("tr:RTE2_5:ts:RTE2_5"                );
			//display_results.add("tr:RTE2_5inv:ts:RTE2_5inv"			  );
			//display_results.add("tr:RTE2:ts:RTE2test"			  );
			//display_results.add("tr:ALL:ts:RTE2test"			  );
			display_results.add("ALL,RTE2test"			  );
			

			String line_first = in.readLine();
			
			String data_set = "";
			String experimentBase = "";
			String experiment = "";
			String accuracy = "";
			String precision_recall = "";
			String recall = "";
			String precision = "";
			while (line_first != null) {
				line_first = line_first.trim();
				//System.out.println("Line <" + line_first + ">");
				if (line_first.startsWith("===")) {
					//System.out.println("XXXXXX");
					line_first = in.readLine().trim();
					if (line_first.startsWith("Training")) {
						data_set = "" + line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim() + ":";		
					} else throw new Exception("Bad line : " + line_first);
					line_first = in.readLine().trim();
					if (line_first.startsWith("Testing")) {
						data_set = data_set + "," + line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim() ;		
					} else throw new Exception("Bad line : " + line_first);
					line_first = in.readLine().trim();
					if (line_first.startsWith("Test-setting")) {
						experimentBase = line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim() ;		
					} else throw new Exception("Bad line : " + line_first);
					line_first = in.readLine().trim();
					if (line_first.startsWith("Parameter : ")) {
						//data_set = data_set + "," + line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim() ;		
					} else throw new Exception("Bad line : " + line_first);
					line_first = in.readLine().trim();
				} else if (line_first.startsWith("Model")) {
						experiment = line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim();
						line_first = in.readLine();
						//Reading model...OK. (558 support vectors read)
						line_first = in.readLine();
						//Classifying test examples..100..200..300..400..500..600..700..800..done
						line_first = in.readLine();
						//Runtime (without IO) in cpu-seconds: 16.59
						line_first = in.readLine();
						if (line_first.startsWith("Accuracy on test set")) {
							accuracy = line_first.substring(line_first.indexOf(':')+1,line_first.indexOf('(')).trim() ;		
						} else throw new Exception("Bad line : " + line_first);
						//Accuracy on test set: 54.25% (434 correct, 366 incorrect, 800 total)
						line_first = in.readLine();
						if (line_first.startsWith("Precision/recall on test set")) {
							precision_recall = line_first.substring(line_first.indexOf(':')+1,line_first.length()).trim() ;	
							StringTokenizer st = new StringTokenizer(precision_recall,"/");
							precision = st.nextToken();	
							recall = st.nextToken();	
						} else throw new Exception("Bad line : " + line_first);
						//Precision/recall on test set: 52.62%/85.50%
						if (!results.containsKey(data_set)) results.put(data_set,nuovoVettore());
						if (!experimentListTypes.contains(experimentBase+"-"+experiment))  experimentListTypes.add(experimentBase+"-"+experiment);
						Vector experimentList = (Vector) results.get(data_set);
						experimentList.setElementAt(new Results(accuracy,precision,recall),experimentListTypes.indexOf(experimentBase+"-"+experiment));
				}
				line_first = in.readLine();
			}
			in.close();
			
			out.write("Data Set");
			for (Object o : experimentListTypes) {
				out.write("," + o.toString() );
			}
			out.write("\n");
			//Enumeration e = display_results.elements(); 
			Enumeration e = results.keys();
			while (e.hasMoreElements()) {
				Object actual = e.nextElement();
				out.write("" + actual);
				Vector exps = (Vector) results.get(actual);
				if (exps!=null) {
					for (Object o : exps) {
						out.write("," + o.toString() );
					} 
				}
				out.write("\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}

	public static Vector nuovoVettore() {
		Vector v = new Vector(50);
		for (int i = 0; i <50; i++) v.add(new Results());
		return v;
	}

}

