package it.uniroma2.art.marte.SVMwrappers;


import java.io.*;

public class ThrSplitter {

	public static void main(String [] argv) {
		try {
			BufferedReader in = new BufferedReader(new FileReader(argv[0]));
			BufferedReader selector = new BufferedReader(new FileReader(argv[1]));
			BufferedWriter out1 = new BufferedWriter(new FileWriter(argv[0].replace(".svm","")+"_up.svm"));
			BufferedWriter out2 = new BufferedWriter(new FileWriter(argv[0].replace(".svm","")+"_down.svm"));
			float thr = new Float(argv[2]);
			String line = in.readLine();
			String s = selector.readLine();
			int i = 0;
			while (line != null) {
				System.out.print("."+i);
				i++;
				if (new Float(s)>thr) out1.write(line + "\n");
				else out2.write(line + "\n");
				line = in.readLine();
				s = selector.readLine();
			}
			System.out.println();
			in.close();
			selector.close();
			out1.close();
			out2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

}