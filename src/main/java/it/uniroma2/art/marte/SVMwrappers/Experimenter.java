package it.uniroma2.art.marte.SVMwrappers;


import it.uniroma2.art.marte.SVMwrappers.datasetPreparer.*;
import it.uniroma2.art.marte.utils.*;

import java.io.*;
import java.util.*;

public class Experimenter {
	
	private File what_to_do = null;
	private String train_file = null;
	private String test_file = null;
	private String org_file = null;

	private File base_ex_dir = null;
	
	private boolean n_fold = false;
	private boolean learning_curve = false;

	private int n_of_folds = 2;
	private long seed = 1;
	
	private boolean qa = false;
	
	private String additional_svm_par = "";
	
	public enum OS { LINUX, WIN; }


	/**
	prepares the batch file [bin_dir]/current_experiment[.bat] for the experiment in 
	on operating system using a train set and a test set.
	Results will be added in [base_ex_dir]/results.txt
	*/
	public static void experiment(File what_to_do,File bin_dir, File base_ex_dir, OS os, String train, String test, String additional_svm_par,String name_extention) throws Exception {
		//File train_f = new File(new File(base_ex_dir,"classic"),train + ".svm");
		//File test_f = new File(new File(base_ex_dir,"classic"),test + ".svm");
		//File train_f = new File(base_ex_dir,train + ".svm");
		//File test_f = new File(base_ex_dir,test + ".svm");
		File train_f = FileFactory.find(train,base_ex_dir,"svm");
		File test_f = FileFactory.find(test,base_ex_dir,"svm");
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(System.currentTimeMillis());
		File results_f = new File(new File(System.getProperty("arte.experiments.dir")),"results_"+c.get(Calendar.YEAR)+ "_" + c.get(Calendar.MONTH) + "_"+ c.get(Calendar.DAY_OF_MONTH) +".txt");
		File svm_learn = new File(bin_dir,"svm_learn");
		File svm_classify = new File(bin_dir,"svm_classify");
		File model_f = new File(new File(base_ex_dir,"models"),train + "_model.svm");
		File classifications_f = new File(new File(base_ex_dir,"results"),test + ".out");
		
		BufferedWriter out = new BufferedWriter(new FileWriter(new File(new File(bin_dir,"tmp"),"current_experiment"+name_extention +(os==OS.WIN?".bat":""))));
		String call = (os==OS.WIN?"call ":"");

		out.write((os==OS.WIN?"@echo off":"#!/bin/bash")+"\n");
		out.write("echo ========================================== >> "+results_f+"\n");
		out.write("echo     Training  : "+train+"                    >> "+results_f+"\n");
		out.write("echo     Testing : "+test+"                       >> "+results_f+"\n");
		out.write("echo     Test-setting : "+ base_ex_dir.getName() +" >> "+results_f+"\n");
		out.write("echo  	Parameter : %PAR%=%VALUE%                >> "+results_f+"\n");
		out.write("echo ========================================== >> "+results_f+"\n");
		out.write("\n");
		
		Vector <SvmExperiment> experiments = new Vector();
		if (what_to_do == null) {
			//experiments.add(new SvmExperiment("Dist","-t 4 -u 100 -T 1"));
			experiments.addElement(new SvmExperiment("SyntTree","-t 4 -u 010 -T 1"));
			experiments.addElement(new SvmExperiment("TreePlac","-t 4 -u 001 -T 1"));
			//experiments.add(new SvmExperiment("SyntTree+Dist","-t 4 -u 110 -T 1"));
			//experiments.add(new SvmExperiment("TreePlac+Dist","-t 4 -u 101 -T 1"));
			//experiments.add(new SvmExperiment("EntDetect","-t 4 -u 00001 -T 1"));
			//experiments.add(new SvmExperiment("EntDetect+Dist","-t 4 -u 10001 -T 1"));
			//experiments.add(new SvmExperiment("EntDetect2","-t 4 -u 00002 -T 1"));
			//experiments.add(new SvmExperiment("EntDetect2+Dist","-t 4 -u 10002 -T 1"));
			//experiments.add(new SvmExperiment("DistSyntTree","-t 4 -u 0001 -T 1"));
			//experiments.add(new SvmExperiment("DistSyntTree+Dist","-t 4 -u 0002 -T 1"));
		} else {
			BufferedReader in = new BufferedReader(new FileReader(what_to_do));
			String line = in.readLine();		
			while (line != null) {
				String[] a = line.split(":");
				System.out.println("Experiment" + a[0].trim() + ":"+ a[1].trim());
				experiments.addElement(new SvmExperiment(a[0].trim(),a[1].trim()));
				line = in.readLine();
			}
		} 
		for (SvmExperiment e:experiments) {	
			out.write("echo Model: "+ e.name +" >> "+results_f+"\n");
			out.write(call + svm_learn+" " + e.parameters + " "+train_f+" "+ model_f+"\n");
			out.write(call + svm_classify+" "+test_f+" "+model_f+" "+classifications_f+" >> "+results_f+"\n");
			out.write("\n");
		}
		
		out.close();
	}

	/**
	prepares the batch file [bin_dir]/current_experiment[.bat] for the n-fold cross-validation experiment in 
	an operating system (os) using the instance set, the number n of bins, and the seed for the pseudo-random number generator.
	Results will be added in [base_ex_dir]/results.txt
	*/
	public static void experiment(File what_to_do, File bin_dir, File base_ex_dir, OS os, String instance_set, String additional_svm_par, int n, long seed) throws Exception {
		PlainBinRandomSeparator separator = new PlainBinRandomSeparator(seed);
		BufferedWriter ac_ex = new BufferedWriter(new FileWriter(new File(new File(bin_dir,"tmp"),"current_experiment"+(os==OS.WIN?".bat":""))));
		String call = (os==OS.WIN?"call ":"");
		File [] bins = separator.generateBins(base_ex_dir, instance_set, n);
		for (int i = 0; i < n; i++) {
			ac_ex.write((os==OS.LINUX?"cat ":"copy "));
			boolean first = true;
			for (int j=0; j<n; j++) {
				if (j!=i)  
					if (first) {ac_ex.write("" + bins[j]);first = false;}
					else ac_ex.write((os==OS.LINUX?" " + bins[j]:"+" + bins[j]));
			} 
			String train_name = (new File(instance_set)).getName() + "_tr_" + i + "_s" + seed;
			ac_ex.write((os==OS.LINUX?">":" ") + (new File(base_ex_dir,train_name + ".svm")) + "\n");
			String test_name = instance_set + "_ts_" + i;
			ac_ex.write((os==OS.LINUX?"cp ":"copy ") + bins[i] + " " + (new File(base_ex_dir,test_name + ".svm")) + "\n\n");
			ac_ex.write((os==OS.LINUX?"chmod 755 "+new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n":""));
			ac_ex.write(call + new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n");
			experiment(what_to_do,bin_dir, base_ex_dir, os, train_name, test_name, additional_svm_par,""+i);
		}
		ac_ex.close();
	}

	/**
	prepares the batch file [bin_dir]/current_experiment[.bat] for the n-fold cross-validation experiment in 
	an operating system (os) using the instance set, the number n of bins, and the seed for the pseudo-random number generator.
	Results will be added in [base_ex_dir]/results.txt
	Adds to each iteration set the file in base_file.
	*/
	public static void experiment( File what_to_do, File bin_dir, File base_ex_dir, OS os, String base_file, String instance_set, String additional_svm_par, int n, long seed) throws Exception {
		PlainBinRandomSeparator separator = new PlainBinRandomSeparator(seed);
		BufferedWriter ac_ex = new BufferedWriter(new FileWriter(new File(new File(bin_dir,"tmp"),"current_experiment"+(os==OS.WIN?".bat":""))));
		String call = (os==OS.WIN?"call ":"");
		File [] bins = separator.generateBins(base_ex_dir, instance_set, n);
		for (int i = 0; i < n; i++) {
			ac_ex.write((os==OS.LINUX?"cat ":"copy ") + FileFactory.find(base_file,base_ex_dir,"svm"));
			for (int j=0; j<n; j++) {
				if (j!=i)  
					ac_ex.write((os==OS.LINUX?" " + bins[j]:"+" + bins[j]));
			} 
			String train_name = (new File(instance_set)).getName() + "_tr_" + i + "_s" + seed;
			ac_ex.write((os==OS.LINUX?">":" ") + (new File(base_ex_dir,train_name + ".svm")) + "\n");
			String test_name = instance_set + "_ts_" + i;
			ac_ex.write((os==OS.LINUX?"cp ":"copy ") + bins[i] + " " + (new File(base_ex_dir,test_name + ".svm")) + "\n\n");
			ac_ex.write((os==OS.LINUX?"chmod 755 "+new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n":""));
			ac_ex.write(call + new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n");
			experiment(what_to_do,bin_dir, base_ex_dir, os, train_name, test_name, additional_svm_par,""+i);
		}
		ac_ex.close();
	}


	/**
	prepares the batch file [bin_dir]/current_experiment[.bat] for the n-fold cross-validation experiment 
	with a learning curve depending on the n of the bins in 
	on an operating system (os) using the instance set, the number n of bins, and the seed for the pseudo-random number generator.
	Results will be added in [base_ex_dir]/results.txt
	Adds to each iteration set the file in base_file.
	*/
	public static void experimentLearningCurve( File what_to_do, File bin_dir, File base_ex_dir, OS os, String training_set, String testing_set, String additional_svm_par, int n, long seed) throws Exception {
		PlainBinRandomSeparator separator = new PlainBinRandomSeparator(seed);
		BufferedWriter ac_ex = new BufferedWriter(new FileWriter(new File(new File(bin_dir,"tmp"),"current_experiment"+(os==OS.WIN?".bat":""))));
		String call = (os==OS.WIN?"call ":"");
		File [] bins = separator.generateBins(base_ex_dir, training_set, n);
		for (int i = 0; i < n; i++) {
			for (int j=0; j<n; j++) {
				ac_ex.write((os==OS.LINUX?"cat ":"copy "));
				for (int k=0; k<=j ; k++) {
					ac_ex.write((os==OS.LINUX?" " + bins[k]:"+" + bins[k]));
				}
				String train_name = (new File(training_set)).getName() + "_tr_" + i + "_" + j ;
				ac_ex.write((os==OS.LINUX?">":" ") + (new File(base_ex_dir,train_name + ".svm")) + "\n");
				ac_ex.write((os==OS.LINUX?"chmod 755 "+new File(new File(bin_dir,"tmp"),"current_experiment" + i + "_" + j + (os==OS.WIN?".bat":"")) + "\n\n":""));
				ac_ex.write(call + new File(new File(bin_dir,"tmp"),"current_experiment" + i + "_" + j + (os==OS.WIN?".bat":"")) + "\n\n");
				experiment(what_to_do, bin_dir, base_ex_dir, os, train_name, testing_set, additional_svm_par,""+i + "_" +j);
			} 
			File f = bins[0];
			for (int j=0; j<n-1; j++) {
				bins[j]=bins[j+1];
			}
			bins[n-1]=f;
		}
		ac_ex.close();
	}





	/**
	prepares the batch file [bin_dir]/current_experiment[.bat] for the n-fold cross-validation experiment in 
	an operating system (os) using the instance set, the number n of bins, and the seed for the pseudo-random number generator.
	Results will be added in [base_ex_dir]/results.txt
	*/
	public static void experimentQuestionBased(File what_to_do, File original_file, File bin_dir, File base_ex_dir, OS os, String instance_set, String additional_svm_par, int n, long seed) throws Exception {
		QuestionPlainBinRandomSeparator separator = new QuestionPlainBinRandomSeparator(seed);
		BufferedWriter ac_ex = new BufferedWriter(new FileWriter(new File(new File(bin_dir,"tmp"),"current_experiment"+(os==OS.WIN?".bat":""))));
		String call = (os==OS.WIN?"call ":"");
		File [] bins = separator.generateBins(original_file, base_ex_dir, instance_set, n);
		for (int i = 0; i < n; i++) {
			ac_ex.write((os==OS.LINUX?"cat ":"copy "));
			boolean first = true;
			for (int j=0; j<n; j++) {
				if (j!=i)  
					if (first) {ac_ex.write("" + bins[j]);first = false;}
					else ac_ex.write((os==OS.LINUX?" " + bins[j]:"+" + bins[j]));
			} 
			String train_name = (new File(instance_set)).getName() + "_tr_" + i + "_s" + seed;
			ac_ex.write((os==OS.LINUX?">":" ") + (new File(base_ex_dir,train_name + ".svm")) + "\n");
			String test_name = instance_set + "_ts_" + i;
			ac_ex.write((os==OS.LINUX?"cp ":"copy ") + bins[i] + " " + (new File(base_ex_dir,test_name + ".svm")) + "\n\n");
			ac_ex.write((os==OS.LINUX?"chmod 755 "+new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n":""));
			ac_ex.write(call + new File(new File(bin_dir,"tmp"),"current_experiment" + i +(os==OS.WIN?".bat":"")) + "\n\n");
			experiment(what_to_do, bin_dir, base_ex_dir, os, train_name, test_name, additional_svm_par,""+i);
		}
		ac_ex.close();
	}


	private void run() throws Exception {
		OS os = (File.separatorChar=='/'? OS.LINUX: OS.WIN);
		if (n_fold && test_file == null) 
			if (!qa) 
				experiment(what_to_do, new File(System.getProperty("arte.bin.dir")), base_ex_dir, os, train_file, additional_svm_par,n_of_folds,seed);
			else 
				experimentQuestionBased(what_to_do,new File(org_file),new File(System.getProperty("arte.bin.dir")), base_ex_dir, os, train_file, additional_svm_par,n_of_folds,seed);
		else if (n_fold && test_file != null) 
				experiment(what_to_do,new File(System.getProperty("arte.bin.dir")), base_ex_dir, os, test_file, train_file, additional_svm_par,n_of_folds,seed);
		else if (learning_curve) 
				experimentLearningCurve(what_to_do,new File(System.getProperty("arte.bin.dir")), base_ex_dir, os, train_file, test_file, additional_svm_par,n_of_folds,seed);
		else 
			experiment(what_to_do,new File(System.getProperty("arte.bin.dir")), base_ex_dir, os, train_file, test_file, additional_svm_par,"");
		
	}

 /** 
  * Explains input parameters
  */
	private static void usage() {
		System.out.println("\n");
		System.out.println("Usage: Experimenter [-tr <training file> -ts <testing file>]");
		System.out.println("                    [-lc -tr <training file> -ts <testing file> [-n <number of the bins>] ]");
		System.out.println("                    [-nfold -tr <instance file>");
		System.out.println("                       [-qa <original file to compare questions or texts>]");
		System.out.println("                       [-n <number of the bins>]");
		System.out.println("                       [-s <seed of the random generator>]]");
		System.out.println("                    [-dir <experiment base dir>]");
		System.out.println(" Parameters: ");
		System.out.println(" -lc   learning curve");
		System.out.println(" -qa   examples with the same question or the same hypothesis are put in the same bin");
		System.exit(0);
	}


	public static void setCurrentExperiment(String type, String experiment) throws Exception {
		File temp_dir = new File(new File(System.getProperty("arte.bin.dir")),"tmp");
		temp_dir.mkdir();
		File experiment_properties = new File(temp_dir,"current_experiment_settings");
		Properties p = new Properties();
		if (experiment_properties.exists()) {
			FileInputStream input = new FileInputStream(experiment_properties);
			p.load(input);
			input.close();
		} 
		FileOutputStream output = new FileOutputStream(experiment_properties);
		p.setProperty(type,experiment);
		p.store(output,"Current Experiment Settings");
		output.close();
	}

	public static String getCurrentExperiment(String type) throws Exception {
		Properties p = new Properties(); 
		FileInputStream f = new FileInputStream( 
		                     new File(new File(new File(System.getProperty("arte.bin.dir")),"tmp"),"current_experiment_settings"));
		p.load(f);
		String out = p.getProperty(type);
		f.close();		                     
		return out;
	}

  private void parseParameters(String[] argv) throws Exception {
	 String lexProc=null,tasks=null;
	 boolean ctr = false;
	for (int i=0; i < argv.length ; i++) 
		{
		if (argv[i].equals("-tr"))       {i++; train_file = argv[i]; }
		else if (argv[i].equals("-ts"))  {i++; test_file = argv[i]; }
		else if (argv[i].equals("-qa"))  {i++; org_file = argv[i]; qa = true; }
		else if (argv[i].equals("-n"))  {i++; n_of_folds = new Integer(argv[i]).intValue(); }
		else if (argv[i].equals("-s"))  {i++; seed = new Long(argv[i]).longValue(); }
		else if (argv[i].equals("-exp"))  {i++; what_to_do = new File(argv[i]); }
		else if (argv[i].equals("-lc"))  {learning_curve=true; }
		else if (argv[i].equals("-nfold"))  {n_fold=true; }
		else if (argv[i].equals("-dir")) {i++; base_ex_dir = new File(argv[i]);}
		}
	if (!n_fold && ((train_file==null) || (test_file==null))) usage();
	if (n_fold && (train_file==null)) usage();
	if (learning_curve && ((train_file==null) || (test_file==null)) ) usage();
	
	// SETTING THE REMAINING PARAMETERS WITH GENERAL SETTINGS
	if (base_ex_dir == null) 
		base_ex_dir = new File(new File(System.getProperty("arte.default.output.dir")),getCurrentExperiment("arte.pre_plus_processing"));
	}	
	
	public static void main(String[] argv) throws Exception {
		if (argv.length == 0) usage();
		else {
			Experimenter experimenter = new Experimenter();
			ArteConfigurationHandler.initialize();
			experimenter.parseParameters(argv);
			experimenter.run();	
		}                
			
	}//end main

}
