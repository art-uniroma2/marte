package it.uniroma2.art.marte.SVMwrappers.datasetPreparer;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class SpecialPlainBinRandomSeparator {

	Random r = null;
	
	int  noOfBins = 5;
	
	
	public SpecialPlainBinRandomSeparator(long seed) {
		r = new Random(seed);
	}


	public Hashtable createListOfQuestions(BufferedReader ds) throws Exception {
		Hashtable questions = new Hashtable();
		String line = ds.readLine();
		while (line != null) {
			// OLD VERSION 
			int begin = line.indexOf("|BT|");
			// OLD VERSION 
			begin = line.indexOf("|BT|",begin+1);
			// OLD VERSION 
			int end = line.indexOf("|BT|",begin+1);
			
			// OLD VERSION 
			String question = line.substring(begin,end);  
			//System.out.println("Q:" + question);
			if (!questions.containsKey(question)) questions.put(question,new Vector());
			Vector v = (Vector) questions.get(question);
			v.add(line);
			questions.put(question,v);
			line = ds.readLine();
		}
		return questions;
	}


	public void separate(BufferedReader ds, BufferedWriter[] bins) throws Exception {
		Hashtable questions = createListOfQuestions(ds) ;
		Enumeration e = questions.keys();
		System.out.println("# of questions: " + questions.size());

		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			Vector v = (Vector) questions.get(key);
			int bin = r.nextInt(noOfBins);
			for (Object o : v) bins[bin].write(o + "\n");;
		}
	}


	public static void main( String [] argv ) throws Exception {
		if (argv.length == 3) {
			// 1) Input
			// 2) Name for output
			// 3) percentage of the split
			
			String inFile = argv[0];
			String addName = argv[1];
			int noOfBins = (new Integer(argv[2])).intValue();
			//boolean append = !(argv[1].equals("begin"));
			
			
			File f = new File(inFile);
			File fout = new File(f.getParent() + "\\bins\\");
			fout.mkdirs();
			BufferedReader source = new BufferedReader(new FileReader(inFile));
			BufferedWriter[] bins = new BufferedWriter[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bins[i] = new BufferedWriter(new FileWriter(fout.getPath() + "\\"+addName+ i +".svm",true));
			//SpecialPlainBinRandomSeparator rs = new SpecialPlainBinRandomSeparator(1);
			SpecialPlainBinRandomSeparator rs = new SpecialPlainBinRandomSeparator(63748923);
			//SpecialPlainBinRandomSeparator rs = new SpecialPlainBinRandomSeparator(459034);
			//PlainBinRandomSeparator rs = new PlainBinRandomSeparator(63748923);
			rs.noOfBins = noOfBins;
			rs.separate(source,bins);
			source.close();
			for (int i=0; i < noOfBins; i++) bins[i].close();
		
		
		} else {
			System.out.println("Arguments: <input> <name of the experiment> <number of bins>");
		}
	}	
	
	
}