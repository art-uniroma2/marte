package it.uniroma2.art.marte.SVMwrappers;

public class SVMDecision implements Comparable<SVMDecision> {
	double decision;
	String line;
	int id ;
	
	public SVMDecision(int id, double decision,String line) {
		this.decision = decision;
		this.line = line;
		this.id = id;
	}
	
	public int compareTo(SVMDecision o) {
		double final_decision = decision - o.decision ; 
		return (final_decision == 0 ? (int) 0 : (final_decision > 0 ? (int) 1: (int) -1)); 
	}
	
	public String getLine() {return line;}
	public double getDecision() {return decision;}
	public int getId() {return id;}
}
