package it.uniroma2.art.marte.SVMwrappers.datasetPreparer;

import it.uniroma2.art.marte.structures.*;
import it.uniroma2.art.marte.utils.*;

import java.util.*;
import java.io.*;
import java.lang.reflect.Array;


public class QuestionPlainBinRandomSeparator {

	Random r = null;
	
	int  noOfBins = 5;
	
	
	public QuestionPlainBinRandomSeparator(long seed) {
		r = new Random(seed);
	}


	public Hashtable createListOfQuestions(BufferedReader ds, DataSet input_dataset) throws Exception {
		Hashtable questions = new Hashtable();
		int i = 0;
		
		String line = ds.readLine();
		while (line != null) {
			String question = (input_dataset.getPair(i).getQuestion() != null && !input_dataset.getPair(i).getQuestion().equals("empty")? input_dataset.getPair(i).getQuestion() : input_dataset.getPair(i).getHypo());
			if (!questions.containsKey(question)) {
				questions.put(question,new Vector());
				System.out.println("New question: " + question);
			}
			Vector v = (Vector) questions.get(question);
			v.add(line);
			questions.put(question,v);
			line = ds.readLine();
			i++;
		}
		return questions;
	}

	public void separate(BufferedReader ds, DataSet original_data_set, BufferedWriter[] bins) throws Exception {
		Hashtable questions = createListOfQuestions(ds,original_data_set) ;
		Enumeration e = questions.keys();
		System.out.println("# of questions: " + questions.size());

		while (e.hasMoreElements()) {
			String key = (String) e.nextElement();
			Vector v = (Vector) questions.get(key);
			int bin = r.nextInt(noOfBins);
			for (Object o : v) bins[bin].write(o + "\n");;
		}
	}


	public File [] generateBins(File original_file, File base_ex_dir, String instance_set, int n_of_bins) throws Exception {

			File dir_out = new File(base_ex_dir, "bins");
			dir_out.mkdirs();
			noOfBins = n_of_bins;

			DataSet org_data_set = new DataSet(original_file.toString(),true);

			BufferedReader source = new BufferedReader(new FileReader(FileFactory.find(instance_set,base_ex_dir,"svm")));
		
			String instance_set_name = (new File(instance_set)).getName();

			File[] bin_files = new File[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bin_files[i] = new File(dir_out,instance_set_name + "_" + i +".svm");

			BufferedWriter[] bins = new BufferedWriter[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bins[i] = new BufferedWriter(new FileWriter(bin_files[i]));
			separate(source,org_data_set,bins);
			source.close();
			for (int i=0; i < noOfBins; i++) bins[i].close();
			return bin_files;
	} 

/*
	public static void main( String [] argv ) throws Exception {
		if (argv.length == 3) {
			// 1) Input
			// 2) Name for output
			// 3) percentage of the split
			
			String inFile = argv[0];
			String addName = argv[1];
			int noOfBins = (new Integer(argv[2])).intValue();
			//boolean append = !(argv[1].equals("begin"));
			
			
			File f = new File(inFile);
			File fout = new File(f.getParent() + "\\bins\\");
			fout.mkdirs();
			BufferedReader source = new BufferedReader(new FileReader(inFile));
			BufferedWriter[] bins = new BufferedWriter[noOfBins]; 
			for (int i=0; i < noOfBins; i++) bins[i] = new BufferedWriter(new FileWriter(fout.getPath() + "\\"+addName+ i +".svm",true));

			QuestionPlainBinRandomSeparator rs = new QuestionPlainBinRandomSeparator(63748923);

			rs.noOfBins = noOfBins;
			rs.separate(source,bins);
			source.close();
			for (int i=0; i < noOfBins; i++) bins[i].close();
		
		
		} else {
			System.out.println("Arguments: <input> <name of the experiment> <number of bins>");
		}
	}	
*/
	
}
