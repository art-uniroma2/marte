package it.uniroma2.art.marte;

import it.uniroma2.art.marte.postprocessors.*;
import it.uniroma2.art.marte.structures.*;

import java.util.*;
import java.io.*;




public class postProcessing {
	
	private static String rteFile = null;
	private static String parsedFile = null;
	private static String indexFile = null;
	private static String classFile = null;
	private static String outFile = null;
	private Vector<PostProcessorEnum> listPostProc = null;
	private Vector <PostProcessor> post_processors = null;

 /**
  * Enumeration used to represent all the possible post processors that can be 
  * applied by a post processing object.
  */ 	
 public enum PostProcessorEnum{
	//these are available post processors.
	INV("inversion post processor", "simply inverts the sign of prediction");
	
	private String _label, _description;

	PostProcessorEnum(String label, String description) {
		_label = label;
		_description = description;
	}
	
	public String getLabel() {
		return _label;
	}
	
	public String getDescription() {
		return _description;
	}

	public static String listOfValues() {
		String result = "";
		for (PostProcessorEnum p : PostProcessorEnum.values()) {
			result += p.toString() + ",";
		}
		return result.substring(0, result.length() - 1);
	}
 }


 /**
  * Static method that initializes a postProcessing object. Initialiation cosists in
  * initializing all the processors, lexical processor,dictionaries and parameters that 
  * will be used .
  *
  * @param a    the postProcessing object to initialize.
  */ 
	public static void postProcessingInitializer(postProcessing pp) {
		try {
			System.out.println("--------- Initializing ---------");

			pp.post_processors = new Vector();
			if (pp.listPostProc.contains(PostProcessorEnum.INV)) pp.post_processors.add(new InversionPostProcessor());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	System.out.println("all done!\n--------------------------------\n");
	}



 /**
  * Lunches postProcessing on a RTE dataset.
  *
  * @param ds        dataset to be analyzed with predicted class and value
  * @param svmpairs  vector of svmPair containing parsed pairs.
  * @return          analyzed dataset with predicted entailment scores.
  */ 
private DataSet postprocessing(DataSet ds) throws Exception
	{
	System.out.println("\n------------------------------------");
	System.out.println("--------- Analyzing " + ds.size() + " pairs ------");
	System.out.println("------------------------------------\n");
	for (int i=0; i<ds.getAllPairs().size();i++)
		{
		 	Pair pair = ds.getPair(i);
		 	System.err.print("." + i);
		 	System.out.println("\nOLD : " + pair.getValue() + "(" + pair.getConfidence());
		 	for (PostProcessor p : post_processors)
					pair = p.run(pair);
			System.out.println("NEW : " + pair.getValue() + "(" + pair.getConfidence());			
		}//end for 
 	return ds;
	}


private void run() throws Exception {	    
	 	BufferedWriter bw = new BufferedWriter(new FileWriter(new File(outFile)));
	 	DataSet ds = new DataSet(parsedFile,false);
	 	ds = addParsedPairs(ds,rteFile);
		ds = assignClasses(ds, indexFile,classFile);
		ds = postprocessing(ds);
		bw.write(ds.toXML());
		bw.close();
}//end Run



private DataSet addParsedPairs(DataSet ds, String plainDatasetFile) throws Exception{
	DataSet dsPlain = new DataSet(plainDatasetFile,true);
	for (int i=0; i<ds.getAllPairs().size();i++)
		{
		Pair pair = ds.getPair(i);
		pair.setH(dsPlain.getPair(i).getHypo());
		pair.setT(dsPlain.getPair(i).getText());
		}
	return ds;
}	


private DataSet assignClasses(DataSet ds, String idFile, String clsFile) throws Exception{
	BufferedReader brId = new BufferedReader(new FileReader(new File(idFile)));
	BufferedReader brCls = new BufferedReader(new FileReader(new File(clsFile)));
	String inLineId,inLineCls;
	while ((inLineId=brId.readLine())!=null)
		{
		inLineCls=brCls.readLine();
		int id=Integer.parseInt(inLineId);
		float value = Float.parseFloat(inLineCls);
		Pair pair = ds.getPairWithId(id);
		if (value<0) pair.setValue(false); else pair.setValue(true);
		pair.setConfidence(Math.abs(value)); 
		}
	return ds;
}	



 /**
  * Returns a vector of the lexical processors to be used for anchoring,
  * from a comma separated string containing the processors names
  */
 private Vector<PostProcessorEnum> selectPostProc(String listOfProcessors){
		Vector<PostProcessorEnum> processors = new Vector<PostProcessorEnum>();
		if (listOfProcessors != null) 
			{
			String[] list = listOfProcessors.split(",");
			for (String s : list) 
				{
				PostProcessorEnum p = PostProcessorEnum.valueOf(s);
				if (p != null)
					processors.add(p);
				}
			}
		return processors;
	}



 /**
  * Parse input parameters. If not defined by user,
  * reads them from configuration file. 
  */
  private void parseParameters(String[] argv) throws Exception {
	 String postProc=null;
	 for (int i=0; i < argv.length ; i++) 
		{
		if (argv[i].equals("-if"))       {i++; rteFile = argv[i];}
		else if (argv[i].equals("-pf"))       {i++; parsedFile = argv[i];}
		else if (argv[i].equals("-df"))       {i++; indexFile = argv[i];}
		else if (argv[i].equals("-cf"))       {i++; classFile = argv[i];}
		else if (argv[i].equals("-of"))       {i++; outFile = argv[i];}
		else if (argv[i].equals("-p"))   {i++; postProc = argv[i];}	
		else if (argv[i].equals("-?") || argv[i].equals("-h") || argv[i].equals("-help")) {usage();}
		else throw new Exception("Unknown option : " + argv[i]);
		}
		
		if ((rteFile==null) || (indexFile==null) || (classFile==null) || (outFile==null) ) usage();
		if ((listPostProc=selectPostProc(postProc)).size()==0) 
			listPostProc=selectPostProc(System.getProperty("postprocessing.processors"));
  }
  
  
 /** 
  * Explains input parameters
  */
	private static void usage()
		{
		System.out.println("\n");
		System.out.println("Usage: postProcessing -if <RTE XML file> -pf <parsed XML RTE file> -df <index File> -cf <class file> -of <output file>");
		System.out.println("                   [-p <list of post processors>]");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" where <RTE XML file> is a file containing the original RTE dataset in XML format.");
		System.out.println("\n");
		System.out.println(" where <parsed RTE XML file> is a file containing the Charniak parsed RTE dataset in XML format.");
		System.out.println("\n");
		System.out.println(" where <index file>   is a file containing in each row the id of the RTE pairs whose classification is in the same row"); 
		System.out.println("                      in the <classification file>. This file is obtained by Arte and has a .lst suffix");
		System.out.println("\n");
		System.out.println(" where <class file>   is a file containing in each row the classification of each pair in the original RTE dataset file.");
		System.out.println("                      Classification is a positive (=true ent.) or negative (=false ent.) real value");
		System.out.println("                      This file is obtained by Arte and is named resutls.txt");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println(" Options are:");

		System.out.println("   -p  <list of lex proc>  ordered list of post processors (in comma separated format) to be applied ");
		System.out.println("                           Order should follow the importance precedence of processors. Possible processors are: INV. ["  + System.getProperty("arte.lexsim.processors") + "]"); 
		System.out.println("                    	      - INV (Inverter) : invert the prediction sign");
		System.out.println("\n\n");
		System.exit(0);
		}		

		
public static void main(String[] argv) throws Exception {
	if (argv.length == 0) usage();
	else {
		postProcessing pp = new postProcessing();
		pp.parseParameters(argv);
		postProcessing.postProcessingInitializer(pp);
		pp.run();	
	}                
		
}//end main

}//end class

